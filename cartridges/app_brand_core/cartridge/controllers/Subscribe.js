'use strict';

var server = require('server');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var URLUtils = require('dw/web/URLUtils');

/**
 * Renders email form template on homepage and provides a connection between subscribe.xml and email.isml
 */
server.get('Email',
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        var subscribeForm = server.forms.getForm('subscribe');
        subscribeForm.clear();

        res.render('/subscribe/email', {
            subscribeForm: subscribeForm
        });
        return next();
    });

/**
 * Renders subscribe form template in a new page without email value and with visible email input
 */
server.get('Show',
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        var subscribeForm = server.forms.getForm('subscribe');
        var Resource = require('dw/web/Resource');
        var breadcrumbs = [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: Resource.msg('link.header.subscribe', 'forms', null)
            }
        ];
        res.render('/subscribe/subscribe', {
            subscribeForm: subscribeForm,
            submitted: req.querystring.submitted,
            breadcrumbs: breadcrumbs
        });
        return next();
    });

/**
 * Renders subscribe form template in a new page, posts email address value to Save action
 */
server.post('Start',
    server.middleware.https,
    csrfProtection.validateRequest,
    csrfProtection.generateToken,
    function (req, res, next) {
        res.json({
            redirectUrl: URLUtils.url('Subscribe-Show').toString(),
            success: true
        });
        return next();
    });

/**
 * Saves subscribe form information
 */
server.post('Save',
    server.middleware.https,
    csrfProtection.validateRequest,
    csrfProtection.generateToken,
    csrfProtection.validateRequest,
    function (req, res, next) {
        var subscribeForm = server.forms.getForm('subscribe');
        var formErrors = require('*/cartridge/scripts/formErrors');
        var errors = formErrors.getFormErrors(subscribeForm);

        if (Object.keys(errors).length > 0) {
            res.json({
                fieldErrors: [errors],
                success: false
            });
            return next();
        }

        var CustomerMgr = require('dw/customer/CustomerMgr');
        var ProfileHelper = require('*/cartridge/scripts/helpers/profileHelper');

        var localeID = req.locale.id;
        var customerInfo = {
            profile: {
                firstName: subscribeForm.firstname.value,
                lastName: subscribeForm.lastname.value,
                email: subscribeForm.email.value,
                phoneMobile: subscribeForm.phoneMobile.value || '',
                title: subscribeForm.title.titleList.htmlValue || '',
                birthday: subscribeForm.birthday.value || '',
                custom: {
                    emailOptin: subscribeForm.accountpreferences.optinlist.optinemail.checked,
                    smsOptin: subscribeForm.accountpreferences.optinlist.optinsms.checked,
                    telephoneOptin: subscribeForm.accountpreferences.optinlist.optintelephone.checked,
                    whatsappOptin: subscribeForm.accountpreferences.optinlist.optinwhatsapp.checked
                }
            }
        };

        var currentCustomer = CustomerMgr.getCustomerByLogin(subscribeForm.email.value);
        if (currentCustomer && currentCustomer.profile) {
            customerInfo = currentCustomer;
            localeID = customerInfo.profile.preferredLocale;
            ProfileHelper.saveOptinInformation(customerInfo.profile, subscribeForm.accountpreferences.optinlist);
        }

        var result = ProfileHelper.updateNewsletter(customerInfo.profile, localeID);

        if (result.isOk()) {
            res.json({
                success: true
            });
        } else {
            res.json({
                success: false
            });
        }

        return next();
    });

server.get('ShowModalForm', server.middleware.https, csrfProtection.validateRequest, csrfProtection.generateToken, function (req, res, next) {
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    var Resource = require('dw/web/Resource');
    var formImage = req.querystring.formImage;
    var formImageAltText = req.querystring.formImageAltText;
    var formButtonDisplayName = req.querystring.formButtonDisplayName;

    var template = 'subscribe/subscribe.isml';
    var subscribeForm = server.forms.getForm('subscribe');
    var context = {
        headerText: Resource.msg('quickview.subscribe.form.header', 'homepage', null),
        closeButtonText: Resource.msg('link.quickview.close', 'product', null),
        template: template,
        subscribeForm: subscribeForm,
        formImage: formImage,
        formImageAltText: formImageAltText,
        formButtonDisplayName: formButtonDisplayName
    };

    res.setViewData(context);
    var viewData = res.getViewData();
    var renderedTemplate = renderTemplateHelper.getRenderedHtml(viewData, viewData.template);
    res.json({
        renderedTemplate: renderedTemplate
    });


    next();
});

module.exports = server.exports();
