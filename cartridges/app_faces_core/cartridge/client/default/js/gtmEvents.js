'use strict';
/**
 * Initializes GTM events for Faces
 */
function initEvents() {
    var $doc = $(document);
    window.dataLayer = window.dataLayer || [];
    var currencyCode = $('input.js-gtm-site-currencycode').val();
    $doc.on('gtm:enhancedEcommerceImpressions', function () {
        var productImpressions = [];
        var position = 0;
        $('[data-gtm-enhancedecommerce-impression]').each(function () {
            var $this = $(this);
            var gtmEnhancedEcommerceImpression = $this.data('gtm-enhancedecommerce-impression');
            if (gtmEnhancedEcommerceImpression.position) {
                return;
            }
            gtmEnhancedEcommerceImpression.position = ++position;
            $this.data('gtm-enhancedecommerce-impression', gtmEnhancedEcommerceImpression);
            productImpressions.push(gtmEnhancedEcommerceImpression);
        });
        if (productImpressions.length) {
            window.dataLayer.push({
                event: 'sendProductImpression',
                ecommerce: {
                    currencyCode: currencyCode,
                    impressions: productImpressions
                }
            });
        }
    });

    $doc.on('gtm:enhancedEcommercePushPageDataLayer', function (context, data) {
        if (!data.pageDataLayerEnhancedEcommerce) {
            return;
        }

        data.pageDataLayerEnhancedEcommerce.forEach(function (dataLayerEnhancedEcommerce) {
            if ('ecommerce' in dataLayerEnhancedEcommerce && 'detail' in dataLayerEnhancedEcommerce.ecommerce) {
                window.dataLayer.push({
                    event: 'productView',
                    dataLayerEnhancedEcommerce
                });
            }
        });
    });
}

module.exports = {
    initEvents: initEvents
};
