'use strict';

var util = require('brand_core/util');
var commonHelpers = require('app_brand_core/cartridge/scripts/helpers/commonHelpers');

module.exports = function () {
    $(document).on('click', '.js-scroll-to-el', function (event) {
        event.preventDefault();

        var width = $(window).width();

        if (commonHelpers.isMobileView(width, window.RA_BREAKPOINTS) || commonHelpers.isTabletView(width, window.RA_BREAKPOINTS)) {
            util.scrollBrowser($(this).offset().top);
        }
    });
};
