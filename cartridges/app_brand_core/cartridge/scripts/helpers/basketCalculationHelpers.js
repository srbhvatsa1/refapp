'use strict';

var originalExports = Object.keys(module.superModule || {});
originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});
var base = module.exports;

/**
 * Calculates the amount to be payed by a non complimentary payment instrument based
 * on the given basket/order. The method subtracts the amount of all redeemed complimentary payments
 * from the order total and returns this value.
 *
 * @param {Object} lineItemCtnr - LineIteam Container (Basket or Order)
 * @returns {dw.value.Money} non gift certificate amount
 */
function getNonComplimentaryPaymentAmount(lineItemCtnr) {
    var Money = require('dw/value/Money');
    var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');

    // The total redemption amount of all complimentary payment instruments in the basket.
    var complimentaryPaymentTotal = new Money(0.0, lineItemCtnr.getCurrencyCode());

    // Gets the list of all complimentary payment instruments
    var allPaymentInstrs = lineItemCtnr.getPaymentInstruments();
    var iter = allPaymentInstrs.iterator();
    var orderPI = null;

    // Sums the total redemption amount.
    while (iter.hasNext()) {
        orderPI = iter.next();
        if (paymentMethodHelpers.isComplementaryPaymentMethod(orderPI.paymentMethod)) {
            complimentaryPaymentTotal = complimentaryPaymentTotal.add(orderPI.paymentTransaction.amount);
        }
    }

    // Gets the order total.
    var orderTotal = lineItemCtnr.totalGrossPrice;

    // Calculates the amount to charge for the payment instrument.
    // This is the remaining open order total that must be paid.
    var amountOpen = orderTotal.subtract(complimentaryPaymentTotal);

    // Returns the open amount to be paid.
    return amountOpen;
}

base.getNonComplimentaryPaymentAmount = getNonComplimentaryPaymentAmount;
module.exports = base;
