<isinclude template="/components/modules" sf-toolkit="off" />
<isdecorate template="common/layout/checkoutOnlyLogos">
    <isscript>
        var assets = require('*/cartridge/scripts/assets.js');
        assets.addJs('/js/checkout.js');
        assets.addCss('/css/checkout.css');
    </isscript>

    <isif condition="${pdict.reportingURLs && pdict.reportingURLs.length}">
        <isinclude template="reporting/reportingUrls" />
    </isif>
    <div class="checkout-login-container">
        <div id="checkout-main" class="container data-checkout-stage checkout-main max-width-100-lg-down <isif condition="${pdict.order.usingMultiShipping && pdict.order.shipping.length > 1}">multi-ship</isif>" data-customer-type="${pdict.customer.registeredUser ? 'registered' : 'guest'}" data-checkout-stage="${pdict.currentStage}" data-checkout-get-url="${URLUtils.https('CheckoutServices-Get')}">
            <div class="row">
                <div class="col-12 px-0">
                    <h4 class="checkout-section-title">
                        <isprint value="${Resource.msg('label.checkout', 'checkout', null)}" encoding="off" />
                    </h4>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-8 px-lg-3">
                            <iscomment> Login </iscomment>
                            <div class="row step-wrapper checkout-sign-tab px-0 px-lg-3">
                                <div class="col-12 px-0">
                                    <div class="step-title">
                                        <h5 class="js-step-number">
                                            <isprint value="${Resource.msg('label.checkout.step.sign.in', 'checkout', null)}" encoding="off" />
                                        </h5>
                                        <div class="step-summary">
                                            <span>
                                                <isprint value="${Resource.msg('link.header.myAccount.welcome.back', 'account', null)}" encoding="off" />
                                            </span>
                                            <span class="text-capitalize">
                                                ${pdict.customer && pdict.customer.profile && pdict.customer.profile.firstName
                                                ? pdict.customer.profile.firstName : Resource.msg('label.guest', 'checkout', null)}
                                            </span>
                                        </div>
                                        <isif condition="${!pdict.CurrentCustomer.authenticated}">
                                            <a class="edit-button" href="${URLUtils.url('Checkout-Login')}">
                                                ${Resource.msg('action.edit.step', 'checkout', null)}
                                            </a>
                                        </isif>
                                    </div>
                                </div>
                            </div>
                            <iscomment> Samples </iscomment>
                            <isif condition="${pdict.bonusDiscountItems[0].bonusProducts.length}">
                                <div class="row step-wrapper js-checkout-samples-tab px-0 px-lg-3"
                                data-total-sample-count="${pdict.bonusDiscountItems[0].bonusProducts.length}">
                                    <div class="col-12 px-0">
                                        <div class="step-title">
                                            <h5 class="js-step-number">
                                                <isprint value="${Resource.msg('label.checkout.step.select.samples', 'checkout', null)}" encoding="off" />
                                            </h5>
                                            <isif condition="${pdict.bonusDiscountItems && pdict.bonusDiscountItems.length > 0}">
                                                <div class="step-summary d-none js-selected-sample-count"
                                                    data-text="${Resource.msg('label.x.samples.selected', 'checkout', null)}"
                                                    data-total-sample-count="${pdict.bonusDiscountItems[0].maxpids}">
                                                    ${Resource.msgf('label.x.samples.selected', 'checkout', null, pdict.selectedBonusDiscountItems, pdict.bonusDiscountItems[0].maxpids)}
                                                </div>
                                                <span class="edit-button js-edit-samples-btn">
                                                    ${Resource.msg('action.edit.step', 'checkout', null)}
                                                </span>
                                            </isif>
                                        </div>
                                    </div>
                                    <div class="col-12 px-0">
                                        <isif condition="${pdict.bonusDiscountItems && pdict.bonusDiscountItems.length > 0}">
                                            <div class="col-12 px-0">
                                                <div class="cart-section-wrapper js-acc-container open">
                                                    <div class="sample-section">
                                                        <h3 data-toggle-class-open="parent" class="px-3 px-lg-0">
                                                            <span class="js-selected-sample-count"
                                                                data-text="${Resource.msg('label.x.samples.selected', 'checkout', null)}"
                                                                data-total-sample-count="${pdict.bonusDiscountItems[0].maxpids}">
                                                                ${Resource.msgf('label.x.samples.selected', 'checkout', null, pdict.selectedBonusDiscountItems, pdict.bonusDiscountItems[0].maxpids)}
                                                            </span>
                                                        </h3>
                                                        <ul class="list-unstyled p-0">
                                                            <li>
                                                                <isinclude template="cart/bonusDiscountLineItems" />
                                                            </li>
                                                        </ul>
                                                        <div class="col-12 px-lg-0 next-step-button">
                                                            <div class="row">
                                                                <div class="mt-4 col-lg-6 d-flex justify-content-center align-items-center">
                                                                    <div class="text-uppercase sample-show-more js-sample-show-more">
                                                                        Show <span></span> More
                                                                    </div>
                                                                </div>
                                                                <div class="mt-4 col-lg-6 d-flex align-items-center justify-content-center">
                                                                    <button class="w-xs-100 w-md-50 w-lg-100 btn btn-primary px-5 d-block ${pdict.valid && pdict.valid.error ? 'disabled' : ''}">
                                                                        ${Resource.msg('button.continue.shipping', 'checkout', null)}
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <hr class="checkout-seperator"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </isif>
                                    </div>
                                </div>
                            </isif>
                            <iscomment> Shipping </iscomment>
                            <div class="row step-wrapper checkout-shipping-tab px-0">
                                <div class="col-12 px-0 px-lg-3">
                                    <div class="step-title">
                                        <h5 class="js-step-number">
                                            <isprint value="${Resource.msg('label.checkout.step.shipping', 'checkout', null)}" encoding="off" />
                                        </h5>
                                        <div class="shipping-summary">
                                            <span class="edit-button">
                                                ${Resource.msg('action.edit.step', 'checkout', null)}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 px-0 px-lg-3">
                                    <isinclude template="checkout/shipping/shipping" />
                                    <div class="col-12 shipping-summary shipping-wrapper">
                                        <isinclude template="checkout/shipping/shippingSummary" />
                                    </div>
                                </div>
                                <div class="alert alert-danger error-message mt-2 <isif condition="${pdict.pspErrorMessage}">d-block</isif>" role="alert">
                                    <p class="error-message-text js-error-message-text"
                                        data-shipping-error="${Resource.msg('error.no.shipping.address', 'checkout', null)}"
                                        data-billing-error="${Resource.msg('error.no.billing.address', 'checkout', null)}">
                                        <isif condition="${pdict.pspErrorMessage}">
                                            <isprint value="${pdict.pspErrorMessage}" />
                                        </isif>
                                    </p>
                                </div>
                                <div class="col-12 next-step-button px-0 px-lg-3">
                                    <div class="d-flex justify-content-center justify-content-md-start px-3 mx-md-4 mx-lg-0 px-lg-0">
                                        <button class="btn btn-primary px-5 w-100 w-md-50 submit-shipping js-submit-shipping-btn" type="submit" name="submit" value="submit-shipping" <isif condition="${pdict.order.usingMultiShipping && !pdict.order.shippable}">disabled</isif>>
                                            ${Resource.msg('button.continue.payment', 'checkout', null)}
                                        </button>
                                    </div>
                                    <div class="col-12 px-lg-0">
                                        <hr class="checkout-seperator"/>
                                    </div>
                                </div>
                            </div>
                            <iscomment> Payment </iscomment>
                            <div class="row step-wrapper checkout-review-tab px-0">
                                <div class="col-12 px-0 px-lg-3">
                                    <div class="step-title">
                                        <h5 class="js-step-number">
                                            <isprint value="${Resource.msg('label.checkout.step.review', 'checkout', null)}" encoding="off" />
                                        </h5>
                                    </div>
                                </div>
                                <div class="col-12 px-0 px-lg-3">
                                    <isinclude template="checkout/billing/billing" />
                                </div>
                                <div class="submit-payment-button-holder col-12 next-step-button submit-payment">
                                    <div class="submit-payment-button-holder-inner border-lg-0">
                                        <button class="btn btn-primary w-100 js-submit-payment-btn" type="submit" name="submit" value="submit-payment">
                                                <i class="icon-security"></i>
                                                ${Resource.msg('button.secure.checkout', 'checkout', null)}
                                        </button>
                                        <div class="terms-and-condition-note">
                                            <iscontentasset aid="payment-agree-terms-conditions" />
                                        </div>

                                        <button class="btn btn-primary place-order js-place-order-btn" data-action="${URLUtils.url('CheckoutServices-PlaceOrder')}"
                                                    type="submit" name="submit" value="place-order">${Resource.msg('button.place.order', 'checkout', null)}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 px-lg-3 px-0 border-top border-lg-0">
                            <div class="next-step-box">
                                <isinclude template="checkout/components/orderSummaryCheckout" />
                                <div class="row d-none d-lg-block">
                                    <div class="col-12">
                                        <div class="summary-payment-description">
                                            <iscontentasset aid="summary-payment-description" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <isinclude template="checkout/orderProductSummary" />
                            <div class="checkout-customer-service d-none d-lg-block">
                                <iscontentasset aid="cart-customer-service" />
                            </div>
                            <div class="summary-payment-description d-block d-lg-none mt-3">
                                <iscontentasset aid="summary-payment-description" />
                            </div>

                            <div class="d-lg-none">
                                <iscontentasset aid="checkout-supported-payment-methods" />
                            </div>
                            <div class="order-summary-contact d-block d-lg-none">
                                <iscontentasset aid="order-summary-contact" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <isinclude template="checkout/shipping/shippingAddressTemplate" />
    <isinclude template="checkout/billing/termsAndConditionsModal" />
</isdecorate>
