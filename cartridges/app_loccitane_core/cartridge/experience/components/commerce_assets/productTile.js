'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var URLUtils = require('dw/web/URLUtils');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();
    var ProductFactory = require('*/cartridge/scripts/factories/product');

    var productTileParams = { pview: 'tile', pid: context.content.product.ID };
    var product = ProductFactory.get(productTileParams);
    var productUrl = URLUtils.url('Product-Show', 'pid', product.id).relative().toString();
    model.product = product;
    model.display = {
        showAddToBagButton: content.showAddToBagButton,
        showShopNowButton: content.showShopNowButton,
        showQuickView: content.showQuickView,
        ratings: content.showRatings,
        showBadges: content.showBadges,
        swatches: content.showSwatches
    };

    model.urls = {
        product: productUrl
    };

    return new Template('experience/components/commerce_assets/productTile').render(model).text;
};
