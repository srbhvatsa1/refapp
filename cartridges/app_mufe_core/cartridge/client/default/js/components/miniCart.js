'use strict';

var cart = require('brand_core/cart/cart');
var updateMiniCart = true;

/**
 * appends params to a url
 * @param {string} data - data returned from the server's ajax call
 */
function displayMessageAndRemoveFromCart(data) {
    $.spinner().stop();
    var status = data.success ? 'alert-success' : 'alert-danger';

    if ($('.add-to-wishlist-messages').length === 0) {
        $('body').append('<div class="add-to-wishlist-messages "></div>');
    }
    $('.add-to-wishlist-messages')
        .append('<div class="add-to-wishlist-alert text-center ' + status + '">' + data.msg + '</div>');

    setTimeout(function () {
        $('.add-to-wishlist-messages').remove();
    }, 1500);
    var $targetElement = $('a[data-pid="' + data.pid + '"]').closest('.product-info').find('.remove-product');
    var itemToMove = {
        actionUrl: $targetElement.data('action'),
        productID: $targetElement.data('pid'),
        productName: $targetElement.data('name'),
        uuid: $targetElement.data('uuid')
    };
    $('body').trigger('afterRemoveFromCart', itemToMove);
    setTimeout(function () {
        $('.cart.cart-page #removeProductModal').modal();
    }, 2000);
}
/**
 * move items from Cart to wishlist
 * returns json object with success or error message
 */
function moveToWishlist() {
    $('body').on('click', '.product-move .move', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var pid = $(this).data('pid');
        var optionId = $(this).closest('.product-info').find('.lineItem-options-values').data('option-id');
        var optionVal = $(this).closest('.product-info').find('.lineItem-options-values').data('value-id');
        optionId = optionId || null;
        optionVal = optionVal || null;
        if (!url || !pid) {
            return;
        }

        $.spinner().start();
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
                pid: pid,
                optionId: optionId,
                optionVal: optionVal
            },
            success: function (data) {
                displayMessageAndRemoveFromCart(data);
            },
            error: function (err) {
                displayMessageAndRemoveFromCart(err);
            }
        });
    });
}

module.exports = {
    init: function () {
        cart.init();

        $('.minicart').on('count:update', function (event, count) {
            if (count && $.isNumeric(count.quantityTotal)) {
                var $minicartQuantity = $('.minicart-quantity');
                $minicartQuantity.text(count.quantityTotal);
                $('.minicart .minicart-link').attr({
                    'aria-label': count.minicartCountOfItems,
                    title: count.minicartCountOfItems
                });
                if (count.quantityTotal === 0) {
                    $minicartQuantity.addClass('d-none');
                } else {
                    $minicartQuantity.removeClass('d-none');
                }
            }
        });

        $('body').on('click', '.js-continue-shopping, .js-minicart-backdrop', function () {
            var $minicartLink = $('.js-minicart-button');
            var $minicartBackdrop = $('.js-minicart-backdrop');

            if ((event.type === 'focusout' && $('.minicart').has(event.target).length > 0)
            || (event.type === 'mouseleave' && $(event.target).is('.minicart .quantity'))
            || $('body').hasClass('modal-open')) {
                event.stopPropagation();
                return;
            }
            $('.header-top .popover').removeClass('show');
            $('.header-top .popover').addClass('d-none');
            $minicartLink.removeClass('link-active');
            $minicartBackdrop.hide();
        });

        $('.minicart').on('click touchstart', function (e) {
            var $headerTop = $('.header-top');
            var $minicart = $('.minicart');
            var $minicartLink = $('.js-minicart-button');
            var $minicartBackdrop = $('.js-minicart-backdrop');
            if ($(window).width() >= window.RA_BREAKPOINTS.xl) {
                e.preventDefault();
            }
            // Stop here if we don't need to show popover on given breakpoint and below
            var disableHoverBreakpoint = $minicart.data('disable-hover-breakpoint');
            if (window.RA_BREAKPOINTS[disableHoverBreakpoint] <= $(window).width()) return;

            if ($('.search:visible').length === 0) return;

            if ($headerTop.find('.popover.show').length === 0) {
                var $popover = $headerTop.find('.popover');

                if (!updateMiniCart) {
                    $popover.addClass('show');
                    $popover.removeClass('d-none');
                    $minicartLink.addClass('link-active');
                    $minicartBackdrop.show();
                    return;
                }

                var url = $minicart.data('action-url');

                $popover.addClass('show');
                $popover.removeClass('d-none');
                $popover.spinner().start();
                $minicartLink.addClass('link-active');
                $minicartBackdrop.show();
                $.get(url, function (data) {
                    $popover.empty();
                    $popover.append(data);
                    updateMiniCart = false;
                    $.spinner().stop();
                });
            }
        });

        $('body').on('change', '.minicart .quantity', function () {
            if ($(this).parents('.bonus-product-line-item').length && $('.cart-page').length) {
                location.reload();
            }
        });

        $('body').on('product:afterAddToCart', function () {
            updateMiniCart = true;
        });

        $('body').on('cart:update', function () {
            var $minicartLink = $('.js-minicart-button');
            var $minicartBackdrop = $('.js-minicart-backdrop');

            updateMiniCart = true;
            $('.header-top .popover').removeClass('show');
            $('.header-top .popover').addClass('d-none');
            $minicartLink.removeClass('link-active');
            $minicartBackdrop.hide();
        });
    },
    moveToWishlist: moveToWishlist
};
