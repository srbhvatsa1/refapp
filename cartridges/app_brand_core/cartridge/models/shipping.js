'use strict';

var base = module.superModule;

var hasBonusProduct = require('*/cartridge/models/productLineItem/hasBonusProduct');
var shippingMethodBasketShippingCosts = require('*/cartridge/models/shipping/shippingMethodBasketShippingCosts');
var shipmentSpecialRequest = require('*/cartridge/models/shipment/shipmentSpecialRequest');
var shipmentTracking = require('*/cartridge/models/shipment/shipmentTracking');

/**
 * @constructor
 * @classdesc Model that represents shipping information
 *
 * @param {dw.order.Shipment} shipment - the default shipment of the current basket
 * @param {Object} address - the address to use to filter the shipping method list
 * @param {Object} customer - the current customer model
 * @param {string} containerView - the view of the product line items (order or basket)
 */
function ShippingModel(shipment, address, customer, containerView) {
    base.call(this, shipment, address, customer, containerView);

    this.isPriceHidden = shipment ? shipment.custom.isPriceHidden : null;
    this.isGiftWrapped = shipment ? shipment.custom.isGiftWrapped : null;

    hasBonusProduct(this, shipment ? shipment.productLineItems : null);
    shippingMethodBasketShippingCosts(this, shipment, containerView);
    shipmentSpecialRequest(this, shipment);
    shipmentTracking(this, shipment);
}

module.exports = ShippingModel;
