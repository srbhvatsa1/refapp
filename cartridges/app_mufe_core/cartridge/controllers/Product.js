'use strict';

var server = require('server');
server.extend(module.superModule);

server.get('ShadeFinder', function (req, res, next) {
    var Site = require('dw/system/Site');

    var shadefinder = Site.getCurrent().getCustomPreferenceValue('shadeFinderConfig');
    var shadefinder2 = JSON.parse(shadefinder);
    res.render('common/shadeFinder', {
        shadefinder: shadefinder,
        shadefinder2: shadefinder2
    });
    next();
});

module.exports = server.exports();
