'use strict';

var server = require('server');
server.extend(module.superModule);

/**
 * Calculates the unit and total prices of items
 * that are just added to cart.
 * Extended to disable to provide bonusDiscountLineItem
 */

server.append('Show', function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');
    var Resource = require('dw/web/Resource');
    var viewData = res.getViewData();

    viewData.breadcrumbs = [
        {
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('label.your.bag', 'cart', null)
        }
    ];

    res.setViewData(viewData);

    next();
});

server.append('AddProduct', function (req, res, next) {
    var viewData = res.getViewData();

    // disable pdp bonus items popup in loccitane
    viewData.newBonusDiscountLineItem = {};

    res.setViewData(viewData);

    next();
});

/**
 * UpdateCartGift Controller Action
 * Updates gift attributes on basket's defaultShipment
 */
server.replace('UpdateCartGift', function (req, res, next) {
    var currentSite = require('dw/system/Site').current;

    // check if the current site gift is enabled
    if (!currentSite.getCustomPreferenceValue('enableSendAsGift')) {
        res.json({
            error: true
        });

        return next();
    }

    var BasketMgr = require('dw/order/BasketMgr');
    var URLUtils = require('dw/web/URLUtils');

    var currentBasket = BasketMgr.getCurrentBasket();

    // check the current basket
    if (!currentBasket) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });

        return next();
    }

    var formErrors = require('*/cartridge/scripts/formErrors');

    var giftForm = server.forms.getForm('gift');
    var fieldErrors = formErrors.getFormErrors(giftForm);
    if (!giftForm.valid || Object.keys(fieldErrors).length) {
        res.json({
            error: true,
            fieldErrors: [fieldErrors],
            serverErrors: []
        });

        return next();
    }

    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

    var isGift = giftForm.isGift.checked;
    var giftMessage = isGift && !currentSite.getCustomPreferenceValue('disableGiftMessage') ? giftForm.giftMessage.value : null;
    var isPriceHidden = isGift ? giftForm.isPriceHidden.checked : null;
    var isGiftWrapped = giftForm.isGiftWrapped.checked;

    var updateCartGiftResult = COHelpers.setGift(currentBasket.defaultShipment, isGift, giftMessage, isPriceHidden, isGiftWrapped);

    if (updateCartGiftResult.error) {
        res.json({
            error: updateCartGiftResult.error,
            fieldErrors: [],
            serverErrors: [updateCartGiftResult.errorMessage]
        });

        return next();
    }


    res.json({
        success: true
    });

    return next();
});

module.exports = server.exports();
