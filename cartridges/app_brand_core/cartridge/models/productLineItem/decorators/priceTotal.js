'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
var Money = require('dw/value/Money');

/**
 * get the total price for the product line item
 * @param {dw.value.Money} money - Money
 * @returns {string} Formatted money
 */
function getFormattedMoney(money) {
    return renderTemplateHelper.getRenderedHtml({ price: money }, 'product/components/pricing/currencyFormatted');
}

/**
 * get the total price for the product line item
 * @param {dw.order.ProductLineItem} lineItem - API ProductLineItem instance
 * @param {Object} priceModel - Price model object including sales and list prices of product
 * @returns {Object} an object containing the product line item total info.
 */
function getTotalPrice(lineItem, priceModel) {
    var context;
    var price;
    var result = {};
    var product = lineItem.product;
    var template = 'checkout/productCard/productCardProductRenderedTotalPrice';

    price = lineItem.adjustedPrice;

    // The platform does not include prices for selected option values in a line item product's
    // price by default.  So, we must add the option price to get the correct line item total price.
    collections.forEach(lineItem.optionProductLineItems, function (item) {
        price = price.add(item.adjustedPrice);
    });

    result.price = getFormattedMoney(price);

    if (lineItem.priceAdjustments.getLength() > 0) {
        // There are discounts applied on sale price
        if (priceModel && priceModel.list && priceModel.list.decimalPrice) {
            // Assign list price to nonAdjustedPrice
            result.nonAdjustedPrice = getFormattedMoney(new Money(Math.ceil(
                priceModel.list.decimalPrice * lineItem.quantity), price.currencyCode));
        } else {
            // Assign sale price to nonAdjustedPrice if there is no list price defined for this product
            var salePrice = getFormattedMoney(lineItem.getPrice());
            if (salePrice !== result.price) {
                result.nonAdjustedPrice = salePrice;
            }
        }
    } else if (product.custom.worth) {
        // If there no discounts, set worth price
        result.worthPrice = getFormattedMoney(new Money(Math.ceil(
            product.custom.worth * lineItem.quantity), price.currencyCode));
    } else if (priceModel && priceModel.list && priceModel.list.decimalPrice) {
        // If there no discounts and worth price, set worth list price
        result.listPrice = getFormattedMoney(new Money(priceModel.list.decimalPrice * lineItem.quantity, price.currencyCode));
    }

    context = {
        lineItem: {
            UUID: lineItem.UUID,
            priceTotal: result
        }
    };

    result.decimalPrice = price.decimalValue;
    result.renderedPrice = renderTemplateHelper.getRenderedHtml(context, template);

    return result;
}


module.exports = function (object, lineItem) {
    Object.defineProperty(object, 'priceTotal', {
        enumerable: true,
        value: getTotalPrice(lineItem, object.price)
    });
};
