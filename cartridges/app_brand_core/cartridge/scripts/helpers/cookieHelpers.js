'use strict';
/* global request, response */
var Cookie = require('dw/web/Cookie');
var StringUtils = require('dw/util/StringUtils');
var siteLocaleCookieName = require('*/cartridge/scripts/helpers/constants').siteLocaleCookieName || 'site_locale';

/**
 * Sets the the cookie to the response with provided cookie values
 * @param {string} cookieName - The cookie name
 * @param {string} cookieValue - The value of the cookie
 * @param {number} cookieMaxAge - The maxAge of the cookie in seconds
 * @param {string} cookieDomain - The domain name to be set in cookie
 */
function setCookie(cookieName, cookieValue, cookieMaxAge, cookieDomain) {
    if (!cookieName) {
        return;
    }

    var siteCookie = new Cookie(cookieName, cookieValue);

    if (cookieMaxAge) {
        siteCookie.setMaxAge(cookieMaxAge);
    }

    if (cookieDomain) {
        siteCookie.setDomain(cookieDomain);
    }

    siteCookie.setPath('/');

    response.addHttpCookie(siteCookie);
}

/**
 * Gets the the cookie from request.getHttpCookies if exists
 * @param {string} cookieName - The cookie name
 * @returns {dw.web.Cookie} the cookie that was set before, null if not found
 */
function getCookie(cookieName) {
    if (!cookieName) {
        return null;
    }

    var cookies = request.getHttpCookies();


    // check if the cookie exists
    for (var cookieKey in cookies) { // eslint-disable-line
        if (cookies[cookieKey]) {
            var cookie = cookies[cookieKey];
            if (cookie.name === cookieName) {
                return cookie;
            }
        }
    }

    // cookie not found
    return null;
}

/**
 * Sets the last locale cookie to the user's browser
 * @param {string} locale - The locale to be set as value into the cookie
 * @param {string} host - The host name of the site
 */
function setSiteLocaleCookie(locale, host) {
    var domain = '';
    var cookieName = siteLocaleCookieName;
    host = (host || request.httpHost); // eslint-disable-line no-param-reassign

    // get the domain part
    // get the subdomain part
    // set cookie name with subdomain
    // set cookie to domain
    if (host) {
        var splittedHost = host.split('.');
        domain = splittedHost && splittedHost.length > 2 ? splittedHost.slice(1).join('.') : splittedHost.join('.');
        var subDomain = splittedHost[0];
        cookieName = StringUtils.format('{0}_{1}', subDomain, cookieName);
    }

    // set new locale to the session
    session.custom.locale = locale; // eslint-disable-line no-param-reassign, no-undef

    // set one year for the expire
    // one year in seconds: 60 * 60 * 24 * 365
    setCookie(cookieName, locale, 31536000, domain);
}

/**
 * Gets the last locale cookie if exists
 * @returns {dw.web.Cookie} the cookie that was set before, null if not found
 */
function getSiteLocaleCookie() {
    var cookieName = siteLocaleCookieName;
    if (request.httpHost) {
        // cookie name is with subdomain
        // it is needed since 1 site: multiple subdomain
        var subDomain = request.httpHost.split('.')[0];
        cookieName = StringUtils.format('{0}_{1}', subDomain, cookieName);
    }

    return getCookie(cookieName);
}

module.exports = {
    setSiteLocaleCookie: setSiteLocaleCookie,
    getSiteLocaleCookie: getSiteLocaleCookie,
    setCookie: setCookie,
    getCookie: getCookie
};
