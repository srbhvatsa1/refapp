'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var PageRenderHelper = require('*/cartridge/experience/utilities/PageRenderHelper.js');

/**
 * Render logic for the mosaicComponent
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var component = context.component;

    model.regions = PageRenderHelper.getRegionModelRegistry(component);
    model.customClasses1 = context.content.classes1 || 'col-6';
    model.customClasses2 = context.content.classes2 || 'col-6';
    return new Template('experience/components/commerce_layouts/mosaicComponent').render(model).text;
};
