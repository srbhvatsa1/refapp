'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');
/**
 * Render logic for storefront.imageWithText component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */


module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.text = content.text;
    model.image = ImageTransformation.getScaledImage(content.image);
    model.imageAlt = content.imageAlt;
    model.imagePosition = content.imagePositionLeft ? 'image-position-left' : 'image-position-right';

    return new Template('experience/components/commerce_assets/positionedImageWithText').render(model).text;
};
