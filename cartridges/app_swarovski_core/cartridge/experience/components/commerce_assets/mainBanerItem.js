'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');

/**
 * Render logic for storefront.imageAndText component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */

module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.image = content.image ? ImageTransformation.getScaledImage(content.image) : null;
    model.imageMobile = content.imageMobile ? ImageTransformation.getScaledImage(content.imageMobile) : null;
    model.alt = content.alt || null;
    model.title = content.title || null;
    model.buttonText1 = content.buttonText1 || null;
    model.buttonLink1 = content.buttonLink1 || '#';
    model.buttonText2 = content.buttonText2 || null;
    model.buttonLink2 = content.buttonLink2 || '#';
    model.useVideo = content.useVideo || false;
    model.descriptionBelow = content.descriptionBelow || false;
    model.fullWight = content.fullWight || false;
    model.videoUrl = content.videoUrl || '#'; // TODO IMPROVE WITH URLuTILS

    return new Template('experience/components/commerce_assets/mainBanerItem').render(model).text;
};
