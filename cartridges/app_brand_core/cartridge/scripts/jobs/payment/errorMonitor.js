'use strict';
var logger = require('dw/system/Logger').getLogger('monitor.payment.error');
var pspPaymentStatusNoteSubject = 'Payment Status';

/**
 * Sends orders that have payment error with details as a mail to given addresses
 * @param {dw.util.ArrayList} orders - list of orders that have payment error
 * @param {string} emailAddresses comma seperated email addresses
 * @returns {boolean} true if successful
 */
function sendAsMail(orders, emailAddresses) {
    var Site = require('dw/system/Site');
    var Mail = require('dw/net/Mail');
    var Resource = require('dw/web/Resource');
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

    var email = new Mail();

    try {
        emailAddresses.split(',').forEach(function (emailAddress) {
            email.addTo(emailAddress);
        });

        email.setSubject(Resource.msgf('subject', 'psporderpayment', null, Site.current.ID));
        email.setFrom(Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com');
        email.setContent(renderTemplateHelper.getRenderedHtml({ orders: orders }, 'jobs/payment/errorNotification'), 'text/html', 'UTF-8');
        email.send();

        return true;
    } catch (e) {
        logger.error('Error on sendAsMail function in jobs/payment/errorMonitor.js, message: {0}', e.message);
    }

    return false;
}

/**
 * Iterates orders within given period to check lastPaymentStatus is error
 * to report/send them into given email list
 * @param {dw.util.HashMap} args - inputs from the BM
 * @returns {dw.system.Status} status object
 */
function execute(args) {
    var OrderMgr = require('dw/order/OrderMgr');
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var Status = require('dw/system/Status');
    var Site = require('dw/system/Site');
    var Calendar = require('dw/util/Calendar');
    var ArrayList = require('dw/util/ArrayList');
    var collections = require('*/cartridge/scripts/util/collections');

    var intervalInHours = args.IntervalInHours;
    var emailAddresses = args.EmailAddresses;

    if (!intervalInHours || !emailAddresses) {
        return new Status(Status.ERROR, '500', 'Parameters are not well formed!');
    }

    var calendar = Site.current.calendar;
    calendar.add(Calendar.HOUR, -1 * intervalInHours);

    var ordersIterator = OrderMgr.queryOrders(
        'custom.lastPaymentStatus = {0} AND creationDate >= {1}',
        'orderNo asc',
        'error',
        calendar.time
    );

    var orders = new ArrayList();

    while (ordersIterator.hasNext()) {
        var order = ordersIterator.next();
        var paymentProcessor;
        var errorNote;

        var paymentInstruments = order.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);
        if (!paymentInstruments.empty && paymentInstruments[0].paymentTransaction) {
            paymentProcessor = paymentInstruments[0].paymentTransaction.paymentProcessor;
        }

        var orderNotes = order.getNotes(); // Returns the list of notes for this object, ordered by creation time from oldest to newest.
        orderNotes.reverse();

        errorNote = collections.find(orderNotes, function (note) {
            return note.subject === pspPaymentStatusNoteSubject;
        });

        orders.add({
            orderNo: order.orderNo,
            status: order.status.displayValue,
            paymentStatus: order.paymentStatus.displayValue,
            paymentProcessor: paymentProcessor ? paymentProcessor.ID : '-',
            detail: errorNote ? errorNote.text : '-'
        });
    }

    if (orders.empty) {
        return new Status(Status.OK);
    }

    if (sendAsMail(orders, emailAddresses)) {
        return new Status(Status.OK);
    }

    return new Status(Status.ERROR);
}

module.exports = {
    execute: execute
};
