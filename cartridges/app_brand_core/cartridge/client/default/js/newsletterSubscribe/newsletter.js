'use strict';

var formValidation = require('base/components/formValidation');

module.exports = {
    submitSubscribeForm: function () {
        $('form.subscribe-email-form,form.subscribe-newsletter-form').submit(function (e) {
            var $form = $(this);
            e.preventDefault();
            var url = $form.attr('action');
            $('.js-error, .js-success', $form).addClass('d-none');
            $form.spinner().start();
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: $form.serialize(),
                success: function (data) {
                    $form.spinner().stop();
                    if (!data.success) {
                        if (data.fieldErrors) {
                            formValidation($form, data);
                        } else {
                            $('.js-error', $form).removeClass('d-none');
                        }
                    } else {
                        if (data.redirectUrl) { //eslint-disable-line
                            window.location = data.redirectUrl;
                        } else {
                            $('.js-success', $form).removeClass('d-none');
                        }
                    }
                },
                error: function () {
                    $('.js-error', $form).removeClass('d-none');
                    $form.spinner().stop();
                }
            });
            return false;
        });
    }
};
