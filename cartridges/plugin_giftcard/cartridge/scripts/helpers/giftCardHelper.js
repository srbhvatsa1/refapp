'use strict';

var GiftCertificateMgr = require('dw/order/GiftCertificateMgr');
var Transaction = require('dw/system/Transaction');
var collections = require('*/cartridge/scripts/util/collections');

/**
 * Send an email to recipient of gift certificate
 * @param {dw.order.GiftCertificate} giftCertificate - giftCertificate API object
 */
function sendGiftCertificateEmail(giftCertificate) {
    var Resource = require('dw/web/Resource');
    var Site = require('dw/system/Site');
    var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');

    var emailObj = {
        to: giftCertificate.recipientEmail,
        subject: Resource.msg('giftcertificateemail.subject', 'email', null),
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com'
    };

    emailHelpers.sendEmail(emailObj, 'giftCard/giftCertificateEmail', { GiftCertificate: giftCertificate });
}

/**
 * Tries to find the giCard products in the order product line items
 * If any then creates the GiftCertificate in the system
 * and send the gift certificate emails to the receipents if needed
 *
 * @param {dw.order.Order} order - The order object to be submitted.
 */
function createGiftCertificates(order) {
    if (!order) {
        return;
    }

    collections.forEach(order.productLineItems, function (pli) {
        if (pli.gift) {
            Transaction.wrap(function () {
                var giftCertificate = GiftCertificateMgr.createGiftCertificate(pli.proratedPrice.value);
                if (giftCertificate) {
                    // set gift certificate attributes
                    giftCertificate.setRecipientEmail(pli.custom.giftCardRecipientEmail);
                    giftCertificate.setRecipientName(pli.custom.giftCardRecipientName);
                    giftCertificate.setSenderName(pli.custom.giftCardFromName);
                    giftCertificate.setMessage(pli.giftMessage);
                    giftCertificate.setOrderNo(order.orderNo);

                    var Calendar = require('dw/util/Calendar');
                    var Site = require('dw/system/Site');

                    // add site pref expiration perid days to currentDate as expirationDate
                    var giftCardValidDurationInDays = Site.getCurrent().getCustomPreferenceValue('giftCardValidDurationInDays') || 365;
                    if (giftCardValidDurationInDays > 0) {
                        var expirationDateCalendar = new Calendar();
                        expirationDateCalendar.add(Calendar.DAY_OF_YEAR, giftCardValidDurationInDays);
                        giftCertificate.custom.expirationDate = expirationDateCalendar.time;
                    }

                    // set productlineitem code attribute
                    pli.custom.giftCardCode = giftCertificate.giftCertificateCode; // eslint-disable-line no-param-reassign

                    // if the product's giftCardType is virtual then send an email to the recipient
                    if (pli.product.custom.giftCardType.value === 'virtual') {
                        sendGiftCertificateEmail(giftCertificate);
                    }
                }
            });
        }
    });
}

/**
 * Returns the Gift Certificate identified by the specified gift certificate code.
 *
 * @param {string} giftCertificateCode - giftCertificateCode
 * @param {string} currencyCode - currencyCode
 * @returns {Object} the Gift Certificate identified by the specified code or null and the status.
 */
function getGiftCertificateByCode(giftCertificateCode, currencyCode) {
    var Status = require('dw/system/Status');
    var GiftCertificateStatusCodes = require('dw/order/GiftCertificateStatusCodes');
    var GiftCertificate = require('dw/order/GiftCertificate');
    var Resource = require('dw/web/Resource');

    var giftCertificate = GiftCertificateMgr.getGiftCertificateByCode(giftCertificateCode);
    var result = {
        error: true,
        giftCertificate: giftCertificate
    };

    if (!giftCertificate) { // make sure exists
        result.status = new Status(Status.ERROR,
            GiftCertificateStatusCodes.GIFTCERTIFICATE_NOT_FOUND,
            Resource.msg('giftcertificate.error.notfound', 'forms', null));
    } else if (!giftCertificate.isEnabled()) { // make sure it is enabled
        result.status = new Status(Status.ERROR,
            GiftCertificateStatusCodes.GIFTCERTIFICATE_DISABLED,
            Resource.msg('giftcertificate.error.disabled', 'forms', null));
    } else if (giftCertificate.getStatus() === GiftCertificate.STATUS_PENDING) { // make sure it is available for use
        result.status = new Status(Status.ERROR,
            GiftCertificateStatusCodes.GIFTCERTIFICATE_PENDING,
            Resource.msg('giftcertificate.error.notavailable', 'forms', null));
    } else if (giftCertificate.getStatus() === GiftCertificate.STATUS_REDEEMED) { // make sure it has not been fully redeemed
        result.status = new Status(Status.ERROR,
            GiftCertificateStatusCodes.GIFTCERTIFICATE_INSUFFICIENT_BALANCE,
            Resource.msg('giftcertificate.error.insufficientbalance', 'forms', null));
    } else if (giftCertificate.balance.currencyCode !== currencyCode) { // make sure the GC is in the right currency
        result.status = new Status(Status.ERROR,
            GiftCertificateStatusCodes.GIFTCERTIFICATE_CURRENCY_MISMATCH,
            Resource.msg('giftcertificate.error.currencymismatch', 'forms', null));
    } else if (giftCertificate.custom.expirationDate && giftCertificate.custom.expirationDate < new Date()) { // make sure the GC is not expired
        result.status = new Status(Status.ERROR,
            GiftCertificateStatusCodes.GIFTCERTIFICATE_CURRENCY_MISMATCH,
            Resource.msg('giftcertificate.error.expired', 'forms', null));
    } else {
        result.error = false;
        result.status = new Status(Status.OK);
    }

    return result;
}

/**
 * Creates a gift certificate payment instrument from the given gift certificate ID for the basket. The
 * method attempts to redeem the current balance of the gift certificate. If the current balance exceeds the
 * order total, this amount is redeemed and the balance is lowered.
 *
 * @transactional
 * @param {dw.order.GiftCertificate} giftCertificate - The gift certificate.
 * @param {dw.order.LineItemCtnr} lineItemCtnr - The lineItemCtnr (current basket) object to be submitted.
 * @returns {dw.order.PaymentInstrument} The created PaymentInstrument.
 */
function createGiftCertificatePaymentInstrument(giftCertificate, lineItemCtnr) {
    var Money = require('dw/value/Money');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

    var paymentInstrument = Transaction.wrap(function () {
        // Removes any duplicates.
        // Iterates over the list of payment instruments to check.
        var gcPaymentInstrs = lineItemCtnr.getGiftCertificatePaymentInstruments(giftCertificate.getGiftCertificateCode()).iterator();
        var existingPI = null;

        // Removes found gift certificates, to prevent duplicates.
        while (gcPaymentInstrs.hasNext()) {
            existingPI = gcPaymentInstrs.next();
            lineItemCtnr.removePaymentInstrument(existingPI);
        }

        // Fetches the balance and the order total.
        var balance = giftCertificate.getBalance();
        var orderTotal = lineItemCtnr.getTotalGrossPrice();

        // Sets the amount to redeem equal to the remaining balance.
        var amountToRedeem = balance;

        // Since there may be multiple gift certificates, adjusts the amount applied to the current
        // gift certificate based on the order total minus the aggregate amount of the current gift certificates.

        var giftCertTotal = new Money(0.0, lineItemCtnr.getCurrencyCode());

        // Iterates over the list of gift certificate payment instruments
        // and updates the total redemption amount.
        gcPaymentInstrs = lineItemCtnr.getGiftCertificatePaymentInstruments().iterator();
        var orderPI = null;

        while (gcPaymentInstrs.hasNext()) {
            orderPI = gcPaymentInstrs.next();
            giftCertTotal = giftCertTotal.add(orderPI.getPaymentTransaction().getAmount());
        }

        // Calculates the remaining order balance.
        // This is the remaining open order total that must be paid.
        var orderBalance = orderTotal.subtract(giftCertTotal);

        // The redemption amount exceeds the order balance.
        // use the order balance as maximum redemption amount.
        if (orderBalance < amountToRedeem) {
            // Sets the amount to redeem equal to the order balance.
            amountToRedeem = orderBalance;
        }

        // Creates a payment instrument from this gift certificate.
        var gcPaymentInstrument = lineItemCtnr.createGiftCertificatePaymentInstrument(giftCertificate.getGiftCertificateCode(), amountToRedeem);
        var gcPaymentTransaction = gcPaymentInstrument.getPaymentTransaction();
        gcPaymentTransaction.setAmount(amountToRedeem);
        var paymentProcessor = require('dw/order/PaymentMgr').getPaymentMethod('GIFT_CERTIFICATE').getPaymentProcessor();
        gcPaymentTransaction.setPaymentProcessor(paymentProcessor);
        gcPaymentTransaction.setTransactionID(gcPaymentTransaction.getUUID());

        COHelpers.recalculateBasket(lineItemCtnr);
        return gcPaymentInstrument;
    });

    return paymentInstrument;
}

/**
 * Removes gift certificate payment instrument from the given gift certificate instrument's transactionID for the basket.
 *
 * @transactional
 * @param {string} transactionID - The gift certificate payment instrument's transactionID.
 * @param {dw.order.LineItemCtnr} lineItemCtnr - The lineItemCtnr (current basket) object to be submitted.
 * @returns {boolean} true if successfully removed, false otherwise
 */
function removeGiftCertificatePaymentInstrument(transactionID, lineItemCtnr) {
    var success = false;
    collections.forEach(lineItemCtnr.getGiftCertificatePaymentInstruments(), function (giftCertificatePaymentInstrument) {
        var giftCertificatePaymentInstrumentTransaction = giftCertificatePaymentInstrument.getPaymentTransaction();
        if (giftCertificatePaymentInstrumentTransaction && giftCertificatePaymentInstrumentTransaction.getTransactionID() === transactionID) {
            Transaction.wrap(function () {
                lineItemCtnr.removePaymentInstrument(giftCertificatePaymentInstrument);
                success = true;
            });
        }
    });

    if (success) {
        var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
        Transaction.wrap(function () {
            COHelpers.recalculateBasket(lineItemCtnr);
        });
    }

    return success;
}

module.exports = {
    createGiftCertificates: createGiftCertificates,
    getGiftCertificateByCode: getGiftCertificateByCode,
    createGiftCertificatePaymentInstrument: createGiftCertificatePaymentInstrument,
    removeGiftCertificatePaymentInstrument: removeGiftCertificatePaymentInstrument
};
