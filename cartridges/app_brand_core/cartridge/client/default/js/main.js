window.jQuery = window.$ = require('jquery');
var processInclude = require('base/util');
window.RA_BREAKPOINTS = require('./config/breakpoints.json').breakpoints;

$(document).ready(function () {
    processInclude(require('./dataStates'));
    processInclude(require('./components/tooltip'));
    processInclude(require('./components/cookieWarning'));
    processInclude(require('./header'));
    processInclude(require('./product/addToCartNotification'));
    processInclude(require('base/components/footer'));
    processInclude(require('./components/miniCart'));
    processInclude(require('base/components/collapsibleItem'));
    processInclude(require('./components/clientSideValidation'));
    processInclude(require('base/components/countrySelector'));
    processInclude(require('./newsletterSubscribe/newsletter'));
    processInclude(require('./components/cleave'));
    processInclude(require('./components/selectdate'));
    processInclude(require('./components/password'));
    processInclude(require('./components/login'));
    processInclude(require('base/components/toolTip'));
    processInclude(require('./components/popup'));
});

require('bootstrap/js/src/util.js');
require('bootstrap/js/src/alert.js');
// require('bootstrap/js/src/button.js');
require('bootstrap/js/src/carousel.js');
require('bootstrap/js/src/collapse.js');
// require('bootstrap/js/src/dropdown.js');
require('bootstrap/js/src/modal.js');
require('bootstrap/js/src/scrollspy.js');
require('bootstrap/js/src/tab.js');
require('bootstrap/js/src/tooltip.js');
// require('bootstrap/js/src/popover.js');

require('base/components/spinner');

/* jQuery Plugins */
processInclude(require('./plugins/sticky'));
processInclude(require('./plugins/productLoader'));
processInclude(require('./plugins/consentTracking'));
processInclude(require('./plugins/searchWithSuggestions'));
