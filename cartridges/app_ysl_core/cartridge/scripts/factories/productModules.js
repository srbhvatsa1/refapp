'use strict';

var originalExports = Object.keys(module.superModule);
originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});
var base = module.exports;

base.modules['you-may-also-like'] = base.modules['recommendations-alternative'];
base.modules['perfect-routine'] = base.modules['recommendations-complementary'];

module.exports = base;
