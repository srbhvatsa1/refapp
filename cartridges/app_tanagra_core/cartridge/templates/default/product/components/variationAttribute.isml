<iscomment>

Template Notes:

- This template is intended to be referenced from an <isinclude> tag within an <isloop> in another
  source template.  The <isloop> is expected to have a "status" attribute, named "attributeStatus".
- ${attributeStatus.last} is checked to see whether a particular attribute row should include the
  Quantity drop-down menu

</iscomment>
<div class="attribute attribute-${attr.id}">
    <isif condition="${attr.swatchable}">
        <iscomment>Select color button</iscomment>
        <isif condition="${!(isBundle && product.productType === 'variant' && !attr.selectedValue)}">
            <label class="form-control-label d-xl-none" for="${attr.id}">${attr.displayName}</label>
            <isif condition="${attr.selectedValue}">
                <button type="button"
                    class="btn btn-link btn-block btn-select custom-select ${attr.values.length > 1 ? '' : 'single-select-option'}"
                    data-toggle-class-open="parent">
                    <span data-attr-value="${attr.selectedValue.value}"
                        class="${attr.id}-value swatch-circle swatch-value js-selected-swatch"
                        style="background-image: url(${attr.selectedValue.images['swatch'][0].url})"
                    ></span>
                    <span class="js-selected-swatch-name">${attr.selectedValue.displayValue}</span>
                </button>
            <iselse/>
                <button type="button"
                    class="btn btn-link btn-block btn-select custom-select"
                    data-toggle-class-open="parent">
                    <span class="swatch-circle swatch-value js-selected-swatch d-none"></span>
                    <span class="js-selected-swatch-name">
                        ${Resource.msgf('button.select', 'product', null, attr.displayName)}
                    </span>
                </button>
            </isif>
        </isif>

        <iscomment>Select color dropdown</iscomment>
        <isif condition="${attr.values.length > 1}">
            <div class="swatches">
                <iscomment>Selected color</iscomment>
                <isif condition="${!(isBundle && product.productType === 'variant' && !attr.selectedValue)}">
                    <isif condition="${attr.selectedValue}">
                        <a href="javascript:void(0)"
                            title="${attr.selectedValue.displayValue}">
                            <span data-attr-value="${attr.selectedValue.value}"
                                class="${attr.id}-value swatch-circle swatch-value selected"
                                style="background-image: url(${attr.selectedValue.images['swatch'][0].url})"
                            ></span>
                        </a>
                    </isif>
                </isif>

                <iscomment>Other color options</iscomment>
                <isloop items="${attr.values}" var="attrValue">
                    <isif condition="${!(isBundle && product.productType === 'variant' && !attrValue.selected)}">
                        <isif condition="${!attrValue.selected}">
                            <a href="${attrValue.url || 'javscript:void(0)'}" ${ product.productType === "variant" && isBundle ? "disabled" : "" }
                                title="${attrValue.displayValue}">
                                <span data-attr-value="${attrValue.value}"
                                    class="
                                        ${attr.id}-value
                                        swatch-circle
                                        swatch-value
                                        ${attrValue.selectable ? 'selectable' : 'unselectable'}
                                    "
                                    style="background-image: url(${attrValue.images['swatch'].length > 0 ? attrValue.images['swatch'][0].url : ''})"
                                ></span>
                            </a>
                        </isif>
                    </isif>
                </isloop>
            </div>
        </isif>

    <iselse/>
        <isif condition="${attr.values.length > 1}">
            <iscomment>Attribute Values Drop Down Menu</iscomment>
            <label class="form-control-label" for="${attr.id}">${attr.displayName}</label>
            <select id="${attr.id}" class="btn-select custom-select form-control select-${attr.id} ${attr.values.length > 1 ? '' : 'single-select-option'}" ${ (product.productType === "variant" && isBundle) || attr.disabled ? "disabled" : "" }>
                <isif condition="${!attr.selectedValue}">
                    <option value="${attr.resetUrl}" class="js-attr-default-option">
                        ${Resource.msgf('button.select', 'product', null, attr.displayName)}
                    </option>
                </isif>
                <isloop items="${attr.values}" var="attrValue">
                    <option value="${attrValue.url}"
                            data-attr-value="${attrValue.value ? attrValue.value : ''}"
                            data-subtext="${attrValue.price ? attrValue.price : ''}"
                            ${!attrValue.selectable ? 'disabled' : ''}
                            ${attrValue.selected ? 'selected' : ''}
                    >
                        ${attrValue.displayValue}
                    </option>
                </isloop>
            </select>
        </isif>
    </isif>
</div>
