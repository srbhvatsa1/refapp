/* globals google */
'use strict';

/**
 *  Calculate instagram date
 * @param {*} date1 first date
 * @param {*} date2 sec date
 * @returns {string} date string
 */
function calcDate(date1, date2) {
    var diff = Math.floor(date1.getTime() - date2.getTime());
    var day = 1000 * 60 * 60 * 24;
    var language = window.SiteSettings.language;
    var arabicNumbers = [
        '٠',
        '١',
        '٢',
        '٣',
        '٤',
        '٥',
        '٦',
        '٧',
        '٨',
        '٩'
    ];

    var days = Math.floor(diff / day);
    var months = Math.floor(days / 31);
    var years = Math.floor(months / 12);
    var translateNumber = function (num) {
        var currentNumber = num.toString();
        var arabicNumber = '';
        for (var i = 0; i < currentNumber.length; i++) {
            arabicNumber += arabicNumbers[currentNumber.charAt(i)];
        }

        return arabicNumber;
    };

    if (years) {
        return language === 'ar' ? ('سنوات ' + translateNumber(years) + ' منذ') : (years + ' years ago');
    } else if (months) {
        return language === 'ar' ? ('أشهر ' + translateNumber(months) + ' قبل') : (months + ' months ago');
    }
    if (days === 0) {
        return language === 'ar' ? 'اليوم' : 'today';
    } else if (days === 1) {
        return language === 'ar' ? 'في الامس' : 'yesterday';
    }
    return language === 'ar' ? ('أيام ' + translateNumber(days) + ' منذ') : (days + ' days ago');
}

var instagram = {
    id: '#instagram-feed-content',
    photoTemplate: '<li><a href="{{link}}" target="_blank"><figure><img src="{{imgSrc}}" alt=""></figure>' +
    '<label>{{created_date}}</label></li>',
    imageResolution: 'standard',
    count: 4
};

instagram.init = function () {
    instagram.getPhotos();
};

instagram.getPhotos = function () {
    $.ajax({
        url: 'https://www.instagram.com/tanagrame/?__a=1',
        data: {
            access_token: instagram.token,
            count: instagram.count
        },
        dataType: 'json',
        type: 'GET',
        success: function (data) {
            var items = data.graphql.user.edge_owner_to_timeline_media.edges;
            var itemLink;
            $(instagram.id).html('');
            items.some(function (nodeItem, index) {
                var item = nodeItem.node;
                itemLink = 'https://www.instagram.com/p/' + item.shortcode;
                var jsTimestamp = new Date(item.taken_at_timestamp * 1000);
                var currentDate = new Date();
                var dateDiff = calcDate(currentDate, jsTimestamp);
                var photoTemp = instagram.photoTemplate
                    .replace('{{link}}', itemLink)
                    .replace('{{created_date}}', dateDiff)
                    .replace('{{comment}}', item.edge_media_to_comment.count)
                    .replace('{{like}}', item.edge_liked_by.count)
                    .replace('{{imgSrc}}', item.thumbnail_resources[1].src);
                $(instagram.id).append(photoTemp);

                return index === (instagram.count - 1);
            });
        },
        error: function () {
        }
    });
};

module.exports = {
    init: function () {
        instagram.init();
    }
};
