/* eslint-disable */

'use strict';

var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var FileReader = require('dw/io/FileReader');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var XMLStreamReader = require('dw/io/XMLStreamReader');

/**
 * Returns variation products from all products in the catalog
 * @param {string} catalogId Catalog ID
 * @returns {Array} Array of variation products
 */
function getProductsByRefinement(catalogId) {
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var productVariations = {};
    var mapMasterProducts = {};
    var neededCatalog = CatalogMgr.getCatalog(catalogId);
    var products = ProductMgr.queryProductsInCatalog(neededCatalog);

    while (products.hasNext()) {
        var product = products.next();
        var tempID = product.custom.temp_id;
        var tempMasterID = product.custom.temp_masterid;
        if (product.isOnline() && tempMasterID) {
            productVariations[product.ID] = tempID;

            if (tempMasterID in mapMasterProducts) {
                mapMasterProducts[tempMasterID].push(product.ID);
            } else {
                mapMasterProducts[tempMasterID] = [product.ID];
            }
        }
    }

    return {
        mapMasterProducts: mapMasterProducts,
        productVariations: productVariations
    };
}

/**
 * Start writing XML file
 * @param {dw.io.XMLStreamWriter} xsw XMLStreamWriter to write the XML file
 */
function startDocument(xsw, catalogID) {
    xsw.writeStartDocument('UTF-8', '1.0');
    xsw.writeRaw(
        '\n<catalog xmlns="http://www.demandware.com/xml/impex/catalog/2006-10-31" catalog-id="' + catalogID + '">'
    );
}

/**
 * End writing XML file
 * @param {dw.io.XMLStreamWriter} xsw XMLStreamWriter to write the XML file
 */
function endDocument(xsw) {
    xsw.writeRaw('\n</catalog>');
    xsw.close();
}

/**
 * 
 * @param {string} productID  product id
 * @param {Object} mapMasterProducts master product (key) with variations (array value)
 */
function createVariantsXML(productID, mapMasterProducts) {
    var variants = '<variants>';
    if (productID in mapMasterProducts) {
        mapMasterProducts[productID].forEach(function (variationID) {
            variants += '<variant product-id="'+ variationID +'"/>';
        });
    }
    return new XML(variants + '</variants>');
}

/**
 * @param {string} filePath path in webdav
 * @returns {Object} XML stream reader
 */
/* eslint-disable */
function createFileReaderXML(filePath, variationsObject, mapMasterProducts, catalogID) {
    var objectFile = new File('IMPEX/src/' + filePath);
    var fileReader;
    var fileXML;
    var productID;
    var gcXML;
    var gcn;

    var file = new File('IMPEX/src/saveCatalogs/new_catalog.xml');
    if (file.exists()) file.remove();
    file.createNewFile();
    var writer = new FileWriter(file);

    if (objectFile.exists()) {
        fileReader = new FileReader(objectFile);
        fileXML = new XMLStreamReader(fileReader);
        var xsw = new XMLStreamWriter(writer);

        startDocument(xsw, catalogID);

        var xmlObject = null;
        while (fileXML.hasNext()) {
            fileXML.next();
            if (fileXML.getEventType() === dw.io.XMLStreamConstants.START_ELEMENT && fileXML.getLocalName() === 'product' && fileXML.getAttributeValue(null, 'product-id') in mapMasterProducts) {
                xmlObject = fileXML.readXMLObject();
                productID = xmlObject.@['product-id'][0];
                gcXML = new XML(xmlObject);
                gcn = new Namespace(gcXML.namespace());
                gcXML.gcn::['variations'].gcn::['variants'] = createVariantsXML(productID, mapMasterProducts);

                xsw.writeRaw(gcXML);
            }
        }
        endDocument(xsw);

        xsw.close();
        writer.close();
    }

    return fileXML;
}

module.exports = {
    getProductsByRefinement: getProductsByRefinement,
    createFileReaderXML: createFileReaderXML
};
