'use strict';

var collections = require('*/cartridge/scripts/util/collections');

/**
 * Returns a sorted collection of currently online subcategories of this catalog category.
 * Checks also the custom.localeOffline value
 *
 * @param {dw.catalog.Category} category - the category to be checked
 * @return {dw.util.ArrayList} - the online sub category collection
 */
function getOnlineSubCategories(category) {
    if (!category) {
        return null;
    }

    var ArrayList = require('dw/util/ArrayList');

    var onlineSubCategories = new ArrayList();
    collections.forEach(category.getOnlineSubCategories(), function (onlineSubCategory) {
        // check also the localeOffline value
        if (onlineSubCategory.custom.localeOffline.value === 'true') {
            return;
        }

        onlineSubCategories.add(onlineSubCategory);
    });

    return onlineSubCategories;
}

/**
 * Returns online products assigned to this category.
 * Checks also the custom.localeOffline value of the products
 *
 * @param {dw.catalog.Category} category - the category to be checked
 * @return {dw.util.ArrayList} - the online product collection
 */
function getOnlineProducts(category) {
    if (!category) {
        return null;
    }

    var ArrayList = require('dw/util/ArrayList');

    var onlineProducts = new ArrayList();
    collections.forEach(category.getOnlineProducts(), function (onlineProduct) {
        // check also the localeOffline value
        if (onlineProduct.custom.localeOffline.value === 'true') {
            return;
        }

        onlineProducts.add(onlineProduct);
    });

    return onlineProducts;
}

/**
 * Returns if the given category has online sub categories
 *
 * @param {dw.catalog.Category} category - the category to be checked
 * @return {boolean} - true if category has online sub categories, false otherwise
 */
function hasOnlineSubCategories(category) {
    return collections.some(category.getOnlineSubCategories(), function (onlineSubCategory) {
        // check also the localeOffline value
        return onlineSubCategory.custom.localeOffline.value !== 'true';
    });
}

/**
 * Returns if the given category has online products
 *
 * @param {dw.catalog.Category} category - the category to be checked
 * @return {boolean} - true if category has online products, false otherwise
 */
function hasOnlineProducts(category) {
    return collections.some(category.getOnlineProducts(), function (onlineProduct) {
        // check also the localeOffline value
        return onlineProduct.custom.localeOffline.value !== 'true';
    });
}

module.exports = {
    getOnlineSubCategories: getOnlineSubCategories,
    hasOnlineSubCategories: hasOnlineSubCategories,
    getOnlineProducts: getOnlineProducts,
    hasOnlineProducts: hasOnlineProducts
};
