'use strict';
var processInclude = require('base/util');
var formValidation = require('base/components/formValidation');
var base = require('base/profile/profile');
var notificationHelper = require('brand_core/helpers/notificationHelper')(
    '.error-messaging'
);

base.submitPassword = function () {
    $('form.change-password-form').submit(function (e) {
        var $form = $(this);
        e.preventDefault();
        var url = $form.attr('action');
        $form.spinner().start();
        $('form.change-password-form').trigger('password:edit', e);
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: $form.serialize(),
            success: function (data) {
                $form.spinner().stop();
                if (!data.success) {
                    formValidation($form, data);
                } else {
                    $('.success-message').removeClass('d-none');
                    var $saveButton = $('.js-change-password-btn');
                    var $backAccount = $('.js-back-myAccount-link');
                    if ($saveButton.length && $backAccount.length) {
                        $saveButton.addClass('d-none');
                        $backAccount.addClass('d-block');
                    }
                }
            },
            error: function (err) {
                if (err && err.responseJSON) {
                    if (err.responseJSON.redirectUrl) {
                        window.location.href = err.responseJSON.redirectUrl;
                    } else {
                        notificationHelper.error(err.responseJSON.errorMessage);
                    }
                } else {
                    notificationHelper.generalError();
                }
                $form.spinner().stop();
            }
        });
        return false;
    });
};

module.exports = base;
processInclude(module.exports);
