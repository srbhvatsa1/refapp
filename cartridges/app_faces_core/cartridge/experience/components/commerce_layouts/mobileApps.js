'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');


/**
 * Render logic for storefront.imageAndText component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.title = content.title || '';
    model.description = content.description || '';
    model.widgetLink1 = content.link3 || '#';
    model.widgetLink2 = content.link4 || '#';
    model.image1 = ImageTransformation.getScaledImage(content.image1);
    model.image2 = ImageTransformation.getScaledImage(content.image2);
    model.image3 = ImageTransformation.getScaledImage(content.image3);
    model.image4 = ImageTransformation.getScaledImage(content.image4);
    model.buttonLink = content.link || '#';
    model.buttonText = content.linkTtext || '';

    return new Template('experience/components/commerce_layouts/mobileApps').render(model).text;
};
