'use strict';

/**
 * Change mini cart data tooltip if cart was updated
 *
 * @param {nunber} quantityTotal a number of products in the cart
 * @return {void}
 */
function updateTooltip(quantityTotal) {
    var $miniCartLink = $('.minicart-link');
    var emptyText = $miniCartLink.data('emptyText');
    if (quantityTotal) {
        $miniCartLink.attr('data-original-title', '');
    } else {
        $miniCartLink.attr('data-toggle', 'tooltip');
        $miniCartLink.attr('data-original-title', emptyText);
    }
    $miniCartLink.attr('title', '');
}

module.exports = {
    initUpdateTooltip: function () {
        $('.minicart').on('count:update', function (event, count) {
            updateTooltip(count.quantityTotal);
        });
    }
};

