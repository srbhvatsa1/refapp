'use strict';
var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var RRHelpers = require('*/cartridge/scripts/helpers/ratingsReviewsHelpers');

server.get('Summary', function (req, res, next) {
    var productId = req.querystring.pid;
    var currentUserProfile = req.currentCustomer.profile;
    var canReview = RRHelpers.canCustomerReviewProduct(currentUserProfile, productId);

    var RatingsReviewsSummaryModel = require('*/cartridge/models/product/ratingsReviewsSummary');
    res.render('/product/components/ratingsReviews/productRatingSummary', {
        ratingsReviews: new RatingsReviewsSummaryModel(productId),
        reviewAlreadySubmitted: canReview.reviewAlreadySubmitted,
        userLoggedIn: canReview.userLoggedIn,
        productBought: canReview.productBought,
        allowWriteReview: canReview.allowWriteReview
    });
    next();
});

server.get('Reviews', cache.applyDefaultCache, function (req, res, next) {
    var pid = req.querystring.pid;
    var start = req.querystring.start || 0;
    var limit = req.querystring.limit || 2;
    var sortingCriteria = req.querystring.sortingCriteria;

    var reviews = RRHelpers.collectReviewsForProduct(pid, start, limit, sortingCriteria, req.locale.id);

    res.render('/product/components/ratingsReviews/reviews', reviews);
    next();
});

server.get('GetForm', function (req, res, next) {
    var form = server.forms.getForm('reviewForm');
    form.pid.value = req.querystring.pid;
    res.render('/product/components/ratingsReviews/reviewForm', {
        reviewForm: form
    });
    next();
});

server.post('Submit', function (req, res, next) {
    var currentUserProfile = req.currentCustomer.profile;
    var form = server.forms.getForm('reviewForm');
    var result = RRHelpers.submitReview(currentUserProfile, req.locale.id, form);

    res.json(result);
    return next();
});

server.post('VoteUp', function (req, res, next) {
    var reviewId = req.form.reviewId;
    var result = RRHelpers.voteReview(reviewId, true);
    res.json(result);
    next();
});

server.post('VoteDown', function (req, res, next) {
    var reviewId = req.form.reviewId;
    var result = RRHelpers.voteReview(reviewId, false);
    res.json(result);
    next();
});

server.get('MyReviews', function (req, res, next) {
    var currentUserProfile = req.currentCustomer.profile;

    if (!currentUserProfile) {
        return next();
    }
    var myReviews = RRHelpers.collectMyReviews(currentUserProfile);
    if (!myReviews) {
        return next();
    }

    res.render('/account/components/ratingsReviews/myReviews', myReviews);
    return next();
});

module.exports = server.exports();
