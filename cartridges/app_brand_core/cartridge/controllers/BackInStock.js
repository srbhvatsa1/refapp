'use strict';
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var server = require('server');
var ProductMgr = require('dw/catalog/ProductMgr');

var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

/**
 * Renders backin stock components as remote include
 * BackInStock-Start
 */
server.get('Start', server.middleware.include, csrfProtection.generateToken, function (req, res, next) {
    var backInStockForm = server.forms.getForm('backinstock');
    backInStockForm.clear();
    var formTemplate = '/backInStock/components/addToBackInStock';
    var pid = req.querystring.pid;
    backInStockForm.pid.value = pid;
    var isEnabled = false;

    if (!pid) {
        res.render(formTemplate, {
            isEnabled: isEnabled,
            backInStockForm: backInStockForm
        });
        return next();
    }
    // add customer e-mail address by default if customer is logged in.
    if (req.currentCustomer && req.currentCustomer.profile && req.currentCustomer.profile.email) {
        backInStockForm.email.value = req.currentCustomer.profile.email;
    }

    var product = ProductMgr.getProduct(pid);
    if (!product) {
        res.render(formTemplate, {
            isEnabled: isEnabled,
            backInStockForm: backInStockForm
        });
        return next();
    }
    var availabilityModel = inventoryHelpers.getProductAvailabilityModel(product);
    backInStockForm.pid.value = product.ID;
    if (availabilityModel && !availabilityModel.inStock) {
        if ('enableBackInStockNotification' in product.custom && product.custom.enableBackInStockNotification) {
            isEnabled = true;
        }

        if (!isEnabled) {
            // if this is enabled at category level
            var categories = product.getAllCategories();
            if (categories) {
                var collections = require('*/cartridge/scripts/util/collections');
                collections.forEach(categories, function (category) {
                    if ('enableBackInStockNotification' in category.custom &&
                    category.custom.enableBackInStockNotification) {
                        isEnabled = true;
                    }
                });
            }
        }
    }

    res.render(formTemplate, {
        isEnabled: isEnabled,
        backInStockForm: backInStockForm
    });
    return next();
});

/**
 * Saves BackInStock Notification
 */
server.post('Save', csrfProtection.validateAjaxRequest, function (req, res, next) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Resource = require('dw/web/Resource');
    var formErrors = require('*/cartridge/scripts/formErrors');
    var backInStockForm = server.forms.getForm('backinstock');
    var errors = formErrors.getFormErrors(backInStockForm);

    if (!backInStockForm.save.submitted || !backInStockForm.valid || Object.keys(errors).length) {
        res.json({
            success: false,
            fieldErrors: [errors],
            serverErrors: []
        });
        return next();
    }

    var email = backInStockForm.email.value.toLowerCase();
    var pid = backInStockForm.pid.value;
    var backInStockObject = CustomObjectMgr.queryCustomObject('BackInStock', 'custom.email = {0} AND custom.productId = {1}', email, pid);

    if (backInStockObject) {
        res.json({
            success: true,
            message: Resource.msg('backinstock.save.success', 'backinstock', null)
        });
        return next();
    }

    try {
        // create custom object.
        var UUIDUtils = require('dw/util/UUIDUtils');
        var Transaction = require('dw/system/Transaction');
        Transaction.wrap(function () {
            backInStockObject = CustomObjectMgr.createCustomObject('BackInStock', UUIDUtils.createUUID());
            backInStockObject.custom.email = email;
            backInStockObject.custom.productId = pid;
            backInStockObject.custom.locale = req.locale.id;
        });
        res.json({
            success: true,
            message: Resource.msg('backinstock.save.success', 'backinstock', null)
        });
    } catch (e) {
        var logger = require('dw/system/Logger').getLogger('BackInStock');
        logger.error('An unpexted error occured : {0}', e.message);
        res.json({
            success: false,
            fieldErrors: [],
            serverErrors: [Resource.msg('backinstock.general.error', 'backinstock', null)]
        });
    }

    return next();
});

module.exports = server.exports();
