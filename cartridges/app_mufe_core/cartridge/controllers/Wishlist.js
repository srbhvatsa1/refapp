'use strict';

/* global customer */
/* global request */
var server = require('server');
server.extend(module.superModule);

var URLUtils = require('dw/web/URLUtils');
var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var BreadcrumbsHelpers = require('*/cartridge/scripts/helpers/breadcrumbsHelpers');
var Resource = require('dw/web/Resource');
var PAGE_SIZE_ITEMS = 15;

/**
 * Converts comma separated product id list to an array
 * @param {string} pid - Comma separated list of product ids
 * @return {string[]} Array of product ids
 */
function prepareProductIds(pid) {
    return (pid || '').split(/[\s,]+/);
}

server.replace('Show', consentTracking.consent, server.middleware.https, csrfProtection.generateToken, function (req, res, next) {
    var list = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
    var WishlistModel = require('*/cartridge/models/productList');
    var userName = '';
    var firstName;
    var rememberMe = false;
    if (req.currentCustomer.credentials) {
        rememberMe = true;
        userName = req.currentCustomer.credentials.username;
    }
    var loggedIn = req.currentCustomer.profile;

    var target = req.querystring.rurl || 1;
    var actionUrl = URLUtils.url('Account-Login');
    var createAccountUrl = URLUtils.url('Account-SubmitRegistration', 'rurl', target).relative().toString();
    var navTabValue = req.querystring.action;
    var breadcrumbs = [
        {
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        }
    ];
    if (loggedIn) {
        firstName = req.currentCustomer.profile.firstName;
        breadcrumbs.push({
            htmlValue: Resource.msg('page.title.myaccount', 'account', null),
            url: URLUtils.url('Account-Show').toString()
        });
    }

    var profileForm = server.forms.getForm('profile');
    profileForm.clear();
    var wishlistModel = new WishlistModel(
        list,
        {
            type: 'wishlist',
            publicView: false,
            pageSize: PAGE_SIZE_ITEMS,
            pageNumber: 1
        }
    ).productList;
    res.render('/wishlist/wishlistLanding', {
        wishlist: wishlistModel,
        navTabValue: navTabValue || 'login',
        rememberMe: rememberMe,
        userName: userName,
        actionUrl: actionUrl,
        profileForm: profileForm,
        breadcrumbs: breadcrumbs,
        oAuthReentryEndpoint: 1,
        loggedIn: loggedIn,
        firstName: firstName,
        socialLinks: loggedIn,
        publicOption: loggedIn,
        createAccountUrl: createAccountUrl
    });
    next();
});

server.append('Show', function (req, res, next) {
    BreadcrumbsHelpers.addLastBreadcrumb(res, Resource.msg('wishlist.hero.image.header', 'wishlist', null));
    var itemIdsStr = [];
    var viewData = res.getViewData();
    var items = viewData.wishlist.items;
    for (var i = 0; i < items.length; i++) {
        itemIdsStr.push(items[i].pid);
    }
    viewData.itemIdsStr = itemIdsStr.join(',');
    res.setViewData(viewData);
    next();
});

server.replace('AddToWishlistButton', function (req, res, next) {
    var isAlreadyAdded = false;
    var isVisible = true;
    var pidStr = req.querystring.pid;
    var pidList = prepareProductIds(req.querystring.pid);
    var isMultiple = pidList.length > 1;
    var templatePath = 'wishlist/components/addToWishListButton';
    var action = URLUtils.url(isMultiple ? 'Wishlist-AddProducts' : 'Wishlist-AddProduct');

    if (pidList.length > 0) {
        // get customer wishlist and check if the product is in list
        // show static text if they added the product before
        var list = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
        var addedItemCount = 0;

        pidList.forEach(function (pid) {
            var config = {
                qty: 1,
                req: req,
                type: 10
            };
            if (productListHelper.itemExists(list, pid, config)) {
                addedItemCount++;
            }
        });

        isAlreadyAdded = addedItemCount === pidList.length;
    } else {
        isVisible = false;
    }

    res.render(templatePath, {
        pid: pidStr,
        action: action,
        isVisible: isVisible,
        isMultiple: isMultiple,
        isAlreadyAdded: isAlreadyAdded
    });

    next();
});

module.exports = server.exports();
