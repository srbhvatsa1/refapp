'use strict';

/**
 * Creates a plain object that contains ratings reviews summary for the product
 * @param {string} pid - Id of the product to get the rating summary from
 * @returns {Object} an object that contains information about the ratings and reviews of the product
 */
function createRatingsReviewsSummaryObject(pid) {
    var ProductMgr = require('dw/catalog/ProductMgr');
    var product = ProductMgr.getProduct(pid);
    var result;
    if (product) {
        result = {
            averageRating: product.custom.averageRating,
            ratingCount: product.custom.ratingCount,
            recommendedPercentage: product.custom.recommendedPercentage,
            ratingsDistribution: JSON.parse(product.custom.ratingDistribution)
        };
    } else {
        result = null;
    }
    return result;
}

/**
 * RatingsReviews class
 * @param {dw.catalog.Product} apiProduct - Product to get review summary of
 * @constructor
 */
function ratingsReviewsSummary(apiProduct) {
    return createRatingsReviewsSummaryObject(apiProduct);
}

module.exports = ratingsReviewsSummary;
