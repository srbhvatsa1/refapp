'use strict';

var gtmEnhancedEcommerce = require('*/cartridge/models/product/decorators/gtmEnhancedEcommerce');
var gtmEnhancedEcommerceDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmEnhancedEcommerceDataLayerHelpers');

module.exports = function (object, product) { // eslint-disable-line no-unused-vars
    var CurrentSite = require('dw/system/Site').current;
    if (!CurrentSite.getCustomPreferenceValue('GoogleTagManagerId')) {
        return;
    }

    gtmEnhancedEcommerce(object);

    Object.defineProperty(object.gtmEnhancedEcommerce, 'productClick', {
        enumerable: true,
        value: {
            event: 'productClick',
            ecommerce: {
                click: {
                    products: [gtmEnhancedEcommerceDataLayerHelpers.getProductObject(object, 1)]
                }
            }
        }
    });
};
