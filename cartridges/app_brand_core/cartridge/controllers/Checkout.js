'use strict';
/* global customer */

var server = require('server');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

server.extend(module.superModule);

/**
 * If the guest checkout is disabled for the current site and the customer is not logged in we redirect to login page
 */
server.prepend('Begin',
    server.middleware.https,
    function (req, res, next) {
        var URLUtils = require('dw/web/URLUtils');
        var Site = require('dw/system/Site');

        if (Site.getCurrent().getCustomPreferenceValue('disableCheckout')) {
            res.redirect(URLUtils.url('Home-Show'));
            return next();
        }

        if (Site.getCurrent().getCustomPreferenceValue('disableGuestCheckout') && !req.currentCustomer.raw.authenticated) {
            res.redirect(URLUtils.url('Checkout-Login'));
        }

        return next();
    }
);

server.append(
    'Begin', function (req, res, next) {
        var Resource = require('dw/web/Resource');
        var BasketMgr = require('dw/order/BasketMgr');
        var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');

        var currentBasket = BasketMgr.getCurrentBasket();

        var viewData = res.getViewData();
        var smsVerificationForm = server.forms.getForm('smsverification');
        viewData.forms.smsVerification = smsVerificationForm;

        if (req.querystring.psperror) {
            viewData.pspErrorMessage = Resource.msg('error.technical', 'checkout', null);
        }

        viewData.countryCode = localeHelpers.getCurrentCountryCode();
        viewData.isMadaPaymentCardApplicable =
            COHelpers.isMadaPaymentCardApplicable(currentBasket, req.currentCustomer.raw, viewData.countryCode, null);

        res.setViewData(viewData);
        next();
    }
);

server.append(
    'Login', function (req, res, next) {
        var Site = require('dw/system/Site');
        var Locale = require('dw/util/Locale');
        var BasketMgr = require('dw/order/BasketMgr');
        var OrderModel = require('*/cartridge/models/order');

        var viewData = res.getViewData();
        var currentCustomer = req.currentCustomer.raw;
        var currentBasket = BasketMgr.getCurrentBasket();
        var currentLocale = Locale.getLocale(req.locale.id);
        var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');

        var orderModel = new OrderModel(
            currentBasket,
            {
                customer: currentCustomer,
                usingMultiShipping: usingMultiShipping,
                shippable: true,
                countryCode: currentLocale.country,
                containerView: 'basket'
            }
        );
        if (orderModel) {
            viewData.order = orderModel;
        }

        var oAuthFacebookProviderID = Site.getCurrent().getCustomPreferenceValue('oAuthFacebookProviderID');
        if (oAuthFacebookProviderID) {
            viewData.oAuthFacebookProviderID = oAuthFacebookProviderID;
        }

        res.setViewData(viewData);
        next();
    }
);

/**
 * Assigns Email to Customer Basket & Shipping address.
 */
server.post('AssignEmail', csrfProtection.validateAjaxRequest, server.middleware.https, function (req, res, next) {
    var formErrors = require('*/cartridge/scripts/formErrors');
    var URLUtils = require('dw/web/URLUtils');
    var guestEmailForm = server.forms.getForm('guestemail');
    var errors = formErrors.getFormErrors(guestEmailForm);
    var BasketMgr = require('dw/order/BasketMgr');
    var currentBasket = BasketMgr.getCurrentBasket();

    if (!guestEmailForm.save.submitted || !guestEmailForm.valid || Object.keys(errors).length) {
        res.json({
            success: false,
            fieldErrors: [errors]
        });
        return next();
    }

    var email = guestEmailForm.email.value;
    COHelpers.setBasketCustomerEmail(email, currentBasket);
    res.json({
        success: true,
        redirectUrl: URLUtils.url('Checkout-Begin').toString()
    });

    return next();
});

module.exports = server.exports();
