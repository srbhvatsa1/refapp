'use strict';
var Transaction = require('dw/system/Transaction');
var BasketMgr = require('dw/order/BasketMgr');
var Site = require('dw/system/Site');
var Locale = require('dw/util/Locale');
/* global customer, request, session */

/**
 * Checks site configuration and global request to indicate necessity of sms verification
 * @returns  {boolean} returns true or false
 */
function isVerificationRequired() {
    var smsRequiredLocs = Site.getCurrent().getCustomPreferenceValue('enableSmsVerification');
    if (smsRequiredLocs && request && request.locale) {
        var locale = Locale.getLocale(request.locale);
        var allowedLocs = Array.prototype.slice.call(smsRequiredLocs); // convert set of string value to array

        if (allowedLocs.indexOf(locale.ID) > -1 || allowedLocs.indexOf(locale.country) > -1) {
            // agent is using the system on behalf of the customer, so no need for sms verification
            if (request.session && request.session.isUserAuthenticated()) {
                return false;
            }

            return true;
        }
    }
    return false;
}

/**
 * Checks value existence and clean input
 * @param {string} value value to be trimmed and case lowered
 * @returns {string} safe value or empty string
 */
function safeValue(value) {
    var StringUtils = require('dw/util/StringUtils');
    return value && value.length ? StringUtils.trim(value).toLowerCase() : '';
}

/**
 * Compares addresses and phone number for sms verification status
 * @param {dw.order.OrderAddress} address1 address1
 * @param {dw.customer.CustomerAddress} address2 address2
 * @returns {boolean} status of address
 */
function compareAddresses(address1, address2) {
    var status = true;
    if (safeValue(address1.phone) !== safeValue(address2.phone)) {
        // if one of the fields does not match with one of customer address, sms verification should be performed again.
        status = false;
    }

    return status;
}

/**
 * Reads customer & basket custom attributes to assign sms verification status as true or false to display relevant forms for Cash on Delivery
 * @returns {boolean} status of verification
 */
function getSmsVerificationStatus() {
    var isVerified = false;
    // early quit when verification is required.
    if (!isVerificationRequired()) {
        isVerified = true;
        return true;
    }
    var currentBasket = BasketMgr.getCurrentBasket();

    if (currentBasket && currentBasket.getDefaultShipment() && currentBasket.getDefaultShipment().shippingAddress) {
        var address = currentBasket.getDefaultShipment().shippingAddress;

        if (address.custom.isSmsVerified) {
            isVerified = true;
        } else if (customer.authenticated &&
            customer.profile &&
            customer.profile.addressBook &&
            customer.profile.addressBook.addresses &&
            !customer.profile.addressBook.addresses.isEmpty()) {
            var addresses = customer.profile.addressBook.addresses;
            for (var i = 0; i < addresses.length; i++) {
                var customerAddress = addresses[i];
                var isSameAddress = compareAddresses(customerAddress, address);
                // same address and customer verified before.
                if (isSameAddress && 'isSmsVerified' in customerAddress.custom && customerAddress.custom.isSmsVerified) {
                    try {
                        Transaction.wrap(function () {
                            address.custom.isSmsVerified = true;
                        });
                    } catch (e) {
                        // do nothing.
                    }

                    isVerified = true;
                }
            }
        }
    }

    return isVerified;
}

/**
 * Updates customer addresses verification status if customer is authenticated & have matching addresses in address book
 */
function updateCustomerSmsVerificationStatus() {
    var currentBasket = BasketMgr.getCurrentBasket();
    if (currentBasket && currentBasket.getDefaultShipment() && currentBasket.getDefaultShipment().shippingAddress) {
        if (customer.authenticated &&
            customer.profile &&
            customer.profile.addressBook &&
            customer.profile.addressBook.addresses &&
            !customer.profile.addressBook.addresses.isEmpty()) {
            var shippingAddress = currentBasket.getDefaultShipment().shippingAddress;
            var addresses = customer.profile.addressBook.addresses;
            for (var i = 0; i < addresses.length; i++) {
                var address = addresses[i];
                var isSameAddress = compareAddresses(shippingAddress, address);
                if (isSameAddress && !address.custom.isSmsVerified) {
                    Transaction.begin();
                    address.custom.isSmsVerified = true;
                    Transaction.commit();
                }
            }
        }
    }
}
/**
 * Updates Basket SMS verification status
 * @param {boolean} status status to update
 */
function updateBasketSmsVerificationStatus(status) {
    var currentBasket = BasketMgr.getCurrentBasket();
    if (currentBasket && currentBasket.getDefaultShipment() && currentBasket.getDefaultShipment().shippingAddress) {
        var address = currentBasket.getDefaultShipment().shippingAddress;
        Transaction.wrap(function () {
            address.custom.isSmsVerified = status;
        });
    }
}

/**
 * Updates both Basket & Customer SMS verification statuses
 * @param {boolean} status status to update
 */
function updateSmsVerificationStatus(status) {
    updateBasketSmsVerificationStatus(status);
    updateCustomerSmsVerificationStatus();
}
/**
 * Generates OTP based on site configuration
 * @param {dw.system.Site} site current Site
 * @returns {string} otp
 */
function generateOTP(site) {
    var otp = '';
    var smsVerificationPattern = site.getCustomPreferenceValue('smsVerificationPattern');
    var smsVerificationPinLength = site.getCustomPreferenceValue('smsVerificationPinLength');
    switch (smsVerificationPattern.value) {
        case 'alphanumeric':
            var UUIDUtils = require('dw/util/UUIDUtils');
            otp = UUIDUtils.createUUID().substr(0, smsVerificationPinLength);
            break;
        default:
            var digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
            for (var i = 0; i < smsVerificationPinLength; i++) {
                otp += digits[Math.floor(Math.random() * digits.length)];
            }
            break;
    }

    return otp;
}

/**
 * Requests OTP Message to Customer through service
 * @param {string} phoneNumber phoneNumber
 * @returns {Object} status of request with message
 */
function requestOTP(phoneNumber) {
    var Resource = require('dw/web/Resource');
    var StringUtils = require('dw/util/StringUtils');
    var otpService = require('*/cartridge/scripts/services/otpService');
    var logger = require('dw/system/Logger').getLogger('otp');
    var currentSite = require('dw/system/Site').current;
    var currentBasket = require('dw/order/BasketMgr').getCurrentBasket();
    var otpStorage = session.custom.otp || (currentBasket ? currentBasket.custom.otp : {});
    var currentToken = !!otpStorage ? JSON.parse(otpStorage) : {}; // eslint-disable-line
    var currentTimeStamp = new Date().getTime();
    if ('lastRequestTime' in currentToken && currentToken.lastRequestTime) {
        var timeDiff = Math.abs(currentTimeStamp - currentToken.lastRequestTime) / 1000;
        if (timeDiff < currentSite.getCustomPreferenceValue('smsVerificationRequestLimit')) {
            return {
                success: false,
                message: Resource.msgf('error.smsverification.request.limit', 'forms', null, currentSite.getCustomPreferenceValue('smsVerificationRequestLimit'))
            };
        }
    }

    var contentAssetId = currentSite.getCustomPreferenceValue('smsVerificationAssetId');

    if (!contentAssetId) {
        logger.error('Content Asset for OTP message is NOT defined, please define asset id for OTP message from Site Preferences > Custom > OTP Settings');
        return {
            success: false,
            message: Resource.msg('error.smsverification.technical', 'forms', null)
        };
    }

    var ContentMgr = require('dw/content/ContentMgr');
    var otpContent = ContentMgr.getContent(contentAssetId);
    if (!otpContent) {
        logger.error('Content Asset {0} for OTP message is NOT defined, please define asset', contentAssetId);
        return {
            success: false,
            message: Resource.msg('error.smsverification.technical', 'forms', null)
        };
    }

    var countryJson = currentSite.getCustomPreferenceValue('smsVerificationFrom') ? JSON.parse(currentSite.getCustomPreferenceValue('smsVerificationFrom')) : {};

    var locale = Locale.getLocale(request.locale);
    var src = '1234567890'; // default phone number

    if (locale.country in countryJson && countryJson[locale.country]) {
        src = countryJson[locale.country];
    }
    var otp = generateOTP(currentSite);
    var otpMessage = StringUtils.format(otpContent.custom.body, otp);
    var requestPayLoad = {
        src: src,
        dst: phoneNumber,
        text: otpMessage
    };

    var serviceResult = otpService.call(requestPayLoad);

    if (serviceResult.isOk()) {
        otpStorage = JSON.stringify({ lastRequestTime: currentTimeStamp, otp: otp });
        session.custom.otp = otpStorage;
        if (currentBasket) {
            Transaction.wrap(function () {
                currentBasket.custom.otp = JSON.stringify({ lastRequestTime: currentTimeStamp, otp: otp });
            });
        }
        return {
            success: true
        };
    }
    logger.error(serviceResult.errorMessage);
    var errorMessage = !!serviceResult.errorMessage ? JSON.parse(serviceResult.errorMessage) : serviceResult.errorMessage; // eslint-disable-line
    return {
        success: false,
        message: 'error' in errorMessage ? errorMessage.error : errorMessage
    };
}
/**
 * Verifies pinCode
 * @param {string} pinCode PinCode to be verified against session otp value
 * @returns {Object} success status with message for errors
 */
function verifyOTP(pinCode) {
    var currentBasket = require('dw/order/BasketMgr').getCurrentBasket();
    var otpStorage = session.custom.otp || (currentBasket ? currentBasket.custom.otp : {});
    var storedOTP = !!otpStorage ? JSON.parse(otpStorage) : {}; // eslint-disable-line

    if ('otp' in storedOTP && storedOTP.otp && storedOTP.otp.equalsIgnoreCase(pinCode)) {
        return {
            success: true
        };
    }
    var Resource = require('dw/web/Resource');
    return {
        success: false,
        message: Resource.msg('error.invalid.smsverification.code', 'forms', null)
    };
}

module.exports = {
    isVerificationRequired: isVerificationRequired,
    getSmsVerificationStatus: getSmsVerificationStatus,
    updateSmsVerificationStatus: updateSmsVerificationStatus,
    updateBasketSmsVerificationStatus: updateBasketSmsVerificationStatus,
    updateCustomerSmsVerificationStatus: updateCustomerSmsVerificationStatus,
    compareAddresses: compareAddresses,
    requestOTP: requestOTP,
    verifyOTP: verifyOTP
};
