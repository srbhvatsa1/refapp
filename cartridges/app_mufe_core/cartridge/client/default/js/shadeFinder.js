/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
'use strict';
var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./searchShade'));
});

var debounce = require('lodash/debounce');
var endpoint = $('.shadeFinderStart').data('url');
var minChars = 3;

$('#btn-shadeFinderStart').trigger('click');

$('#btn-shadeFinderStart').click(function () {
    $('html, body').animate({ scrollTop: 120 }, 1000);
    var e = document.getElementById('threshold');
    var jsonShadeValue = $('#startShadeFinder').html();
    jsonShadeValue = JSON.parse(jsonShadeValue);
    $('#btn-shadeFinderStart').addClass('d-none');
    var windowSize = $(window).width();
    if (windowSize < 500) {
        YMK.init({
            shadeFinderVersion: 'v4',
            disable2ColorComparison: true,
            hideAllButtonsOnResult: true,
            shadeFinderDeltaEThreshold: e.value,
            skinSmoothStrength: 1
        });
    } else {
        YMK.init({
            shadeFinderVersion: 'v4',
            disable2ColorComparison: true,
            hideAllButtonsOnResult: true,
            shadeFinderDeltaEThreshold: e.value,
            skinSmoothStrength: 1,
            width: 500,
            height: 619
        });
    }

    YMK.open(true, null, true);

    YMK.addEventListener('shadeFinderComplete', function (r) {
        $('#output').text('').text('shade-found:' + JSON.stringify(r));

        var shadeValue;
        var idvalue = r.recommendations;
        var i;
        var shadeId = [];
        var colorvalue = [];
        var shadevalue = [];
        var ShadeSoftwareID = [];
        for (i = 0; i < idvalue.length; i++) {
            if (i < 3) {
                shadeId.push(jsonShadeValue[idvalue[i].guid].shadeId);
                colorvalue.push(jsonShadeValue[idvalue[i].guid].colorCode);
                shadevalue.push(jsonShadeValue[idvalue[i].guid].shadeName);
                ShadeSoftwareID.push(idvalue[i].guid);
            }
        }

        YMK.enableCompare();

        // YMK.applyMakeupBySku(itemGuid: string[,callbackOrPattern: function or string[, callback: function]])
        $('.threshold-shadenumber').append('Shade ID: ' + colorvalue[0] + ', ID Produit ' + shadeId[0] + ', Best match: ' + shadevalue[0] + '<br>');
        $('.shade-value1').append(shadevalue[0]);
        $('.shade-value2').append(shadevalue[1]);
        $('.shade-value3').append(shadevalue[2]);
        $('#IDshadenumber').html(shadeId);
        var indexText = $('.skintone-subtitle-index').data('text');
        var pfcolorText = $('.skintone-subtitle-pfcolor').data('text');
        $('.skintone-subtitle-index').empty().html(indexText + colorvalue[1]);
        $('.skintone-subtitle-pfcolor').empty().html(pfcolorText + shadevalue[1]);
        $('#threshold').val(shadeId[0]);
        if (!shadeId[2]) {
            $('.skintone-click3').hide();
        } else {
            $('.skintone-click3').show();
        }
        $('.skintone-wrapper').show();
        $('.matching-wrapper').show();
        $('.square1 circle').attr('fill', colorvalue[1]);
        $('.square2 circle').attr('fill', colorvalue[0]);
        $('.square3 circle').attr('fill', colorvalue[2]);
        $('.skintone-click2').attr('data-value', shadeId[0]);
        $('.skintone-click1').attr('data-value', shadeId[1]);
        $('.skintone-click3').attr('data-value', shadeId[2]);
        $('.custom-shade-search').addClass('d-block');
        $('.skintone-click2').trigger('click', [shadeId, colorvalue]);
    });
});
