'use strict';

var generalUtils = require('~/cartridge/scripts/utils/generalUtils');
var config = generalUtils.config;

/**
 * @desc Generates xml of media files
 * @param {Object} args - job parameters
 * @returns {void}
 */
function getMediaFiles(args) {
    if (config.imageType === 'images' || config.imageType === 'both') {
        var akeneoMediaUrl = args.AkeneoMediaUrl;

        if (akeneoMediaUrl) {
            var akeneoMediaFiles = require('~/cartridge/scripts/akeneoMediaFiles/akeneoMediaFiles');
            akeneoMediaFiles.generateMediaFiles(akeneoMediaUrl);
        } else {
            throw new Error('ERROR : missing akeneoMediaUrl');
        }
    }
}

/**
 * @desc Generates xml of asset files
 * @param {Object} args - job parameters
 * @returns {void}
 */
function getAssetsFiles(args) {
    if (config.imageType === 'assets' || config.imageType === 'both') {
        var akeneoAssetFamiliesUrl = args.AkeneoAssetFamiliesUrl;
        var akeneoAssetUrl = args.AkeneoAssets;
        var isDeltaUpdate = args.DeltaUpdate && args.DeltaUpdate === 'true';
        if (akeneoAssetUrl) {
            var AkeneoServicesHandler = require('~/cartridge/scripts/utils/akeneoServicesHandler');
            var akeneoAssets = require('~/cartridge/scripts/akeneoMediaFiles/akeneoAssets');
            var getAssetFamilies = AkeneoServicesHandler.serviceRequestAssetsAkeneo(config.serviceGeneralUrl, akeneoAssetFamiliesUrl);
            if (getAssetFamilies.length > 0) {
                for (var i = 0; i < getAssetFamilies.length; i++) {
                    akeneoAssets.generateAssetFiles(akeneoAssetFamiliesUrl, akeneoAssetUrl, getAssetFamilies[i], isDeltaUpdate);
                }
            }
        } else {
            throw new Error('ERROR : missing akeneoAssetUrl');
        }
    }
}

/**
 * @desc Generates pricebook xml file of akeneo products
 */
function priceBookXml() {
    try {
        var importedTime = require('~/cartridge/scripts/jobs/importedTime');
        importedTime.clearImportedTime();
        var akeneoPriceBook = require('~/cartridge/scripts/akeneoPriceBook/akeneoPriceBook');
        akeneoPriceBook.generatePricebookXml();
    } catch (e) {
        throw new Error('Error occured due to ' + e.stack + ' with Error: ' + e.message);
    }
}

/**
 * @desc Generates attributes xml file
 * @returns {void}
 */
function getAttributes() {
    try {
        var akeneoAttributes = require('~/cartridge/scripts/akeneoAttributes/akeneoAttributes');
        akeneoAttributes.generateAttributesXml();
    } catch (e) {
        throw new Error('Error occured due to ' + e.stack + ' with Error: ' + e.message);
    }
}


/**
 * @desc Prepares file for import
 * @param {Object} args - job parameters
 * @returns {void}
 */
function prepareImport(args) {
    var archiveFileName = 'import-meta-data-akeneo';
    if (args.AkeneoFluxPath) {
        var akeneoPrepareImport = require('~/cartridge/scripts/utils/akeneoPrepareImport');
        akeneoPrepareImport.prepareFileForImport(args.AkeneoFluxPath, archiveFileName);
    }
}

/**
 * @desc deletes family variants cache files from webdav location
 * @returns {void}
 */
function clearFamilyVariantsCache() {
    var customCacheWebdav = require('~/cartridge/scripts/io/customCacheWebdav');
    customCacheWebdav.clearCache(config.cacheDirectory.familyVariants.baseLocation);
    customCacheWebdav.clearCache(config.cacheDirectory.attributes.baseLocation);
}

/**
 * @desc clears the impex catalog location by archiving files
 * @returns {void}
 */
function clearImpexLocation() {
    generalUtils.adjustLocale();
    generalUtils.clearDirectoryAkeneo();
}

/**
 * @desc deletes model product cache files from webdav location
 * @returns {void}
 */
function clearModelProductsCache() {
    var customCacheWebdav = require('~/cartridge/scripts/io/customCacheWebdav');
    customCacheWebdav.clearCache(config.cacheDirectory.modelProducts.baseLocation);
}

/* Exported functions*/
module.exports = {
    getMediaFiles: getMediaFiles,
    getAssetsFiles: getAssetsFiles,
    getAttributes: getAttributes,
    priceBookXml: priceBookXml,
    prepareImport: prepareImport,
    clearFamilyVariantsCache: clearFamilyVariantsCache,
    clearImpexLocation: clearImpexLocation,
    clearModelProductsCache: clearModelProductsCache
};
