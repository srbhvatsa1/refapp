
<isinclude template="/components/modules" sf-toolkit="off" />
<isdecorate template="common/layout/page">
    <isscript>
        var assets = require('*/cartridge/scripts/assets.js');
        assets.addJs('/js/addressBook.js');
        assets.addCss('/css/account/addressBook.css');
    </isscript>
    <isconfirm confirm_action="delete" confirm_type="address" />
    <div class="hero slant-down account-image">
        <h1 class="page-title">${Resource.msg('label.addressbook','account',null)}</h1>
    </div>
    <div class="container">
        <isinclude template="components/breadcrumbs/pageBreadcrumbs"/>
        <isif condition="${pdict.addressBook.length === 0}">
             <div class="row justify-content-center">
                 <div class="col">
                     <h3>${Resource.msg('msg.no.saved.addresses','address',null)}</h3>
                 </div>
            </div>
        </isif>
        <iscomment>Rows for addresses in the Address Book</iscomment>
        <isset name="addressCount" value="${0}" scope="page"/>
        <isloop items="${pdict.addressBook}" var="address">
            <isif condition="${pdict.countryCode === address.address.countryCode.value}">
                <isset name="addressCount" value="${addressCount + 1}" scope="page"/>
                <isset name="isPreferredAddress" value="${address.address.ID === pdict.addressBook[0].address.ID || addressCount === 1}" scope="page"/>
                <div class="row justify-content-center js-address-item" id="uuid-${address.address.UUID}">
                    <div class="col-sm-8 col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h2 class="pull-left">
                                    <span>${address.address.ID}</span>
                                    <span class="js-address-default-text ${isPreferredAddress === false ? 'd-none' : ''}">
                                        ${Resource.msg('label.addressbook.defaultaddress','account',null)}
                                    </span>
                                </h2>
                                <a href="${URLUtils.url('Address-EditAddress', 'addressId', address.address.ID)}"
                                    class="pull-right"
                                    aria-label="${Resource.msg('label.addressbook.editaddress','account',null)} : ${address.address.ID == pdict.addressBook[0].address.ID ? pdict.addressBook[0].address.ID+' ('+Resource.msg('label.addressbook.defaultaddress','account',null)+')' : address.address.ID}">
                                    ${Resource.msg('link.edit', 'account', null)}
                                </a>
                            </div>
                            <div class="card-body card-body-positioning">
                                <div>${address.address.firstName} ${address.address.lastName}</div>
                                <div dir="ltr">${address.address.address1}</div>
                                <isif condition="${address.address.address2 !== null}">
                                    <div dir="ltr">${address.address.address2}</div>
                                </isif>
                                <div>${address.address.postalCode} ${address.address.city}</div>
                                <div dir="ltr">${address.address.phone}</div>
                                <isif condition="${isPreferredAddress === false}">
                                    <div class="js-address-set-default-link">
                                        <a href="${URLUtils.url('Address-SetDefault', 'addressId', address.address.ID)}"
                                            class="normal"
                                            aria-label="${Resource.msg('label.addressbook.makedefaultaddress','account',null)}">
                                            ${Resource.msg('link.addressbook.makedefault','account',null)}
                                        </a>
                                    </div>
                                </isif>
                                <button
                                    type="button"
                                    class="btn-light js-button-delete-address"
                                    data-toggle="modal"
                                    data-target="#deleteAddressModal"
                                    data-id="${address.address.ID}"
                                    data-default="${isPreferredAddress}"
                                    data-address="${address.address.address1}"
                                    data-url="${pdict.actionUrls.deleteActionUrl}"
                                    aria-label="${Resource.msg('label.addressbook.deleteaddress','account',null)}">
                                    &times;
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </isif>
        </isloop>
        <div class="row justify-content-center">
            <div class="col-6">
                <div class="row">
                    <div class="col text-center">
                        <a href="${URLUtils.url('Account-Show')}"
                            class="normal"
                            aria-label="${Resource.msg('label.addressbook.myaccount','account',null)}">
                            ${Resource.msg('link.profile.backtomyaccount','account',null)}
                        </a>
                    </div>
                    <div class="col">
                        <a href="${URLUtils.url('Address-AddAddress')}"
                            class="btn btn-save text-large-wrap btn-block btn-primary"
                            aria-label="${Resource.msg('label.addressbook.addnewaddress','account',null)}">
                            ${Resource.msg('button.addnew','account',null)}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</isdecorate>
