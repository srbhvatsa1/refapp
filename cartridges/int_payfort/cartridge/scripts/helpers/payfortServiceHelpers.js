'use strict';
/* global request */
/**
 * Class for PayFort HTTP Service Initialization
 * HTTP Services
 * Service Name: payfort.service
 * Profile Nane: payfort.service.profile
 * Cerendials Name: payfort.service.credentials.{CountryCode}
 */

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var StringUtils = require('dw/util/StringUtils');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var payfortServiceConstants = require('*/cartridge/scripts/util/payfortServiceConstants');
var payfortConstants = require('*/cartridge/scripts/util/payfortConstants');

/**
 * Gets the order locale,
 * if order is not provided then current request's locale object
 * @param {dw.order.Order} order - Current order
 * @returns {dw.util.Locale} the locale object
 */
function getOrderLocale(order) {
    var Locale = require('dw/util/Locale');

    var locale = order ? order.getCustomerLocaleID() : request.locale;
    return Locale.getLocale(locale);
}

/**
 * Generates the payFort service credential id
 * based on SitePref.payfortServiceCredentialType
 * Single -> just return SitePref.payfortServiceCredentialId
 * Country -> concat SitePref.payfortServiceCredentialId with the current request or order country
 * Locale -> concat SitePref.payfortServiceCredentialId with the current request or order locale or targetLocale
 * @param {dw.order.Order} order - Current order
 * @param {string} targetLocale - the target locale
 * @returns {string} the service credential id of the payfort service
 */
function getServiceCredentialId(order, targetLocale) {
    // if serviceCredentialType is single
    // then just return sitePref.serviceCredentialId
    if (payfortServiceConstants.serviceCredentialType === payfortConstants.serviceCredentialType.single) {
        return payfortServiceConstants.serviceCredentialId;
    }
    var Locale = require('dw/util/Locale');

    var locale = order ? order.getCustomerLocaleID() : (targetLocale || request.locale);
    switch (payfortServiceConstants.serviceCredentialType) {
        case payfortConstants.serviceCredentialType.country:
            // if serviceCredentialType is country
            // then return sitePref.serviceCredentialId + country
            // payfort.service.credentials + '.' + 'ae' -> payfort.service.credentials.ae
            return StringUtils.format('{0}.{1}', payfortServiceConstants.serviceCredentialId, Locale.getLocale(locale).getCountry().toLowerCase());
        case payfortConstants.serviceCredentialType.locale:
            // if serviceCredentialType is locale
            // then return sitePref.serviceCredentialId + locale
            // payfort.service.credentials + '.' + 'en_AE' -> payfort.service.credentials.en_ae
            return StringUtils.format('{0}.{1}', payfortServiceConstants.serviceCredentialId, locale.toLowerCase());
        default:
            // not supoosed to be here, throw an error
            throw new Error(StringUtils.format('SitePref.payfortServiceCredentialType can not recognized, value: {0}', payfortServiceConstants.serviceCredentialType));
    }
}

/**
 * Generates the payFort apple pay service credential id
 * based on SitePref.applePayServiceCredentialType
 * Single -> just return SitePref.applePayServiceCredentialId
 * Country -> concat SitePref.applePayServiceCredentialId with the current request or order country
 * Locale -> concat SitePref.applePayServiceCredentialId with the current request or order locale or targetLocale
 * @param {dw.order.Order} order - Current order
 * @param {string} targetLocale - the target locale
 * @returns {string} the apple pay service credential id of the payfort service
 */
function getApplePayServiceCredentialId(order, targetLocale) {
    // if applepay serviceCredentialType is single
    // then just return sitePref.applePayServiceCredentialId
    if (payfortServiceConstants.applePayServiceCredentialType === payfortConstants.serviceCredentialType.single) {
        return payfortServiceConstants.applePayServiceCredentialId;
    }
    var Locale = require('dw/util/Locale');

    var locale = order ? order.getCustomerLocaleID() : (targetLocale || request.locale);
    switch (payfortServiceConstants.applePayServiceCredentialType) {
        case payfortConstants.serviceCredentialType.country:
            // if applepay serviceCredentialType is country
            // then return sitePref.applePayServiceCredentialId + country
            // payfort.service.credentials + '.' + 'ae' -> payfort.service.credentials.ae
            return StringUtils.format('{0}.{1}', payfortServiceConstants.applePayServiceCredentialId, Locale.getLocale(locale).getCountry().toLowerCase());
        case payfortConstants.serviceCredentialType.locale:
            // if applepay serviceCredentialType is locale
            // then return sitePref.applePayServiceCredentialId + locale
            // payfort.service.credentials + '.' + 'en_AE' -> payfort.service.credentials.en_ae
            return StringUtils.format('{0}.{1}', payfortServiceConstants.applePayServiceCredentialId, locale.toLowerCase());
        default:
            // not supoosed to be here, throw an error
            throw new Error(StringUtils.format('SitePref.applePayServiceCredentialType can not recognized, value: {0}', payfortServiceConstants.applePayServiceCredentialType));
    }
}

/**
 * Gets the payFort service url
 * based on action parameter
 * @param {string} action - Current action for the request
 * @returns {string} the service credential id of the payfort service
 */
function getServiceURL(action) {
    return (action === 'TOKENIZATION')
        ? payfortServiceConstants.tokenizationURL
        : payfortServiceConstants.apiURL;
}

/**
 * Generated the signature of the request
 * @param {string} channelType - The current Channel Type of the integration
 * @param {Object} payload - The Request object to be extended
 * @param {string} phrase - The Request or Response phrase to be used for generating signature
 * @param {string} shaType - The SHA algorithm
 * @param {boolean} isResponse - The indicator if the payload id from payfort
 * @returns {string} the generated signature
 */
function getSignature(channelType, payload, phrase, shaType, isResponse) {
    var signature = '';
    var excludedKeys = isResponse
        ? payfortConstants.signatureExcludedKeys.default
        : payfortConstants.signatureExcludedKeys[channelType] || payfortConstants.signatureExcludedKeys.default;

    Object.keys(payload).sort().forEach(function (key) {
        if (excludedKeys.indexOf(key) === -1) {
            if (typeof payload[key] === 'object') {
                signature = StringUtils.format('{0}{1}{2}', signature, key, '={');
                var objectKeyLength = Object.keys(payload[key]).length;
                Object.keys(payload[key]).forEach(function (objectKey, index) {
                    signature = StringUtils.format('{0}{1}={2}{3}', signature, objectKey, payload[key][objectKey], (objectKeyLength > (index + 1) ? ', ' : ''));
                });
                signature = StringUtils.format('{0}{1}', signature, '}');
            } else {
                signature = StringUtils.format('{0}{1}={2}', signature, key, payload[key]);
            }
        }
    });
    signature = StringUtils.format('{0}{1}{0}', phrase, signature);

    var MessageDigest = require('dw/crypto/MessageDigest');
    var messageDigest;
    switch (shaType) {
        case payfortConstants.shaType.sha512:
            messageDigest = new MessageDigest(MessageDigest.DIGEST_SHA_512);
            break;
        case payfortConstants.shaType.sha256:
        default:
            messageDigest = new MessageDigest(MessageDigest.DIGEST_SHA_256);
            break;
    }

    return messageDigest.digest(signature);
}

/**
 * Calculates the amount value that is send to payfort
 * Before sending the amount value of any transaction,
 * it is needed to multiply the value with the currency decimal code according to ISO code 3.
 * For example: If the amount value was 500 AED; according to ISO code 3,
 * the value should be multiplied with 100 (2 decimal points); so it will be sent in the request as 50000.
 * Another example: If the amount value was 100 JOD; according to ISO code 3,
 * the value should be multiplied with 1000 (3 decimal points); so it will be sent in the request as 100000.
 * @param {dw.value.Monet} amount - the amount of the transaction
 * @returns {string} returns the new amount as payfort requested
 */
function calculatePayFortAmount(amount) {
    var Currency = require('dw/util/Currency');

    var currency = Currency.getCurrency(amount.currencyCode);
    return Math.round(amount.multiply(Math.pow(10, currency.defaultFractionDigits)).value).toFixed();
}

/**
 * Maps the SFCC Credit Card Type with Payfort Credit Card Type (payment_option)
 * @param {string} sfccCreditCardType - SFCC default credit card type
 * @return {string} returns mapped credit card type
 */
function getCreditCardType(sfccCreditCardType) { // eslint-disable-line no-unused-vars
    return payfortConstants.creditCardTypes[(sfccCreditCardType || '').toLowerCase()]
        || (sfccCreditCardType || '').toUpperCase();
}

/**
 * Prepares the service
 * Sets the credential of the service
 * Sets the url of the service
 * @param {dw.svc.Service} svc - The payfort service
 * @param {dw.order.Order} order - The current order
 * @param {string} action - The action -> TOKENIZATION, AUTHORIZATION, PURCHASE, ...
 */
function prepareService(svc, order, action) {
    svc.setCredentialID(getServiceCredentialId(order));
    svc.setURL(getServiceURL(action));
}

/**
 * Prepares the service for apple pay
 * Sets the credential of the service
 * Sets the url of the service
 * @param {dw.svc.Service} svc - The payfort service
 * @param {dw.order.Order} order - The current order
 * @param {string} action - The action -> TOKENIZATION, AUTHORIZATION, PURCHASE, ...
 */
function prepareApplePayService(svc, order, action) {
    svc.setCredentialID(getApplePayServiceCredentialId(order));
    svc.setURL(getServiceURL(action));
}

/**
 * Gets the service credential
 * Prepares the service
 * Returns the credential of the service
 * @param {dw.svc.Service} svc - The payfort service
 * @param {dw.order.Order} order - The current order
 * @param {string} action - The action -> TOKENIZATION, AUTHORIZATION, PURCHASE, ...
 * @returns {dw.svc.ServiceCredential} the service credential of the service
 */
function getServiceCredential(svc, order, action) {
    prepareService(svc, order, action);
    var serviceConfig = svc.getConfiguration();
    return serviceConfig.getCredential();
}

/**
 * Gets the service credential for apple pay
 * Prepares the service
 * Returns the credential of the service
 * @param {dw.svc.Service} svc - The payfort service
 * @param {dw.order.Order} order - The current order
 * @param {string} action - The action -> AUTHORIZATION, PURCHASE
 * @returns {dw.svc.ServiceCredential} the service credential of the service
 */
function getApplePayServiceCredential(svc, order, action) {
    prepareApplePayService(svc, order, action);
    var serviceConfig = svc.getConfiguration();
    return serviceConfig.getCredential();
}

/**
 * Prepares the PayFort Service Payload Object
 * According to given action
 * Returns the payload
 * @param {dw.svc.ServiceCredential} serviceCredential - The payfort service credential
 * @param {dw.order.Order} order - The current order
 * @param {Object} paymentInformation - The paymentInformation object
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument
 * @param {string} action - The action -> TOKENIZATION, AUTHORIZATION, PURCHASE
 * @returns {Object} the payload JSON Request object
 */
function getPayload(serviceCredential, order, paymentInformation, paymentInstrument, action) {
    var URLUtils = require('dw/web/URLUtils');

    var payload = {
        access_code: serviceCredential.password,
        merchant_identifier: serviceCredential.user,
        merchant_reference: order.orderNo,
        language: getOrderLocale(order).getLanguage()
    };

    switch (action) {
        case payfortConstants.command.tokenization:
            payload.service_command = action;
            payload.return_url = URLUtils.https('PayFortReturn-Tokenization').toString();
            break;
        case payfortConstants.command.authorization:
        case payfortConstants.command.purchase:
            payload.command = action;
            payload.return_url = URLUtils.https('PayFortReturn-Authorization').toString();
            payload.amount = calculatePayFortAmount(paymentInstrument.paymentTransaction.amount);
            payload.currency = order.getCurrencyCode();
            payload.customer_email = order.getCustomerEmail();
            payload.payment_option = getCreditCardType(paymentInstrument.getCreditCardType());
            payload.eci = payfortConstants.eci.ecommerce;
            payload.order_description = order.orderNo;
            payload.customer_name = order.customerName;
            if (order.billingAddress.phone) {
                payload.phone_number = order.billingAddress.phone;
            }
            if (request.httpRemoteAddress) {
                payload.customer_ip = request.httpRemoteAddress;
            }

            // if payment_option is MADA
            // and command is AUTHORIZATION
            // update command to PURCHASE
            // since AUTHORIZATION is not allowed for MADA payments
            if (payload.payment_option === payfortConstants.creditCardTypes.mada
                && action === payfortConstants.command.authorization) {
                payload.command = payfortConstants.command.purchase;
            }
            break;
        case payfortConstants.command.capture:
            payload.command = action;
            payload.amount = calculatePayFortAmount(paymentInstrument.paymentTransaction.amount);
            payload.currency = order.getCurrencyCode();
            payload.order_description = order.orderNo;
            break;
        case payfortConstants.command.voidAuthorization:
            payload.command = action;
            payload.order_description = order.orderNo;
            break;
        case payfortConstants.command.refund:
            payload.command = action;
            payload.amount = calculatePayFortAmount(paymentInstrument.paymentTransaction.amount);
            payload.currency = order.getCurrencyCode();
            payload.order_description = order.orderNo;
            break;
        case payfortConstants.command.checkStatus:
            payload.query_command = action;
            break;
        case payfortConstants.command.inactivateToken: // inactivate -> action=INACTIVE
        case payfortConstants.command.activateToken: // activate -> action=ACTIVE (ps: not in use)
            payload.service_command = payfortConstants.command.updateToken;
            payload.token_name = paymentInstrument.creditCardToken;
            payload.token_status = action;
            break;
        default:
            // not supoosed to be here, throw an error
            throw new Error(StringUtils.format('the action can not be recognized, orderNo: {0}, action: {0}', order.orderNo, action));
    }

    if (paymentInformation) {
        payload.expiry_date = StringUtils.format('{0}{1}', paymentInformation.expirationYear, paymentInformation.expirationMonth);
        payload.card_holder_name = paymentInformation.cardOwner;

        if (paymentInformation.securityCode) {
            payload.card_security_code = paymentInformation.securityCode;
        }

        if (paymentInformation.cardNumber) {
            payload.card_number = paymentInformation.cardNumber;
        }

        if (paymentInformation.saveCard) {
            payload.remember_me = payfortConstants.rememberMe.yes;
        }

        if (paymentInformation.cardToken) {
            payload.token_name = paymentInformation.cardToken;
            delete payload.expiry_date;
        }
    }

    // generate the signature based on payload attributes
    payload.signature = getSignature(
        serviceCredential.custom.payfortChannelType.value,
        payload,
        serviceCredential.custom.payfortSHARequestPhrase,
        serviceCredential.custom.payfortSHAType.value,
        false
    );

    return payload;
}

/**
 * Prepares the ApplePay PayFort Service Payload Object
 * According to given action
 * Returns the payload
 * @param {dw.svc.ServiceCredential} serviceCredential - The payfort service credential
 * @param {dw.order.Order} order - The current order
 * @param {Object} applePayData - The applePayData object
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument
 * @param {string} action - The action -> AUTHORIZATION, PURCHASE
 * @returns {Object} the payload JSON Request object
 */
function getApplePayPayload(serviceCredential, order, applePayData, paymentInstrument, action) {
    var payload = {
        digital_wallet: 'APPLE_PAY',
        access_code: serviceCredential.password,
        merchant_identifier: serviceCredential.user,
        merchant_reference: order.orderNo,
        language: getOrderLocale(order).getLanguage()
    };

    switch (action) {
        case payfortConstants.command.authorization:
        case payfortConstants.command.purchase:
            payload.command = action;
            payload.amount = calculatePayFortAmount(paymentInstrument.paymentTransaction.amount);
            payload.currency = order.getCurrencyCode();
            payload.customer_email = order.getCustomerEmail();
            payload.apple_data = applePayData.payment.token.paymentData.data;
            payload.apple_signature = applePayData.payment.token.paymentData.signature;
            payload.apple_header = {
                apple_transactionId: applePayData.payment.token.paymentData.header.transactionId,
                apple_ephemeralPublicKey: applePayData.payment.token.paymentData.header.ephemeralPublicKey,
                apple_publicKeyHash: applePayData.payment.token.paymentData.header.publicKeyHash
            };
            payload.apple_paymentMethod = {
                apple_displayName: applePayData.payment.token.paymentMethod.displayName,
                apple_network: applePayData.payment.token.paymentMethod.network,
                apple_type: applePayData.payment.token.paymentMethod.type
            };
            payload.apple_version = applePayData.payment.token.paymentData.version;
            payload.eci = payfortConstants.eci.ecommerce;
            payload.order_description = order.orderNo;
            if (request.httpRemoteAddress) {
                payload.customer_ip = request.httpRemoteAddress;
            }
            payload.customer_name = order.customerName;
            if (order.billingAddress.phone) {
                payload.phone_number = order.billingAddress.phone;
            }
            break;
        default:
            // not supoosed to be here, throw an error
            throw new Error(StringUtils.format('the apple pay action can not be recognized, orderNo: {0}, action: {0}', order.orderNo, action));
    }

    // generate the signature based on payload attributes
    payload.signature = getSignature(
        serviceCredential.custom.payfortChannelType.value,
        payload,
        serviceCredential.custom.payfortSHARequestPhrase,
        serviceCredential.custom.payfortSHAType.value,
        false
    );

    return payload;
}

/**
 * Prepares the service
 * Prepares the PayFort Service Payload Object
 * Returns the payload
 * @param {dw.svc.Service} svc - The payfort service
 * @param {dw.order.Order} order - The current order
 * @param {Object} paymentInformation - The paymentInformation object
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument
 * @param {string} action - The action -> TOKENIZATION, AUTHORIZATION, PURCHASE
 * @returns {Object} the payload JSON Request object
 */
function initService(svc, order, paymentInformation, paymentInstrument, action) {
    var serviceCredential;
    switch (paymentInstrument.paymentMethod) {
        case PaymentInstrument.METHOD_DW_APPLE_PAY:
            serviceCredential = getApplePayServiceCredential(svc, order, action);
            break;
        default:
            serviceCredential = getServiceCredential(svc, order, action);
            break;
    }

    return getPayload(
        serviceCredential,
        order,
        paymentInformation,
        paymentInstrument,
        action
    );
}

/**
 * Creates an instance of the payfort.service service.
 * {dw.svc.LocalServiceRegistry} - payfort service object
 */
var payFortService = LocalServiceRegistry.createService('payfort.service', {
    /**
     * PayFort Service ServiceCallback createRequest function
     * @param {dw.svc.Service} svc - The service
     * @param {Object} payload - the request JSON object
     * @returns {object} the request data object
     */

    createRequest: function (svc, payload) {
        svc.setAuthentication('NONE');
        svc.addHeader('Content-Type', 'application/json');

        return JSON.stringify(payload);
    },
    parseResponse: function (svc, client) {
        return JSON.parse(client.text);
    }
});

/**
 * Tries to validate the response by matching the signature
 * @param {Object} postedPayload - that was posted
 * @param {dw.svc.ServiceCredential} serviceCredential - prepared service credential
 * @return {boolean} true if signatures match, false otherwise
 */
function validateSignature(postedPayload, serviceCredential) {
    if (!postedPayload || !postedPayload.signature) {
        return false;
    }

    var signature = getSignature(
        serviceCredential.custom.payfortChannelType.value,
        postedPayload,
        serviceCredential.custom.payfortSHAResponsePhrase,
        serviceCredential.custom.payfortSHAType.value,
        true
    );
    return postedPayload.signature === signature;
}

module.exports = {
    payFortService: payFortService,
    initService: initService,
    getServiceCredential: getServiceCredential,
    getApplePayServiceCredential: getApplePayServiceCredential,
    getPayload: getPayload,
    getApplePayPayload: getApplePayPayload,
    getServiceCredentialId: getServiceCredentialId,
    validateSignature: validateSignature
};
