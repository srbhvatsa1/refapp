'use strict';

var base = module.superModule;

/**
 * If base calculate is success then process basket for abandoned cart
 * @param {dw.order.Basket} basket The basket to be calculated
 * @returns {dw.system.Status} The status object
 */
function calculate(basket) {
    var status = base.calculate(basket);
    if (!status || status.error) {
        return status;
    }

    var abandonedCartHelpers = require('*/cartridge/scripts/helpers/abandonedCartHelpers');
    abandonedCartHelpers.processBasket();

    return status;
}

Object.keys(base || {}).filter(function (baseExport) {
    return baseExport !== 'calculate';
}).forEach(function (baseExport) {
    module.exports[baseExport] = base[baseExport];
});

module.exports.calculate = calculate;
