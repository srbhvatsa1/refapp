<isinclude template="/components/modules" sf-toolkit="off" />
<isloop var="shippingModel" items="${pdict.order.shipping}" status="shippingLoop">
    <isif condition="${shippingLoop.first}">

        <div class="col-12">
            <iscomment>Shipping information</iscomment>
            <div class="confirmation-shipping-method">
                <p class="confirmation-shipping-name">
                    ${shippingModel.selectedShippingMethod.displayName}
                    ${Resource.msg('confirmation.shipping', 'confirmation', null)},
                    ${pdict.order.totals.totalShippingCost}
                </p>
                <p class="confirmation-shipping-arrival-time">
                    <isif condition="${shippingModel.selectedShippingMethod.estimatedArrivalTime}">
                        ( ${shippingModel.selectedShippingMethod.estimatedArrivalTime} )
                    </isif>
                </p>
            </div>
        </div>

        <iscomment>Billing Address</iscomment>
        <div class="col-md-4 confirmation-sold">
            <span class="confirmation-shipping-inline-title">
                ${Resource.msg('confirmation.sold', 'confirmation', null)}
            </span><br/>
            <isif condition="${pdict.order.billing.billingAddress.address.firstName !== null && pdict.order.billing.billingAddress.address.lastName !== null}">
                <p class="confirmation-billing-line">
                    ${pdict.order.billing.billingAddress.address.firstName}
                    ${pdict.order.billing.billingAddress.address.lastName}
                </p>
            </isif>
            <isaddress address_value="${pdict.order.billing.billingAddress.address}" />
            <isif condition="${pdict.order.billing.billingAddress.address.phone !== null}">
                <p class="confirmation-billing-line" dir="ltr">
                    ${pdict.order.billing.billingAddress.address.phone}
                </p>
            </isif>
        </div>

        <iscomment>Shipping Address</iscomment>
        <div class="col-md-4 confirmation-shipped">
            <div class="single-shipping" data-shipment-summary="${shippingModel.UUID}">
                <isif condition="${shippingModel.shippingAddress !== null}">
                    <isset name="address" value="${shippingModel.shippingAddress}" scope="page"/>
                <iselse/>
                    <isset name="address" value="{}" scope="page"/>
                </isif>
                <div class="summary-details shipping">
                    <span class="confirmation-shipping-inline-title">
                        ${Resource.msg('confirmation.shipped', 'confirmation', null)}
                    </span>
                    <isinclude template="checkout/addressSummary" />
                    <div class="shipping-phone" dir="ltr">
                        ${shippingModel.shippingAddress && shippingModel.shippingAddress.phone ? shippingModel.shippingAddress.phone : ''}
                    </div>
                </div>
            </div>
        </div>

        <iscomment>Payment information</iscomment>
        <div class="col-md-4">
            <div class="confirmation-payment-info">
                <div class="credit-card-type">
                    <span class="confirmation-shipping-inline-title">
                        ${Resource.msg('confirmation.payment', 'confirmation', null)}
                    </span><br/>
                    <isloop items="${pdict.order.billing.payment.selectedPaymentInstruments}" var="payment">
                        <isif condition="${payment.paymentMethod === 'CREDIT_CARD'}">
                            <isset name="cardType" value="${payment.type.toLowerCase().replace(/\s/g, '')}" scope="page"/>
                            <div class="d-flex">
                                <span class="text-uppercase">
                                    ${Resource.msg('label.' + cardType, 'creditCard', null)}
                                </span>
                                <span>&nbsp;</span>
                                <span>*${payment.lastFour}</span>
                                <span class="mx-3"></span>
                                <iscreditcard creditcard_type="${payment.type}" />
                            </div>
                        </isif>
                        <isif condition="${payment.paymentMethod === 'COD'}">
                            <span class="text-uppercase">
                                ${Resource.msg('msg.payment.type.cod', 'confirmation', 'Cash on Delivery')}
                            </span>
                        </isif>
                        <isif condition="${payment.paymentMethod === 'GIFT_CERTIFICATE'}">
                            <span class="text-uppercase">
                                ${Resource.msg('msg.payment.giftcertificate', 'confirmation', 'Gift Certificate')}
                            </span>
                        </isif>
                        <isif condition="${payment.paymentMethod === 'DW_APPLE_PAY'}">
                            <span class="text-uppercase">
                                ${Resource.msg('msg.paid.using.apple.pay', 'applePay', 'Apple Pay')}
                            </span>
                        </isif>
                        <isif condition="${payment.paymentMethod === 'KNET'}">
                            <span class="text-uppercase">
                                ${Resource.msg('msg.payment.type.knet', 'confirmation', 'KNET')}
                            </span>
                            <isif condition="${payment.knetPaymentId}">
                                <p><strong>${Resource.msg('knet.payment.id.label', 'knet', null)} </strong>${payment.knetPaymentId}</p>
                            </isif>
                            <isif condition="${payment.knetTransactionId}">
                                <p><strong>${Resource.msg('knet.transaction.id.label', 'knet', null)} </strong>${payment.knetTransactionId}</p>
                            </isif>
                        </isif>
                    </isloop>
                </div>
            </div>
        </div>
    </isif>
</isloop>
