'use strict';

var server = require('server');

server.post(
    'Confirm',
    server.middleware.https,
    function (req, res, next) {
        var URLUtils = require('dw/web/URLUtils');

        var id = req.querystring.ID;
        var token = req.querystring.token;

        res.redirect(URLUtils.url('Order-Confirm', 'ID', id, 'token', token));

        next();
    }
);

module.exports = server.exports();
