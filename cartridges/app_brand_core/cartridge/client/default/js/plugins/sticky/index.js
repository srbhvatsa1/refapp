/* eslint no-param-reassign:0 */

'use strict';

window.jQuery = window.$ = require('jquery');
var throttle = require('lodash/throttle');
var pluginator = require('../pluginator');
var pluginPrefix = 'sticky';
var $win = $(window);
var $doc = $(document);

/**
 * Plugin constructor
 * @param {JQuery} $el - Search input element
 * @param {string} id - Id of the current plugin
 * @param {Object} options - Plugin opitons
 */
function Sticky($el, id, options) {
    this.$el = $el;
    this.id = id;
    this.options = options;
    this.isSticked = false;
    if (this.options.dynamicOffsetTarget !== '') {
        this.options.offsetTop = this.$el.height() - ($(this.options.dynamicOffsetTarget).height() || 0);
    }
    this.throttleScroll = throttle(this.scroll, this.options.throttleTime);
    this.throttleResize = throttle(this.resize, this.options.throttleTime);
}

Sticky.prototype.init = function () {
    var self = this;

    this.scroll();
    this.resize();

    $win.on('scroll', function () {
        self.throttleScroll();
    });
    $win.on('resize', function () {
        self.throttleResize();
    });
};

Sticky.prototype.destroy = function () {
    $win.off('scroll', this.throttleScroll);
    $win.off('resize', this.throttleResize);
};

/**
 * Makes header sticky on scroll
 */
Sticky.prototype.scroll = function () {
    this.$el.addClass(this.options.stickyClass);
    if ($win.scrollTop() >= this.options.offsetTop) {
        if (this.isSticked === false) {
            this.isSticked = true;
            this.$el.addClass(this.options.stickedStateClass);
            this.$el.trigger(pluginPrefix + ':stick');
        }
    } else if (this.isSticked) {
        this.isSticked = false;
        this.$el.removeClass(this.options.stickedStateClass);
        this.$el.trigger(pluginPrefix + ':unstick');
    }
};

/**
 * Recalculates header height and sets this value to parent's padding top
 */
Sticky.prototype.resize = function () {
    if (this.options.dynamicOffsetTarget !== '') {
        this.options.offsetTop = this.$el.height() - ($(this.options.dynamicOffsetTarget).height() || 0);
    }

    if (this.$el.hasClass('js-sticky-menu')) {
        return;
    }

    if (this.isSticked === false) {
        var $clone = this.$el.clone().css('visiblity', 'hidden');
        this.$el.parent().prepend($clone);
        this.$el.parent().css('padding-top', $clone.height());
        $clone.remove();
    } else {
        this.$el.parent().css('padding-top', this.$el.height());
    }
    this.$el.trigger(pluginPrefix + ':resize');
};

module.exports = function () {
    pluginator({
        prefix: pluginPrefix,
        name: 'sticky',
        Constructor: Sticky,
        exports: [
            'scroll',
            'resize'
        ],
        defaultOptions: {
            stickyClass: 'fixed-top',
            stickedStateClass: 'sticked',
            offsetTop: 100,
            dynamicOffsetTarget: '',
            throttleTime: 100
        }
    });

    // Initialize plugin automatically
    $doc.ready(function () {
        $('.js-sticky').sticky();
    });
};
