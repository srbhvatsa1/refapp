'use strict';

/**
 * Attempts setting the given credentialId to service
 * @param {dw.svc.HTTPService} svc Service
 * @param {string} newCredentialID new CredentialID to set to service
 * @returns {boolean} returns true if attempt is successful, false otherwise
 */
function setCredentialID(svc, newCredentialID) {
    // Attempt to set to site-specific credential
    try {
        svc.setCredentialID(newCredentialID);
        return true;
    } catch (e) {
        return false;
    }
}

/**
 * Attempts finding and setting the correct credential to the given service. Priority of credentials is as follows:
 * 1. SiteId (<credential>-<SiteId>)
 * 2. Brand (<credential>-<Brand>)
 * 3. Default (<credential>)
 * @param {dw.svc.HTTPService} svc Service
 */
function setRelevantCredentialID(svc) {
    var Site = require('dw/system/Site');
    var StringUtils = require('dw/util/StringUtils');
    var origCredentialID = svc.getCredentialID() || svc.getConfiguration().getID();
    var credArr = origCredentialID.split('-');
    var credentialSiteID = credArr.length > 1 ? credArr[credArr.length - 1] : null;
    var currentSiteID = Site.current.ID;
    if (credentialSiteID === currentSiteID) {
        // highest priority credentialId is the one with SiteId, if that's already correctly set, we do nothing
        return;
    }

    // try setting credentialID for current site
    var credentialIDForCurrentSite = StringUtils.format('{0}-{1}', credArr[0], currentSiteID);
    var siteCredentialIsSet = setCredentialID(svc, credentialIDForCurrentSite);
    if (siteCredentialIsSet) {
        // credential with siteId found and set
        return;
    }

    var siteBrand = Site.current.getCustomPreferenceValue('siteBrand');
    if (siteBrand) {
        // try setting credentialID for current brand
        var credentialIDForCurrentBrand = StringUtils.format('{0}-{1}', credArr[0], siteBrand);
        var brandCredentialIsSet = setCredentialID(svc, credentialIDForCurrentBrand);
        if (brandCredentialIsSet) {
            // credential with brand found and set
            return;
        }
    }

    // site-specific or brand-specific credential doesn't exist, reset to original
    svc.setCredentialID(origCredentialID);
}

module.exports = {
    setRelevantCredentialID: setRelevantCredentialID
};
