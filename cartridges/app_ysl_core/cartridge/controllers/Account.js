'use strict';

var server = require('server');
server.extend(module.superModule);

var Resource = require('dw/web/Resource');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');

/**
 * Account Beauty Profile
 */
server.get('BeautyProfile', server.middleware.https, csrfProtection.generateToken, userLoggedIn.validateLoggedIn, function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');

    var breadcrumbs = [
        {
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('page.title.myaccount', 'account', null),
            url: URLUtils.url('Account-Show').toString()
        },
        {
            htmlValue: Resource.msg('label.beauty.profile', 'account', null)
        }
    ];

    res.render('account/beautyProfile', {
        breadcrumbs: breadcrumbs
    });
    next();
});


var gtmDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmDataLayerHelpers');

/**
 * Extend viewdata by setting page data layer object
 */
server.append('Show', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    next();
});

/**
 * Extend viewdata by setting page data layer object
 */
server.append('EditProfile', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    next();
});

/**
 * Extend viewdata by setting page data layer object
 */
server.append('EditPassword', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    next();
});

/**
 * Extend viewdata by setting page data layer object
 */
server.append('PasswordReset', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    next();
});

module.exports = server.exports();
