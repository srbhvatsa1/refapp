'use strict';

var server = require('server');
server.extend(module.superModule);

/**
 * Extend viewdata by all brands landing
 */
server.append('Show', function (req, res, next) {
    this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
        var viewData = res.getViewData();
        if (res.view === 'rendering/category/allBrandsLanding' && viewData.productSearch.productSearch) {
            var brandHelpers = require('*/cartridge/scripts/helpers/brandHelpers');

            viewData.allBrands = brandHelpers.getSearchAllBrands(viewData.productSearch.productSearch);
        }
        res.setViewData(viewData);
    });
    return next();
});

module.exports = server.exports();
