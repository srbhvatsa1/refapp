'use strict';

var urlHelper = require('../helpers/urlHelper');
var notificationHelper = require('../helpers/notificationHelper')('.error-messaging');
var formValidation = require('base/components/formValidation');
var cleave = require('../components/cleave');

var url;
var modalId;

module.exports = {
    deletePaymentMethod: function () {
        $('.js-button-delete-payment').on('click', function () {
            var $btn = $(this);
            var $parent = $btn.closest('.js-payment-method-item');
            var $creditCardType = $parent.find('.js-credit-card-type');
            modalId = $btn.data('target');
            url = urlHelper.setParameter('UUID', $btn.data('id'), $btn.data('url'));
            $(modalId).find('.js-container-item-name').html($creditCardType.clone());
        });
    },

    deletePaymentMethodConfirmation: function () {
        $('.js-button-confirm-delete-payment').click(function () {
            var $modal = $(modalId);
            var $dialog = $modal.find('modal-dialog');

            $dialog.spinner().start();
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    if (!data) {
                        notificationHelper.generalError();
                        return;
                    }

                    // Remove payment method from UI
                    $('#uuid-' + data.UUID).remove();

                    // Hide confirmation dialog
                    $modal.modal('hide');

                    // Stop spinner
                    $dialog.spinner().stop();

                    // Update data of remaining addresses
                    var $preferredMethod = $('.js-payment-method-item').first();
                    if ($preferredMethod.length === 0) {
                        // Show no records message
                        $('body').addClass('show-no-records show-no-payment-methods');
                    } else {
                        // Update default address
                        $preferredMethod.find('.js-text-default').removeClass('d-none');
                        $preferredMethod.find('.js-button-delete-payment').data('default', true);
                        $preferredMethod.find('.js-link-set-default').remove();
                    }
                },
                error: function (err) {
                    if (err && err.responseJSON) {
                        if (err.responseJSON.redirectUrl) {
                            window.location.href = err.responseJSON.redirectUrl;
                        } else {
                            $modal.modal('hide');
                            notificationHelper.error(err.responseJSON.errorMessage);
                        }
                    } else {
                        notificationHelper.generalError();
                    }
                    $dialog.spinner().stop();
                }
            });
        });
    },

    submitPayment: function () {
        $('form.payment-form').submit(function (e) {
            var $form = $(this);
            e.preventDefault();
            url = $form.attr('action');
            $form.spinner().start();
            $('form.payment-form').trigger('payment:submit', e);

            var formData = cleave.serializeData($form);

            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: formData,
                success: function (data) {
                    $form.spinner().stop();
                    if (!data.success) {
                        formValidation($form, data);
                    } else {
                        location.href = data.redirectUrl;
                    }
                },
                error: function (err) {
                    if (err.responseJSON.redirectUrl) {
                        window.location.href = err.responseJSON.redirectUrl;
                    }
                    $form.spinner().stop();
                }
            });
            return false;
        });
    },

    handleCreditCardNumber: function () {
        if ($('#cardNumber').length && $('#cardType').length) {
            cleave.handleCreditCardNumber('#cardNumber', '#cardType');
        }
    },

    setDefaultPayment: function () {
        $('.js-link-set-default').on('click', function (e) {
            e.preventDefault();

            // Stop here if url is undefined
            if (!this.href) return;

            var $paymentMethodItem = $(this).closest('.js-payment-method-item');

            // Start spinner
            $paymentMethodItem.spinner().start();

            $.ajax({
                url: this.href,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    if (data) {
                        if (data.success && data.redirectUrl) {
                            window.location.href = data.redirectUrl;
                            return;
                        }
                    } else {
                        notificationHelper.generalError();
                    }
                    $paymentMethodItem.spinner().stop();
                },
                error: function (err) {
                    if (err && err.responseJSON) {
                        if (err.responseJSON.redirectUrl) {
                            window.location.href = err.responseJSON.redirectUrl;
                        } else {
                            notificationHelper.error(err.responseJSON.errorMessage);
                        }
                    } else {
                        notificationHelper.generalError();
                    }
                    $paymentMethodItem.spinner().stop();
                }
            });
        });
    }
};
