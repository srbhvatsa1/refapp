'use strict';

$(document).ready(function () {
    var Cookies = require('js-cookie');

    /**
     * sets the popup close time
     * @param {string} popupId popup id
     */
    function setPopupTime(popupId) {
        Cookies.set(popupId, new Date().getTime());
    }

    /**
     * gets the popup close time
     * @param {string} popupId popup id
     * @returns {number} last popup close time
     */
    function getPopupTime(popupId) {
        return Cookies.get(popupId);
    }

    /**
     * checks if popup expired
     * @param {string} popupId popup id
     * @param {number} popupExpirationDay popup expiration in days
     * @returns {boolean} whether the popup expired
     */
    function isPopupExpired(popupId, popupExpirationDay) {
        var previousPopupDisplayTime = getPopupTime(popupId);

        if (previousPopupDisplayTime === undefined || previousPopupDisplayTime === null) {
            return true;
        }

        var currentTimestamp = new Date().getTime();
        let previousTimeStamp = parseInt(previousPopupDisplayTime, 10);

        return (currentTimestamp - previousTimeStamp) > (popupExpirationDay * 24 * 60 * 60 * 1000);
    }

    $('.js-popup-container').each(function () {
        var $popupModal = $(this).find('.js-popup-modal');
        var popupId = $popupModal.data('popupid');
        var popupExpirationDay = $popupModal.data('expiration');

        // If content asset is not defined or offline, don't go further
        if ($(this).find('.content-asset').length === 0 || !isPopupExpired(popupId, popupExpirationDay)) {
            return;
        }

        // Show popup
        $popupModal.modal('show');
    });

    // Attach close event
    $('.js-popup-cancel-btn').on('click', function () {
        var $popupModal = $(this).closest('.js-popup-modal');
        var popupId = $popupModal.data('popupid');
        setPopupTime(popupId);
    });
});
