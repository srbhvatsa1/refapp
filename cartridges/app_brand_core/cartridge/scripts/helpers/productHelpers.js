'use strict';

var base = module.superModule;
var collections = require('*/cartridge/scripts/util/collections');
var ATTRIBUTE_NAME_COLOR = 'color';
var ATTRIBUTE_NAME_SIZE = 'size';

/**
 * Normalize product and return Product variation model
 * @param  {dw.catalog.Product} product - Product instance returned from the API
 * @param  {Object} productVariables - variables passed in the query string to
 *                                     target product variation group
 * @return {dw.catalog.ProductVarationModel} Normalized variation model
 */
function getVariationModel(product, productVariables) {
    var variationModel = product.variationModel;
    if (!variationModel.master && !variationModel.selectedVariant) {
        variationModel = null;
    } else if (productVariables) {
        var variationAttrs = variationModel.productVariationAttributes;
        Object.keys(productVariables).forEach(function (attr) {
            if (attr && productVariables[attr].value) {
                var dwAttr = collections.find(variationAttrs,
                    function (item) { return item.attributeID === attr || item.ID === attr; });
                var dwAttrValue = collections.find(variationModel.getAllValues(dwAttr),
                    function (item) { return item.value === productVariables[attr].value; });
                if (dwAttr && dwAttrValue) {
                    variationModel.setSelectedAttributeValue(dwAttr.ID, dwAttrValue.ID);
                }
            }
        });
    }
    return variationModel;
}

/**
 * If a product is master and only have one variant for a given attribute - auto select it
 * @param {dw.catalog.Product} apiProduct - Product from the API
 * @param {Object} params - Parameters passed by querystring
 *
 * @returns {Object} - Object with selected parameters
 */
function normalizeSelectedAttributes(apiProduct, params) {
    if (!apiProduct.master) {
        return params.variables;
    }

    var variables = params.variables || {};
    if (apiProduct.variationModel) {
        collections.forEach(apiProduct.variationModel.productVariationAttributes, function (attribute) {
            var allValues = apiProduct.variationModel.getAllValues(attribute);
            if (allValues.length === 1) {
                variables[attribute.ID] = {
                    id: apiProduct.ID,
                    value: allValues.get(0).ID
                };
            }
        });
    }

    return Object.keys(variables) ? variables : null;
}

/**
 * Get information for model creation
 * @param {dw.catalog.Product} apiProduct - Product from the API
 * @param {Object} params - Parameters passed by querystring
 *
 * @returns {Object} - Config object
 */
function getConfig(apiProduct, params) {
    var variables = normalizeSelectedAttributes(apiProduct, params);
    var variationModel = getVariationModel(apiProduct, variables);
    if (variationModel) {
        apiProduct = variationModel.selectedVariant || apiProduct; // eslint-disable-line
    }
    var PromotionMgr = require('dw/campaign/PromotionMgr');
    var promotions = PromotionMgr.activeCustomerPromotions.getProductPromotions(apiProduct);
    var optionsModel = base.getCurrentOptionModel(apiProduct.optionModel, params.options);
    var options = {
        variationModel: variationModel,
        options: params.options,
        optionModel: optionsModel,
        promotions: promotions,
        quantity: params.quantity,
        variables: variables,
        apiProduct: apiProduct,
        productType: base.getProductType(apiProduct)
    };

    return options;
}


/**
 * Get a specific ProductVariationAttribute from a variationModel.productVariationAttributes collection
 * @param  {dw.util.Collection} productVariationAttributes - attribute value
 * @param  {string} attrId - attribute ID
 * @return {dw.catalog.ProductVariationAttribute} Represents a product variation attribute
 */
function getProductVariationAttribute(productVariationAttributes, attrId) {
    var dwAttr = collections.find(productVariationAttributes, function (item) {
        return item.attributeID === attrId || item.ID === attrId;
    });
    return dwAttr;
}

/**
 * Sets the initial attribute value into the params.variables to have selected attribute value
 * for now, only color
 * first color variation valiuye is being set, if no color is set for the request
 * @param {dw.catalog.Product} apiProduct - Product instance returned from the API
 * @param {Object} params - the query string params
 * @returns {Object} - the default selected params
 */
function setInitialAttrValues(apiProduct, params) {
    if (!apiProduct.master) {
        return params;
    }
    var variationModel = apiProduct.variationModel;
    if (!variationModel) {
        return params;
    }

    var variables = params.variables || {};
    var productVariationAttributes = variationModel.productVariationAttributes;
    if (!productVariationAttributes) {
        return params;
    }

    var productColorVariationAttribute = getProductVariationAttribute(productVariationAttributes, ATTRIBUTE_NAME_COLOR);
    var productSizeVariationAttribute = getProductVariationAttribute(productVariationAttributes, ATTRIBUTE_NAME_SIZE);
    if (productColorVariationAttribute && !variables[ATTRIBUTE_NAME_COLOR] && !variables[productColorVariationAttribute.ID]) {
        if (productColorVariationAttribute) {
            var productColorVariationAttributeValues = variationModel.getAllValues(productColorVariationAttribute);
            if (productColorVariationAttributeValues && productColorVariationAttributeValues.length) {
                var orderableColorValue = collections.find(productColorVariationAttributeValues, function (value) {
                    return variationModel.hasOrderableVariants(productColorVariationAttribute, value);
                });
                if (orderableColorValue) {
                    variables[ATTRIBUTE_NAME_COLOR] = {
                        id: apiProduct.ID,
                        value: orderableColorValue.value
                    };

                    params.variables = variables; // eslint-disable-line no-param-reassign
                }
            }
        }
    } else if (productSizeVariationAttribute && !variables[ATTRIBUTE_NAME_SIZE] && !variables[productSizeVariationAttribute.ID]) {
        if (productSizeVariationAttribute) {
            var productSizeVariationAttributeValues = variationModel.getAllValues(productSizeVariationAttribute);
            if (productSizeVariationAttributeValues && productSizeVariationAttributeValues.length) {
                var orderableSizeValue = collections.find(productSizeVariationAttributeValues, function (value) {
                    return variationModel.hasOrderableVariants(productSizeVariationAttribute, value);
                });
                if (orderableSizeValue) {
                    variables[(ATTRIBUTE_NAME_SIZE || productSizeVariationAttribute.ID)] = {
                        id: apiProduct.ID,
                        value: orderableSizeValue.value
                    };

                    params.variables = variables; // eslint-disable-line no-param-reassign
                }
            }
        }
    }

    return params;
}


/**
 * Get the product tile context object
 * @param {Object} productTileParams - the JSON productTileParams
 * @return {Object} the product tile context object
 */
function getProductTileContext(productTileParams) {
    var URLUtils = require('dw/web/URLUtils');
    var ProductFactory = require('*/cartridge/scripts/factories/product');

    // The productTileParams could have the following:
    // pid - the Product ID
    // ratings - boolean to determine if the reviews should be shown in the tile.
    // swatches - boolean to determine if the swatches should be shown in the tile.
    //
    // pview - string to determine if the product factory returns a model for
    //         a tile or a pdp/quickview display

    // just to be sure the pview is tile
    productTileParams.pview = 'tile'; // eslint-disable-line no-param-reassign

    var product;
    var productUrl;
    var quickViewUrl;

    // remove this logic once the Product factory is
    // able to handle the different product types
    try {
        product = ProductFactory.get(productTileParams);
        productUrl = URLUtils.url('Product-Show', 'pid', product.id).relative().toString();
        quickViewUrl = URLUtils.url('Product-ShowQuickView', 'pid', product.id)
            .relative().toString();
    } catch (e) {
        product = false;
        productUrl = URLUtils.url('Home-Show');// change to coming soon page
        quickViewUrl = URLUtils.url('Home-Show');
    }

    var context = {
        product: product,
        urls: {
            product: productUrl,
            quickView: quickViewUrl
        },
        display: {}
    };

    Object.keys(productTileParams).forEach(function (key) {
        if (productTileParams[key] === 'true') {
            context.display[key] = true;
        } else if (productTileParams[key] === 'false') {
            context.display[key] = false;
        } else {
            context.display[key] = productTileParams[key];
        }
    });

    return context;
}

/**
 * Creates the breadcrumbs object
 * @param {string} cgid - category ID from navigation and search
 * @param {string} pid - product ID
 * @param {Array} breadcrumbs - array of breadcrumbs object
 * @returns {Array} an array of breadcrumb objects
 */
function getAllBreadcrumbs(cgid, pid, breadcrumbs) {
    var URLUtils = require('dw/web/URLUtils');
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var Resource = require('dw/web/Resource');

    var category;
    var product;

    if (pid) {
        product = ProductMgr.getProduct(pid);

        // add product name
        breadcrumbs.push({
            htmlValue: product.name,
            url: URLUtils.url('Product-Show', 'pid', pid).toString()
        });

        category = !product.primaryCategory && product.variant
            ? product.masterProduct.primaryCategory
            : product.primaryCategory;
    } else if (cgid) {
        category = CatalogMgr.getCategory(cgid);
    }

    if (category) {
        breadcrumbs.push({
            htmlValue: category.displayName,
            url: URLUtils.url('Search-Show', 'cgid', category.ID)
        });

        if (category.parent && category.parent.ID !== 'root') {
            return getAllBreadcrumbs(category.parent.ID, null, breadcrumbs);
        }
    }

    // add home
    breadcrumbs.push({
        htmlValue: Resource.msg('global.home', 'common', null),
        url: URLUtils.home().toString()
    });

    return breadcrumbs;
}

/**
 * Renders the Product Details Page
 * @param {Object} querystring - query string parameters
 * @param {Object} reqPageMetaData - request pageMetaData object
 * @returns {Object} contain information needed to render the product page
 */
function showProductPage(querystring, reqPageMetaData) {
    var URLUtils = require('dw/web/URLUtils');
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');

    var params = querystring;
    var product = ProductFactory.get(params);
    var addToCartUrl = URLUtils.url('Cart-AddProduct');
    var canonicalUrl = URLUtils.url('Product-Show', 'pid', product.id);
    var breadcrumbs = getAllBreadcrumbs(null, product.id, []).reverse();
    var template = 'product/productDetails';

    if (product.productType === 'bundle' && !product.template) {
        template = 'product/bundleDetails';
    } else if (product.productType === 'set' && !product.template) {
        template = 'product/setDetails';
    } else if (product.template) {
        template = product.template;
    }

    pageMetaHelper.setPageMetaData(reqPageMetaData, product);
    pageMetaHelper.setPageMetaTags(reqPageMetaData, product);
    var schemaData = require('*/cartridge/scripts/helpers/structuredDataHelper').getProductSchema(product);

    return {
        template: template,
        product: product,
        addToCartUrl: addToCartUrl,
        resources: base.getResources(),
        breadcrumbs: breadcrumbs,
        canonicalUrl: canonicalUrl,
        schemaData: schemaData
    };
}

/**
 * Converts comma separated string list to an array
 * @param {string} str - Comma separated list of some items
 * @return {string[]} Array of string
 */
function strToArr(str) {
    return (str || '').split(/[\s,]+/);
}

/**
 * Adds single or multiple products to wishlist
 * @param {string} pids - Comma separated list of product ids
 * @param {Request} req request object
 * @return {Object} Response object that can be returned as json
 */
function addProductsToWishlist(pids, req) {
    var Resource = require('dw/web/Resource');
    var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
    var list = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
    var pidList = strToArr(pids);
    var errorIds = [];

    pidList.forEach(function (pid) {
        var config = {
            qty: 1,
            optionId: null,
            optionValue: null,
            req: req,
            type: 10
        };
        var itemExists = productListHelper.itemExists(list, pid, config);
        var isAlreadyAdded = productListHelper.addItem(list, pid, config);

        if (itemExists === false && isAlreadyAdded === false) {
            errorIds.push(pid);
        }
    });

    if (pidList.length === 0 || errorIds.length === pidList.length) {
        return {
            error: false,
            pid: pids,
            msg: Resource.msg('wishlist.addtowishlist.failure.msg', 'wishlist', null)
        };
    } else if (errorIds.length > 0) {
        return {
            error: true,
            pid: pids,
            msg: Resource.msgf('wishlist.addtowishlist.partial.success.msg', 'wishlist', null,
                errorIds.join(', '))
        };
    }

    return {
        success: true,
        pid: pids,
        msg: Resource.msg('wishlist.addtowishlist.success.msg', 'wishlist', null)
    };
}

/**
 * Returns an html string of the content asset that has an id with
 *  {prefix}-for-{productId} or
 *  {prefix}-for-all-products
 * @param {string} productId product Id
 * @param {string} prefix prefix for the content asset id
 * @returns {string} returns html string of content asset
 */
function getProductContentAsset(productId, prefix) {
    var ContentMgr = require('dw/content/ContentMgr');
    var contentAsset = ContentMgr.getContent(prefix + '-for-' + productId);
    if (contentAsset &&
        contentAsset.online &&
        contentAsset.custom &&
        contentAsset.custom.body) {
        return contentAsset.custom.body.markup;
    }

    contentAsset = ContentMgr.getContent(prefix + '-for-all-products');
    if (contentAsset &&
        contentAsset.online &&
        contentAsset.custom &&
        contentAsset.custom.body) {
        return contentAsset.custom.body.markup;
    }

    return null;
}

/**
 * @param {Array} attrSizeValues - attribute size values array
 * @returns {Array} sortedAttrSizeValues - sorted attribute size values array
 */
function sortVariationAttributeSizeValues(attrSizeValues) {
    if (!attrSizeValues || !attrSizeValues.length) {
        return attrSizeValues;
    }

    var sortedSizesArr = ['XXXS', 'XXS', 'XS', 'P', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL', 'XXXXL'];

    return attrSizeValues.sort(function (size1, size2) {
        var size1IndexOf = sortedSizesArr.indexOf(size1.value);
        var size2IndexOf = sortedSizesArr.indexOf(size2.value);
        if (size1IndexOf > -1 && size2IndexOf > -1) {
            return size1IndexOf - size2IndexOf;
        }
        return parseFloat(size1.value) - parseFloat(size2.value);
    });
}

base.showProductPage = showProductPage;
base.getAllBreadcrumbs = getAllBreadcrumbs;
base.getProductTileContext = getProductTileContext;
base.addProductsToWishlist = addProductsToWishlist;
base.getProductContentAsset = getProductContentAsset;
base.sortVariationAttributeSizeValues = sortVariationAttributeSizeValues;
base.setInitialAttrValues = setInitialAttrValues;
base.getVariationModel = getVariationModel;
base.getConfig = getConfig;
module.exports = base;
