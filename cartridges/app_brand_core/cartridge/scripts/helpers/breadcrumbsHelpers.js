'use strict';

/**
 * Adds an unclickable breadcrumb at the end of the breadcrumbs
 * @param {string} res response object
 * @param {string} breadcrumb text to show as the last breadcrumb (unclickable)
 */
function addLastBreadcrumb(res, breadcrumb) {
    var viewData = res.getViewData();
    if (!viewData) {
        return;
    }
    if (!viewData.breadcrumbs) {
        viewData.breadcrumbs = [];
    }
    viewData.breadcrumbs.push({
        htmlValue: breadcrumb
    });
}

module.exports = {
    addLastBreadcrumb: addLastBreadcrumb
};
