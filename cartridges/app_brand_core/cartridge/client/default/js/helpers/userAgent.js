'use strict';

/**
 * Detect if client is using IOS Safari
 * @returns {boolean} true if browser is IOS Safari
 */
function isMobileSafari() {
    var navUserAgent = navigator.userAgent.toLowerCase();
    return (~navUserAgent.indexOf('safari') && !~navUserAgent.indexOf('crios') && navUserAgent.match(/(ipod|iphone|ipad)/));
}

var isMobileWebOS = function () {
    return (/webOS|iPhone|iPad|iPod/i.test(navigator.userAgent));
};

module.exports = {
    isMobileSafari: isMobileSafari(),
    isMobileWebOS: isMobileWebOS()
};
