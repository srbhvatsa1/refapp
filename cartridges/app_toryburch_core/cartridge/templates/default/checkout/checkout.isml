<isdecorate template="common/layout/checkout">
    <isscript>
        var assets = require('*/cartridge/scripts/assets.js');
        assets.addJs('/js/checkout.js');
        assets.addCss('/css/checkout.css');
    </isscript>

    <isif condition="${pdict.reportingURLs && pdict.reportingURLs.length}">
        <isinclude template="reporting/reportingUrls" />
    </isif>

    <div id="checkout-main" class="container data-checkout-stage checkout-main <isif condition="${pdict.order.usingMultiShipping && pdict.order.shipping.length > 1}">multi-ship</isif>" data-customer-type="${pdict.customer.registeredUser ? 'registered' : 'guest'}" data-checkout-stage="${pdict.currentStage}" data-checkout-get-url="${URLUtils.https('CheckoutServices-Get')}">
        <div class="row">
            <div class="checkout-main-content col-12 col-lg-8">
                <iscomment>Checkout Forms: Shipping, Payment, Coupons, Billing, etc</iscomment>
                <div class="alert alert-danger error-message" role="alert" <isif condition="${pdict.pspErrorMessage}">style="display:block"</isif>>
                    <div class="error-message-text js-error-message-text"
                        data-shipping-error="${Resource.msg('error.no.shipping.address', 'checkout', null)}"
                        data-billing-error="${Resource.msg('error.no.billing.address', 'checkout', null)}">
                        <isif condition="${pdict.pspErrorMessage}">
                            <p><isprint value="${pdict.pspErrorMessage}" /></p>
                        </isif>
                        <isif condition="${session.custom.knetPaymentId}">
                            <p><strong>${Resource.msg('knet.payment.id.label', 'knet', null)} </strong>${session.custom.knetPaymentId}</p>
                        </isif>
                        <isif condition="${session.custom.knetTransactionId}">
                            <p><strong>${Resource.msg('knet.transaction.id.label', 'knet', null)} </strong>${session.custom.knetTransactionId}</p>
                        </isif>
                        <isscript>
                            delete session.custom.knetPaymentId;
                            delete session.custom.knetTransactionId;
                        </isscript>
                    </div>
                </div>

                <iscomment>Step 1: Shipping</iscomment>
                <isinclude template="checkout/shipping/shipping" />
                <div class="card shipping-summary">
                    <div class="card-header clearfix">
                        <h2 class="pull-left card-header-custom">
                            <isprint value="${Resource.msg('heading.checkout.shipping', 'checkout', null)}" encoding="off" />
                        </h2>
                        <span class="edit-button pull-right">${Resource.msg('action.edit.step', 'checkout', null)}</span>
                    </div>
                    <div class="card-body">
                        <isinclude template="checkout/shipping/shippingSummary" />
                    </div>
                </div>

                <div class="next-step-button next-payment">
                    <div class="next-step-button-inner">
                        <button class="btn btn-primary btn-large btn-block submit-shipping js-submit-shipping-btn <isif condition="${pdict.order.usingMultiShipping && !pdict.order.shippable}">disabled</isif>" type="submit" name="submit" value="submit-shipping" <isif condition="${pdict.order.usingMultiShipping && !pdict.order.shippable}">disabled</isif>>
                            ${Resource.msg('button.next.payment', 'checkout', null)}
                        </button>
                    </div>
                </div>

                <iscomment>Step 2: Payment and Billing</iscomment>
                <isinclude template="checkout/billing/billing" />
                <div class="card payment-summary">
                    <div class="card-header clearfix">
                        <h2 class="pull-left card-header-custom">
                            <isprint value="${Resource.msg('heading.payment', 'checkout', null)}" encoding="off" />
                        </h2>
                        <span class="edit-button pull-right">${Resource.msg('action.edit.step', 'checkout', null)}</span>
                    </div>

                    <div class="card-body">
                        <isinclude template="checkout/billing/billingSummary" />
                    </div>
                </div>

                <iscomment>Checkout Workflow Buttons</iscomment>
                <div class="row">
                    <div class="col-12 next-step-button">
                        <div class="next-step-button-inner">
                            <button class="btn btn-primary btn-large btn-block d-md-none submit-shipping js-submit-shipping-btn <isif condition="${pdict.order.usingMultiShipping && !pdict.order.shippable}">disabled</isif>" type="submit" name="submit" value="submit-shipping" <isif condition="${pdict.order.usingMultiShipping && !pdict.order.shippable}">disabled</isif>>
                                ${Resource.msg('button.next.payment', 'checkout', null)}
                            </button>

                            <button class="btn btn-primary btn-large btn-block submit-payment js-submit-payment-btn" type="submit" name="submit" value="submit-payment">
                                ${Resource.msg('button.next.place.order', 'checkout', null)}
                            </button>

                            <button class="btn btn-primary btn-large btn-block place-order js-place-order-btn" data-action="${URLUtils.url('CheckoutServices-PlaceOrder')}"
                                    type="submit" name="submit" value="place-order">
                                ${Resource.msg('button.place.order', 'checkout', null)}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="checkout-contact-info-wrapper d-lg-none">
                    <iscontentasset aid="checkout-contact-info"/>
                </div>
            </div>

            <iscomment>Order Totals, Details and Summary</iscomment>
            <div class="order-summary-container col-12 col-lg-4">
                <div class="card order-summary">
                    <div class="accordion global-accordion info-accordion order-summary-accordion" id="accordionOrderSummary">
                        <div class="card">
                            <div class="card-header" id="headingOrderSummary">
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOrderSummary" aria-expanded="false" aria-controls="collapseOrderSummary">
                                        ${Resource.msg('order.summary.title', 'checkout', null)}
                                        <span class="icon icon-arrow-down"></span>
                                        <span class="sub-total pull-right">${pdict.order.totals.grandTotal}</span>
                                    </button>
                                </h2>
                            </div>

                            <div id="collapseOrderSummary" class="collapse" aria-labelledby="headingOrderSummary" data-parent="#accordionOrderSummary">
                                <div class="card-body p-0">
                                    <div class="card-body order-total-summary p-0">
                                        <isinclude template="checkout/orderTotalSummary" />
                                        <isinclude template="checkout/orderProductSummary" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="checkout-contact-info-wrapper d-none d-lg-block">
                    <iscontentasset aid="checkout-contact-info"/>
                </div>
            </div>
        </div>
    </div>
    <isinclude template="checkout/shipping/shippingAddressTemplate" />
</isdecorate>
