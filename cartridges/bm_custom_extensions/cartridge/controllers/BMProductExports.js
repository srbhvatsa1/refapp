'use strict';
/* global request */
/* global response */

/**
 * @module controllers/BMProductExports
 */

var boguard = require('bc_library/cartridge/scripts/boguard');
var ISML = require('dw/template/ISML');
var URLUtils = require('dw/web/URLUtils');

/**
 * Opens the Product Exports page
 */
function start() {
    var noResult = request.httpParameterMap.noResult.intValue;
    var Site = require('dw/system/Site');

    var allowedLocales = Site.getCurrent().allowedLocales;
    ISML.renderTemplate('products/products.isml', { noResult: noResult, allowedLocales: allowedLocales });
}

/**
 * Picks the relevant price books with requested locale
 * @param {string} locale locale id
 * @returns {pricebook[]} relevant pricebooks
 */
function pickRelevantPriceBooks(locale) {
    var countries = require('app_brand_core/cartridge/config/countries');
    var PriceBookMgr = require('dw/catalog/PriceBookMgr');
    var Site = require('dw/system/Site');

    var currentSite = Site.getCurrent();
    var pricebooks = PriceBookMgr.getSitePriceBooks();

    var currentCurrency = currentSite.defaultCurrency;
    for (var i = 0; i < countries.length; i++) {
        var country = countries[i];
        if (country.id === locale) {
            currentCurrency = country.currencyCode;
            break;
        }
    }

    var relevantPriceBooks = [];
    for (var j = 0; j < pricebooks.length; j++) {
        var pricebook = pricebooks[j];
        if (pricebook.currencyCode === currentCurrency) {
            relevantPriceBooks.push(pricebook.ID);
        }
    }

    return relevantPriceBooks;
}

/**
 * Collects all products according to given locale
 * @param {string} locale requested locale
 * @returns {dw.catalog.Product[]} list of products
 */
function collectAllProducts(locale) {
    var ArrayList = require('dw/util/ArrayList');
    var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

    var allProducts = new ArrayList();

    var productInventoryList = inventoryHelpers.getInventoryList(locale);
    if (productInventoryList) {
        var CatalogMgr = require('dw/catalog/CatalogMgr');
        var ProductMgr = require('dw/catalog/ProductMgr');

        var masterCatalog = CatalogMgr.getSiteCatalog();

        if (!masterCatalog) {
            allProducts.add({
                product: {
                    ID: 'Site doesn"t have a primary catalog'
                },
                stockValue: 0
            });
            return allProducts;
        }

        var productHits = ProductMgr.queryProductsInCatalog(masterCatalog);
        var distinctProducts = new ArrayList();

        while (productHits.hasNext()) {
            var product = productHits.next();
            if (distinctProducts.contains(product)) {
                continue;
            }
            distinctProducts.add(product.ID);

            var availabilityModel = inventoryHelpers.getProductAvailabilityModel(product, locale);
            var ats = 0;
            if (availabilityModel && availabilityModel.inventoryRecord && availabilityModel.inventoryRecord.ATS) {
                ats = availabilityModel.inventoryRecord.ATS.value;
            }
            allProducts.add({
                product: product,
                stockValue: ats
            });
        }
    }

    return allProducts;
}

/**
 * Export products
 */
function exportProducts() {
    var StringUtils = require('dw/util/StringUtils');
    var requestedLocale = request.httpParameterMap.locale.stringValue;
    request.setLocale(requestedLocale);
    var productList = collectAllProducts(requestedLocale);
    if (productList.length === 0) {
        response.redirect(URLUtils.url('BMProductExports-Start', 'noResult', 1));
        return;
    }

    var relevantPriceBooks = pickRelevantPriceBooks(requestedLocale);

    var fileName = '';
    var shouldExportInventoryLevels = request.httpParameterMap.exportInventoryLevels.booleanValue;
    if (shouldExportInventoryLevels) {
        ISML.renderTemplate('products/productExportWithInventoryLevels.isml', { products: productList });
        fileName = StringUtils.format('ProductsWithInventoryLevels-{0}.xls', requestedLocale);
    } else {
        ISML.renderTemplate('products/productExport.isml', { products: productList, pricebooks: relevantPriceBooks });
        fileName = StringUtils.format('Products-{0}.xls', requestedLocale);
    }
    response.addHttpHeader('Content-Disposition', 'attachment; filename=' + fileName);
}

exports.Start = boguard.ensure(['get'], start);

exports.ExportProducts = boguard.ensure(['get'], exportProducts);
