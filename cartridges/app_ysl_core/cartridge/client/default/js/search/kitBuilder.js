var urlHelper = require('brand_core/helpers/urlHelper');
var nextArrow = require('../templates/slickArrowNext');
var prevArrow = require('../templates/slickArrowPrev');
var carousel = require('brand_core/components/carousel');

var $doc = $(document);
var kitStore = localStorage;

/**
 * Created a unique key for local storage
 * @param {jQuery} $step - step title jquery element
 * @returns {string} a unique key
 */
function getKey() {
    return ['kit-builder', $('.js-category-id').val()].join('-');
}

/**
 * Returns current step id
 * @returns {string} step id
 */
function getCurrentStepId() {
    var $currentStep = $('.js-kit-builder-step.selected');
    return $currentStep.data('refinement-id');
}

/**
 * Returns selected items from local storage
 * @param {string} step - step id
 * @returns {Object[]} an array of selected items
 */
function getSelectedItems(step) {
    var selectedItems = JSON.parse(kitStore.getItem(getKey()) || '[]');
    if (step) {
        return selectedItems.filter(function (item) {
            return item.step === step;
        });
    }
    return selectedItems;
}

/**
 * Checks whether the product is already selected or not
 * @param {string} pid - a product id
 * @param {string} step - a step id
 * @returns {boolean} - result of check
 */
function isProductSelected(pid) {
    var selectedItems = getSelectedItems(getCurrentStepId());
    var selectedItem = selectedItems.filter(function (item) {
        return item.pid === pid;
    });

    if (selectedItem.length === 0) {
        selectedItem = selectedItems.filter(function (item) {
            return item.masterId === pid;
        });
    }
    return selectedItem.length > 0;
}

/**
 * Set selected item to local storage
 * @param {Object} item - array of selected items
 */
function setItem(item) {
    if (isProductSelected(item.pid) === false) {
        var selectedItems = getSelectedItems();
        selectedItems.push(item);
        kitStore.setItem(getKey(), JSON.stringify(selectedItems));
    }
}

/**
 * Remove item from local storage
 * @param {string} pid - product id
 */
function removeItem(pid) {
    kitStore.setItem(getKey(), JSON.stringify(getSelectedItems().filter(function (item) {
        return item.pid !== pid;
    })));
}

/**
 * Remove all items from local storage
 */
function removeAllItems() {
    kitStore.removeItem(getKey());
}

/**
 * Updates selected items of current step
 * @param {jQuery} $step - step jquery element
 */
function updateSelectedItems($step) {
    var $selectedItemsContainer = $step.find('.js-kit-builder-selected-items-container');
    var selectedItems = getSelectedItems($step.data('refinement-id'));
    if (selectedItems && selectedItems.length > 0) {
        if (selectedItems.length === 1 && $step.hasClass('js-kit-builder-review') === false) {
            $selectedItemsContainer.html('<img src="' + selectedItems[0].img + '"/>').removeClass('d-none');
        } else {
            $selectedItemsContainer.html('<span>' + selectedItems.length + '</span>').removeClass('d-none');
        }
    } else {
        $selectedItemsContainer.html('').addClass('d-none');
    }
}

/**
 * Updates selected items of all steps
 * @returns {jQuery} - step jquery element
 */
function updateAllSelectedItems() {
    $('.js-kit-builder-error').html('').addClass('d-none');
    return $('.js-kit-builder-step').each(function () {
        updateSelectedItems($(this));
    });
}

/**
 * Updates product state (checks wheter in kit builder set or not)
 * @param {jQuery} $productTile - product tile jquery element
 * @param {boolean} tileClick - product tile click event
 * @returns {jQuery} - product tile jquery element
 */
function updateProductState($productTile, tileClick) {
    var pid = $productTile.attr('data-pid');
    var isSelected = isProductSelected(pid);
    var $attrLink = $productTile.find('.js-tile-attribute-link');
    $attrLink.addClass('disabled');

    if (isSelected) {
        $productTile.find('.js-kit-builder-select').addClass('d-none');
        $productTile.find('.js-kit-builder-remove').removeClass('d-none');
        $productTile.addClass('selected');
        $attrLink.find('.selected').parent().removeClass('disabled');
        if (tileClick) {
            $('.js-kit-builder-step.selected').parent().next().find('.js-kit-builder-step')
                .trigger('click');
        }
    } else {
        $productTile.find('.js-kit-builder-select').removeClass('d-none');
        $productTile.find('.js-kit-builder-remove').addClass('d-none');
        $productTile.removeClass('selected');
        $attrLink.removeClass('disabled');

        var $reviewStep = $('.js-kit-builder-review');
        if ($reviewStep.hasClass('selected')) {
            if (getSelectedItems().length === 0) {
                $('.js-kit-builder-step').first().trigger('click');
            } else {
                $reviewStep.trigger('click');
            }
        }
    }
    return $productTile;
}

/**
 * Inits start page
 */
function initStartPage() {
    // Check whether kit builder has saved items or not
    var $startPage = $('.js-kit-builder-start-page');
    if ($startPage.children().length) {
        $('.js-refine-category').each(function () {
            var $this = $(this);
            if (getSelectedItems($this.data('refinement-id')).length) {
                $this.addClass('state-has-selected-items');
            }
        });

        if (getSelectedItems().length > 0) {
            $('.js-kit-builder-start').addClass('d-none');
            $('.js-kit-builder-continue').removeClass('d-none');
        } else {
            $('.js-kit-builder-start').removeClass('d-none');
            $('.js-kit-builder-continue').addClass('d-none');
        }
    } else {
        $('.js-search-results').removeClass('d-none');
    }
}

/**
 * Show errors
 * @param {jQuery} $el - jquery element for errors
 * @param {string} type - error type
 * @param {array} interpolationValues - array of values that will be replaced with keys in error message
 */
function showError($el, type, interpolationValues) {
    var message = $('.js-kit-builder-error-texts').data(type);
    if (interpolationValues) {
        for (var i = 0; i < interpolationValues.length; i++) {
            var value = interpolationValues[i];
            message = message.replace('{' + i + '}', value);
        }
    }
    $el.html(message).removeClass('d-none');
}

module.exports = function init() {
    initStartPage();

    $doc
        // Start kit-builder
        .on('click', '.js-kit-builder-start', function () {
            var $firstStep = $('.js-refine-category').first();
            var url = urlHelper.setParameter('kitbuilder', 1, $firstStep.attr('href'));
            $firstStep.attr('href', url);
            $firstStep.trigger('click');
        })

        // Start kit-builder
        .on('click', '.js-kit-builder-continue', function () {
            var $lastStep = $('.js-refine-category.state-has-selected-items').last();
            var url = urlHelper.setParameter('kitbuilder', 1, $lastStep.attr('href'));
            $lastStep.attr('href', url);
            $lastStep.trigger('click');
        })

        // Add product to kit builder set
        .on('click', '.js-kit-builder-select', function () {
            var $this = $(this);
            var $productTile = $this.closest('.js-product-tile-container');
            var $attrLink = $productTile.find('.js-tile-attribute-link');
            var $productTileSelectModal = $this.closest('.modal');

            $doc.one('tile:update', function (e, res) {
                setItem({
                    pid: res.product.id,
                    masterId: urlHelper.getParameter('pid', '?' + res.queryString),
                    img: $productTile.find('.js-tile-image').attr('src'),
                    attrId: $attrLink.find('.selected').parent().attr('data-attr-id'),
                    step: getCurrentStepId(),
                    url: window.RA_URL['Product-Variation'] + '?' + res.queryString
                });
                updateProductState($productTile, true);
                updateAllSelectedItems();
                if ($productTileSelectModal.length > 0) {
                    $productTileSelectModal.modal('hide');
                }
            });

            if ($attrLink.length) {
                if ($attrLink.find('.selected').length) {
                    $attrLink.find('.selected').parent().trigger('click');
                } else {
                    $attrLink.first().trigger('click');
                }
            }
        })

        // Remove product from kit builder set
        .on('click', '.js-kit-builder-remove', function () {
            var $this = $(this);
            var $productTile = $this.closest('.js-product-tile-container');
            var pid = $productTile.attr('data-pid');
            removeItem(pid);
            updateProductState($this.closest('.js-product-tile-container'));
            updateAllSelectedItems();
        })

        .on('click', '.js-kit-builder-review', function (e) {
            e.preventDefault();

            var $this = $(this);
            var selectedItems = getSelectedItems();
            var $errorContainer = $('.js-kit-builder-error');

            if (selectedItems.length === 0) {
                showError($errorContainer, 'no-products-selected');
                return;
            }

            var $steps = $('.js-refine-category');
            if (selectedItems.length < $steps.length) {
                var requiredSteps = [];
                $steps.each(function () {
                    var $step = $(this);
                    var stepId = $step.attr('data-refinement-id');
                    var hasStepSelectedItems = selectedItems.filter(function (item) {
                        return item.step === stepId;
                    }).length > 0;
                    if (hasStepSelectedItems === false) {
                        requiredSteps.push($step.attr('data-refinement-name'));
                    }
                });
                if (requiredSteps.length === 1) {
                    showError($errorContainer, 'not-enough-products-selected-singular', [requiredSteps.join(', ')]);
                } else {
                    showError($errorContainer, 'not-enough-products-selected-plural', [requiredSteps.join(', ')]);
                }
                return;
            }

            $.spinner().start();

            $('.js-kit-builder-step.selected').removeClass('selected');
            $('.js-search-results').addClass('state-kit-builder-review');
            $this.addClass('selected');

            var $grid = $('.js-product-grid');
            var selectedItemIds = [];
            var totalSalesPrice = 0;
            var totalListPrice = 0;
            var template = '';
            var productsTemplate = '';
            var summaryProductsTemplate = '';
            var summaryTemplate = '';
            var priceCurrency = '';

            /* eslint-disable */
            selectedItems.forEach(function (item) {
                $.get(item.url, function (res) {
                    selectedItemIds.push(item.pid);

                    // Prepare total price
                    if (res.product.price) {
                        if (res.product.price.sales) {
                            totalSalesPrice += res.product.price.sales.value;
                        }
                        if (res.product.price.list) {
                            totalListPrice += res.product.price.list.value;
                        } else {
                            totalListPrice += res.product.price.sales.value;
                        }

                        priceCurrency = res.product.price.sales.currency;
                    }

                    // Prepare selected attribute
                    var $selectedAttribute = '';
                    if (res.product.variationAttributes &&
                        res.product.variationAttributes.length &&
                        res.product.variationAttributes[0].values &&
                        res.product.variationAttributes[0].values.length) {
                        var selectedAttributes = res.product.variationAttributes[0].values.filter(function (value) {
                            return value.selected;
                        });
                        if (selectedAttributes.length) {
                            var selectedAttribute = selectedAttributes[0];
                            if (res.product.variationAttributes[0].swatchable &&
                                selectedAttribute.images &&
                                selectedAttribute.images.swatch &&
                                selectedAttribute.images.swatch[0]) {
                                $selectedAttribute =
                                    '<span class="swatch swatch-circle" style=\'background-image: url("' + selectedAttribute.images.swatch[0].url + '")\'></span>' +
                                    '<span class="color-name" title="' + selectedAttribute.displayValue + '">' + selectedAttribute.displayValue + '</span>';
                            } else {
                                $selectedAttribute = '<span>' + res.product.variationAttributes[0].displayName + ': ' + selectedAttribute.displayValue + '</span>';
                            }
                        }
                    }

                    // Todo: Add category  - res.product.category.displayName

                    // Prepare item template
                    productsTemplate += [
                        '<div class="kit-product-tile product-tile-container js-product-tile-container col-12 col-lg-4" data-pid="' + res.product.id + '">',
                            '<div class="kit-product-tile-inner">',
                                '<div class="kit-product-tile-category">' + (res.product.category && res.product.category.displayName ? res.product.category.displayName : 'Category') + '</div>',
                                '<div class="kit-product-tile-image review-image"><img src="' + res.product.images.large[0].url + '" /></div>',
                                '<div class="kit-product-tile-details">',
                                    '<div class="review-title">' + res.product.productName + '</div>',
                                    '<small class="kit-product-tile-desc text-secondary d-none">' + (res.product.shortDescription ? res.product.shortDescription : '') + '</small>',
                                    '<div class="d-flex align-items-center mt-2">' + $selectedAttribute + '</div>',
                                    '<div class="kit-product-tile-actions">',
                                        '<button type="button" class="kit-product-tile-action btn btn-link" data-step="' + item.step + '">' + window.RA_RESOURCE['common.button.change.product'] + '</button>',
                                        '<button type="button" class="kit-product-tile-action btn btn-link review-remove-btn js-kit-builder-remove">' + window.RA_RESOURCE['common.button.remove'] + '</button>',
                                    '</div>',
                                '</div>',
                            '</div>',
                        '</div>',
                    ].join('');

                    // Prepare summary products template
                    summaryProductsTemplate += [
                        '<div class="kitbuilder-summary-item js-kitbuilder-summary-item">',
                            '<div class="kitbuilder-summary-item-label">' + res.product.productName + '</div>',
                            '<div class="kitbuilder-summary-item-value js-kitbuilder-summary-item-value">' + res.product.price.html + '</div>',
                        '</div>'
                    ].join('');

                    // All items are loaded render template
                    if (selectedItems.length === selectedItemIds.length) {
                        $.get(window.RA_URL['Product-Price'], {
                            sales: totalSalesPrice,
                            list: totalListPrice,
                            currency: res.product.price.sales.currency
                        }, function(price) {
                            // Prepare order summary template

                            summaryTemplate += [
                                '<div class="kitbuilder-summary">',
                                    '<div class="kitbuilder-summary-inner">',
                                        '<div class="kitbuilder-summary-title">Order Summary</div>',
                                        '<div class="kitbuilder-summary-totals">',
                                            summaryProductsTemplate,
                                            '<div class="kitbuilder-summary-item kitbuilder-summary-total">',
                                                '<div class="kitbuilder-summary-item-label">Total <span class="kitbuilder-summary-item-label-desc">(' + window.RA_RESOURCE['label.including.x.percent.vat'] +')<span></div>',
                                                '<div class="kitbuilder-summary-item-value js-kitbuilder-summary-total">' + totalSalesPrice + ' ' + priceCurrency + '</div>',
                                            '</div>',
                                        '</div>',
                                    '</div>',
                                '</div>'
                            ].join('');

                            // Prepare add to cart button template
                            summaryTemplate += [
                                '<div class="kitbuilder-summary-actions col-12 mb-3">',
                                    '<div class="row flex-row-reverse">',
                                        '<div class="col-12 col-md-5">',
                                            '<div class="row">',
                                                '<div class="kitbuilder-summary-actions-item col-4 pr-0">',
                                                    '<div class="bg-dark text-white text-center d-flex align-items-center justify-content-center h-100">',
                                                        '<div class="price">',
                                                            price.sales && '<span class="sales"><span class="value">' + price.sales.formatted + '</span></span>',
                                                            price.list && '<span class="strike-through d-block list"><span class="value">' + price.list.formatted + '</span></span>',
                                                        '</div>',
                                                    '</div>',
                                                '</div>',
                                                '<div class="kitbuilder-summary-actions-item col-8 pl-0">',
                                                    '<button class="js-add-to-cart-multiple btn btn-primary btn-block" data-pids="' + selectedItemIds.join(',') + '">',
                                                        '<i class="icon-shopping-bag fs-icon-sm mr-2"></i>',
                                                         window.RA_RESOURCE['label.add.all'] + ' (' + selectedItemIds.length + ') ' +  window.RA_RESOURCE['label.to.bag'],
                                                    '</button>',
                                                '</div>',
                                            '</div>',
                                        '</div>',
                                    '</div>',
                                '</div>'
                            ].join('');

                            // Render all items and add to cart button

                            template += [
                                '<div class="kitbuilder col-12">',
                                    '<div class="kitbuilder-title">',
                                        'Review your choices',
                                    '</div>',
                                    '<div class="kitbuilder-products row js-kitbuilder-carousel carousel-flex">',
                                        productsTemplate,
                                    '</div>',
                                    '<div class="kitbuilder-summary-section">',
                                        summaryTemplate,
                                    '</div>',
                                '</div>'
                            ].join('');
                            
                            
                            $grid.html(template);

                            if ($(window).width() < window.RA_BREAKPOINTS.lg) {
                                carousel($('.js-kitbuilder-carousel'), {
                                    infinite: false,
                                    slidesToScroll: 1,
                                    nextArrow: nextArrow,
                                    prevArrow: prevArrow,
                                    dots: true,
                                    responsive: [
                                        {
                                            breakpoint: window.RA_BREAKPOINTS.lg,
                                            settings: {
                                                unslick: true
                                            }
                                        }
                                    ]
                                });
                            }

                            $.spinner().stop();
                        });
                    }
                });
            });
            /* eslint-enable */
        })

        // Select variation from product tile modal
        .on('change', '.js-product-tile-modal-variation', function () {
            var $this = $(this);
            var url = $this.val();
            var $productTile = $this.closest('.js-product-tile');

            $productTile.find('.js-tile-attribute-link[href="' + url + '"]').trigger('click');
        })
        // Add all selected items to cart
        .on('click', '.js-add-to-cart-multiple', function () {
            var $this = $(this);
            var pids = ($this.data('pids') || '').split(/[\s,]+/);
            var pidsObjList = [];

            // Don't go further, if there isn't any product id
            if (pids.length === 0) return;

            // Start spinner
            $.spinner().start();

            // Prepare form data
            for (var i = 0; i < pids.length; i++) {
                pidsObjList.push({
                    pid: pids[i],
                    qty: 1
                });
            }
            var form = {
                pidsObj: JSON.stringify(pidsObjList)
            };

            $.ajax({
                url: window.RA_URL['Cart-AddProduct'],
                method: 'POST',
                data: form,
                success: function () {
                    removeAllItems();
                    window.location = window.RA_URL['Cart-Show'];
                },
                error: function () {
                    $.spinner().stop();
                }
            });
        })

        // Remove start page once te kit builder is stated
        .on('search:success', function (e, result) {
            var $results = $(result.response);
            if ($results.filter('.js-kit-builder-start-page').length === 0) {
                $('.js-kit-builder-start-page').remove();
            }

            $('.js-search-results').removeClass('state-kit-builder-review');

            var selectedItems = getSelectedItems();
            $('.js-product-tile-container').each(function () {
                var $this = $(this);
                var pid = $this.attr('data-pid');
                var $attrLink = $this.find('.js-tile-attribute-link');

                $doc.one('tile:update', function () {
                    updateProductState($this);
                });

                if ($attrLink.length) {
                    var selectedItem = selectedItems.filter(function (item) {
                        return item.pid === pid;
                    });

                    if (selectedItem.length === 0) {
                        selectedItem = selectedItems.filter(function (item) {
                            return item.masterId === pid;
                        });
                    }

                    if (selectedItem.length) {
                        var attrId = selectedItem[0].attrId;
                        var $selectedAttrLink = $attrLink.filter('[data-attr-id="' + attrId + '"]');
                        if ($selectedAttrLink.length) {
                            $selectedAttrLink.trigger('click');
                        } else {
                            $attrLink.first().trigger('click');
                        }
                    } else {
                        $attrLink.first().trigger('click');
                    }
                }
            });
            updateAllSelectedItems();
        });
};
