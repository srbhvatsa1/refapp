'use strict';
/* global request */
/* global response */

/**
 * @module controllers/BMOrderExports
 */

var boguard = require('bc_library/cartridge/scripts/boguard');
var ISML = require('dw/template/ISML');
var OrderMgr = require('dw/order/OrderMgr');
var URLUtils = require('dw/web/URLUtils');

/**
 * Gets allowed countries
 * @returns {Array} - the countries array
 */
function getAllowedCountries() {
    var Locale = require('dw/util/Locale');
    var StringUtils = require('dw/util/StringUtils');
    var Resource = require('dw/web/Resource');
    var currentSite = require('dw/system/Site').current;

    var allowedLocales = currentSite.getAllowedLocales();
    var countries;

    if (allowedLocales) {
        countries = [];
        var locales = [];
        for (var i = 0; i < allowedLocales.length; i++) {
            var loc = Locale.getLocale(allowedLocales[i]);
            if (locales.indexOf(loc.country) === -1) {
                countries.push({
                    code: loc.country,
                    name: Resource.msg(StringUtils.format('select.option.country.{0}', loc.ISO3Country).toLowerCase(), 'forms', loc.displayCountry)
                });
                locales.push(loc.country);
            }
        }
    }
    return countries;
}

/**
 * Opens the Order Exports page
 */
function start() {
    var noResult = request.httpParameterMap.noResult.intValue;
    ISML.renderTemplate('orders/orders.isml', { noResult: noResult, countries: getAllowedCountries() });
}

/**
 * Collects orders given the filter criterias from request
 * @param {dw.system.Request} request request parameter
 * @returns {dw.util.SeekableIterator} Iterator of the orders
 */
function collectOrders(request) {
    var StringUtils = require('dw/util/StringUtils');

    var createdDateFrom = request.httpParameterMap.input_createdDateFrom.stringValue;
    var createdDateTo = request.httpParameterMap.input_createdDateTo.stringValue;
    var modifiedDateFrom = request.httpParameterMap.input_modifiedDateFrom.stringValue;
    var modifiedDateTo = request.httpParameterMap.input_modifiedDateTo.stringValue;
    var orderStatus = request.httpParameterMap.orderStatus.intValue;
    var paymentStatus = request.httpParameterMap.paymentStatus.intValue;
    var shippingStatus = request.httpParameterMap.shippingStatus.intValue;
    var confirmationStatus = request.httpParameterMap.confirmationStatus.intValue;
    var country = request.httpParameterMap.country.stringValue;

    var queryArray = [];
    if (createdDateFrom) {
        createdDateFrom = new Date(createdDateFrom);
        createdDateFrom.setHours(0, 0, 0, 0);
        queryArray.push('creationDate >= ' + createdDateFrom.toISOString().split('.')[0]);
    }
    if (createdDateTo) {
        createdDateTo = new Date(createdDateTo);
        createdDateTo.setHours(23, 59, 59, 999);
        queryArray.push('creationDate <= ' + createdDateTo.toISOString().split('.')[0]);
    }
    if (modifiedDateFrom) {
        modifiedDateFrom = new Date(modifiedDateFrom);
        modifiedDateFrom.setHours(0, 0, 0, 0);
        queryArray.push('lastModified >= ' + modifiedDateFrom.toISOString().split('.'));
    }
    if (modifiedDateTo) {
        modifiedDateTo = new Date(modifiedDateTo);
        modifiedDateTo.setHours(23, 59, 59, 999);
        queryArray.push('lastModified <= ' + modifiedDateTo.toISOString().split('.'));
    }
    if (country) {
        queryArray.push(StringUtils.format('customerLocaleID ILIKE {0}*_{1}{0}', '\'', country));
    }
    if (orderStatus !== -1) {
        queryArray.push('status = ' + orderStatus);
    }
    if (paymentStatus !== -1) {
        queryArray.push('paymentStatus = ' + paymentStatus);
    }
    if (shippingStatus !== -1) {
        queryArray.push('shippingStatus = ' + shippingStatus);
    }
    if (confirmationStatus !== -1) {
        queryArray.push('confirmationStatus = ' + confirmationStatus);
    }

    if (queryArray.length > 6) {
        queryArray = queryArray.slice(0, 6);
    }

    var queryString = queryArray.join(' AND ');

    var ordersIterator = OrderMgr.queryOrders(queryString, 'creationDate desc', null);

    return ordersIterator;
}

/**
 * Export orders
 */
function exportOrders() {
    var ordersIterator = collectOrders(request);

    if (ordersIterator.count === 0) {
        response.redirect(URLUtils.url('BMOrderExports-Start', 'noResult', 1));
        return;
    }

    var StringUtils = require('dw/util/StringUtils');

    var fileName = '';
    var template = '';
    if (request.httpParameterMap.includeLineItems.booleanValue) {
        template = 'orders/orderExportWithLineItems.isml';
        fileName = 'OrdersWithLineItems.xls';
    } else {
        template = 'orders/orderExport.isml';
        fileName = 'Orders.xls';
    }

    response.setContentType('application/vnd.ms-excel');
    response.addHttpHeader('Content-Disposition', 'attachment; filename=' + fileName);

    ISML.renderTemplate('decorators/excelFileDecoratorHeader.isml');
    var index = 0;
    var order = null;
    while (ordersIterator.hasNext()) {
        order = ordersIterator.next();
        ISML.renderTemplate(template, {
            order: order,
            even: !!(index % 2),
            index: index,
            orderNoFormatter: StringUtils.pad('', order.orderNo.length).replace(/ /g, '0'),
            customerNoFormatter: StringUtils.pad('', order.customerNo.length).replace(/ /g, '0')
        });
        index++;
    }
    ISML.renderTemplate('decorators/excelFileDecoratorFooter.isml');

    // need to have it to trigger download file
    var out = response.writer;
    out.print('');
}

exports.Start = boguard.ensure(['get'], start);

exports.ExportOrders = boguard.ensure(['get'], exportOrders);
