'use strict';

var base = module.superModule;

/**
 * Checks CurrentBasket to define GiftCardCodePayment is available
 * Checks if the current site has giftCard enabled
 * Checks if the GIFT_CERTIFICATE payment method is available
 * Checks if the current product line items are not a gift card product
 * @param {dw.order.Basket} currentBasket - the target Basket object
 * @param {Array} applicablePaymentMethods - array of object that contain information about the applicable payment methods for the current cart
 * @returns {boolean} if available then true, false otherwise
 */
function isGiftCardCodePaymentAvailable(currentBasket, applicablePaymentMethods) {
    var Site = require('dw/system/Site');

    // set as false for default value
    var isAvailable = false;

    // check if the site is GiftCardEnabled
    if (!Site.getCurrent().getCustomPreferenceValue('isGiftCardEnabled')) {
        return isAvailable;
    }

    // check current basket's applicablePaymentMethods
    // if there is GIFT_CERTIFICATE payment method is available
    if (applicablePaymentMethods) {
        isAvailable = applicablePaymentMethods.some(function (paymentMethod) {
            return paymentMethod.ID === 'GIFT_CERTIFICATE';
        });
    }

    if (isAvailable) {
        var collections = require('*/cartridge/scripts/util/collections');

        // check current productlineitems
        // if there is no product as marked with gift (giftCard product)
        isAvailable = collections.every(currentBasket.productLineItems, function (pli) {
            return !pli.gift;
        });
    }

    return isAvailable;
}

/**
 * Payment class that represents payment information for the current basket
 *
 * Check to see if the current site's giftCard feature is enabled
 * Check to see if the payment method GIFT_CERTIFICATE is available and also
 * check to see if there are any gift cards are in the cart. If there are gift
 * cards in the cart, the form must be disabled.
 *
 * @param {dw.order.Basket} currentBasket - the target Basket object
 * @param {dw.customer.Customer} currentCustomer - the associated Customer object
 * @param {string} countryCode - the associated Site countryCode
 * @constructor
 */
function Payment(currentBasket, currentCustomer, countryCode) {
    base.call(this, currentBasket, currentCustomer, countryCode);
    this.isGiftCardCodePaymentAvailable = isGiftCardCodePaymentAvailable(currentBasket, this.applicablePaymentMethods);
}

module.exports = Payment;
