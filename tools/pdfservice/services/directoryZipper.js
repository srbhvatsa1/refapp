/**
 * Zips everything under the given working directory path
 *
 * @param {string} workingDirectoryPath path to zip under
 *
 * @returns {archiver} instance of archiver
 */
exports.zipEverything = (workingDirectoryPath) => {
    var archiver = require('archiver');
    var archive = archiver('zip', { zlib: { level: 1 } });

    // deletes the temporary folder after the archiving is completed
    archive.on('close', async () => {
        const fs = require('fs-extra');
        await fs.remove(workingDirectoryPath);
        console.log('Working directory is deleted');
        console.groupEnd();
    });

    console.group('Working directory is getting zipped');

    archive.directory(workingDirectoryPath, '/');
    archive.finalize();

    console.log('zipping finalized');

    return archive;
}