'use strict';

/* global customer */
var server = require('server');
server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');

server.replace(
    'List',
    server.middleware.https,
    server.middleware.get,
    csrfProtection.generateToken,
    consentTracking.consent,
    function (req, res, next) {
        res.redirect(URLUtils.https('Case-Create'));
        next();
    }
);

server.replace(
    'Show',
    server.middleware.https,
    server.middleware.get,
    csrfProtection.generateToken,
    consentTracking.consent,
    function (req, res, next) {
        res.redirect(URLUtils.https('Case-Create'));
        next();
    }
);

server.replace(
    'Create',
    server.middleware.https,
    server.middleware.get,
    csrfProtection.generateToken,
    consentTracking.consent,
    function (req, res, next) {
        var reCapthaHelpers = require('*/cartridge/scripts/helpers/reCaptchaHelpers');

        var contactusform = server.forms.getForm('contactus');
        contactusform.clear();
        if (customer.authenticated) {
            contactusform.firstname.value = customer.profile.firstName;
            contactusform.lastname.value = customer.profile.lastName;
            contactusform.email.value = customer.profile.email;
            contactusform.phone.value = customer.profile.phoneMobile;
        }

        var breadcrumbs = [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: Resource.msg('label.contactus', 'forms', null)
            }
        ];

        res.render('case/contactus', {
            submitted: req.querystring.submitted,
            form: contactusform,
            breadcrumbs: breadcrumbs,
            reCaptha: reCapthaHelpers.getReCaptcha('contactus')
        });
        next();
    }
);

server.replace(
    'SaveCase',
    server.middleware.https,
    server.middleware.post,
    consentTracking.consent,
    csrfProtection.validateRequest,
    function (req, res, next) {
        var formErrors = require('*/cartridge/scripts/formErrors');
        var reCapthaHelpers = require('*/cartridge/scripts/helpers/reCaptchaHelpers');

        var contactusform = server.forms.getForm('contactus');
        var verifyReCaptchaResult = reCapthaHelpers.verifyReCaptcha('contactus', req.form);

        if (contactusform.valid && verifyReCaptchaResult.valid) {
            /**
             * @type {dw.system.HookMgr}
             */
            var HookMgr = require('dw/system/HookMgr');
            var hookID = 'app.case.created';
            // call hook if defined, otherwise send out email
            if (HookMgr.hasHook(hookID)) {
                var result = HookMgr.callHook(
                    hookID,
                    hookID.slice(hookID.lastIndexOf('.') + 1),
                    contactusform
                );
                if (!result || result.status !== 'OK') {
                    contactusform.base.invalidateFormElement(Resource.msg('error.message.case.creation.failed', 'forms', null));
                    res.json({
                        serverErrors: [Resource.msg('error.general', 'common', null)],
                        success: false
                    });
                    return next();
                }

                var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
                var emailObj = {
                    to: contactusform.email.value,
                    subject: contactusform.title.value,
                    from: require('dw/system/Site').current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
                    type: emailHelpers.emailTypes.contactUs
                };
                emailHelpers.sendEmail(emailObj, 'mail/contactus', contactusform);
            } else {
                var Mail = require('dw/net/Mail');
                // var Resource = require('dw/web/Resource');

                var email = new Mail();
                email.addTo(require('dw/system/Site').current.getCustomPreferenceValue('customerServiceEmail')
                || 'no-reply@salesforce.com');
                email.setSubject(contactusform.reason.value);
                email.setFrom(contactusform.email.value);

                email.setContent(contactusform.comment.value, 'text/html', 'UTF-8');
                email.send();
            }

            res.json({
                success: true
            });
        } else {
            res.json({
                success: false,
                fields: formErrors.getFormErrors(contactusform)
            });
        }
        return next();
    }
);

module.exports = server.exports();
