'use strict';

/**
 * Count down from chracter limit of an input
 */
function countCharacters() {
    var $input = $(this);
    var $label = $input.parent().find('.js-count-characters-label');
    var charLength = Math.max($input.attr('maxLength') - $input.val().length, 0);
    var text = $label.data(charLength === 1 ? 'text-singular' : 'text-plural');
    $label.text(text.replace('{0}', charLength));
}

module.exports = function () {
    $('.js-count-characters')
        .on('input', countCharacters)
        .each(countCharacters);
};
