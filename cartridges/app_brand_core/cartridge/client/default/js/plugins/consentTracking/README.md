# SFRA Consent Tracking jQuery Plugin

The Consent Tracking Plugin allow you to show a warning with accept and/or reject buttons on every page.

## Usage

You can use following ISML to show the consent tracking warning. This template needs to be returned from *ConsentTracking-Check* method. The element is a placeholder for plugin options. Only data attrbiutes and class names will be evaluated by the plugin, any other element or content will be ignored.
```html
    <span class="api-${pdict.consentApi} ${pdict.tracking_consent == null ? '' : 'consented' } js-consent-tracking"
        data-consent-asset-online="${pdict.caOnline}"
        data-consent-url-content="${URLUtils.url('ConsentTracking-GetContent', 'cid', 'tracking_hint')}"
        data-consent-url-accept="${URLUtils.url('ConsentTracking-SetSession', 'consent', 'true')}"
        data-consent-url-reject="${URLUtils.url('ConsentTracking-SetSession', 'consent', 'false')}"
        data-consent-text-accept="${Resource.msg('button.consentTracking.yes', 'common', null)}"
        data-consent-text-reject="${Resource.msg('button.consentTracking.no', 'common', null)}"
        data-consent-modal-heading="${Resource.msg('heading.consentTracking.track.consent', 'common', null)}"
        ></span>
```

If the option placeholder element has `js-consent-tracking` class the plugin initializes itself automatically with supplied data attributes, otherwise you need to call `.consentTracking()` method manually.
```js
  $('.consent-options-with-custom-class').consentTracking();
```

You can pass options object for customization.  (see opitons table below)
```js
  $('.consent-options-with-custom-class').consentTracking({
    showAcceptButton: false
  });
```

---

## Options

All options listed below are also available as HTML5 data attributes. In order to use them on search input element, you need to convert keys from camelCase to data-sws-hyphenated-text as follows. Let's say you don't want to show accept button, in this case *showAcceptButton* should be *data-consent-show-accept-button* as follows;
```html
    <span class="api-${pdict.consentApi} ${pdict.tracking_consent == null ? '' : 'consented' } js-consent-tracking"
        data-consent-show-accept-button="false"
        ></span>
```
<br />

| Option | Description | Default |
|---|---|---|
| selPlaceholder | Selector for placeholder element that tracking content asset returned from the server is appended | <span style="color:#ce8349;">.js-consent-placeholder</span> |
| urlContent | Url that gets tracking content asset | null |
| urlAccept | Url that performs accept action | null |
| urlReject | Url that performs reject action | null |
| textAccept | Accept button's text | <span style="color:#ce8349;">Yes</span> |
| textReject | Reject button's text | <span style="color:#ce8349;">No</span> |
| acceptButtonClass | Class name(s) that will be added to accept button | <span style="color:#ce8349;">btn btn-primary</span> |
| rejectButtonClass | Class name(s) that will be added to reject button | <span style="color:#ce8349;">btn btn-primary</span> |
| cancelButtonClass | Class name(s) that will be added to cancel button | <span style="color:#ce8349;">btn btn-link btn-cancel</span> |
| cancelButtonIconClass | Class name(s) that will be added to cancel button's icon | <span style="color:#ce8349;">fa fa-times</span> |
| cancelButtonAction | Action that will be called on close button click | <span style="color:#ce8349;">accept</span> |
| showAcceptButton | If <span style="color:#569cb3;">false</span>, accept button will be hidden  | <span style="color:#569cb3;">true</span> |
| showRejectButton | If <span style="color:#569cb3;">false</span>, reject button will be hidden  | <span style="color:#569cb3;">true</span> |
| assetOnline | If <span style="color:#569cb3;">false</span>, consent tracking warning won't be rendered  | <span style="color:#569cb3;">true</span> |
| modal | If <span style="color:#569cb3;">false</span>, consent tracking warning will be appended into the *selPlaceholder* element  | <span style="color:#569cb3;">true</span> |
| modalHeading | Modal title | <span style="color:#ce8349;">Tracking Consent</span> |

---

## Events

You can listen events as follows;

```js
  $('.js-consent-tracking').on('consent:ready', function() {
      // do something
  });
```

| Event | Description |
|---|---|
| <span style="color:#ce8349;">consent:ready</span> | Triggers when content is fetched from server |
| <span style="color:#ce8349;">consent:accept</span> | Triggers when accept action's response is returned from server |
| <span style="color:#ce8349;">consent:reject</span> | Triggers when reject action's response is returned from server |
| <span style="color:#ce8349;">consent:close</span> | Triggers when content asset is removed from the DOM |
