'use strict';

var base = module.superModule;

/**
 * Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * Extend model with storePickupEnabled attribute
 * Extend model with virtualGiftCardEnabled attribute
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {dw.order.Shipment} [shipment] - a Shipment
 */
function ShippingMethodModel(shippingMethod, shipment) {
    base.call(this, shippingMethod, shipment);

    // extend model with storePickupEnabled attribute
    this.storePickupEnabled = shippingMethod ? shippingMethod.custom.storePickupEnabled : null;

    // extend model with virtualGiftCardEnabled attribute
    this.virtualGiftCardEnabled = shippingMethod ? shippingMethod.custom.virtualGiftCardEnabled : null;

    // add shippingMethod to the object as row
    // will be removed after basket shipping costs are set
    this.raw = shippingMethod;
}

module.exports = ShippingMethodModel;
