'use strict';

var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
var StoreModel = require('*/cartridge/models/store');

/**
 * Creates an array of objects containing store information
 * @param {dw.util.Set} storesObject - a set of <dw.catalog.Store> objects
 * @returns {Array} an array of objects that contains store information
 */
function createStoresObject(storesObject) {
    return Object.keys(storesObject).map(function (key) {
        var store = storesObject[key];
        var storeModel = new StoreModel(store);
        return storeModel;
    });
}

/**
 * Creates an array of objects containing the coordinates of the store's returned by the search
 * @param {dw.util.Set} storesObject - a set of <dw.catalog.Store> objects
 * @returns {Array} an array of coordinates objects with store info
 */
function createGeoLocationObject(storesObject) {
    var context;
    var templateStoreInfoWindow = 'storeLocator/storeInfoWindow';
    var storeInfoHtml;
    return Object.keys(storesObject).map(function (key) {
        var store = storesObject[key];
        context = { store: store };
        storeInfoHtml = renderTemplateHelper.getRenderedHtml(context, templateStoreInfoWindow);
        return {
            name: store.name,
            latitude: store.latitude,
            longitude: store.longitude,
            infoWindowHtml: storeInfoHtml,
            infoListHtml: storeInfoHtml
        };
    });
}

/**
 * @constructor
 * @classdesc The stores model
 * @param {dw.util.Set} storesResultsObject - a set of <dw.catalog.Store> objects
 * @param {Object} searchKey - what the user searched by (location or postal code)
 * @param {number} searchRadius - the radius used in the search
 * @param {dw.web.URL} actionUrl - a relative url
 */
function stores(storesResultsObject, searchKey, searchRadius, actionUrl) {
    this.stores = createStoresObject(storesResultsObject);
    this.locations = JSON.stringify(stores.createGeoLocationObject(this.stores));
    this.searchKey = searchKey;
    this.radius = searchRadius;
    this.actionUrl = actionUrl;
    this.googleMapsApi = storeHelpers.getGoogleMapsApi(['places']);
    this.radiusOptions = [15, 30, 50, 100, 300];
    this.storesResultsHtml = this.stores ? storeHelpers.createStoresResultsHtml(this.stores) : null;
}

stores.createGeoLocationObject = createGeoLocationObject;
module.exports = stores;
