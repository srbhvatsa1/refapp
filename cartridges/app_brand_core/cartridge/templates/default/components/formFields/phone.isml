<isscript>
    var currentLocale = require('dw/util/Locale').getLocale(request.locale);
    var phoneCodes = require('*/cartridge/config/phonecodes');
    var countryCode;
    var currentPhoneCode;
    var currentPhonePrefix;
    var currentPhoneRemovePrefix;
    if (!!pdict.inputphone_value && pdict.inputphone_multiregion) {
        for (var country in phoneCodes) {
            if (pdict.inputphone_value.indexOf(phoneCodes[country].code) !== -1) {
                countryCode = country;
                currentPhoneCode = phoneCodes[country].code;
                currentPhonePrefix = phoneCodes[country].prefix || '';
                currentPhoneRemovePrefix = phoneCodes[country].removePrefix || '';
                break;
            }
        }
    }

    if (!countryCode) {
        countryCode = currentLocale.getCountry();
        currentPhoneCode = phoneCodes[countryCode] ? phoneCodes[countryCode].code || '' : '';
        currentPhonePrefix = phoneCodes[countryCode] ? phoneCodes[countryCode].prefix || '' : '';
        currentPhoneRemovePrefix = phoneCodes[countryCode] ? phoneCodes[countryCode].removePrefix || '' : '';
    }
    var currentValue = pdict.inputphone_value && !!pdict.inputphone_value ? pdict.inputphone_value.replace(currentPhoneCode + currentPhonePrefix, '') : '';
    var fieldName = pdict.inputphone_formfield.htmlName;
</isscript>

<div class="form-group
    ${pdict.inputphone_formfield.mandatory === true ? 'required' : ''}
    ${fieldName}
    ${pdict.inputtext_formgroupclass}">
    <isif condition="${!pdict.inputphone_hidelabel}">
        <label class="form-control-label" for="${'id_' + fieldName}">
            ${pdict.inputphone_label || pdict.inputphone_formfield.label || Resource.msg('label.input.phonenumber', 'forms', null)}
        </label>
    </isif>
    <isif condition="${ pdict.inputphone_multiregion }">
        <div class="${pdict.inputphone_row_class ? pdict.inputphone_row_class : 'row'}">
            <div class="col-5
                ${pdict.inputphone_longcodedisplay == 'true' ? 'col-lg-6' : 'col-lg-4'}">
                <select class="form-control custom-select phone-country-code-selector js-phone-country ${pdict.inputphone_class || ''}"
                    data-size="6"
                    data-width="auto"
                    id="phoneAreaCode"
                    dir="ltr"
                    <isprint value="${pdict.inputphone_disabled ? 'disabled' : ''}" encoding="off" />
                >
                    <isloop items="${ Object.keys(phoneCodes) }" var="country">
                        <option id="${ country }"
                                data-country="${ country }"
                                data-phone-code="${ phoneCodes[country].code }"
                                data-phone-prefix="${ phoneCodes[country].prefix || '' }"
                                data-phone-removeprefix="${ phoneCodes[country].removePrefix || '' }"
                                value="${ phoneCodes[country].code }"
                            ${ countryCode === country ? 'selected' : ''}>
                            ${pdict.inputphone_longcodedisplay == 'true' && phoneCodes[country].codeDisplay ? phoneCodes[country].codeDisplay : phoneCodes[country].code}
                        </option>
                    </isloop>
                </select>
            </div>
            <div class="col-7
                ${pdict.inputphone_longcodedisplay == 'true' ? 'col-lg-6' : 'col-lg-8'}
                phone-number-container">
                <input type="tel" dir="ltr"
                    id="${'id_' + fieldName}"
                    autocomplete="tel"
                    class="form-control js-phone ${pdict.inputphone_class || ''}"
                    name="${ fieldName + '_pt' }"
                    data-country="${ countryCode }"
                    data-phone-code="${ currentPhoneCode }"
                    data-phone-prefix="${ currentPhonePrefix }"
                    data-phone-removeprefix="${ currentPhoneRemovePrefix }"
                    data-raw-value="${ pdict.inputphone_value || '' }"
                    data-target="${ fieldName }"
                    <isprint value="${pdict.inputphone_readonly? 'readonly' : ''}" encoding="off" />
                    <isprint value="${pdict.inputphone_disabled ? 'disabled' : ''}" encoding="off" />
                />
                <input type="tel" dir="ltr"
                    class="d-none js-hidden-input js-phone-ghost"
                    name="${ fieldName }"
                    <isprint value="${pdict.inputphone_readonly ? 'readonly' : ''}" encoding="off" />
                    <isprint value="${pdict.inputphone_disabled ? 'disabled' : ''}" encoding="off" />
                    <isprint value="${ pdict.inputphone_formfield.attributes}" encoding="off" />
                    data-missing-error="<isprint value="${pdict.inputphone_missingerror || Resource.msg('error.message.required', 'forms', null)}" encoding="htmldoublequote" />"
                    data-range-error="<isprint value="${pdict.inputphone_rangeerror || Resource.msg('error.message.range', 'forms', null)}" encoding="htmldoublequote" />"
                    data-pattern-mismatch="<isprint value="${pdict.inputphone_parseerror || Resource.msg('error.message.parse', 'forms', null)}" encoding="htmldoublequote" />"
                    aria-describedby="${'form_tel_' + fieldName + '_error'}"
                />
                <div class="invalid-feedback" id="${'form_tel_' + fieldName + '_error'}"><isprint value="${pdict.inputphone_formfield.error}" /></div>
            </div>
        </div>
    <iselse/>
        <div class="${pdict.inputphone_row_class ? pdict.inputphone_row_class : 'row'}">
            <div class="region-code col-2 d-flex align-items-center text-nowrap justify-content-end"  dir="ltr" style="max-height: 2.5rem">${currentPhoneCode}</div>
            <div class="col-10 phone-number-container">
                <input type="tel" dir="ltr"
                    id="${'id_' + fieldName}"
                    autocomplete="tel"
                    class="form-control js-phone ${pdict.inputphone_class || ''}"
                    placeholder="${Resource.msg('label.input.phone.placeholder', 'forms', null)}"
                    data-target="${ fieldName }"
                    data-country="${ countryCode }"
                    data-phone-code="${ currentPhoneCode }"
                    data-phone-prefix="${ currentPhonePrefix }"
                    data-phone-removeprefix="${ currentPhoneRemovePrefix }"
                    data-raw-value="${ pdict.inputphone_value || '' }"
                    <isprint value="${pdict.inputphone_readonly ? 'readonly' : ''}" encoding="off" />
                    <isprint value="${pdict.inputphone_disabled ? 'disabled' : ''}" encoding="off" />
                />
                <input type="text" dir="ltr"
                    class="d-none js-hidden-input js-phone-ghost"
                    name="${ fieldName }"
                    <isprint value="${pdict.inputphone_readonly? 'readonly' : ''}" encoding="off" />
                    <isprint value="${pdict.inputphone_disabled ? 'disabled' : ''}" encoding="off" />
                    <isprint value="${ pdict.inputphone_formfield.attributes}" encoding="off" />
                    data-missing-error="<isprint value="${pdict.inputphone_missingerror || Resource.msg('error.message.required', 'forms', null)}" encoding="htmldoublequote" />"
                    data-range-error="<isprint value="${pdict.inputphone_rangeerror || Resource.msg('error.message.range', 'forms', null)}" encoding="htmldoublequote" />"
                    data-pattern-mismatch="<isprint value="${pdict.inputphone_parseerror || Resource.msg('error.message.parse', 'forms', null)}" encoding="htmldoublequote" />"
                    aria-describedby="${'form_tel_' + fieldName + '_error'}"
                />
                <div class="invalid-feedback" id="${'form_tel_' + fieldName + '_error'}"><isprint value="${pdict.inputphone_formfield.error}" /></div>
            </div>
        </div>
    </isif>
</div>
