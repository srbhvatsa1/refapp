
'use strict';

var Status = require('dw/system/Status');
var File = require('dw/io/File');
var StringUtils = require('dw/util/StringUtils');
var Pipelet = require('dw/system/Pipelet');
var Site = require('dw/system/Site');
var CatalogMgr = require('dw/catalog/CatalogMgr');
var logger = require('dw/system/Logger').getLogger('data.slot');
var SLOT_FOLDER_RELATIVE_PATH = 'src/data-management/slot';
var REFERENCE_FILE_NAME = 'reference-slots.xml';

/**
 * Exports current site slot configuration to the given file
 * @param {string} exportFileName - the export file name
 * @param {boolean} overwriteExportFile - Optional flag indicating whether to overwrite an existing export file or not. true=overwrite (default), false=don't overwrite. Default is true.
 * @returns {dw.system.Status} status object
 */
function exportSlots(exportFileName, overwriteExportFile) {
    return new Pipelet('ExportSlots').execute({
        ExportFile: StringUtils.format('{0}{1}{2}', SLOT_FOLDER_RELATIVE_PATH.replace('src/', ''), File.SEPARATOR, exportFileName),
        OverwriteExportFile: overwriteExportFile || true
    });
}

/**
 * Runs for reference Site and exports its content-slots
 * that target Site(s) will use the exported slots.xml as reference
 *
 * @param {dw.util.HashMap} args - the arguments
 * @param {dw.job.JobStepExecution} jobStepExecution - the job step execution object
 * @returns {dw.system.Status} status object
 */
function reference(args, jobStepExecution) { // eslint-disable-line no-unused-vars
    var slotsFolder = new File(StringUtils.format('{0}{1}{2}{1}', File.IMPEX, File.SEPARATOR, SLOT_FOLDER_RELATIVE_PATH));
    if (!slotsFolder.exists()) {
        slotsFolder.mkdirs();
    }

    var exportSlotsResult = exportSlots(REFERENCE_FILE_NAME, true);
    if (exportSlotsResult.Status && exportSlotsResult.Status.error) {
        logger.error(exportSlotsResult.Status.message);
        return exportSlotsResult.Status;
    }

    return new Status(Status.OK);
}

/**
 * Checks if the slot has already been configured at the target site
 * @param {XMLList} targetSlotConfigurationsXML - target site current slot configurations
 * @param {string} slotId - the slot id to be checked if the configuration is already defined
 */
function hasSlotConfiguration(targetSlotConfigurationsXML, slotId) {
    for(var i = 0; i < targetSlotConfigurationsXML.length(); i++ ){
        var targetSlotConfigurationXML = targetSlotConfigurationsXML[i];
        var targetSlotId = targetSlotConfigurationXML['@slot-id'].toString();
        if (targetSlotId === slotId) {
            return true;
        }
    }

    return false;
}

/**
 * Writes prepared xml to the target slot-configuration file
 * @param {dw.io.File} targetFile - the target file to be imported
 * @param {XML} newTargetSlotConfigurationsXML - the prepared XML slot-configurations to be imported
 * @returns {boolean} true if the file has been modified (means there are slot-configurations to be imported), false otherwise
 */
function writeXMLToFile(targetFile, newTargetSlotConfigurationsXML) {
    // check if there is slot-configurations to be imported
    // if not, then return
    if (!newTargetSlotConfigurationsXML || !newTargetSlotConfigurationsXML.children().length()) {
        return false;
    }
    var FileWriter = require('dw/io/FileWriter');
    var XMLStreamWriter = require('dw/io/XMLStreamWriter');

    var fileWriter = new FileWriter(targetFile, 'UTF-8', false);
    var xmlStreamWriter = new XMLStreamWriter(fileWriter);

    xmlStreamWriter.writeStartDocument('UTF-8', '1.0');
    xmlStreamWriter.writeCharacters('\n');
    xmlStreamWriter.writeRaw(newTargetSlotConfigurationsXML.toXMLString());


    xmlStreamWriter.close();
    fileWriter.close();

    return true;
}

/**
 * Validates the given xml file against the given xsd schema definition and returns a status reporting the results of the validation.
 * If the validation operation executed without any process error,
 * regardless of whether or not the XML file is valid, then the pipelet returns next.
 * If the given xml file or xsd schema file do not exist in the given path the pipelet exits in a pipelet error.
 * The status containes the status of the validation.
 * @param {string} importFileName - the import file name
 * @returns {dw.system.Status} status object
 */
function validateXMLFile(importFileName) {
    return new Pipelet('ValidateXMLFile').execute({
        File: StringUtils.format('{0}{1}{2}', SLOT_FOLDER_RELATIVE_PATH.replace('src/', ''), File.SEPARATOR, importFileName),
        Schema: 'slot.xsd'
    });
}

/**
 * Imports the content-slots.xml file to the current site
 * @param {string} importFileName - the import file name
 * @param {string} importMode - Import mode. Possible values are MERGE, UPDATE, DELETE, REPLACE.
 * @returns {dw.system.Status} status object
 */
function importSlots(importFileName, importMode) {
    return new Pipelet('ImportSlots').execute({
        ImportFile: StringUtils.format('{0}{1}{2}', SLOT_FOLDER_RELATIVE_PATH.replace('src/', ''), File.SEPARATOR, importFileName),
        ImportMode: importMode || 'MERGE'
    });
}

/**
 * Loads the reference slot xml file
 * Exports the current site slots
 * Compares with the reference file
 * If current site's slot doesn't have any configuration
 * then copy it to file to be imported to the current site
 * Lastly run ImportSlots to import prepared slot configuration xml
 * @param {dw.util.HashMap} args - the arguments
 * @param {dw.job.JobStepExecution} jobStepExecution - the job step execution object
 * @returns {dw.system.Status} status object
 */
function target(args, jobStepExecution) { // eslint-disable-line no-unused-vars
    var currentSite = Site.current;

    var message;
    var referenceFile = new File(StringUtils.format('{0}{1}{2}{1}{3}', File.IMPEX, File.SEPARATOR, SLOT_FOLDER_RELATIVE_PATH, REFERENCE_FILE_NAME));
    if (!referenceFile.exists()) {
        message = StringUtils.format('Site: {0}, The reference file could not be found, file: {1}', currentSite.ID, referenceFile.fullPath);
        logger.error(message);
        return new Status(Status.ERROR, '500', message);
    }

    var targetFileName = StringUtils.format('{0}-slots', currentSite.ID);
    var exportSiteSlotsResult = exportSlots(targetFileName, true);
    if (exportSiteSlotsResult.Status && exportSiteSlotsResult.Status.error) {
        logger.error(exportSiteSlotsResult.Status.message);
        return exportSiteSlotsResult.Status;
    }

    var targetFile = new File(StringUtils.format('{0}{1}{2}{1}{3}', File.IMPEX, File.SEPARATOR, SLOT_FOLDER_RELATIVE_PATH, targetFileName));
    if (!targetFile.exists()) {
        message = StringUtils.format('Site: {0}, The target file could not be found, file: {1}', currentSite.ID, targetFile.fullPath);
        logger.error(message);
        return new Status(Status.ERROR, '500', message);
    }

    var FileReader = require('dw/io/FileReader');
    var XMLStreamReader = require('dw/io/XMLStreamReader');
    var XMLStreamConstants = require('dw/io/XMLStreamConstants');

    // read reference slots xml
    var referenceFileReader = new FileReader(referenceFile, 'UTF-8');
    var referenceXmlStreamReader = new XMLStreamReader(referenceFileReader);
    var referenceSlotConfigurationsXML;
    var localElementName;
    while (referenceXmlStreamReader.hasNext()) {
        if (referenceXmlStreamReader.next() === XMLStreamConstants.START_ELEMENT) {
            localElementName = referenceXmlStreamReader.getLocalName();
            if (localElementName === 'slot-configurations') {
                referenceSlotConfigurationsXML = referenceXmlStreamReader.getXMLObject();
            }
        }
    }

    // read target slots xml
    var targetFileReader = new FileReader(targetFile, 'UTF-8');
    var targetXmlStreamReader = new XMLStreamReader(targetFileReader);
    var targetSlotConfigurationsXML;
    while (targetXmlStreamReader.hasNext()) {
        if (targetXmlStreamReader.next() === XMLStreamConstants.START_ELEMENT) {
            localElementName = targetXmlStreamReader.getLocalName();
            if (localElementName === 'slot-configurations') {
                targetSlotConfigurationsXML = targetXmlStreamReader.getXMLObject();
            }
        }
    }

    var namespace = referenceSlotConfigurationsXML.namespace();
    referenceSlotConfigurationsXML = referenceSlotConfigurationsXML.namespace::['slot-configuration'];
    targetSlotConfigurationsXML = targetSlotConfigurationsXML.namespace::['slot-configuration'];
    var newTargetSlotConfigurationsXML = <slot-configurations xmlns="http://www.demandware.com/xml/impex/slot/2008-09-08"></slot-configurations>;
    for(var i = 0; i < referenceSlotConfigurationsXML.length(); i++ ){
        var referenceSlotConfigurationXML = referenceSlotConfigurationsXML[i];
        var referenceSlotId = referenceSlotConfigurationXML['@slot-id'].toString();
        var referenceContext = referenceSlotConfigurationXML['@context'].toString();
        var referenceContextId = referenceSlotConfigurationXML['@context-id'].toString();
        var referenceConfigurationId = referenceSlotConfigurationXML['@configuration-id'].toString();

        if (!hasSlotConfiguration(targetSlotConfigurationsXML, referenceSlotId)) {
            switch(referenceContext) {
                case 'category':
                    // check if the category exists at the current site
                    if (referenceContextId && CatalogMgr.getCategory(referenceContextId)) {
                        newTargetSlotConfigurationsXML.appendChild(referenceSlotConfigurationXML.copy());
                    }
                case 'folder': // sites use one shared library, so no need existance check
                case 'global': // sites use same code base, so no need existance check
                default:
                    newTargetSlotConfigurationsXML.appendChild(referenceSlotConfigurationXML.copy());
            }

        }
    }

    referenceFileReader.close();
    referenceXmlStreamReader.close();

    targetFileReader.close();
    targetXmlStreamReader.close();

    // write prepared xml to the targetFile
    if (writeXMLToFile(targetFile, newTargetSlotConfigurationsXML)) {
        // validate xml file
        var validateXMLFileResult = validateXMLFile(targetFileName);
        if (validateXMLFileResult.Status && validateXMLFileResult.Status.error) {
            logger.error(validateXMLFileResult.Status.message);
            return validateXMLFileResult.Status;
        }

        // import xml file
        var importSiteSlotsResult = importSlots(targetFileName);
        if (importSiteSlotsResult.Status && importSiteSlotsResult.Status.error) {
            logger.error(importSiteSlotsResult.Status.message);
            return importSiteSlotsResult.Status;
        }
    }

    return new Status(Status.OK);
}

module.exports = {
    reference: reference,
    target: target
};
