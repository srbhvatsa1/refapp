'use strict';

module.exports = function (object, shipment) {
    if (!shipment) {
        return;
    }

    Object.defineProperty(object, 'specialRequest', {
        enumerable: true,
        value: shipment.custom.specialRequest
    });
};
