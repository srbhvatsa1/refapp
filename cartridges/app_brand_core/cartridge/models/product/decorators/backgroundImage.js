'use strict';

/**
 * Checks if given category has an ancestor category with given ancestorCategoryId
 * @param {*} category Category object that needs to be checked if has an ancestor with given id
 * @param {string} ancestorCategoryId Ancestor category id
 * @returns {boolean} true if given category has ancestor category with given id
 */
function hasCategoryAsAncestor(category, ancestorCategoryId) {
    if (!category.parent) {
        return false;
    }
    if (category.parent.ID === ancestorCategoryId) {
        return true;
    }
    return hasCategoryAsAncestor(category.parent, ancestorCategoryId);
}

/**
 * Gets category responsible of holding the product background image
 * @param {dw.catalog.Product} apiProduct product
 * @returns {dw.catalog.Category} category
 */
function getBackgroundImageCategory(apiProduct) {
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var catalog = CatalogMgr.getSiteCatalog();
    if (!catalog || !catalog.custom.productBackgroundImageCategory) {
        return null;
    }
    var backgroundImageCategoryId = catalog.custom.productBackgroundImageCategory;

    var categories = apiProduct.categories;
    if (categories && !categories.length && apiProduct.variant) {
        categories = apiProduct.masterProduct.categories;
    }

    for (var i = 0; i < categories.length; i++) {
        var category = categories[i];
        if (hasCategoryAsAncestor(category, backgroundImageCategoryId)) {
            return category;
        }
    }
    return null;
}

/**
 * Gets product background image from category, if cannot find and category has aparent then checks parent, otherwise returns null
 * @param {*} category Category object that needs to be checked if has productBackgroundImage configured
 * @returns {image} returns product background image from category
 */
function getProductBackgroundImageRecursively(category) {
    if (category && category.custom.productBackgroundImage) {
        return category.custom.productBackgroundImage;
    }
    if (category && category.parent) {
        return getProductBackgroundImageRecursively(category.parent);
    }
    return null;
}


/**
 * Finds background image of given product (either from product or its collection category) and returns its url
 * @param {dw.catalog.Product} apiProduct api product object
 * @returns {string} returns url of the image, null if not found
 */
function getBackgroundImage(apiProduct) {
    if (apiProduct.custom.backgroundImage) {
        return apiProduct.custom.backgroundImage.getURL().toString();
    }

    var backgroundImageCategory = getBackgroundImageCategory(apiProduct);
    var backgroundImageFromCategory = getProductBackgroundImageRecursively(backgroundImageCategory);
    if (backgroundImageFromCategory) {
        return backgroundImageFromCategory.getURL().toString();
    }
    return null;
}

module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'backgroundImage', {
        enumerable: true,
        value: getBackgroundImage(apiProduct)
    });
};
