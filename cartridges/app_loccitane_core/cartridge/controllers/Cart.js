'use strict';

var server = require('server');
server.extend(module.superModule);

/**
 * Calculates the unit and total prices of items
 * that are just added to cart.
 * Extended to disable to provide bonusDiscountLineItem
 */
server.append('AddProduct', function (req, res, next) {
    var viewData = res.getViewData();

    // disable pdp bonus items popup in loccitane
    viewData.newBonusDiscountLineItem = {};

    res.setViewData(viewData);

    next();
});

module.exports = server.exports();
