'use strict';

var originalExports = Object.keys(module.superModule);
originalExports.forEach(function (originalExport) {
    module.exports[originalExport] = module.superModule[originalExport];
});
var base = module.exports;

var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
var StoreMgr = require('dw/catalog/StoreMgr');

var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');
var collections = require('*/cartridge/scripts/util/collections');

/**
 * validates that the product line items exist, are online, and have available inventory.
 * @param {dw.order.Basket} basket - The current user's basket
 * @returns {Object} an error object
 */
function validateProducts(basket) {
    var result = {
        error: false,
        hasInventory: true,
        invalidProductMap: {}
    };
    if (!basket) {
        result.error = true;
        return result;
    }

    var productLineItems = basket.productLineItems;

    collections.forEach(productLineItems, function (item) {
        var hasInventory = true;
        if (item.product === null) {
            result.invalidProductMap[item.productID] = 'NO_SUCH_A_PRODUCT';
            result.error = true;
        } else if (!item.product.online
            || item.product.custom.localeOffline.value === 'true') {
            result.invalidProductMap[item.productID] = 'PRODUCT_IS_NOT_ONLINE';
            result.error = true;
        } else if (item.product.priceModel.price.available === false) {
            result.invalidProductMap[item.productID] = 'PRODUCT_HAS_NO_PRICE';
            result.error = true;
        } else if (Object.hasOwnProperty.call(item.custom, 'fromStoreId') && item.custom.fromStoreId) {
            var store = StoreMgr.getStore(item.custom.fromStoreId);
            var storeInventory = ProductInventoryMgr.getInventoryList(store.custom.inventoryListId);
            var storeInventoryRecord = storeInventory.getRecord(item.productID);
            hasInventory = storeInventoryRecord && storeInventoryRecord.ATS.value >= item.quantityValue;
        } else {
            var availabilityModel = inventoryHelpers.getProductAvailabilityModel(item.product);
            var availabilityLevels = availabilityModel.getAvailabilityLevels(item.quantityValue);
            hasInventory = availabilityLevels.notAvailable.value === 0;
        }

        if (!hasInventory) {
            result.invalidProductMap[item.productID] = 'PRODUCT_HAS_NO_INVENTORY';
            result.hasInventory = false;
        }
    });

    return result;
}

base.validateProducts = validateProducts;

module.exports = base;
