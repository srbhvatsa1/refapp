'use strict';

var server = require('server');
server.extend(module.superModule);

/**
 * Replace SetLocate Action
 * To set the locale id value to cookie site_locale value
 * And to redirect with host change if the countries locale has host value
 */
server.replace('SetLocale', function (req, res, next) {
    var countries = require('*/cartridge/config/countries');

    var localeId = req.querystring.code;
    var countryLocale = countries.filter(function (locale) {
        return locale.id === localeId;
    });
    countryLocale = (countryLocale && countryLocale.length) ? countryLocale[0] : null;

    if (!countryLocale) {
        res.json({ error: true });
        return next();
    }
    // qa.faces.com, kw.faces.com, eg.faces.com
    if ('externalHost' in countryLocale && countryLocale.externalHost) {
        var host = 'https://' + countryLocale.externalHost;
        res.json({
            success: true,
            redirectUrl: host
        });
        return next();
    }

    var URLUtils = require('dw/web/URLUtils');
    var URLAction = require('dw/web/URLAction');

    var QueryString = server.querystring;
    var currentSite = require('dw/system/Site').current;

    var queryStringObj = new QueryString(req.querystring.queryString || '');
    if (Object.hasOwnProperty.call(queryStringObj, 'lang')) {
        delete queryStringObj.lang;
    }

    var urlAction = new URLAction(req.querystring.action || 'Home-Show', countryLocale.siteId, localeId);
    var urlHelpers = require('*/cartridge/scripts/helpers/urlHelpers');
    var urlParameters = urlHelpers.normalizeQueryStringParameters(queryStringObj);
    var url = URLUtils.http(urlAction, urlParameters);

    if (countryLocale.siteId !== currentSite.ID) {
        res.json({
            success: true,
            redirectUrl: url.toString()
        });
        return next();
    }

    var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');

    if (localeHelpers.setLocale(localeId, countryLocale.currencyCode)) {
        res.json({
            success: true,
            redirectUrl: url.toString()
        });
    } else {
        res.json({ error: true });
    }

    return next();
});

server.get('IncludeMobileAccountMenu',
    server.middleware.include,
    server.middleware.get,
    function (req, res, next) {
        res.render('/account/mobileAccountMenu');
        next();
    });

module.exports = server.exports();
