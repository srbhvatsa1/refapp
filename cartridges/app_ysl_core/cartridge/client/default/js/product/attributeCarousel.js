'use strict';

var nextArrow = require('../templates/slickArrowNext');
var prevArrow = require('../templates/slickArrowPrev');
var carousel = require('brand_core/components/carousel');

/**
 * Initializes carousel
 * @param {jQuery} $container - Container element that holds carousel items
 */
function initCarousel($container) {
    carousel($container, {
        infinite: false,
        slidesToScroll: 1,
        variableWidth: true,
        nextArrow: nextArrow,
        prevArrow: prevArrow
    });
}

module.exports = function (selector) {
    selector = selector || '.js-attribute-carousel'; // eslint-disable-line

    initCarousel($(selector));

    $('.js-product-carousel-async').on('productLoader:loaded', function () {
        initCarousel($(this).find(selector));
    });

    $('[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(e.target.hash).find(selector).slick('setPosition');
    });

    $(document).on('search:success', function () {
        $(selector).not('.slick-initialized').each(function () {
            initCarousel($(this));
        });
    });
};
