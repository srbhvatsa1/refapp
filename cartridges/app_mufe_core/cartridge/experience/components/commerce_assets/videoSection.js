'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();
    model.imageURL = content.image.file.URL;
    model.title = content.title ? content.title : '';
    model.frame = content.frame;
    model.frameBorder = content.frameBorder ? content.frameBorder : 0;
    model.autoplay = '';
    if (content.autoplay) {
        model.autoplay = 'autoplay;';
    }

    return new Template('experience/components/commerce_assets/videoSection').render(model).text;
};
