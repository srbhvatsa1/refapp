'use strict';

/**
 * Cancel Order
 * @param {dw.order.Order} order order to be cancelled
 * @returns {Object} Status
 */
function execute(order) {
    var speedBusService = require('*/cartridge/scripts/services/speedBusService');
    var omsSpeedBusService = require('*/cartridge/scripts/services/omsSpeedBusService');
    var speedBusHelpers = require('*/cartridge/scripts/helpers/speedbusHelpers');
    var logger = require('dw/system/Logger').getLogger('speedbus');
    var cancelOrderModel = require('*/cartridge/models/speedbus/cancelOrder')(order);
    var Order = require('dw/order/Order');
    var StringUtils = require('dw/util/StringUtils');
    var Transaction = require('dw/system/Transaction');
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
    var Site = require('dw/system/Site');

    var requestPayload = {
        method: 'DELETE',
        body: cancelOrderModel
    };
    var serviceResult;
    var speedBusServiceType = Site.current.getCustomPreferenceValue('speedBusServiceType');

    if (speedBusServiceType && speedBusServiceType.value === 'oms') {
        serviceResult = omsSpeedBusService.call(requestPayload);
    } else {
        serviceResult = speedBusService.call(requestPayload);
    }
    var serviceErrorResponse = StringUtils.format('status: {0} errorMessage: {1}', serviceResult.status, serviceResult.errorMessage);
    if (serviceResult.ok) {
        // cancel order
        var serviceResponse = JSON.stringify(serviceResult.getObject());
        if (speedBusHelpers.isExportSucceeded(serviceResult.getObject())) {
            if (order.getExportStatus().value !== Order.EXPORT_STATUS_EXPORTED) {
                hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_EXPORTED, { subject: 'SpeedBus LAYDEL', text: serviceResponse }], function () {
                    Transaction.wrap(function () {
                        order.setExportStatus(Order.EXPORT_STATUS_EXPORTED);
                        order.addNote('SpeedBus LAYDEL', serviceResponse);
                    });
                });
                // SFSC update trigger
                speedBusHelpers.sfscOrderUpdateTrigger(order);
            }

            return {
                success: true
            };
        }
        if (order.getExportStatus().value !== Order.EXPORT_STATUS_FAILED) {
            hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_FAILED, { subject: 'SpeedBus LAYDEL ERROR', text: StringUtils.format('{0} {1}', serviceResponse, serviceErrorResponse) }], function () {
                Transaction.wrap(function () {
                    order.setExportStatus(Order.EXPORT_STATUS_FAILED);
                    order.addNote('SpeedBus LAYDEL ERROR', serviceResponse);
                });
            });
        }
        logger.error('While cancelling order {0} an unexpected issue is occured \n\n{1}\n\n{2}', order.orderNo, serviceResponse, serviceErrorResponse);
        return {
            success: false,
            message: serviceErrorResponse,
            raw: serviceResult
        };
    }
    if (order.getExportStatus().value !== Order.EXPORT_STATUS_FAILED) {
        hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_FAILED, { subject: 'SpeedBus LAYDEL ERROR', text: serviceErrorResponse }], function () {
            Transaction.wrap(function () {
                order.setExportStatus(Order.EXPORT_STATUS_FAILED);
                order.addNote('SpeedBus LAYDEL ERROR', serviceErrorResponse);
            });
        });
    }
    logger.error('While cancelling order {0} an unexpected issue is occured {1}', order.orderNo, serviceErrorResponse);
    return {
        success: false,
        message: serviceErrorResponse,
        raw: serviceResult
    };
}

module.exports = {
    execute: execute
};
