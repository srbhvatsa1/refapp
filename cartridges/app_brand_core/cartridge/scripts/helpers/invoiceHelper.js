'use strict';

var StringUtils = require('dw/util/StringUtils');

/**
 * Gets locales in certain order that you want to see the merged pdfs in (en first, ar second)
 * @returns {string[]} Array of locales
 */
function getLocalesOrdered() {
    var Site = require('dw/system/Site');
    var locales = Site.getCurrent().getAllowedLocales();
    locales.sort();
    locales.reverse();
    return locales.toArray();
}

/**
 * Finds nth index of given string
 * @param {string} str string to search in
 * @param {string} pattern searched string
 * @param {int} n index to start looking for at
 * @returns {int} nth index of pattern given in str
 */
function nthIndex(str, pattern, n) {
    var L = str.length;
    var i = -1;
    // eslint-disable-next-line no-param-reassign
    while (n-- && i++ < L) {
        i = str.indexOf(pattern, i);
        if (i < 0) break;
    }
    return i;
}

/**
 * Generates a name for the pdf invoice
 *
 * @param {string} orderNo orderNo
 * @param {string} locale locale
 *
 * @return {string} invoice file name
 */
function getInvoiceName(orderNo, locale) {
    var sortPrefix = '';
    if (locale) {
        var locales = getLocalesOrdered();
        sortPrefix = locales.indexOf(locale);
        if (sortPrefix === -1) {
            // sometimes locale come in a format like ar_AE_#u-nu-latn, so we extract the locale like this.
            var indexOfSecondUnderscore = nthIndex(locale, '_', 2);
            // eslint-disable-next-line no-param-reassign
            locale = locale.substr(0, indexOfSecondUnderscore);
            sortPrefix = locales.indexOf(locale);
        }
    }

    return locale ?
        StringUtils.format('{0}{1}_{2}.pdf', sortPrefix + 1, orderNo, locale.split('_')[0]) :
        StringUtils.format('{0}.pdf', orderNo);
}

/**
 * Gets path for invoice file (relative to IMPEX, IMPEX not included)
 *
 * @param {string} orderNo orderNo
 * @param {string} invoicePdfName name of the file
 *
 * @returns {string} relative path to invoice file
 */
function getInvoicePath(orderNo, invoicePdfName) {
    var Site = require('dw/system/Site');
    var currentSiteId = Site.getCurrent().getID();
    return StringUtils.format('src/invoices/{0}/{1}/{2}', currentSiteId, orderNo, invoicePdfName);
}

module.exports = {
    getInvoiceName: getInvoiceName,
    getInvoicePath: getInvoicePath,
    getLocalesOrdered: getLocalesOrdered
};
