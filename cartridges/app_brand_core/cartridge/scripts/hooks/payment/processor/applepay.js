'use strict';
/* global session, request, customer */

var Status = require('dw/system/Status');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var Logger = require('dw/system/Logger');
var Resource = require('dw/web/Resource');
var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');
var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

/**
 * Prepares the apple pay line items array
 * @param {dw.order.LineItemCtnr} lineItemCtnr - the LineItemCtnr object
 * @returns {Array} the prepared line items array
 */
function getLineItems(lineItemCtnr) {
    var lineItems = [];

    // subtotal
    lineItems.push({
        amount: lineItemCtnr.getAdjustedMerchandizeTotalPrice(false).value,
        label: Resource.msg('label.subtotal', 'cart', null),
        type: 'final'
    });

    // gift certificate
    var totalGiftCertificateAmount = paymentMethodHelpers.getLineItemCtnrPaymentMethodAmount(lineItemCtnr, PaymentInstrument.METHOD_GIFT_CERTIFICATE);
    if (totalGiftCertificateAmount && totalGiftCertificateAmount.value > 0) {
        lineItems.push({
            amount: totalGiftCertificateAmount.value,
            label: Resource.msg('label.giftcertificate', 'cart', null),
            type: 'final'
        });
    }

    // shipping
    lineItems.push({
        amount: lineItemCtnr.adjustedShippingTotalPrice.value,
        label: Resource.msg('label.shipping.cost', 'cart', null),
        type: 'final'
    });

    // tax
    lineItems.push({
        amount: lineItemCtnr.totalTax.value,
        label: Resource.msg('label.sales.tax', 'cart', null),
        type: 'final'
    });

    return lineItems;
}

/**
 * Prepares the apple pay total object
 * @param {dw.order.LineItemCtnr} lineItemCtnr - the LineItemCtnr object
 * @param {Object} total - the apple pay total object
 * @returns {Object} the prepared total object
 */
function getTotal(lineItemCtnr, total) {
    return {
        amount: basketCalculationHelpers.getNonComplimentaryPaymentAmount(lineItemCtnr).value,
        label: total.label,
        type: 'final'
    };
}

exports.authorizeOrderPayment = function (order, event) { // eslint-disable-line no-unused-vars
    var paymentInstruments = order.getPaymentInstruments(PaymentInstrument.METHOD_DW_APPLE_PAY);
    if (paymentInstruments.empty) {
        Logger.error('Unable to find Apple Pay payment instrument for order:{0}, responseData: {1}',
            order.orderNo,
            JSON.stringify(event)
        );
        return new Status(Status.ERROR);
    }
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

    // Handles payment authorization
    var handlePaymentResult = COHelpers.handlePayments(order, order.orderNo, event);
    if (handlePaymentResult.error) {
        return new Status(Status.ERROR);
    }

    return new Status(Status.OK);
};

exports.placeOrder = function (order) {
    var ApplePayHookResult = require('dw/extensions/applepay/ApplePayHookResult');
    var URLUtils = require('dw/web/URLUtils');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var Request = require('server/request');

    // create SFCC request object
    var req = new Request(
        typeof request !== 'undefined' ? request : {},
        typeof customer !== 'undefined' ? customer : {},
        typeof session !== 'undefined' ? session : {});

    // process processPlaceOrder common method
    var processPlaceOrderResult = COHelpers.processPlaceOrder(req, order);
    if (processPlaceOrderResult.error) {
        return new ApplePayHookResult(new Status(Status.ERROR), null);
    }

    return new ApplePayHookResult(new Status(Status.OK), URLUtils.url('ApplePay-Confirm', 'ID', order.orderNo, 'token', order.orderToken));
};

exports.getRequest = function (basket, request) { // eslint-disable-line no-unused-vars
    var PaymentMgr = require('dw/order/PaymentMgr');
    var HookMgr = require('dw/system/HookMgr');
    var StringUtils = require('dw/util/StringUtils');

    // check if the current payment processor's Apple Pay is enabled
    // and get the hided/shown areas
    var applePay = null;
    var applePayPaymentMethod = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_DW_APPLE_PAY);

    if (applePayPaymentMethod && applePayPaymentMethod.paymentProcessor) {
        var hook = StringUtils.format('app.payment.processor.{0}', applePayPaymentMethod.paymentProcessor.ID.toLowerCase());
        if (HookMgr.hasHook(hook)) {
            applePay = HookMgr.callHook(hook, 'ApplePay');
        }
    }

    if (!applePay) {
        if (session.custom.applepaysession) {
            delete session.custom.applepaysession;
        }

        if (session.custom.applepayshowonpdp) {
            delete session.custom.applepayshowonpdp;
        }

        if (session.custom.applepayshowoncart) {
            delete session.custom.applepayshowoncart;
        }

        return;
    }

    session.custom.applepaysession = 'yes';
    session.custom.applepayshowonpdp = applePay.showOnPDP ? 'yes' : 'no';
    session.custom.applepayshowoncart = applePay.showOnCart ? 'yes' : 'no';

    request.lineItems = getLineItems(basket); // eslint-disable-line no-param-reassign
    request.total = getTotal(basket, request.total); // eslint-disable-line no-param-reassign
    request.shippingMethods = []; // eslint-disable-line no-param-reassign
};

exports.paymentMethodSelected = function (basket, event, response) { // eslint-disable-line no-unused-vars
    response.lineItems = getLineItems(basket); // eslint-disable-line no-param-reassign
    response.total = getTotal(basket, response.total); // eslint-disable-line no-param-reassign
    response.shippingMethods = []; // eslint-disable-line no-param-reassign
};

exports.createOrder = function (basket, event) { // eslint-disable-line
    /* TODO: subtract gift certificate amount
    var applePayPaymentInstrument = basket.getPaymentInstruments(PaymentInstrument.METHOD_DW_APPLE_PAY)[0];
    var totalApplePayAmount;
    if (basket.custom.giftCertificateAppliedAmount && basket.custom.giftCertificateAppliedAmount > 0) {
        // TODO: re apply gift certificate as payment instrument

        var totalGiftCertificateAmount = paymentMethodHelpers.getLineItemCtnrPaymentMethodAmount(basket, PaymentInstrument.METHOD_GIFT_CERTIFICATE);

        totalApplePayAmount = applePayPaymentInstrument.paymentTransaction.amount;
        applePayPaymentInstrument.paymentTransaction.setAmount(totalApplePayAmount.subtract(totalGiftCertificateAmount));
    }
    */

    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

    basketCalculationHelpers.calculateTotals(basket);
    return COHelpers.createOrder(basket);
};
