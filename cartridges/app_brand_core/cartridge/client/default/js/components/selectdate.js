'use strict';

var moment = require('moment');

(function ($) {
    var SelectDate = function (element) {
        this.init(element);
    };

    SelectDate.prototype = {
        constructor: SelectDate,
        init: function (element) {
            this.map = {
                // key: [regexp, moment.method]
                day: ['D', 'date'],
                month: ['M', 'month'],
                year: ['Y', 'year']
            };
            this.$element = $(element);
            this.$day = this.$element.find('select.js-day');
            this.$month = this.$element.find('select.js-month');
            this.$year = this.$element.find('select.js-year');
            this.$date = this.$element.find('input.js-date');

            // update original input on change
            this.$element.on('change', 'select', $.proxy(function () {
                this.$date.val(this.getValue()).change();
            }, this));
        },

        /*
         Returns current date value from selects.
        */
        getValue: function () {
            var dt;
            var values = {};
            var that = this;
            var notSelected = false;

            // getting selected values
            $.each(this.map, function (k) {
                var def = k === 'day' ? 1 : 0;

                values[k] = that['$' + k] ? parseInt(that['$' + k].val(), 10) : def;

                if (isNaN(values[k])) {
                    notSelected = true;
                    return false;
                }

                return true;
            });

            // if at least one visible select not selected - return empty string
            if (notSelected) {
                return '';
            }

            dt = moment([values.year, values.month, values.day]);

            return dt.isValid() ? dt.format('MM/DD/YYYY') : '';
        },

        setValue: function (value) { // eslint-disable-line no-unused-vars

        },

        /*
         highlight selects if date is invalid
        */
        highlight: function (dt) { // eslint-disable-line no-unused-vars
        },

        leadZero: function (v) {
            return v <= 9 ? '0' + v : v;
        },

        destroy: function () {
        }
    };

    $.fn.selectdate = function () { // eslint-disable-line no-param-reassign
        new SelectDate($(this)); // eslint-disable-line no-new
    };
}(window.jQuery));

module.exports = {
    init: function () {
        if (!$('.js-selectdate').length) {
            return;
        }

        var $selectdates = $('.js-selectdate');

        $selectdates.each(function () {
            $selectdates.selectdate();
        });
    }
};
