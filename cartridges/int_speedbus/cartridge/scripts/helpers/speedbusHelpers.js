'use strict';

var HookMgr = require('dw/system/HookMgr');

/**
 * SFSC update trigger
 * @param {dw.order.Order} order - The current order
 */
function sfscOrderUpdateTrigger(order) {
    var hookId = 'app.order.updated';
    if (HookMgr.hasHook(hookId)) {
        HookMgr.callHook(
            hookId,
            'updated',
            order
        );
    }
}
/**
 * Checks validty of response from Speedbus
 * @param {Object} responseBody response from Service
 * @returns {boolean} true or false
 */
function isExportSucceeded(responseBody) {
    if (!!responseBody && 'Response' in responseBody && responseBody.Response === 'Success') {
        return true;
    }
    return false;
}

var baseTenderTypeGroups = {
    COD: 1000,
    Visa: 3000,
    'Master Card': 3010,
    Amex: 3020,
    GIFT_CERTIFICATE: 4030,
    PREPAID: 3000,
    DW_APPLE_PAY: 6003,
    KNET: 3041
};
/**
 * Returns speedbus tender type group code
 * @param {dw.order.PaymentInstrument} paymentInstrument paymentInstrument
 * @returns {int}  Returns code or null
 */
function getTenderTypeGroupCode(paymentInstrument) {
    var tenderTypeGroupCode = null;
    var speedBusTenderTypeGroups = require('dw/system/Site').getCurrent().getCustomPreferenceValue('speedBusTenderTypeGroup');
    try {
        speedBusTenderTypeGroups = speedBusTenderTypeGroups ? JSON.parse(speedBusTenderTypeGroups) : baseTenderTypeGroups;
    } catch (e) {
        require('dw/system/Logger')
            .getLogger('speedbus')
            .error('While parsing speedbus tender type groups and error occured {0}', e.message);
        speedBusTenderTypeGroups = baseTenderTypeGroups;
    }

    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');

    switch (paymentInstrument.paymentMethod) {
        case PaymentInstrument.METHOD_CREDIT_CARD:
            var paymentCardType = paymentInstrument.getCreditCardType();
            tenderTypeGroupCode = paymentCardType in speedBusTenderTypeGroups ? speedBusTenderTypeGroups[paymentCardType] : null;
            break;
        case PaymentInstrument.METHOD_DW_APPLE_PAY:
            tenderTypeGroupCode = speedBusTenderTypeGroups.DW_APPLE_PAY;
            break;
        case paymentMethodHelpers.getCODPaymentMethodId():
            tenderTypeGroupCode = speedBusTenderTypeGroups.COD;
            break;
        case PaymentInstrument.METHOD_GIFT_CERTIFICATE:
            tenderTypeGroupCode = speedBusTenderTypeGroups.GIFT_CERTIFICATE;
            break;
        case paymentMethodHelpers.getPrePaidPaymentMethodId():
            tenderTypeGroupCode = speedBusTenderTypeGroups.PREPAID;
            break;
        case paymentMethodHelpers.getKnetPaymentMethodId():
            tenderTypeGroupCode = speedBusTenderTypeGroups.KNET;
            break;
        default:
            tenderTypeGroupCode = null;
            break;
    }

    return tenderTypeGroupCode;
}

module.exports = {
    sfscOrderUpdateTrigger: sfscOrderUpdateTrigger,
    isExportSucceeded: isExportSucceeded,
    getTenderTypeGroupCode: getTenderTypeGroupCode
};
