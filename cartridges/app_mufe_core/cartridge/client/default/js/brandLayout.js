'use strict';

$(document).ready(function () {
    var $div = $('.js-about-us-banner');
    if ($div.length) {
        var bg = $div.data('bg');
        if (bg.length) {
            $div.css('background-image', 'url(' + bg + ')');
        }
    }
});
