'use strict';

var Status = require('dw/system/Status');
var PaymentMgr = require('dw/order/PaymentMgr');
var collections = require('*/cartridge/scripts/util/collections');


/**
 * Adds COD payment instrument to basket as default to get the service fee on basket
 * @param {dw.order.Basket} basket - the basket based on which the order is created
 * @param {dw.order.Shipment} shipment - the shipment information for the shipment creation
 * @param {Object} shippingMethod - the shipping method that to be/was set to the shipment
 */
function beforePutShippingMethod(basket, shipment, shippingMethod) {
    if(request.clientId === "dw.csc") {
        var paymentMethod = PaymentMgr.getPaymentMethod('COD');
        if (paymentMethod && paymentMethod.active) {
            var paymentInstruments = basket.getPaymentInstruments();

            collections.forEach(paymentInstruments, function (item) {
                basket.removePaymentInstrument(item);
            });

            basket.createPaymentInstrument(
                'COD', basket.totalGrossPrice
            );
        }
    }

    return new Status(Status.OK);
}

module.exports = {
    beforePutShippingMethod: beforePutShippingMethod
};
