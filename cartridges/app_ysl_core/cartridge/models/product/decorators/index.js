'use strict';

var base = module.superModule;

base.colorFamilies = require('*/cartridge/models/product/decorators/colorFamilies');

module.exports = base;
