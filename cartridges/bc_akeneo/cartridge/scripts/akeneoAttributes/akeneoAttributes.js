var ArrayList = require('dw/util/ArrayList');
var Logger = require('dw/system/Logger');

var GeneralUtils = require('~/cartridge/scripts/utils/generalUtils');
var config = GeneralUtils.config;
var customCacheMgr = require('~/cartridge/scripts/io/customCacheWebdav');
var AkeneoServicesHandler = require('~/cartridge/scripts/utils/akeneoServicesHandler');
var akeneoService = require('~/cartridge/scripts/akeneoServices/initAkeneoServices');
var libStringUtils = require('~/cartridge/scripts/utils/libStringUtilsExt');

var pageCounter;
var prodCustomAttrsMapping;
var prodSystemAttrsMapping;
var advancedImportConfigAttrs;

var akeneoAttributes = {};

/**
 * @desc Write an xml element
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 * @param {string} elementName - element name
 * @param {string} chars - chars
 * @param {string} attrKey - attr key
 * @param {string} attrValue - attr value
 */
function writeElement(xswHandle, elementName, chars, attrKey, attrValue) {
    xswHandle.writeStartElement(elementName);
    if (attrKey && attrValue) {
        xswHandle.writeAttribute(attrKey, attrValue);
    }
    xswHandle.writeCharacters(chars);
    xswHandle.writeEndElement();
}

/**
 * @desc Write Header & Static part of system object type extension
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 */
function writeAkeneoAttributesHeader(xswHandle) {
    // XML definition & first node
    xswHandle.writeStartDocument('UTF-8', '1.0');
    xswHandle.writeStartElement('metadata');
    xswHandle.writeAttribute('xmlns', 'http://www.demandware.com/xml/impex/metadata/2006-10-31');

    xswHandle.writeStartElement('type-extension');
    xswHandle.writeAttribute('type-id', 'Product');
}

/**
 * @desc Write Akeneo Attributes Group Definition XML part
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 * @param {dw.util.ArrayList} akeneoAttributesList - list of akeneo attrs
 */
function writeAkeneoAttributesGroupDefinitions(xswHandle, akeneoAttributesList) {
    xswHandle.writeStartElement('group-definitions');
    xswHandle.writeStartElement('attribute-group');
    xswHandle.writeAttribute('group-id', 'AkeneoAttributes');
    writeElement(xswHandle, 'display-name', 'Akeneo Attributes', 'xml:lang', 'x-default');
    var productAttributes = GeneralUtils.getCustomAttributeDefinitions('Product');
    for (var i = 0; i < akeneoAttributesList.length; i++) {
        var akeneoAttribute = akeneoAttributesList[i];
        var camelizedAttributeId = libStringUtils.camelize(akeneoAttribute.code);
        var attributeId = 'akeneo_' + camelizedAttributeId;
        // if attribute is already part of definitions and the type the same then attribute id is fine
        if (camelizedAttributeId in productAttributes) {
            continue;
        } else if (attributeId in prodCustomAttrsMapping.matching && prodCustomAttrsMapping.matching[attributeId]) {
            attributeId = prodCustomAttrsMapping.matching[attributeId];
            if (attributeId in productAttributes) {
                continue;
            }
        }
        if (attributeId.indexOf('akeneo_') !== -1) {
            writeElement(xswHandle, 'attribute', '', 'attribute-id', attributeId);
        }
    }

    // close xml attribute-group
    xswHandle.writeEndElement();

    // close xml group-definitions
    xswHandle.writeEndElement();
}

/**
 * @desc Write Footer & Static part of system object type extension
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 */
function writeAkeneoAttributesFooter(xswHandle) {
    // close xml type-extension
    xswHandle.writeEndElement();

    // XML definition & close first node
    xswHandle.writeEndElement();
    xswHandle.writeEndDocument();
    xswHandle.flush();
}

/**
 * @desc Write specific nodes for attr type Set of Strings
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 * @param {Object} AkeneoAttribute - attr object
 */
function writeAkeneoAttributeAssets(xswHandle, AkeneoAttribute) {
    writeElement(xswHandle, 'type', 'set-of-string');
    writeElement(xswHandle, 'localizable-flag', AkeneoAttribute.localizable.toString());
    writeElement(xswHandle, 'mandatory-flag', false);
    writeElement(xswHandle, 'externally-managed-flag', false);
}

/**
 * @desc Write specific nodes for attr type Image
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 * @param {Object} AkeneoAttribute - attr object
 */
function writeAkeneoAttributeImage(xswHandle, AkeneoAttribute) {
    writeElement(xswHandle, 'type', 'image');
    writeElement(xswHandle, 'localizable-flag', AkeneoAttribute.localizable.toString());
    writeElement(xswHandle, 'mandatory-flag', false);
    writeElement(xswHandle, 'externally-managed-flag', false);
}

/**
 * @desc Write specific nodes for attr type Boolean
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 */
function writeAkeneoAttributeBoolean(xswHandle) {
    writeElement(xswHandle, 'type', 'boolean');
    writeElement(xswHandle, 'site-specific-flag', false);
    writeElement(xswHandle, 'mandatory-flag', false);
    writeElement(xswHandle, 'visible-flag', false);
    writeElement(xswHandle, 'externally-managed-flag', false);
    writeElement(xswHandle, 'order-required-flag', false);
    writeElement(xswHandle, 'externally-defined-flag', false);
}

/**
 * @desc Write specific nodes for attr type Text Area
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 * @param {Object} AkeneoAttribute - attr object
 */
function writeAkeneoAttributeTextArea(xswHandle, AkeneoAttribute) {
    if (AkeneoAttribute.wysiwyg_enabled === true) {
        writeElement(xswHandle, 'type', 'html');
    } else {
        writeElement(xswHandle, 'type', 'text');
    }
    writeElement(xswHandle, 'localizable-flag', AkeneoAttribute.localizable.toString());
    writeElement(xswHandle, 'site-specific-flag', false);
    writeElement(xswHandle, 'mandatory-flag', false);
    writeElement(xswHandle, 'visible-flag', false);
    writeElement(xswHandle, 'externally-managed-flag', false);
    writeElement(xswHandle, 'order-required-flag', false);
    writeElement(xswHandle, 'externally-defined-flag', false);
}

/**
 * @desc Write specific nodes for attr type Text
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 * @param {Object} AkeneoAttribute - attr object
 */
function writeAkeneoAttributeText(xswHandle, AkeneoAttribute) {
    writeElement(xswHandle, 'type', 'string');
    writeElement(xswHandle, 'localizable-flag', AkeneoAttribute.localizable.toString());
    writeElement(xswHandle, 'site-specific-flag', false);
    writeElement(xswHandle, 'mandatory-flag', false);
    writeElement(xswHandle, 'visible-flag', false);
    writeElement(xswHandle, 'externally-managed-flag', false);
    writeElement(xswHandle, 'order-required-flag', false);
    writeElement(xswHandle, 'externally-defined-flag', false);
    writeElement(xswHandle, 'min-length', '0');
}

/**
 * @desc Write specific nodes for attr type Date
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 * @param {Object} AkeneoAttribute - attr object
 */
function writeAkeneoAttributeDate(xswHandle, AkeneoAttribute) {
    writeElement(xswHandle, 'type', 'date');
    writeElement(xswHandle, 'localizable-flag', AkeneoAttribute.localizable.toString());
    writeElement(xswHandle, 'site-specific-flag', false);
    writeElement(xswHandle, 'mandatory-flag', false);
    writeElement(xswHandle, 'visible-flag', false);
    writeElement(xswHandle, 'externally-managed-flag', false);
    writeElement(xswHandle, 'order-required-flag', false);
    writeElement(xswHandle, 'externally-defined-flag', false);
    writeElement(xswHandle, 'min-length', '0');
}

/**
 * @desc Write specific nodes for attr type Number
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 * @param {Object} AkeneoAttribute - attr object
 */
function writeAkeneoAttributeNumber(xswHandle, AkeneoAttribute) {
    writeElement(xswHandle, 'type', 'double');
    writeElement(xswHandle, 'localizable-flag', AkeneoAttribute.localizable.toString());
    writeElement(xswHandle, 'site-specific-flag', false);
    writeElement(xswHandle, 'mandatory-flag', false);
    writeElement(xswHandle, 'visible-flag', false);
    writeElement(xswHandle, 'externally-managed-flag', false);
    writeElement(xswHandle, 'order-required-flag', false);
    writeElement(xswHandle, 'externally-defined-flag', false);
}

/**
 * @desc Writes attribute options to XML
 * @param {Array} attributeOptions - list of attribute options
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML stream writer
 */
function writeAttributeOptionsXML(attributeOptions, xswHandle) {
    if (attributeOptions) {
        for (var i = 0; i < attributeOptions.length; i++) {
            var attributeOption = attributeOptions[i];
            xswHandle.writeStartElement('value-definition');

            var localeKeys = Object.keys(attributeOption.labels);
            for (var j = 0; j < localeKeys.length; j++) {
                var localeKey = localeKeys[j];
                var label = attributeOption.labels[localeKey];

                if (label) {
                    writeElement(xswHandle, 'display', label.toString(), 'xml:lang', localeKey.replace('_', '-').toString());
                }
            }

            writeElement(xswHandle, 'value', attributeOption.code);

            // close XML value-definition
            xswHandle.writeEndElement();
        }
    }
}

/**
 * Gets attribute options from cache
 * @param {Object} akeneoAttribute -akeneoAttribute
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 */
function getAttributeOptions(akeneoAttribute, xswHandle) {
    var optionFileCount = customCacheMgr.getAttributeOptionFileCount(config.APIURL.endpoints.attributes + '/' + akeneoAttribute.code);
    var attributeOptions;

    xswHandle.writeStartElement('value-definitions');

    if (optionFileCount > 1) {
        for (var i = 1; i < optionFileCount; i++) {
            attributeOptions = customCacheMgr.getCache(config.APIURL.endpoints.attributes + '/' + akeneoAttribute.code + '/options' + i);
            writeAttributeOptionsXML(attributeOptions, xswHandle);
        }
    }

    attributeOptions = customCacheMgr.getCache(config.APIURL.endpoints.attributes + '/' + akeneoAttribute.code + '/options');
    writeAttributeOptionsXML(attributeOptions, xswHandle);

    // close XML value-definitions
    xswHandle.writeEndElement();
}

/**
 * @desc Write specific nodes for attr type Simple Select & Multi Select
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 * @param {Object} akeneoAttribute - attr object
 */
function writeAkeneoAttributeSelect(xswHandle, akeneoAttribute) {
    writeElement(xswHandle, 'type', 'enum-of-string');
    writeElement(xswHandle, 'localizable-flag', true);
    writeElement(xswHandle, 'site-specific-flag', false);
    writeElement(xswHandle, 'mandatory-flag', false);
    writeElement(xswHandle, 'visible-flag', false);
    writeElement(xswHandle, 'externally-managed-flag', false);
    writeElement(xswHandle, 'order-required-flag', false);
    writeElement(xswHandle, 'externally-defined-flag', false);

    // multi select case
    if (akeneoAttribute.type === 'pim_catalog_multiselect' || akeneoAttribute.type === 'akeneo_reference_entity_collection') {
        writeElement(xswHandle, 'select-multiple-flag', 'true');
    }

    getAttributeOptions(akeneoAttribute, xswHandle);
}

/**
 * @desc Write Akeneo Attributes XML part
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 * @param {Array} akeneoAttributesList - list of akeneo attrs
 */
function writeAkeneoAttributes(xswHandle, akeneoAttributesList) {
    xswHandle.writeStartElement('custom-attribute-definitions');
    var productAttributes = GeneralUtils.getCustomAttributeDefinitions('Product');
    for (var i = 0; i < akeneoAttributesList.length; i++) {
        var AkeneoAttribute = akeneoAttributesList[i];
        var camelizedAttributeId = libStringUtils.camelize(AkeneoAttribute.code);
        var attributeId = 'akeneo_' + camelizedAttributeId;
        // if attribute is already part of definitions and the type the same then attribute id is fine

        if (attributeId in prodCustomAttrsMapping.matching && prodCustomAttrsMapping.matching[attributeId]) {
            attributeId = prodCustomAttrsMapping.matching[attributeId];
            if (attributeId in productAttributes) {
                // if it is a system attribute or type is not equals put akeneo_ prefix.
                if (productAttributes[attributeId].system === true || productAttributes[attributeId].type !== GeneralUtils.getAkeneoAttributeType(AkeneoAttribute)) {
                    attributeId = 'akeneo_' + camelizedAttributeId;
                }
            }
        } else if (camelizedAttributeId in productAttributes) {
            // if it is a system attribute or type is not equals put akeneo_ prefix.
            if (productAttributes[camelizedAttributeId].system === true || productAttributes[camelizedAttributeId].type !== GeneralUtils.getAkeneoAttributeType(AkeneoAttribute)) {
                attributeId = 'akeneo_' + camelizedAttributeId;
            } else {
                attributeId = camelizedAttributeId;
            }
        }

        xswHandle.writeStartElement('attribute-definition');
        xswHandle.writeAttribute('attribute-id', attributeId);

        // writing localizable name of attr
        if (typeof AkeneoAttribute.labels === 'object') {
            var localeKeys = Object.keys(AkeneoAttribute.labels);
            for (var j = 0; j < localeKeys.length; j++) {
                var localeKey = localeKeys[j];
                var attrLabel = AkeneoAttribute.labels[localeKey];

                if (attrLabel) {
                    writeElement(xswHandle, 'display-name', attrLabel.toString(), 'xml:lang', localeKey.replace('_', '-').toString());
                }
            }
        } else {
            writeElement(xswHandle, 'display-name', AkeneoAttribute.labels.fr_FR, 'xml:lang', 'x-default');
        }

        // Order of the nodes matter in XML file for being imported by SalesForce
        switch (AkeneoAttribute.type) {
            case 'pim_catalog_boolean' :
                writeAkeneoAttributeBoolean(xswHandle);
                break;
            case 'pim_catalog_simpleselect' :
            case 'pim_catalog_multiselect' :
                if (AkeneoAttribute.code.indexOf('response') > -1) {
                    writeAkeneoAttributeAssets(xswHandle, AkeneoAttribute);
                } else {
                    writeAkeneoAttributeSelect(xswHandle, AkeneoAttribute);
                }
                break;
            case 'pim_catalog_file' :
            case 'pim_catalog_text' :
            case 'pim_catalog_metric' :
            case 'pim_reference_data_simpleselect' :
                writeAkeneoAttributeText(xswHandle, AkeneoAttribute);
                break;
            case 'pim_catalog_number' :
                writeAkeneoAttributeNumber(xswHandle, AkeneoAttribute);
                break;
            case 'pim_catalog_date' :
                writeAkeneoAttributeDate(xswHandle, AkeneoAttribute);
                break;
            case 'pim_catalog_image' :
                writeAkeneoAttributeImage(xswHandle, AkeneoAttribute);
                break;
            case 'pim_assets_collection' :
            case 'pim_catalog_price_collection' :
            case 'pim_reference_data_multiselect' :
            case 'akeneo_reference_entity_collection' :
            case 'akeneo_reference_entity' :
                writeAkeneoAttributeAssets(xswHandle, AkeneoAttribute);
                break;
            case 'pim_catalog_textarea' :
            default :
                writeAkeneoAttributeTextArea(xswHandle, AkeneoAttribute);
                break;
        }

        // close XML attribute-definition
        xswHandle.writeEndElement();
    }

    // close xml custom-attribute-definitions
    xswHandle.writeEndElement();
}

/**
 * @desc Get the Akeneo import configured attribute from BM preference.
 * @returns {Array} - config attrs
 */
function getAdvancedImportConfigAttrs() {
    var configAttrs = [];

    if (config.importType === 'advanced') {
        var importConfigObj = config.productsImportBuilderConfig;
        configAttrs = importConfigObj.attributes ? importConfigObj.attributes : [];

        importConfigObj = config.modelProductsImportBuilderConfig;
        var modelProductImportConfigObj = importConfigObj.attributes ? importConfigObj.attributes : [];

        for (var i = 0; i < modelProductImportConfigObj.length; i++) {
            if (configAttrs.indexOf(modelProductImportConfigObj[i]) < 0) {
                configAttrs.push(modelProductImportConfigObj[i]);
            }
        }
    }

    return configAttrs;
}

/**
 * @desc Keeps attributes options and keeps in cache
 * @param {Object} akeneoAttribute - attribute object
 * @param {dw.svc.Service} AkeneoAttributesService - service object
 * @returns {void}
 */
function keepAttrOptionsInCache(akeneoAttribute, AkeneoAttributesService) {
    if (akeneoAttribute.type === 'pim_catalog_simpleselect' || akeneoAttribute.type === 'pim_catalog_multiselect') {
        var attrOptionsUrl = config.serviceGeneralUrl + config.APIURL.endpoints.attributes + '/' + akeneoAttribute.code + '/options';
        AkeneoServicesHandler.nextUrl = attrOptionsUrl + '?limit=' + GeneralUtils.config.APIURL.parameter.pagination;
        AkeneoServicesHandler.serviceAttributeOptions(AkeneoAttributesService, attrOptionsUrl);
    }
}

/**
 * @desc Keeps Attribute in custom cache
 * @param {Object} akeneoAttribute - attribute object
 * @param {dw.svc.Service} AkeneoAttributesService - service object
 */
function keepAttributeInCache(akeneoAttribute, AkeneoAttributesService) {
    var individualAttrUrl = config.APIURL.endpoints.attributes + '/' + akeneoAttribute.code;

    customCacheMgr.setCache(individualAttrUrl, akeneoAttribute);
    keepAttrOptionsInCache(akeneoAttribute, AkeneoAttributesService);
}

/**
 * @desc This function write main part of xml attributes in Impex file.
 * @param {Array} akeneoAttributesList - list of akeneo attrs
 */
function writeAkeneoAttributesXML(akeneoAttributesList) {
    var File = require('dw/io/File');
    var FileUtils = require('~/cartridge/scripts/io/libFileUtils').FileUtils;
    var FileWriter = require('dw/io/FileWriter');
    var XMLIndentingStreamWriter = require('dw/io/XMLIndentingStreamWriter');
    var StringUtils = require('dw/util/StringUtils');
    var Calendar = require('dw/util/Calendar');

    var AKENEO_ATTRS_FLUX_DIR = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'akeneo' + File.SEPARATOR + 'attributes' + File.SEPARATOR;
    var AKENEO_ATTRS_FILE_PATH = 'system-objecttype-extensions-' + pageCounter + '-' + StringUtils.formatCalendar(new Calendar(), 'yyyMMddHHmmss') + '.xml';

    var file = new File(AKENEO_ATTRS_FLUX_DIR + AKENEO_ATTRS_FILE_PATH);

    FileUtils.createFileAndFolders(file);

    var fwHandle; // FileWriter;
    var xswHandle; // XMLIndentingStreamWriter;

    try {
        // Definition of file handler
        fwHandle = new FileWriter(file);
        xswHandle = new XMLIndentingStreamWriter(fwHandle);
        writeAkeneoAttributesHeader(xswHandle);
        writeAkeneoAttributes(xswHandle, akeneoAttributesList);
        writeAkeneoAttributesGroupDefinitions(xswHandle, akeneoAttributesList);
        writeAkeneoAttributesFooter(xswHandle);
    } catch (e) {
        throw new Error('ERROR : While writing XML Attributes file : ' + e.stack + ' with Error: ' + e.message);
    } finally {
        if (xswHandle != null) {
            xswHandle.close();
        }
        if (fwHandle != null) {
            fwHandle.close();
        }
    }
}

/**
 * @desc Handles single page content
 * @param {dw.util.ArrayList} akeneoAttributesList - akeneo attributes list
 * @returns {void}
 */
function handleSinglePage(akeneoAttributesList) {
    var attributesList = [];
    var attrConsider;
    var AkeneoAttributesService = akeneoService.getGeneralService();
    var imageCodes = customCacheMgr.getCache(config.cacheDirectory.attributes.imageCodesList) || [];
    var assetCodes = customCacheMgr.getCache(config.cacheDirectory.attributes.assetCodesList) || [];

    var attributesIterator = akeneoAttributesList.iterator();

    while (attributesIterator.hasNext()) {
        var akeneoAttribute = attributesIterator.next();
        attrConsider = false;

        if (!('akeneo_' + akeneoAttribute.code in prodSystemAttrsMapping.matching)) {
            if (advancedImportConfigAttrs.length) { // if advanced import is configured
                if (advancedImportConfigAttrs.indexOf(akeneoAttribute.code) > -1) {
                    attrConsider = true;
                }
            } else {
                attrConsider = true;
            }
        }

        if (attrConsider) {
            attributesList.push(akeneoAttribute);
            keepAttributeInCache(akeneoAttribute, AkeneoAttributesService);

            if (akeneoAttribute.type === 'pim_catalog_image') {
                imageCodes.push(akeneoAttribute.code);
            } else if (akeneoAttribute.type === 'pim_catalog_asset_collection') {
                assetCodes.push(akeneoAttribute.code);
            }
        }
    }

    customCacheMgr.setCache(config.cacheDirectory.attributes.imageCodesList, imageCodes);
    customCacheMgr.setCache(config.cacheDirectory.attributes.assetCodesList, assetCodes);

    if (attributesList && attributesList.length > 0) {
        writeAkeneoAttributesXML(attributesList);
    } else {
        Logger.error('ERROR : No attributes retrieved from API Akeneo');
    }
}

/**
 * @desc This function will call Akeneo API with ServiceHandler.ds then build xml file corresponding to salesforce's attributes
 * The AkeneoService is initialized with generic url present in Site Preference.
 */
akeneoAttributes.generateAttributesXml = function () {
    if (!config.serviceGeneralUrl) {
        throw new Error('ERROR : Site Preference is missing : akeneoServiceGeneralUrl');
    }

    var attributesListPerPage = new ArrayList();
    var debugConfig = config.debug;
    var AkeneoAttributesService = akeneoService.getGeneralService();
    var attributesApiNextUrl = config.serviceGeneralUrl + config.APIURL.endpoints.attributes + '?limit=' + config.APIURL.parameter.pagination;

    prodCustomAttrsMapping = config.customAttrsMapping;
    prodSystemAttrsMapping = config.systemAttrsMapping;
    advancedImportConfigAttrs = getAdvancedImportConfigAttrs();
    AkeneoServicesHandler.nextUrl = attributesApiNextUrl;
    pageCounter = 0;
    customCacheMgr.clearCache(config.APIURL.endpoints.attributes);

    do {
        attributesListPerPage = AkeneoServicesHandler.serviceRequestAttributesAkeneo(AkeneoAttributesService);
        attributesApiNextUrl = AkeneoServicesHandler.nextUrl;
        handleSinglePage(attributesListPerPage);
        pageCounter++;

        if (debugConfig.breakCodeOnLimit && pageCounter >= debugConfig.pageLimit) {
            break;
        }

        AkeneoServicesHandler.nextUrl = attributesApiNextUrl;
    } while (AkeneoServicesHandler.nextUrl !== '');
};

module.exports = akeneoAttributes;
