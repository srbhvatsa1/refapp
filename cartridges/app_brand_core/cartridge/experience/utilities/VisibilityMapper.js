'use strict';

var map = {
    visible: '',
    hidden: 'd-none',
    'visible-up-sm': 'd-none d-sm-block',
    'visible-up-md': 'd-none d-md-block',
    'visible-up-lg': 'd-none d-lg-block',
    'visible-up-xl': 'd-none d-xl-block',
    'visible-down-xs': 'd-sm-none',
    'visible-down-sm': 'd-md-none',
    'visible-down-md': 'd-lg-none',
    'visible-down-lg': 'd-xl-none'
};

module.exports = function (value) {
    return map[value] || '';
};
