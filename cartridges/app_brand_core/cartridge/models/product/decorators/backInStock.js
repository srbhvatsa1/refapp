'use strict';

module.exports = function (product, apiProduct) {
    /**
     * Appends enableBackInStockNotification property based on availability of product.
     */
    var enableBackInStockNotification = false;
    if ('enableBackInStockNotification' in apiProduct.custom &&
    apiProduct.custom.enableBackInStockNotification &&
    !product.available) {
        enableBackInStockNotification = true;
    }

    if (!enableBackInStockNotification && !product.available) {
        // check categories of product
        var categories = apiProduct.getAllCategories();
        if (categories) {
            var collections = require('*/cartridge/scripts/util/collections');
            collections.forEach(categories, function (category) {
                if ('enableBackInStockNotification' in category.custom &&
                category.custom.enableBackInStockNotification) {
                    enableBackInStockNotification = true;
                }
            });
        }
    }

    Object.defineProperty(product, 'enableBackInStockNotification', {
        enumerable: true,
        value: enableBackInStockNotification
    });
};
