'use strict';

/**
 * Exports Order
 * @param {dw.order.Order} order order to be exported
 * @returns {Object} Status
 */
function execute(order) {
    var speedBusService = require('*/cartridge/scripts/services/speedBusService');
    var omsSpeedBusService = require('*/cartridge/scripts/services/omsSpeedBusService');
    var speedBusHelpers = require('*/cartridge/scripts/helpers/speedbusHelpers');
    var logger = require('dw/system/Logger').getLogger('speedbus');
    var exportOrderModel = require('*/cartridge/models/speedbus/exportOrder')(order);
    var Order = require('dw/order/Order');
    var StringUtils = require('dw/util/StringUtils');
    var Transaction = require('dw/system/Transaction');
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
    var Site = require('dw/system/Site');

    var requestPayload = {
        method: 'POST',
        body: exportOrderModel
    };
    var serviceResult;
    var speedBusServiceType = Site.current.getCustomPreferenceValue('speedBusServiceType');

    if (speedBusServiceType && speedBusServiceType.value === 'oms') {
        serviceResult = omsSpeedBusService.call(requestPayload);
    } else {
        serviceResult = speedBusService.call(requestPayload);
    }
    var serviceErrorResponse = StringUtils.format('status: {0} errorMessage: {1}', serviceResult.status, serviceResult.errorMessage);
    if (serviceResult.isOk()) {
        // mark order as exported
        var serviceResponse = JSON.stringify(serviceResult.getObject());
        if (speedBusHelpers.isExportSucceeded(serviceResult.getObject())) {
            if (order.getExportStatus().value !== Order.EXPORT_STATUS_EXPORTED) {
                hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_EXPORTED, { subject: 'SpeedBus LAYINT', text: serviceResponse }], function () {
                    Transaction.wrap(function () {
                        order.setExportStatus(Order.EXPORT_STATUS_EXPORTED);
                        order.addNote('SpeedBus LAYINT', serviceResponse);
                    });
                });
                // SFSC update trigger
                speedBusHelpers.sfscOrderUpdateTrigger(order);
            }
            return {
                success: true
            };
        }
        if (order.getExportStatus().value !== Order.EXPORT_STATUS_FAILED) {
            hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_FAILED, { subject: 'SpeedBus LAYINT ERROR', text: StringUtils.format('{0} {1}', serviceResponse, serviceErrorResponse) }], function () {
                Transaction.wrap(function () {
                    order.setExportStatus(Order.EXPORT_STATUS_FAILED);
                    order.addNote('SpeedBus LAYINT ERROR', serviceResponse);
                });
            });
        }
        logger.error('While exporting order {0} an unexpected issue is occured {1}', order.orderNo, serviceResponse);
        return {
            success: false,
            message: serviceErrorResponse,
            raw: serviceResult
        };
    }
    if (order.getExportStatus().value !== Order.EXPORT_STATUS_FAILED) {
        hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_FAILED, { subject: 'SpeedBus LAYINT ERROR', text: serviceErrorResponse }], function () {
            Transaction.wrap(function () {
                order.setExportStatus(Order.EXPORT_STATUS_FAILED);
                order.addNote('SpeedBus LAYINT ERROR', serviceErrorResponse);
            });
        });
    }
    logger.error('While exporting order {0} an unexpected issue is occured {1}', order.orderNo, serviceErrorResponse);
    return {
        success: false,
        message: serviceErrorResponse,
        raw: serviceResult
    };
}

module.exports = {
    execute: execute
};
