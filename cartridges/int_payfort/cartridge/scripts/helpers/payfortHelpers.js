'use strict';
/* global request */

var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var PaymentStatusCodes = require('dw/order/PaymentStatusCodes');
var OrderMgr = require('dw/order/OrderMgr');
var Order = require('dw/order/Order');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var StringUtils = require('dw/util/StringUtils');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var Status = require('dw/system/Status');

var collections = require('*/cartridge/scripts/util/collections');
var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
var payfortServiceHelpers = require('*/cartridge/scripts/helpers/payfortServiceHelpers');
var logger = require('dw/system/Logger').getLogger('payfort.helper'); // eslint-disable-line no-unused-vars
var payfortConstants = require('*/cartridge/scripts/util/payfortConstants');

/**
 * Sets the last PSP payment status to the order's custom.lastPaymentStatus
 * @param {dw.order.Order} order - The order to be updated
 * @param {string} lastPaymentStatus - the last PSP payment status
 * @param {string} note - the note to be added to the order if provided
 */
function setLastPSPPaymentStatus(order, lastPaymentStatus, note) {
    if (!order) {
        return;
    }

    Transaction.wrap(function () {
        order.custom.lastPaymentStatus = lastPaymentStatus; // eslint-disable-line no-param-reassign

        if (note) {
            order.addNote(payfortConstants.pspPaymentStatusNoteSubject, note);
        }
    });
}

/**
 * Exclude restricted keys from object to be logged/noted
 * @param {Object} payload - the full payload
 * @return {string} returns the logable payload
 */
function getLogablePayload(payload) {
    if (!payload) {
        return '{}';
    }

    var logablePayload = {};
    Object.keys(payload).sort().forEach(function (key) {
        if (payfortConstants.logExcludedKeys.indexOf(key) === -1) {
            logablePayload[key] = payload[key];
        }
    });
    return JSON.stringify(logablePayload);
}

/**
 * Adds a payload note the the given order
 * @param {dw.order.Order} order The order to be added a note
 * @param {string} subject - the subject of the note
 * @param {Object} payload - the payload to be noted
 */
function addNoteToOrder(order, subject, payload) {
    if (!order) {
        return;
    }

    var note = typeof payload === 'string' ? payload : getLogablePayload(payload);
    note = note.length > 1000 ? note.substr(0, 1000) : note;

    Transaction.wrap(function () {
        order.addNote(subject, note);
    });
}

/**
 * Fails the given order if the status is ORDER_STATUS_CREATED
 * @param {dw.order.Order} order - The order to be failed
 * @param {string} description - The fail description
 * @returns {dw.system.Status} The status of the failure
 */
function failOrder(order, description) {
    if (!order) {
        return new Status(Status.ERROR);
    }

    if (order.status.value !== Order.ORDER_STATUS_CREATED) {
        return new Status(Status.ERROR);
    }

    return Transaction.wrap(function () {
        return hooksHelper('app.order.status.failOrder', 'failOrder', [order, true, description || 'payfort fail'], function () {
            return OrderMgr.failOrder(order, true);
        });
    });
}

/**
 * Prepares the payment information
 * @param {dw.web.FormGroup} creditCardForm - The credit card fields form
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be processed
 * @returns {Object} the payment information data
 */
function preparePaymentInformation(creditCardForm, paymentInstrument) {
    var paymentInformation = {};
    if (paymentInstrument) {
        paymentInformation.cardType = paymentInstrument.creditCardType;
        paymentInformation.cardOwner = paymentInstrument.creditCardHolder;
        paymentInformation.expirationMonth = paymentInstrument.creditCardExpirationMonth > 9
            ? paymentInstrument.creditCardExpirationMonth.toString()
            : StringUtils.format('0{0}', paymentInstrument.creditCardExpirationMonth);
        paymentInformation.expirationYear = (paymentInstrument.creditCardExpirationYear % 100).toString();
        paymentInformation.cardToken = paymentInstrument.creditCardToken || '';
    }

    if (creditCardForm) {
        paymentInformation.cardNumber = creditCardForm.cardNumber.value || '';
        paymentInformation.saveCard = creditCardForm.saveCard.checked || false;
        paymentInformation.securityCode = creditCardForm.securityCode.value || '';
    }

    if (!paymentInformation.securityCode && request.httpParameterMap.securityCode.stringValue) {
        paymentInformation.securityCode = request.httpParameterMap.securityCode.stringValue;
    }

    return paymentInformation;
}

/**
 * Checks if the Mada PaymentCard is exists and active and applicable for the current card number and customer
 * If all is true, then returns Mada PaymentCard object
 * otherwise returns null
 * @param {dw.order.Basket} currentBasket Current users's basket
 * @param {Object} paymentInformation - the payment information
 * @return {dw.order.PaymentCard} returns the Mada PaymentCard, if applicable, null otherwise
 */
function getMadaPaymentCardIfApplicable(currentBasket, paymentInformation) {
    var madaPaymentCard = PaymentMgr.getPaymentCard('Mada');
    if (madaPaymentCard && madaPaymentCard.active) {
        var cardNumber = paymentInformation.cardNumber.value;
        var cardSecurityCode = paymentInformation.securityCode.value;
        var expirationMonth = paymentInformation.expirationMonth.value;
        var expirationYear = paymentInformation.expirationYear.value;

        var madaCreditCardStatus = madaPaymentCard.verify(
            expirationMonth,
            expirationYear,
            cardNumber,
            cardSecurityCode
        );
        if (madaCreditCardStatus && !madaCreditCardStatus.error) {
            if (madaPaymentCard.isApplicable(currentBasket.customer,
                currentBasket.billingAddress.countryCode.value,
                basketCalculationHelpers.getNonComplimentaryPaymentAmount(currentBasket).value)) {
                return madaPaymentCard;
            }
        }
    }
    return null;
}

/**
 * Verifies a credit card against a valid card number
 * and expiration date and possibly invalidates invalid form fields.
 * If the information is valid a
 * credit card payment instrument is created
 * @param {dw.order.Basket} basket Current users's basket
 * @param {Object} paymentInformation - the payment information
 * @return {Object} returns an error object
 */
function handleCreditCard(basket, paymentInformation) {
    var currentBasket = basket;
    var cardErrors = {};
    var cardOwner = paymentInformation.cardOwner.value;
    var cardNumber = paymentInformation.cardNumber.value;
    var cardSecurityCode = paymentInformation.securityCode.value;
    var expirationMonth = paymentInformation.expirationMonth.value;
    var expirationYear = paymentInformation.expirationYear.value;
    var serverErrors = [];
    var creditCardStatus;

    var cardType = paymentInformation.cardType.value;
    var paymentCard = PaymentMgr.getPaymentCard(cardType);

    // check if the provided cardnumber is mada payment card number
    // if so, update cardtype and paymentCard object
    var madaPaymentCard = getMadaPaymentCardIfApplicable(currentBasket, paymentInformation);
    if (madaPaymentCard) {
        paymentCard = madaPaymentCard;
        cardType = madaPaymentCard.cardType;
    }


    if (!paymentInformation.creditCardToken) {
        if (paymentCard) {
            creditCardStatus = paymentCard.verify(
                expirationMonth,
                expirationYear,
                cardNumber,
                cardSecurityCode
            );
        } else {
            cardErrors[paymentInformation.cardNumber.htmlName] =
                Resource.msg('error.invalid.card.number', 'creditCard', null);

            return { fieldErrors: [cardErrors], serverErrors: serverErrors, error: true };
        }

        if (creditCardStatus.error) {
            collections.forEach(creditCardStatus.items, function (item) {
                switch (item.code) {
                    case PaymentStatusCodes.CREDITCARD_INVALID_CARD_NUMBER:
                        cardErrors[paymentInformation.cardNumber.htmlName] =
                            Resource.msg('error.invalid.card.number', 'creditCard', null);
                        break;

                    case PaymentStatusCodes.CREDITCARD_INVALID_EXPIRATION_DATE:
                        cardErrors[paymentInformation.expirationMonth.htmlName] =
                            Resource.msg('error.expired.credit.card', 'creditCard', null);
                        cardErrors[paymentInformation.expirationYear.htmlName] =
                            Resource.msg('error.expired.credit.card', 'creditCard', null);
                        break;

                    case PaymentStatusCodes.CREDITCARD_INVALID_SECURITY_CODE:
                        cardErrors[paymentInformation.securityCode.htmlName] =
                            Resource.msg('error.invalid.security.code', 'creditCard', null);
                        break;
                    default:
                        serverErrors.push(
                            Resource.msg('error.card.information.error', 'creditCard', null)
                        );
                }
            });

            return { fieldErrors: [cardErrors], serverErrors: serverErrors, error: true };
        }
    }

    Transaction.wrap(function () {
        // remove existing payment instruments except complimentary ones
        COHelpers.removeExistingPaymentInstruments(currentBasket);

        // create new CREDIT_CARD payment instrument with METHOD_CREDIT_CARD
        var paymentInstrument = currentBasket.createPaymentInstrument(
            PaymentInstrument.METHOD_CREDIT_CARD,
            basketCalculationHelpers.getNonComplimentaryPaymentAmount(currentBasket)
        );

        // set payment instrument credit card data
        paymentInstrument.setCreditCardHolder(cardOwner);
        paymentInstrument.setCreditCardNumber(cardNumber);
        paymentInstrument.setCreditCardType(cardType);
        paymentInstrument.setCreditCardExpirationMonth(expirationMonth);
        paymentInstrument.setCreditCardExpirationYear(expirationYear);
        if (paymentInformation.creditCardToken) {
            paymentInstrument.setCreditCardToken(paymentInformation.creditCardToken);
        }
    });

    return { fieldErrors: cardErrors, serverErrors: serverErrors, error: false };
}

/**
 * Tokenizes a payment using a credit card.
 * Prepares the parameters
 * Renders the form with parameters
 * Returns the form to be posted to the payfort
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} paymentInformation - The paymentInformation object
 * @return {Object} returns an error object
 */
function tokenizeCreditCard(order, paymentInstrument, paymentProcessor, paymentInformation) {
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

    var payload = payfortServiceHelpers.initService(
        payfortServiceHelpers.payFortService,
        order,
        paymentInformation,
        paymentInstrument,
        payfortConstants.command.tokenization
    );
    addNoteToOrder(order, 'Request TOKENIZATION', payload);

    var context = {
        payload: payload,
        payFortTokenizationUrl: payfortServiceHelpers.payFortService.getURL()
    };
    var template = 'checkout/billing/payFortTokenizationForm';
    var payFortTokenizationForm = renderTemplateHelper.getRenderedHtml(
        context,
        template
    );

    return { fieldErrors: {}, serverErrors: [], error: false, form: payFortTokenizationForm };
}

/**
 * Processes the authorization success response
 * to set relevant data to relevant objects
 * to set relevant status to order
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} postedPayload - The posted payload from checkout.com
 * @return {void}
 */
function processAuthorizedResponse(order, paymentInstrument, paymentProcessor, postedPayload) {
    if (!paymentInstrument.creditCardToken && postedPayload.token_name) {
        // set credit card token
        Transaction.wrap(function () {
            paymentInstrument.setCreditCardToken(postedPayload.token_name);
        });

        // save payment instrument to current customer wallet
        // if saveCard selected and current customer is authenticated
        if ((postedPayload.command === payfortConstants.command.authorization
                || postedPayload.command === payfortConstants.command.purchase)
            && postedPayload.remember_me === payfortConstants.rememberMe.yes
            && order.customer.authenticated
            && order.customer.registered
        ) {
            var customer = order.customer;

            COHelpers.savePaymentInstrumentToWalletWithPaymentInstrument(
                paymentInstrument,
                customer
            );
        }
    }

    // if the command is PURCHASE
    if (postedPayload.command === payfortConstants.command.purchase) {
        Transaction.wrap(function () {
            hooksHelper('app.order.status.setPaymentStatus', 'setPaymentStatus', [order, Order.PAYMENT_STATUS_PAID], function () {
                order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
            });
        });

        // set last payment status as captured
        setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.captured);
    } else { // if authorized
        // set last payment status as authorized
        setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.authorized);
    }
}

/**
 * Helper method for both authorizeCreditCard and authorizeTokenizedCreditCard
 * This part is common for authorizeCreditCard and authorizeTokenizedCreditCard public methods
 * Calls the authorize/purchase API service
 * Returns the result
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {dw.svc.ServiceCredential} serviceCredential - The serviceCredential of the payfort service
 * @param {Object} payload - The prepared payload to be posted
 * @param {string} action - The action -> AUTHORIZATION/PURCHASE or Tokenized AUTHORIZATION/PURCHASE
 * @param {string} orderNoteIdentifier - the itentifier of the order note
 * @return {Object} returns an error object
 */
function callAuthorization(order, paymentInstrument, paymentProcessor, serviceCredential, payload, action) {
    // add request payload as order note
    addNoteToOrder(order, StringUtils.format('Request {0}', action), payload);
    var serviceResult = payfortServiceHelpers.payFortService.call(payload);

    if (serviceResult.isOk()) {
        var authorizationPostedPayload = serviceResult.object;

        if (authorizationPostedPayload.fort_id && paymentInstrument.paymentTransaction) {
            // set transaction id as fort_id
            Transaction.wrap(function () {
                paymentInstrument.paymentTransaction.setTransactionID(authorizationPostedPayload.fort_id);
            });
        }

        // add response payload as order note
        addNoteToOrder(order, StringUtils.format('Response {0}', action), authorizationPostedPayload);

        if (authorizationPostedPayload.status === payfortConstants.serviceStatus.authorizationSuccess // 02 Authorization Success
            || authorizationPostedPayload.status === payfortConstants.serviceStatus.purchaseSuccess) { // 14 Purchase Success.
            // success

            // validate authorization response signature
            if (!payfortServiceHelpers.validateSignature(authorizationPostedPayload, serviceCredential)) {
                // set last payment status as error
                setLastPSPPaymentStatus(
                    order,
                    payfortConstants.pspPaymentStatus.error,
                    'PayFort Response Signature Mismatch!'
                );

                return {
                    error: true,
                    message: StringUtils.format('{0} Response Signature mismatch!, orderNo: {1}', action, order.orderNo)
                };
            }

            processAuthorizedResponse(order, paymentInstrument, paymentProcessor, authorizationPostedPayload);

            return {
                success: true
            };
        } else if (authorizationPostedPayload.status === payfortConstants.serviceStatus.onHold
                    && authorizationPostedPayload.response_code === payfortConstants.serviceResponseCode.threeDSecureCheck
                    && authorizationPostedPayload[payfortConstants.serviceParams.threeDSecureUrl]) { // 20	On hold., 064	3-D Secure check requested.
            // set last payment status as pending
            setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.pending);

            return {
                redirect: true,
                redirectUrl: authorizationPostedPayload[payfortConstants.serviceParams.threeDSecureUrl]
            };
        }

        if (authorizationPostedPayload.status === payfortConstants.serviceStatus.invalidRequest // 00 Invalid Request.
            || authorizationPostedPayload.status === payfortConstants.serviceStatus.uncertainTransaction) { // 15 Uncertain Transaction.
            // set last payment status as error
            setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, authorizationPostedPayload.response_message);
        } else {
            // set last payment status as declined
            setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.declined);
        }

        return {
            error: true,
            message: StringUtils.format('{0} call is not succeeded, orderNo: {1}, status: {2}, responseCode: {3}, responseMessage: {4}',
                action,
                order.orderNo,
                authorizationPostedPayload.status,
                authorizationPostedPayload.response_code,
                authorizationPostedPayload.response_message
            )
        };
    }

    // set last payment status as error
    setLastPSPPaymentStatus(
        order,
        payfortConstants.pspPaymentStatus.error,
        StringUtils.format('status: {0}, errorCode: {1}, errorMessage: {2}, extraErrorMessage: {3}',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    );

    return {
        error: true,
        message: StringUtils.format('Error on Service:{0} call, status: {1}, errorCode: {2}, errorMessage: {3}, extraErrorMessage: {4}',
            action,
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    };
}

/**
 * Authorizes a payment using a credit card.
 * Prepares the parameters
 * Renders the form with parameters
 * Returns the form to be posted to the payfort
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} paymentInformation - The paymentInformation object
 * @return {Object} returns an error object
 */
function authorizeCreditCard(order, paymentInstrument, paymentProcessor, paymentInformation) {
    var Site = require('dw/system/Site');

    var action = Site.getCurrent().getCustomPreferenceValue('payfortPurchaseOnAuthorization')
        ? payfortConstants.command.purchase : payfortConstants.command.authorization;

    var serviceCredential = payfortServiceHelpers.getServiceCredential(payfortServiceHelpers.payFortService, order, action);
    if (serviceCredential.custom.payfortChannelType.value === payfortConstants.channelType.merchantPage20) {
        return tokenizeCreditCard(order, paymentInstrument, paymentProcessor, paymentInformation);
    }

    var payload = payfortServiceHelpers.getPayload(
        serviceCredential,
        order,
        paymentInformation,
        paymentInstrument,
        action
    );

    return callAuthorization(order, paymentInstrument, paymentProcessor, serviceCredential, payload, action);
}

/**
 * Authorizes a payment using apple pay.
 * Prepares the parameters
 * Calls the service
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} applePayData - The applePay data object
 * @return {Object} returns an error object
 */
function authorizeApplePay(order, paymentInstrument, paymentProcessor, applePayData) {
    var Site = require('dw/system/Site');

    var action = Site.getCurrent().getCustomPreferenceValue('payfortPurchaseOnAuthorization')
        ? payfortConstants.command.purchase : payfortConstants.command.authorization;

    var serviceCredential = payfortServiceHelpers.getApplePayServiceCredential(payfortServiceHelpers.payFortService, order, action);

    var payload = payfortServiceHelpers.getApplePayPayload(
        serviceCredential,
        order,
        applePayData,
        paymentInstrument,
        action
    );

    return callAuthorization(order, paymentInstrument, paymentProcessor, serviceCredential, payload, action);
}

/**
 * Authorizes a payment using a credit card.
 * Customizations for payFort processor and
 * payFort logic to authorize credit card payment.
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} postedPayload - The posted payload from payfort
 * @return {Object} returns an error or success object
 */
function authorizeTokenizedCreditCard(order, paymentInstrument, paymentProcessor, postedPayload) {
    var Site = require('dw/system/Site');

    var action = Site.getCurrent().getCustomPreferenceValue('payfortPurchaseOnAuthorization')
        ? payfortConstants.command.purchase : payfortConstants.command.authorization;

    var serviceCredential = payfortServiceHelpers.getServiceCredential(payfortServiceHelpers.payFortService, order, action);

    // validate tokenize response signature
    if (!payfortServiceHelpers.validateSignature(postedPayload, serviceCredential)) {
        // set last payment status as error
        setLastPSPPaymentStatus(
            order,
            payfortConstants.pspPaymentStatus.error,
            'PayFort Response Signature Mismatch!'
        );

        return {
            error: true,
            message: StringUtils.format('Tokenization Response Signature mismatch!, orderNo: {0}', order.orderNo)
        };
    }

    var payload = payfortServiceHelpers.getPayload(
        serviceCredential,
        order,
        null,
        paymentInstrument,
        action
    );

    return callAuthorization(order, paymentInstrument, paymentProcessor, serviceCredential, payload, StringUtils.format('Tokenized {0}', action));
}

/**
 * Continue Successfully Authorized CreditCard payment.
 * Checks the fraud
 * Places the order
 * Sends the confirmation email
 * @param {Object} req - The SFCC request object
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} postedPayload - The posted payload from payfort
 * @return {Object} returns an error or success object
 */
function continueAuthorizedCreditCard(req, order, paymentInstrument, paymentProcessor, postedPayload) { // eslint-disable-line no-unused-vars
    processAuthorizedResponse(order, paymentInstrument, paymentProcessor, postedPayload);
    return COHelpers.processPlaceOrder(req, order);
}

/**
 * Captures the payment.
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to capture
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} postedPayload - The posted payload from payfort
 * @return {Object} returns an error or success object
 */
function capturePayment(order, paymentInstrument, paymentProcessor) { // eslint-disable-line no-unused-vars
    var payload = payfortServiceHelpers.initService(
        payfortServiceHelpers.payFortService,
        order,
        null,
        paymentInstrument,
        payfortConstants.command.capture
    );
    // add request payload as order note
    addNoteToOrder(order, 'Request CAPTURE', payload);
    var serviceResult = payfortServiceHelpers.payFortService.call(payload);

    if (serviceResult.isOk()) {
        var capturePostedPayload = serviceResult.object;

        // add response payload as order note
        addNoteToOrder(order, 'Response CAPTURE', capturePostedPayload);

        if (capturePostedPayload.status === payfortConstants.serviceStatus.captureSuccess) { // 04	Capture Success.
            // validate capture response signature
            if (!payfortServiceHelpers.validateSignature(capturePostedPayload, payfortServiceHelpers.payFortService.getConfiguration().getCredential())) {
                // set last payment status as error
                setLastPSPPaymentStatus(
                    order,
                    payfortConstants.pspPaymentStatus.error,
                    'PayFort Response Signature Mismatch!'
                );

                return {
                    error: true,
                    message: StringUtils.format('Capture Response Signature mismatch!, orderNo: {0}', order.orderNo)
                };
            }

            // success
            // set order as PAID
            Transaction.wrap(function () {
                hooksHelper('app.order.status.setPaymentStatus', 'setPaymentStatus', [order, Order.PAYMENT_STATUS_PAID], function () {
                    order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                });
            });

            // set last payment status as captured
            setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.captured);

            return {
                success: true
            };
        }

        if (capturePostedPayload.status === payfortConstants.serviceStatus.invalidRequest // 00 Invalid Request.
            || capturePostedPayload.status === payfortConstants.serviceStatus.incomplete // 10 Incomplete.
            || capturePostedPayload.status === payfortConstants.serviceStatus.uncertainTransaction) { // 15 Uncertain Transaction.
            // set last payment status as error
            setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, capturePostedPayload.response_message);
        } else {
            // set last payment status as declined
            setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.declined);
        }

        return {
            error: true,
            message: StringUtils.format('CAPTURE call is not succeeded, orderNo: {0}, status: {1}, responseCode: {2}, responseMessage: {3}',
                order.orderNo,
                capturePostedPayload.status,
                capturePostedPayload.response_code,
                capturePostedPayload.response_message
            )
        };
    }

    // set last payment status as error
    setLastPSPPaymentStatus(
        order,
        payfortConstants.pspPaymentStatus.error,
        StringUtils.format('status: {0}, errorCode: {1}, errorMessage: {2}, extraErrorMessage: {3}',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    );

    return {
        error: true,
        message: StringUtils.format('Error on Service:{0} call, status: {1}, errorCode: {2}, errorMessage: {3}, extraErrorMessage: {4}',
            'CAPTURE',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    };
}

/**
 * Checks the payment status of the given order
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to check
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function checkPaymentStatus(order, paymentInstrument, paymentProcessor) { // eslint-disable-line no-unused-vars
    var payload = payfortServiceHelpers.initService(
        payfortServiceHelpers.payFortService,
        order,
        null,
        paymentInstrument,
        payfortConstants.command.checkStatus
    );

    // add request payload as order note
    addNoteToOrder(order, 'Request CHECK_STATUS', payload);
    var serviceResult = payfortServiceHelpers.payFortService.call(payload);

    if (serviceResult.isOk()) {
        var checkStatusPostedPayload = serviceResult.object;

        // add response payload as order note
        addNoteToOrder(order, 'Response CHECK_STATUS', checkStatusPostedPayload);
        if (checkStatusPostedPayload.status === payfortConstants.serviceStatus.checkStatusSuccess) { // 12	Check status success.
            // validate check status response signature
            if (!payfortServiceHelpers.validateSignature(checkStatusPostedPayload, payfortServiceHelpers.payFortService.getConfiguration().getCredential())) {
                // set last payment status as error
                setLastPSPPaymentStatus(
                    order,
                    payfortConstants.pspPaymentStatus.error,
                    'PayFort Response Signature Mismatch!'
                );

                return {
                    error: true,
                    message: StringUtils.format('Check Status Response Signature mismatch!, orderNo: {0}', order.orderNo)
                };
            }

            var pspStatus;
            switch (checkStatusPostedPayload.transaction_status) {
                case payfortConstants.serviceStatus.authorizationSuccess:
                    pspStatus = payfortConstants.pspPaymentStatus.authorized;
                    break;
                case payfortConstants.serviceStatus.captureSuccess:
                case payfortConstants.serviceStatus.purchaseSuccess:
                    pspStatus = payfortConstants.pspPaymentStatus.captured;
                    break;
                case payfortConstants.serviceStatus.refundSuccess:
                    pspStatus = payfortConstants.pspPaymentStatus.refunded;
                    break;
                case payfortConstants.serviceStatus.authorizationVoidSuccess:
                    pspStatus = payfortConstants.pspPaymentStatus.voided;
                    break;
                default:
                    pspStatus = 'Unknown';
                    break;
            }

            return {
                success: true,
                status: pspStatus
            };
        }

        return {
            error: true,
            message: StringUtils.format('CHECK_STATUS call is not succeeded, orderNo: {0}, status: {1}, responseCode: {2}, responseMessage: {3}',
                order.orderNo,
                checkStatusPostedPayload.status,
                checkStatusPostedPayload.response_code,
                checkStatusPostedPayload.response_message
            )
        };
    }

    // set last payment status as error
    setLastPSPPaymentStatus(
        order,
        payfortConstants.pspPaymentStatus.error,
        StringUtils.format('status: {0}, errorCode: {1}, errorMessage: {2}, extraErrorMessage: {3}',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    );

    return {
        error: true,
        message: StringUtils.format('Error on Service:{0} call, status: {1}, errorCode: {2}, errorMessage: {3}, extraErrorMessage: {4}',
            'CHECK_STATUS',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    };
}

/**
 * Cancels the payment.
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to cancel
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function cancelPayment(order, paymentInstrument, paymentProcessor) { // eslint-disable-line no-unused-vars
    var payload = payfortServiceHelpers.initService(
        payfortServiceHelpers.payFortService,
        order,
        null,
        paymentInstrument,
        payfortConstants.command.voidAuthorization
    );
    // add request payload as order note
    addNoteToOrder(order, 'Request VOID_AUTHORIZATION', payload);
    var serviceResult = payfortServiceHelpers.payFortService.call(payload);

    if (serviceResult.isOk()) {
        var voidAuthorizationPostedPayload = serviceResult.object;

        // add response payload as order note
        addNoteToOrder(order, 'Response VOID_AUTHORIZATION', voidAuthorizationPostedPayload);

        if (voidAuthorizationPostedPayload.status === payfortConstants.serviceStatus.authorizationVoidSuccess) { // 08 Authorization Voided Successfully.
            // validate void authorization response signature
            if (!payfortServiceHelpers.validateSignature(voidAuthorizationPostedPayload, payfortServiceHelpers.payFortService.getConfiguration().getCredential())) {
                // set last payment status as error
                setLastPSPPaymentStatus(
                    order,
                    payfortConstants.pspPaymentStatus.error,
                    'PayFort Response Signature Mismatch!'
                );

                return {
                    error: true,
                    message: StringUtils.format('Void Authorization Response Signature mismatch!, orderNo: {0}', order.orderNo)
                };
            }

            // success
            // set order as cancelled
            /*
            Transaction.wrap(function () {
                hooksHelper('app.order.status.cancelOrder', 'cancelOrder', [order, null, 'payfort VOID_AUTHORIZATION'], function () {
                    OrderMgr.cancelOrder(order);
                });
            });
            */

            // set last payment status as voided
            setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.voided);

            return {
                success: true
            };
        }

        return {
            error: true,
            message: StringUtils.format('VOID_AUTHORIZATION call is not succeeded, orderNo: {0}, status: {1}, responseCode: {2}, responseMessage: {3}',
                order.orderNo,
                voidAuthorizationPostedPayload.status,
                voidAuthorizationPostedPayload.response_code,
                voidAuthorizationPostedPayload.response_message
            )
        };
    }

    // set last payment status as error
    setLastPSPPaymentStatus(
        order,
        payfortConstants.pspPaymentStatus.error,
        StringUtils.format('status: {0}, errorCode: {1}, errorMessage: {2}, extraErrorMessage: {3}',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    );

    return {
        error: true,
        message: StringUtils.format('Error on Service:{0} call, status: {1}, errorCode: {2}, errorMessage: {3}, extraErrorMessage: {4}',
            'VOID_AUTHORIZATION',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    };
}

/**
 * Refunds the payment.
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to refund
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function refundPayment(order, paymentInstrument, paymentProcessor) { // eslint-disable-line no-unused-vars
    var payload = payfortServiceHelpers.initService(
        payfortServiceHelpers.payFortService,
        order,
        null,
        paymentInstrument,
        payfortConstants.command.refund
    );
    // add request payload as order note
    addNoteToOrder(order, 'Request REFUND', payload);
    var serviceResult = payfortServiceHelpers.payFortService.call(payload);

    if (serviceResult.isOk()) {
        var refundPostedPayload = serviceResult.object;

        // add response payload as order note
        addNoteToOrder(order, 'Response REFUND', refundPostedPayload);

        if (refundPostedPayload.status === payfortConstants.serviceStatus.refundSuccess) { // 06 Refund Success.
            // validate refund response signature
            if (!payfortServiceHelpers.validateSignature(refundPostedPayload, payfortServiceHelpers.payFortService.getConfiguration().getCredential())) {
                // set last payment status as error
                setLastPSPPaymentStatus(
                    order,
                    payfortConstants.pspPaymentStatus.error,
                    'PayFort Response Signature Mismatch!'
                );

                return {
                    error: true,
                    message: StringUtils.format('Refund Response Signature mismatch!, orderNo: {0}', order.orderNo)
                };
            }

            // set last payment status as refunded
            setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.refunded);

            return {
                success: true
            };
        }

        return {
            error: true,
            message: StringUtils.format('REFUND call is not succeeded, orderNo: {0}, status: {1}, responseCode: {2}, responseMessage: {3}',
                order.orderNo,
                refundPostedPayload.status,
                refundPostedPayload.response_code,
                refundPostedPayload.response_message
            )
        };
    }

    // set last payment status as error
    setLastPSPPaymentStatus(
        order,
        payfortConstants.pspPaymentStatus.error,
        StringUtils.format('status: {0}, errorCode: {1}, errorMessage: {2}, extraErrorMessage: {3}',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    );

    return {
        error: true,
        message: StringUtils.format('Error on Service:{0} call, status: {1}, errorCode: {2}, errorMessage: {3}, extraErrorMessage: {4}',
            'REFUND',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    };
}

/**
 * Adds the feedback custom object to the site
 * @param {string} orderNo - The order number
 * @param {string} status - The status of the payment
 * @param {string} payload - The stringified json payload object
 */
function addFeedback(orderNo, status, payload) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var UUIDUtils = require('dw/util/UUIDUtils');

    Transaction.wrap(function () {
        var feedback = CustomObjectMgr.createCustomObject('PSPFeedback', UUIDUtils.createUUID());
        feedback.custom.orderNo = orderNo;
        feedback.custom.status = status;
        feedback.custom.payload = payload;
    });
}

/**
 * Process order feedback payment status against order payment status
 * If different then arrange order payment status
 * @param {Object} req - The SFCC request object
 * @param {dw.order.Order} order - The order to be checked for feedback status
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be checked for feedback status
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {dw.object.CustomObject} feedback - The feedback custom object
 * @return {Object} returns an error or success object
 */
function feedbackPayment(req, order, paymentInstrument, paymentProcessor, feedback) {
    var transactionStatus;
    var processPlaceOrderResult;
    switch (feedback.custom.status) {
        case payfortConstants.serviceStatus.authorizationSuccess:
        case payfortConstants.serviceStatus.purchaseSuccess:
            switch (order.status.value) {
                case Order.ORDER_STATUS_CREATED:
                    addNoteToOrder(order, 'Feedback Authorization/Purchase Success, Order status: Created', feedback.custom.payload);
                    processPlaceOrderResult = COHelpers.processPlaceOrder(req, order);
                    if (processPlaceOrderResult.error) {
                        transactionStatus = new Status(Status.ERROR);
                    } else {
                        transactionStatus = new Status(Status.OK);

                        // service cloud set
                        require('dw/system/HookMgr').callHook('app.order.created', 'created', order);

                        // if the status is purchaseSuccess
                        if (feedback.custom.status === payfortConstants.serviceStatus.purchaseSuccess) {
                            Transaction.wrap(function () {
                                hooksHelper('app.order.status.setPaymentStatus', 'setPaymentStatus', [order, Order.PAYMENT_STATUS_PAID], function () {
                                    order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                                });
                            });

                            // set last payment status as captured
                            setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.captured);
                        } else { // if authorizationSuccess
                            // set last payment status as authorized
                            setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.authorized);
                        }
                    }

                    break;
                case Order.ORDER_STATUS_CANCELLED:
                    addNoteToOrder(order, 'Feedback Authorization/Purchase Success, Order status: Cancelled', feedback.custom.payload);
                    transactionStatus = Transaction.wrap(function () {
                        return hooksHelper('app.order.status.undoCancelOrder', 'undoCancelOrder', [order, 'payfort feedback'], function () {
                            return OrderMgr.undoCancelOrder(order);
                        });
                    });

                    if (!transactionStatus.error) {
                        // if the status is purchaseSuccess
                        if (feedback.custom.status === payfortConstants.serviceStatus.purchaseSuccess) {
                            Transaction.wrap(function () {
                                hooksHelper('app.order.status.setPaymentStatus', 'setPaymentStatus', [order, Order.PAYMENT_STATUS_PAID], function () {
                                    order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                                });
                            });

                            // set last payment status as captured
                            setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.captured);
                        } else { // if authorizationSuccess
                            // set last payment status as authorized
                            setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.authorized);
                        }
                    }

                    break;
                case Order.ORDER_STATUS_FAILED:
                    addNoteToOrder(order, 'Feedback Authorization/Purchase Success, Order status: Failed', feedback.custom.payload);
                    transactionStatus = Transaction.wrap(function () {
                        return hooksHelper('app.order.status.undoFailOrder', 'undoFailOrder', [order, 'payfort feedback'], function () {
                            return OrderMgr.undoFailOrder(order);
                        });
                    });

                    if (!transactionStatus.error) {
                        processPlaceOrderResult = COHelpers.processPlaceOrder(req, order);

                        if (processPlaceOrderResult.error) {
                            transactionStatus = new Status(Status.ERROR);
                        } else {
                            transactionStatus = new Status(Status.OK);

                            // service cloud set
                            require('dw/system/HookMgr').callHook('app.order.created', 'created', order);

                            // if the status is purchaseSuccess
                            if (feedback.custom.status === payfortConstants.serviceStatus.purchaseSuccess) {
                                Transaction.wrap(function () {
                                    hooksHelper('app.order.status.setPaymentStatus', 'setPaymentStatus', [order, Order.PAYMENT_STATUS_PAID], function () {
                                        order.setPaymentStatus(Order.PAYMENT_STATUS_PAID);
                                    });
                                });

                                // set last payment status as captured
                                setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.captured);
                            } else { // if authorizationSuccess
                                // set last payment status as authorized
                                setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.authorized);
                            }
                        }
                    }

                    break;
                default: break;
            }
            break;
        case payfortConstants.serviceStatus.authorizationFailed:
        case payfortConstants.serviceStatus.purchaseFailed:
            switch (order.status.value) {
                case Order.ORDER_STATUS_CREATED:
                    addNoteToOrder(order, 'Feedback Authorization/Purchase Failed, Order status: Created', feedback.custom.payload);
                    transactionStatus = failOrder(order, 'payfort feedback');
                    if (!transactionStatus.error) {
                        // set last payment status as declined
                        setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.declined);
                    }

                    break;
                case Order.ORDER_STATUS_NEW:
                case Order.ORDER_STATUS_OPEN:
                    addNoteToOrder(order, 'Feedback Authorization/Purchase Failed, Order status: New/Open', feedback.custom.payload);
                    transactionStatus = Transaction.wrap(function () {
                        return hooksHelper('app.order.status.cancelOrder', 'cancelOrder', [order, null, 'payfort feedback'], function () {
                            return OrderMgr.cancelOrder(order);
                        });
                    });

                    if (!transactionStatus.error) {
                        // set last payment status as declined
                        setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.declined);
                    }

                    break;
                default: break;
            }
            break;
        default: break;
    }

    return {
        error: transactionStatus ? transactionStatus.error : false,
        message: transactionStatus ? transactionStatus.message : ''
    };
}

/**
 * Inactivate token on PSP side
 * @param {dw.order.Order} order - The refence order for the payment token
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be processed for inactivate psp token
 * @return {Object} returns an error or success object
 */
function deleteToken(order, paymentInstrument) {
    var payload = payfortServiceHelpers.initService(
        payfortServiceHelpers.payFortService,
        order,
        null,
        paymentInstrument,
        payfortConstants.command.inactivateToken
    );

    var serviceResult = payfortServiceHelpers.payFortService.call(payload);

    if (serviceResult.isOk()) {
        var inactivateTokenPostedPayload = serviceResult.object;

        if (inactivateTokenPostedPayload.status === payfortConstants.serviceStatus.tokenUpdateSuccess) { // 58 Token updated successfully.
            return {
                success: true
            };
        }

        return {
            error: true,
            message: StringUtils.format('INACTIVE token call is not succeeded, orderNo: {0}, status: {1}, responseCode: {2}, responseMessage: {3}',
                order.orderNo,
                inactivateTokenPostedPayload.status,
                inactivateTokenPostedPayload.response_code,
                inactivateTokenPostedPayload.response_message
            )
        };
    }

    return {
        error: true,
        message: StringUtils.format('Error on Service:{0} call, status: {1}, errorCode: {2}, errorMessage: {3}, extraErrorMessage: {4}',
            'UPDATE_TOKEN INACTIVE',
            serviceResult.getStatus(),
            serviceResult.getError(),
            serviceResult.getErrorMessage(),
            serviceResult.getMsg())
    };
}

/**
 * Checks if the saved card locale and target locale have the same service,
 * Different PSP merchants can be used for different country/locale,
 * In order to use saved card functionality, the payment services for savedCard and target locales should be same
 * @param {string} savedCardLocale - the locale for saved card
 * @param {string} targetLocale - the target locale to be checked for saved card is applicable or not
 * @returns {boolean} true if the saved card is applicable for the target locale, false otherwise
 */
function isSavedCardApplicableForLocale(savedCardLocale, targetLocale) {
    return payfortServiceHelpers.getServiceCredentialId(null, savedCardLocale) === payfortServiceHelpers.getServiceCredentialId(null, targetLocale);
}

module.exports = {
    handleCreditCard: handleCreditCard,
    authorizeCreditCard: authorizeCreditCard,
    authorizeApplePay: authorizeApplePay,
    authorizeTokenizedCreditCard: authorizeTokenizedCreditCard,
    continueAuthorizedCreditCard: continueAuthorizedCreditCard,
    addNoteToOrder: addNoteToOrder,
    getLogablePayload: getLogablePayload,
    failOrder: failOrder,
    capturePayment: capturePayment,
    preparePaymentInformation: preparePaymentInformation,
    setLastPSPPaymentStatus: setLastPSPPaymentStatus,
    checkPaymentStatus: checkPaymentStatus,
    cancelPayment: cancelPayment,
    refundPayment: refundPayment,
    addFeedback: addFeedback,
    feedbackPayment: feedbackPayment,
    deleteToken: deleteToken,
    isSavedCardApplicableForLocale: isSavedCardApplicableForLocale
};
