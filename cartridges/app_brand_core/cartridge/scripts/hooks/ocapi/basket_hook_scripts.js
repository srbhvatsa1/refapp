'use strict';

/**
 * basket_hook_scripts.js
 *
 * Handles OCAPI hooks for basket calls
 */

var Status = require('dw/system/Status');
var currentSite = require('dw/system/Site').getCurrent();

/**
 * The validateBasket hook - called after basket get
 * @param {Object} basketResponse basket response object
 * @param {boolean} duringSubmit indicates if it is called during submit.
 * @return {dw.system.Status} returns a status code
 */
exports.validateBasket = function (basketResponse, duringSubmit) { // eslint-disable-line no-unused-vars
    if (duringSubmit) {
        return new Status(Status.OK);
    }
    var hookHelpers = require('*/cartridge/scripts/helpers/hookHelpers');

    hookHelpers.addProductInformation(basketResponse.productItems);

    if (basketResponse.bonusDiscountLineItems) {
        var bonusDiscountLineItems = [];
        for (var i = 0; i < basketResponse.bonusDiscountLineItems.length; i++) {
            var bonusLineSet = basketResponse.bonusDiscountLineItems[i];
            var copiedBonusLineItem = { promotionId: bonusLineSet.promotionId, ID: bonusLineSet.ID, maxBonusItems: bonusLineSet.maxBonusItems, bonusProducts: [] };
            for (var j = 0; j < bonusLineSet.bonusProducts.length; j++) {
                var bonusModifiedItems = {};
                for (var prop in bonusLineSet.bonusProducts[j]) { // eslint-disable-line
                    bonusModifiedItems[prop] = bonusLineSet.bonusProducts[j][prop];
                }
                hookHelpers.adjustProductAttributes(bonusModifiedItems.productId, bonusModifiedItems, 'tile');
                copiedBonusLineItem.bonusProducts.push(bonusModifiedItems);
            }
            bonusDiscountLineItems.push(copiedBonusLineItem);
        }
        basketResponse.c_bonusDiscountLineItems = bonusDiscountLineItems; // eslint-disable-line no-param-reassign
    }
    basketResponse.c_enableGiftWrapping = currentSite.getCustomPreferenceValue('enableGiftWrapping'); // eslint-disable-line no-param-reassign
    var giftProducts = Array.prototype.slice.call(currentSite.getCustomPreferenceValue('giftWrappingProducts'));
    var giftWrappingProducts = [];
    giftProducts.forEach(function (productId) {
        var ret = {
            productId: productId
        };
        hookHelpers.adjustProductAttributes(productId, ret);
        if (Object.keys(ret).length === 1 || !ret.c_assignedToSiteCatalog) {
            return;
        }
        giftWrappingProducts.push(ret);
    });
    basketResponse.c_giftWrappingProducts = JSON.stringify(giftWrappingProducts); // eslint-disable-line no-param-reassign
    basketResponse.c_isSpecialRequestEnabled = currentSite.getCustomPreferenceValue('hideSpecialMessageSection') !== true; // eslint-disable-line no-param-reassign
    return new Status(Status.OK);
};

exports.beforePATCH = function(basket, basketInput) { // eslint-disable-line
    var verificationHelpers = require('*/cartridge/scripts/helpers/verificationHelpers');
    if (basketInput.c_otpRequest && basketInput.c_otpPhone) {
        var otpStatus = verificationHelpers.requestOTP(basketInput.c_otpPhone);
        if (otpStatus.success) {
            return new Status(Status.OK);
        }
        return new Status(Status.ERROR, 101, otpStatus.message);
    }
    if (basketInput.c_otpVerify && basketInput.c_otpCode) {
        var verificationStatus = verificationHelpers.verifyOTP(basketInput.c_otpCode);
        if (verificationStatus.success) {
            verificationHelpers.updateSmsVerificationStatus(true);
            return new Status(Status.OK);
        }
        return new Status(Status.ERROR, 101, verificationStatus.message);
    }
};

exports.afterPUT = function (basket, shipment, shippingAddress) { // eslint-disable-line
    var verificationHelpers = require('*/cartridge/scripts/helpers/verificationHelpers');
    verificationHelpers.getSmsVerificationStatus();
    return new Status(Status.OK);
};
