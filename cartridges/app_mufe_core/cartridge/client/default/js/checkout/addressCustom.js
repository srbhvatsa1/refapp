'use strict';

var urlHelper = require('brand_core/helpers/urlHelper');
var formValidation = require('base/components/formValidation');
var clientSideValidation = require('brand_core/components/clientSideValidation');
var fieldCity = require('brand_core/addressBook/fieldCity');

var formHelpers = require('brand_core/checkout/formErrors');

var comparableAddressFields = [
    'input[name$=_firstName]',
    'input[name$=_lastName]',
    'input[name$=_address1]',
    'input[name$=_address2]',
    'input[name$=_phone]',
    'select[name$=_country],input[name$=_country]',
    'select[name$=_stateCode],input[name$=_stateCode]',
    'input[name$=_city]'
];

/**
 * Syncs shipping and billing addresses
 */
function syncBillingAndShippingAddresses() {
    var $billingAddressForm = $('.js-form-billing-address');
    var $shippingAddressForm = $('.js-form-shipping-address');

    comparableAddressFields.forEach(function (selector) {
        var $billingField = $billingAddressForm.find(selector);
        var $shippingField = $shippingAddressForm.find(selector);

        if ($billingField.length && $shippingField.length) {
            $billingField.val($shippingField.val());
            if ($billingField.is('select')) {
                $billingField.trigger('change');
                $billingField.selectpicker('refresh');
            }
            if ($billingField.is('input')) $billingField.trigger('input');
        }
    });

    formHelpers.clearPreviousErrors($billingAddressForm);
}

/**
 * Updates city select box
 */
function updateCitySelectbox() {
    var $city = $('select.js-selectbox-city');
    if ($city.length) {
        var $selectBox = $('.js-address-selector-selectbox');
        var currentCity = $selectBox.find(':selected').data('city');
        $city.val(currentCity);

        var hasSelectpicker = !!$city.selectpicker;
        if (hasSelectpicker) {
            $city.selectpicker('refresh');
            $city.trigger('change');
        }
    }
}

/**
 * Updates state select box
 */
function updateStateSelectbox() {
    var $state = $('select.js-selectbox-state');
    if ($state.length) {
        var $selectBox = $('.js-address-selector-selectbox');
        var currentState = $selectBox.find(':selected').data('state-code');
        $state.val(currentState);

        var hasSelectpicker = !!$state.selectpicker;
        if (hasSelectpicker) {
            $state.selectpicker('refresh');
            $state.trigger('change');
        }
    }
}

/**
 * Updates set default address checkbox
 * @param {string} addressId - Address ID
 */
function updateSetDefaultAddressCheckbox(addressId) {
    var $selectBox = $('.js-address-selector-selectbox');
    var $setDefaultCheckbox = $('.js-set-address-default-checkbox');
    var $selectedOption = $selectBox.find(':selected');
    if ($selectedOption.data('is-default') || addressId === 'new') {
        $setDefaultCheckbox
            .prop('checked', true)
            .parent().addClass('d-none');
    } else {
        $setDefaultCheckbox
            .prop('checked', false)
            .parent().removeClass('d-none');
    }
}

/**
 * Updates title radio group
 */
function updateTitleRadioGroup() {
    var $selectBox = $('.js-address-selector-selectbox');
    var $selectedOption = $selectBox.find(':selected');
    var title = $selectedOption.data('title') || null;
    $('.js-radio-title')
        .prop('checked', false)
        .filter('[value="' + title + '"]')
        .prop('checked', true);
}

/**
 * Selects given address id on both selectbox and radio group
 * @param {string} addressId - Address ID
 */
function selectAddress(addressId) {
    var $selectBox = $('.js-address-selector-selectbox');
    var $radioGroup = $('.js-address-selector-radio-btn');
    var $address = $selectBox.find('[value="' + addressId + '"]');
    $('.js-address-item').removeClass('address-selected');

    if ($address.length === 0) {
        if (addressId !== 'new') {
            selectAddress('new');
        }
        return;
    }

    $radioGroup.filter(':checked').prop('checked', false);
    $radioGroup.filter('[value="' + addressId + '"]')
        .prop('checked', true)
        .closest('.js-address-item')
        .addClass('address-selected');
    $selectBox.val(addressId);
    $selectBox.trigger('change');
    updateStateSelectbox();
    updateCitySelectbox();
    updateSetDefaultAddressCheckbox(addressId);
    updateTitleRadioGroup();
}

/**
 * Shows edit address form
 */
function showEditAddressForm() {
    var $selectBox = $('.js-address-selector-selectbox');
    $selectBox.parent().find('.btn-show-details').trigger('click');
}

/**
 * Initializes address book select box
 */
function initAddressBookSelectbox() {
    var $radioGroup = $('.js-address-selector-radio-btn:not([value=""])');
    var $hideFormButton = $('.js-hide-shipping-address-form').hide();

    if ($radioGroup.length) {
        var $radioGroupChecked = $radioGroup.filter(':checked');

        if ($radioGroupChecked.length === 0) {
            $radioGroupChecked = $radioGroup.first();
        }

        selectAddress($radioGroupChecked.val());
        $hideFormButton.show();
        $radioGroup.off('change').on('change', function () {
            selectAddress($radioGroup.filter(':checked').val());
        });
    } else if ($('[data-customer-type="registered"]').length) {
        selectAddress('new');
    }
}

/**
 * Disables step buttons
 */
function disableStepButtons() {
    var $shippingForm = $('.js-shipping-form');
    if ($shippingForm.find('.js-save-address-btn').length) {
        $('.js-submit-shipping-btn, .js-submit-payment-btn, .js-place-order-btn')
            .prop('disabled', true);
    }
}

/**
 * Closes address form
 * @param {jQuery} $form - jQuery form element
 */
function closeAddressForm($form) {
    initAddressBookSelectbox();

    // Clear error message
    $('.js-error-message-text').empty().parent().hide();

    // Enable next step button
    $('.js-submit-shipping-btn, .js-submit-payment-btn, .js-place-order-btn')
        .prop('disabled', false);

    // Change mode to customer
    $form.attr('data-address-mode', 'customer');
}

/**
 * One time even listener for shipment method updates that is
 * triggered by opening add or edit address forms
 */
function updateMultiShipInformationOnOpenAddressForm() {
    var $shippingForm = $('.js-shipping-form');
    var mode = $shippingForm.attr('data-address-mode');
    var modes = ['new', 'edit', 'details'];
    if (modes.indexOf(mode) > -1) {
        disableStepButtons();
    }
}

/**
 * Initializes radio group address selector
 */
function initAddressSelector() {
    var $body = $('body');

    $body.on('shipping:updateMultiShipInformation',
        updateMultiShipInformationOnOpenAddressForm);

    $body.on('click', '.js-edit-address-btn', function () {
        var $this = $(this);
        var $form = $this.closest('form');
        var addressId = $this.data('value');

        selectAddress(addressId);
        showEditAddressForm();
        clientSideValidation.functions.validateForm($form[0]);
        disableStepButtons();
    });

    $('.js-add-address-btn').on('click', function () {
        // Clear city and state selection
        fieldCity.clear($(this).closest('form'));

        // Remove title selection
        $('.js-radio-title').prop('checked', false);

        updateSetDefaultAddressCheckbox();
        disableStepButtons();
    });

    initAddressBookSelectbox();
}

/**
 * Updates selected address data
 * @param {string} type - Type of the address form, shipping  or billing
 * @param {Object} data - Data object that is returned from the save address action
 */
function updateAddressSelectorItem(type, data) {
    // Stop here, if data is not supplied
    if (!data || Object.keys(data).length === 0) return;

    var $selectBox = $('.js-address-selector-selectbox');
    var $radioGroup = $('.js-address-selector-radio-btn');
    var $selectedOption = $selectBox.find(':selected');
    var $radioGroupChecked = $radioGroup.filter(':checked');
    var $radioGroupItem = $radioGroupChecked.closest('.js-address-item');
    var addressValue = 'ab_' + data.addressId;
    var address = [
        data.firstName,
        data.lastName + ' -',
        data.address1,
        data.address2,
        data.states ? data.states.stateCode : '',
        data.city
    ].join(' ');

    // New address
    if ($selectedOption.val() !== addressValue) {
        $selectedOption.prop('selected', false);
        $selectedOption = $selectedOption.clone();
        $selectedOption.prop('selected', true);
        $selectedOption.val(addressValue);
        $selectBox.append($selectedOption);

        var now = Date.now();
        var $radioGroupTemplate = $('.js-address-selector-radio-btn[value=""]').closest('.js-address-item');
        var $radioGroupParent = $radioGroupTemplate.parent();
        $radioGroupChecked.prop('checked', false);
        $radioGroupItem = $radioGroupTemplate.clone().removeClass('d-none');
        $radioGroupChecked = $radioGroupItem.find('.js-address-selector-radio-btn');
        $radioGroupChecked.prop('checked', true);
        $radioGroupChecked.prop('disabled', false);
        $radioGroupChecked.val(addressValue);
        $radioGroupChecked.attr('id', 'address' + now);
        $radioGroupChecked.next().attr('for', 'address' + now);
        $radioGroupItem.find('.js-edit-address-btn').data('value', addressValue);
        $radioGroupParent.append($radioGroupItem);
    }

    // Update data properties of selected option
    $selectedOption.data('title', (data.title || {}).titleList);
    $selectedOption.data('first-name', data.firstName);
    $selectedOption.data('last-name', data.lastName);
    $selectedOption.data('address1', data.address1);
    $selectedOption.data('address2', data.address2);
    $selectedOption.data('city', data.city);
    $selectedOption.data('state-code', (data.states || {}).stateCode);
    $selectedOption.data('country-code', data.country);
    $selectedOption.data('phone', data.phone);
    $selectedOption.data('email', data.email);
    $selectedOption.text(address);

    // Update seleted radio button's label
    $radioGroupChecked.next().text(address);

    // Update default address
    if (data.setDefault) {
        $selectBox
            .find('[data-is-default="true"]')
            .data('is-default', false);

        $selectedOption.data('is-default', true);

        $('.js-address-item').removeClass('address-default');
        $('.js-address-item-default-txt').addClass('d-none');

        $radioGroupItem
            .addClass('address-default')
            .find('.js-address-item-default-txt')
            .removeClass('d-none');
    } else {
        $selectedOption.data('is-default', false);
        $radioGroupItem
            .removeClass('address-default')
            .find('.js-address-item-default-txt')
            .addClass('d-none');
    }
}

/**
 * Updates shipment data on basket object
 * Because this is already a fallback request,
 * there is no need to wait a response.
 * @param {jQuery} $form - jQuery form element
 */
function updateBasketShipmentData($form) {
    $.ajax({
        url: $form.attr('action'),
        type: 'post',
        data: $form.serialize()
    });
}

/**
 * Initializes address froms
 */
function initAddressForm() {
    $('.js-hide-shipping-address-form').on('click', function () {
        closeAddressForm($(this).closest('form'));
    });

    $('.js-save-address-btn').on('click', function () {
        var $saveBtn = $(this);
        var $form = $(this).closest('form');
        var $errMsg = $('.js-error-message-text');

        // Clear previous error message
        $errMsg.empty().parent().hide();

        var $syncBillingCheckbox = $('.js-checkbox-sync-shipping-and-billing');
        if ($syncBillingCheckbox.length) {
            var isChecked = $syncBillingCheckbox.is(':checked');
            if (isChecked) syncBillingAndShippingAddresses();
        }

        // Validate form, if there are any errors stop here
        if (clientSideValidation.functions.validateForm($form[0]) === false) return;

        var $selectBox = $('.js-address-selector-selectbox');
        var type = $saveBtn.data('type');
        var addressId = $selectBox.val().replace('ab_', '');
        var url = $saveBtn.data('url');
        var params = $form.serialize();
        var addressOne = ($selectBox.find(':selected').data('country-code') + '-' + $('.js-input-address-one').val()).substring(0, 20);

        if (addressId !== 'new') {
            url = urlHelper.setParameter('addressId', addressId, url);
            params += '&' + $.param({ dwfrm_address_addressId: addressId });
        } else {
            params += '&' + $.param({ dwfrm_address_addressId: addressOne });
        }

        params = params.replace(/dwfrm_shipping_shippingAddress_addressFields/g, 'dwfrm_address');

        $form.spinner().start();

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: params,
            success: function (data) {
                $form.spinner().stop();
                if (data) {
                    if (!data.success) {
                        var dataStr = JSON.stringify(data)
                            .replace(/dwfrm_address/g, 'dwfrm_shipping_shippingAddress_addressFields');
                        formValidation($form, JSON.parse(dataStr));
                    } else {
                        updateAddressSelectorItem(type, data);
                        updateBasketShipmentData($form);
                        closeAddressForm($form);
                    }
                } else {
                    $errMsg.text($errMsg.data(type + '-error'));
                    $errMsg.parent().show();
                }
            },
            error: function () {
                $errMsg.text($errMsg.data(type + '-error'));
                $errMsg.parent().show();
                $form.spinner().stop();
            }
        });
    });

    $('body').on('checkout:updateStage:error', function (e, form) {
        if ($('[data-checkout-stage="shipping"]').length && $(form).attr('data-address-mode') === 'customer') {
            showEditAddressForm();
            disableStepButtons();
        }
    });
}

module.exports = {
    initialize: function () {
        fieldCity.init();
        initAddressSelector();
        initAddressForm();
    }
};
