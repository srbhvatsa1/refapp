'use strict';

var server = require('server');
server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');

server.replace('Show', server.middleware.get, cache.applyPromotionSensitiveCache, function (req, res, next) {
    var productHelpers = require('*/cartridge/scripts/helpers/productHelpers');

    // The req parameter has a property called querystring. In this use case the querystring could
    // have the following:
    // pid - the Product ID
    // ratings - boolean to determine if the reviews should be shown in the tile.
    // swatches - boolean to determine if the swatches should be shown in the tile.
    //
    // pview - string to determine if the product factory returns a model for
    //         a tile or a pdp/quickview display
    var productTileParams = { pview: 'tile' };
    Object.keys(req.querystring).forEach(function (key) {
        productTileParams[key] = req.querystring[key];
    });
    var context = productHelpers.getProductTileContext(productTileParams);
    res.render('product/gridTile.isml', context);

    next();
});

module.exports = server.exports();
