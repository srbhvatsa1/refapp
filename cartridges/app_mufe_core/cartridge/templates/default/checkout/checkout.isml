<isinclude template="/components/modules" sf-toolkit="off" />
<isdecorate template="common/layout/checkout">
    <isscript>
        var assets = require('*/cartridge/scripts/assets.js');
        assets.addJs('/js/checkout.js');
        assets.addCss('/css/checkout.css');
    </isscript>

    <isif condition="${pdict.reportingURLs && pdict.reportingURLs.length}">
        <isinclude template="reporting/reportingUrls" />
    </isif>

    <div class="checkout-step-page">
        <div id="checkout-main" class="data-checkout-stage checkout-main ${pdict.order.usingMultiShipping && pdict.order.shipping.length > 1 ? 'multi-ship' : ''}"
            data-customer-type="${pdict.customer.registeredUser ? 'registered' : 'guest'}"
            data-checkout-stage="${pdict.currentStage}"
            data-checkout-get-url="${URLUtils.https('CheckoutServices-Get')}">

            <div class="checkout-steps-background">
                <div class="checkout-steps-container container mb-2 mb-lg-5">
                    <div class="row">
                        <div class="col">
                            <div class="checkout-steps-wrapper">
                                <isif condition="${pdict.customer.registeredUser}">
                                    <a href="${URLUtils.https('Checkout-Login')}">
                                        <span>${Resource.msg('step.sign.in', 'checkout', null)}</span>
                                         <span class="checkout-check-icon">
                                            <i class="icon-check"></i>
                                        </span>
                                    </a>
                                <iselse/>
                                    <a href="${URLUtils.https('Checkout-Login')}">
                                        ${Resource.msg('step.sign.in', 'checkout', null)}
                                        <span class="checkout-check-icon">
                                            <i class="icon-check"></i>
                                        </span>
                                    </a>
                                </isif>
                                <span class="checkout-steps-seperator"></span>
                                <a href="${'#'}" class="checkout-step-shipping-title js-show-shipping-step">
                                    ${Resource.msg('step.shipping', 'checkout', null)}
                                    <span class="checkout-check-icon">
                                        <i class="icon-check"></i>
                                    </span>
                                </a>
                                <span class="checkout-steps-seperator"></span>
                                <span class="checkout-step-payment-title">${Resource.msg('step.payment', 'checkout', null)}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="cart-menu-vertical d-block d-md-none">
                <div class="cart-section-wrapper js-acc-container checkout-order-product-summary">
                    <h3 data-toggle-class-open="parent">
                        <i class="sc-icon-free-samples tab-icon"></i> 
                        ${Resource.msg('heading.view.order.summary', 'checkout', null)}
                        <i class="sc-icon-arrow-down"></i>
                        <i class="sc-icon-arrow-up"></i>
                        <span class="checkout-grand-total">${pdict.order.totals.grandTotal}</span>
                    </h3>
                    <div class="cart-section-wrapper-content js-view-order-summary-mobile pt-0">
                        <iscomment> checkout/orderProductSummary template is added with js </iscomment>
                    </div> 
                 </div>
            </div> 

            <div class="checkout-steps-header checkout-contact-step">
                <span class="d-md-none">
                    ${Resource.msg('heading.contact.information', 'checkout', null)}
                </span>
            </div>
            <div class="checkout-steps-header checkout-payment-step">
                <span class="d-md-none">
                    ${Resource.msg('label.payment', 'checkout', null)}
                </span>
            </div>
 
            <div class="container w-mobile-100 mb-0 mb-md-5">
                <div class="row">
                    <div class="col-12 bg-white">
                        <div class="checkout-steps-content">
                            <h1 class="container-title shipping-title">
                                <span class="d-none d-md-block">
                                    ${Resource.msg('label.shipping', 'checkout', null)}
                                </span> 
                            </h1>
                            <h1 class="container-title payment-title">
                                <span class="d-none d-md-block"> 
                                    ${Resource.msg('label.payment', 'checkout', null)}
                                </span> 
                            </h1>
                            <div class="row">
                                <div class="col-md-7 col-lg-8 checkout-steps-content-left">
                                    <iscomment>Checkout Forms: Shipping, Payment, Coupons, Billing, etc</iscomment>
                                    <div class="alert alert-danger error-message mt-0 mb-4" role="alert" <isif condition="${pdict.pspErrorMessage}">style="display:block"</isif>>
                                        <div class="error-message-text mb-0 js-error-message-text"
                                            data-shipping-error="${Resource.msg('error.no.shipping.address', 'checkout', null)}"
                                            data-billing-error="${Resource.msg('error.no.billing.address', 'checkout', null)}">
                                            <isif condition="${pdict.pspErrorMessage}">
                                                <p><isprint value="${pdict.pspErrorMessage}" /></p>
                                            </isif>
                                            <isif condition="${session.custom.knetPaymentId}">
                                                <p><strong>${Resource.msg('knet.payment.id.label', 'knet', null)} </strong>${session.custom.knetPaymentId}</p>
                                            </isif>
                                            <isif condition="${session.custom.knetTransactionId}">
                                                <p><strong>${Resource.msg('knet.transaction.id.label', 'knet', null)} </strong>${session.custom.knetTransactionId}</p>
                                            </isif>
                                            <isscript>
                                                delete session.custom.knetPaymentId;
                                                delete session.custom.knetTransactionId;
                                            </isscript>
                                        </div>
                                    </div>

                                    <iscomment>Step 1: Shipping</iscomment>
                                    <isinclude template="checkout/shipping/shipping" />

                                    <iscomment>Step 2: Payment and Billing</iscomment>
                                    <isinclude template="checkout/billing/billing" />
                                </div>

                                <iscomment>Order Totals, Details and Summary</iscomment>
                                <div class="col-md-5 col-lg-4 sticky-sidebar-container js-sticky-sidebar-container ">
                                    <div class="card js-sticky-sidebar mb-0">
                                        <div class="js-sticky-sidebar-inner checkout-sticky-bar">
                                            <div class="card-body order-total-summary">
                                                <isinclude template="checkout/orderTotalSummary" />

                                                <iscomment>Checkout Workflow Buttons</iscomment>
                                                <div class="row">
                                                    <div class="col-12 next-step-button">
                                                        <div>
                                                            <button class="btn btn-primary btn-block submit-shipping js-submit-shipping-btn" type="submit" name="submit" value="submit-shipping"
                                                                <isif condition="${pdict.order.usingMultiShipping && !pdict.order.shippable}">disabled</isif>>
                                                                ${Resource.msg('button.continue.payment', 'checkout', null)}
                                                            </button>

                                                            <button class="btn btn-primary btn-block submit-payment mt-0 js-submit-payment-btn" type="submit" name="submit" value="submit-payment">
                                                                <i class="icon-secure-2 mr-3"></i>
                                                                ${Resource.msg('button.place.order', 'checkout', null)}
                                                                <span>${pdict.order.totals.grandTotal}</span>
                                                            </button>

                                                            <button class="btn btn-primary btn-block place-order mt-0 d-none js-place-order-btn" data-action="${URLUtils.url('CheckoutServices-PlaceOrder')}"
                                                                    type="submit" name="submit" value="place-order">${Resource.msg('button.place.order', 'checkout', null)}
                                                            </button>
                                                            <div class="terms-and-conditions-warning">
                                                                <iscontentasset aid="payment-agree-terms-conditions" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <iscomment>Help and Phone</iscomment>
                                                <div class="row ">
                                                    <div class="col-12">
                                                        <iscontentasset aid="checkout-supported-payment-methods" />
                                                    </div>
                                                </div>
                                            </div>

                                             <div class="cart-menu-vertical d-none d-md-block mt-5">
                                                <div class="view-order-summary js-view-order-summary-desktop">
                                                    <isinclude template="checkout/orderProductSummary" />
                                                </div>
                                            </div> 

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                 
        </div>
    </div>

    <isinclude template="checkout/shipping/shippingAddressTemplate" />
    <isinclude template="checkout/billing/termsAndConditionsModal" />
</isdecorate>
