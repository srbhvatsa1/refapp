'use strict';

var server = require('server');
server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var verificationHelpers = require('*/cartridge/scripts/helpers/verificationHelpers');
/**
 * Handle Ajax shipping form submit
 */
server.replace(
    'SubmitShipping',
    server.middleware.https,
    server.middleware.post,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var BasketMgr = require('dw/order/BasketMgr');
        var URLUtils = require('dw/web/URLUtils');
        var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
        var validationHelpers = require('*/cartridge/scripts/helpers/basketValidationHelpers');
        var currentSite = require('dw/system/Site').current;
        var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');
        var Transaction = require('dw/system/Transaction');

        var currentBasket = BasketMgr.getCurrentBasket();
        var currentCountryCode = localeHelpers.getCurrentCountryCode();
        var validatedProducts = validationHelpers.validateProducts(currentBasket);

        if (!currentBasket || validatedProducts.error) {
            res.json({
                error: true,
                cartError: true,
                fieldErrors: [],
                serverErrors: [],
                redirectUrl: URLUtils.url('Cart-Show').toString()
            });
            return next();
        }

        var form = server.forms.getForm('shipping');
        var result = {};

        // verify shipping form data
        var shippingFormErrors = COHelpers.validateShippingForm(form.shippingAddress.addressFields);

        if (Object.keys(shippingFormErrors).length > 0) {
            req.session.privacyCache.set(currentBasket.defaultShipment.UUID, 'invalid');

            res.json({
                form: form,
                fieldErrors: [shippingFormErrors],
                serverErrors: [],
                error: true
            });
        } else {
            req.session.privacyCache.set(currentBasket.defaultShipment.UUID, 'valid');

            result.address = {
                firstName: form.shippingAddress.addressFields.firstName.value,
                lastName: form.shippingAddress.addressFields.lastName.value,
                address1: form.shippingAddress.addressFields.address1.value,
                address2: form.shippingAddress.addressFields.address2.value,
                city: form.shippingAddress.addressFields.city.value,
                postalCode: form.shippingAddress.addressFields.postalCode.value,
                countryCode: form.shippingAddress.addressFields.country.value || currentCountryCode,
                phone: form.shippingAddress.addressFields.phone.value
            };

            if (Object.prototype.hasOwnProperty
                .call(form.shippingAddress.addressFields, 'title')) {
                result.address.title =
                    form.shippingAddress.addressFields.title.titleList.value;
            }

            if (Object.prototype.hasOwnProperty
                .call(form.shippingAddress.addressFields, 'states')) {
                result.address.stateCode =
                    form.shippingAddress.addressFields.states.stateCode.value;
            }

            if (Object.prototype.hasOwnProperty
                .call(form.shippingAddress.addressFields, 'email')) {
                result.email = form.shippingAddress.addressFields.email.value;
            }

            result.shippingBillingSame =
                form.shippingAddress.shippingAddressUseAsBillingAddress.value;

            result.shippingMethod = form.shippingAddress.shippingMethodID.value
                ? form.shippingAddress.shippingMethodID.value.toString()
                : null;

            var sendAsGiftEnabled = // check if the gift fields have been provided and site has gift enabled
                req.form
                && !!req.form[form.shippingAddress.gift.isGift.htmlName]
                && currentSite.getCustomPreferenceValue('enableSendAsGift');
            if (sendAsGiftEnabled) {
                result.isGift = form.shippingAddress.gift.isGift.checked;
                result.giftMessage = result.isGift && !currentSite.getCustomPreferenceValue('disableGiftMessage') ? form.shippingAddress.gift.giftMessage.value : null;
                result.isPriceHidden = result.isGift ? form.shippingAddress.gift.isPriceHidden.checked : null;
                result.isGiftWrapped = result.isGift || form.shippingAddress.gift.isGiftWrapped.checked;
            }

            // Compare previous basket address & new form values for COD SMS verification
            if (currentBasket && currentBasket.getDefaultShipment() && currentBasket.getDefaultShipment().shippingAddress) {
                var shippingAddress = currentBasket.getDefaultShipment().shippingAddress;
                if (!verificationHelpers.compareAddresses(shippingAddress, result.address) && shippingAddress.custom.isSmsVerified) {
                    verificationHelpers.updateBasketSmsVerificationStatus(false);
                }
            }

            // form special request
            if (form.specialRequest) {
                result.specialRequest = form.specialRequest.value;
            }

            res.setViewData(result);

            this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
                var AccountModel = require('*/cartridge/models/account');
                var OrderModel = require('*/cartridge/models/order');
                var shippingData = res.getViewData();

                COHelpers.copyShippingAddressToShipment(
                    shippingData,
                    currentBasket.defaultShipment
                );

                // Assign customer email
                COHelpers.setBasketCustomerEmail(
                    shippingData.email,
                    currentBasket
                );

                // sets the special request if provided to the shipment
                if (result.specialRequest) {
                    COHelpers.setSpecialRequest(
                        result.specialRequest,
                        currentBasket.defaultShipment
                    );
                }

                if (sendAsGiftEnabled) {
                    var giftResult = COHelpers.setGift(currentBasket.defaultShipment,
                        shippingData.isGift, shippingData.giftMessage, shippingData.isPriceHidden, shippingData.isGiftWrapped);

                    if (giftResult.error) {
                        res.json({
                            error: giftResult.error,
                            fieldErrors: [],
                            serverErrors: [giftResult.errorMessage]
                        });
                        return;
                    }
                }

                COHelpers.copyBillingAddressToBasket(
                    currentBasket.defaultShipment.shippingAddress, currentBasket);

                var billingForm = server.forms.getForm('billing');
                if (billingForm) {
                    Transaction.wrap(function () {
                        currentBasket.billingAddress.setFirstName(billingForm.addressFields.firstName.htmlValue);
                        currentBasket.billingAddress.setLastName(billingForm.addressFields.lastName.htmlValue);
                        currentBasket.billingAddress.setAddress1(billingForm.addressFields.address1.htmlValue);
                        currentBasket.billingAddress.setAddress2(billingForm.addressFields.address2.htmlValue);
                        currentBasket.billingAddress.setCity(billingForm.addressFields.city.htmlValue);
                        currentBasket.billingAddress.setPostalCode(billingForm.addressFields.postalCode.htmlValue);
                        currentBasket.billingAddress.setPhone(billingForm.addressFields.phone.htmlValue);
                    });
                }

                var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
                if (usingMultiShipping === true && currentBasket.shipments.length < 2) {
                    req.session.privacyCache.set('usingMultiShipping', false);
                    usingMultiShipping = false;
                }

                COHelpers.recalculateBasket(currentBasket);

                var basketModel = new OrderModel(
                    currentBasket,
                    {
                        usingMultiShipping: usingMultiShipping,
                        shippable: true,
                        countryCode: currentCountryCode,
                        containerView: 'basket'
                    }
                );

                res.json({
                    customer: new AccountModel(req.currentCustomer),
                    order: basketModel,
                    form: server.forms.getForm('shipping')
                });
            });
        }

        return next();
    }
);

module.exports = server.exports();
