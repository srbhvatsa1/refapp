'use strict';

var server = require('server');

var StringUtils = require('dw/util/StringUtils');
var URLUtils = require('dw/web/URLUtils');
var logger = require('dw/system/Logger').getLogger('payfort.return');
var payfortHelpers = require('*/cartridge/scripts/helpers/payfortHelpers');
var payfortConstants = require('*/cartridge/scripts/util/payfortConstants');

/**
 * Error processed for Return end point
 * @param {dw.order.Order} order - The order to be proccesed
 * @param {string} logMessage - log message to be logged
 */
function returnError(order, logMessage) {
    if (order) {
        payfortHelpers.failOrder(order);
    }

    logger.error(logMessage);
}

/**
 * Processes the response both for Tokenization and Authorization
 * @param {Object} req - the Request Object
 * @param {Object} res - the Response object
 * @param {string} endPoint - the endPoint
 * @returns {Object} - processed result
 */
function processResponse(req, res, endPoint) {
    var OrderMgr = require('dw/order/OrderMgr');
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var PaymentMgr = require('dw/order/PaymentMgr');

    var postedPayload = req.form;
    // check the posted form and merchant_reference in it
    if (!postedPayload || !postedPayload.merchant_reference) {
        returnError(
            null,
            StringUtils.format('Error on PayFortReturn-{0}, posted payload or merchant_reference is empty, payload: {1}', endPoint, payfortHelpers.getLogablePayload(postedPayload))
        );
        res.redirect(URLUtils.url('Cart-Show', 'psperror', '1'));
        return {
            error: true
        };
    }

    var order = OrderMgr.getOrder(postedPayload.merchant_reference);
    if (!order) {
        returnError(
            null,
            StringUtils.format('Error on PayFortReturn-{0}, order is empty, payload: {1}', endPoint, payfortHelpers.getLogablePayload(postedPayload))
        );
        res.redirect(URLUtils.url('Cart-Show', 'psperror', '1'));
        return {
            error: true
        };
    }

    // add payload to order note as response
    payfortHelpers.addNoteToOrder(order, StringUtils.format('Response {0}', endPoint.toUpperCase()), postedPayload);

    // check the status of tokenization/authorization
    // if it is not success then fail the order and stop process
    // 18 : Tokenization success.
    // 02 : Authorization Success.
    // https://docs.payfort.com/docs/in-common/build/index.html#statuses
    if (postedPayload.status !== payfortConstants.serviceStatus.tokenizationSuccess // 18 Tokenization success.
        && postedPayload.status !== payfortConstants.serviceStatus.authorizationSuccess // 02 Authorization Success.
        && postedPayload.status !== payfortConstants.serviceStatus.purchaseSuccess) { // 14 Purchase Success.
        // set last payment status as declined
        payfortHelpers.setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.declined);

        returnError(
            order,
            StringUtils.format('Error on PayFortReturn-{0}, posted payload status is not OK, orderNo: {1}, payload: {2}', endPoint, order.orderNo, payfortHelpers.getLogablePayload(postedPayload))
        );

        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
        return {
            error: true
        };
    }

    // checks the posted token_name
    // if it is empty then fail the order and stop process
    /* NO NEED to fail the order if token_name is not provided, Tokenization Service could be inactive, need to activate it with PayFort ticket for one click checkout
    if (!postedPayload.token_name) {
        // set last payment status as error
        payfortHelpers.setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, 'Token name is not provided.');

        returnError(
            order,
            StringUtils.format('Error on PayFortReturn-{0}, token_name is not in posted payload, orderNo: {1}, payload: {2}', endPoint, order.orderNo, payfortHelpers.getLogablePayload(postedPayload))
        );
        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
        return {
            error: true
        };
    }*/

    var paymentInstruments = order.getPaymentInstruments(PaymentInstrument.METHOD_CREDIT_CARD);
    if (paymentInstruments.empty) {
        // set last payment status as error
        payfortHelpers.setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, 'Payment Instruments are empty.');

        returnError(
            order,
            postedPayload,
            StringUtils.format('Error on PayFortReturn-{0}, order payment instrument is empty, orderNo: {1}, payload: {2}', endPoint, order.orderNo, payfortHelpers.getLogablePayload(postedPayload))
        );
        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
        return {
            error: true
        };
    }

    var paymentInstrument = paymentInstruments[0];
    var paymentMethod = PaymentMgr.getPaymentMethod(paymentInstrument.paymentMethod);
    if (!paymentMethod) {
        // set last payment status as error
        payfortHelpers.setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, 'Payment Method is empty.');

        returnError(
            order,
            StringUtils.format('Error on PayFortReturn-{0}, payment method is empty, orderNo: {1}, payload: {2}', endPoint, order.orderNo, payfortHelpers.getLogablePayload(postedPayload))
        );
        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
        return {
            error: true
        };
    }

    var paymentProcessor = paymentMethod.paymentProcessor;
    if (!paymentProcessor) {
        // set last payment status as error
        payfortHelpers.setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, 'Payment Processor is empty.');

        returnError(
            order,
            StringUtils.format('Error on PayFortReturn-{0}, payment processor is empty, orderNo: {1}, payload: {2}', endPoint, order.orderNo, payfortHelpers.getLogablePayload(postedPayload))
        );
        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
        return {
            error: true
        };
    }

    return {
        success: true,
        postedPayload: postedPayload,
        order: order,
        paymentInstrument: paymentInstrument,
        paymentProcessor: paymentProcessor
    };
}

/**
 * Continues on to finalize Authorize process
 * @param {Object} req - the Request Object
 * @param {Object} res - the Response object
 * @param {Object} next - the next function to be executed
 * @param {string} endPoint - the endPoint
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} postedPayload - The posted JSON payload object
 * @returns {Object} - the result object
 */
function continueAuthorization(req, res, next, endPoint, order, paymentInstrument, paymentProcessor, postedPayload) {
    var continueAuthorizedCreditCardResult = {};
    try {
        continueAuthorizedCreditCardResult = payfortHelpers.continueAuthorizedCreditCard(
            req,
            order,
            paymentInstrument,
            paymentProcessor,
            postedPayload
        );
    } catch (e) {
        // set last payment status as error
        payfortHelpers.setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, e.message);

        returnError(
            order,
            StringUtils.format('Error on PayFortReturn-{0}-continueAuthorization, exception occured, orderNo: {1}, message: {2}, payload: {3}',
                endPoint,
                order.orderNo,
                e.message,
                payfortHelpers.getLogablePayload(postedPayload)
            )
        );
        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
        return next();
    }

    if (continueAuthorizedCreditCardResult.error) {
        returnError(
            order,
            StringUtils.format('Error on PayFortReturn-{0}-continueAuthorization, orderNo: {1}, message: {2}, payload: {3}',
                endPoint,
                order.orderNo,
                continueAuthorizedCreditCardResult.message,
                payfortHelpers.getLogablePayload(postedPayload)
            )
        );

        if (continueAuthorizedCreditCardResult.fraudDetectionStatus
            && continueAuthorizedCreditCardResult.fraudDetectionStatus.status === 'fail') {
            res.redirect(continueAuthorizedCreditCardResult.redirectUrl);
            return next();
        }

        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
        return next();
    }

    // success, redirect to order confirmation
    res.redirect(URLUtils.url('Order-Confirm', 'ID', order.orderNo, 'token', order.orderToken));

    return next();
}

/**
 * Controller To Tokenization Return URL
 * to be called by payfort with Tokenization result
 */
server.post('Tokenization', function (req, res, next) {
    var responseResult = processResponse(req, res, 'Tokenization');
    if (responseResult.error) {
        return next();
    }

    var postedPayload = responseResult.postedPayload;
    var order = responseResult.order;
    var paymentInstrument = responseResult.paymentInstrument;
    var paymentProcessor = responseResult.paymentProcessor;

    var authorizeTokenizedCreditCardResult = {};
    try {
        authorizeTokenizedCreditCardResult = payfortHelpers.authorizeTokenizedCreditCard(
            order,
            paymentInstrument,
            paymentProcessor,
            postedPayload
        );
    } catch (e) {
        // set last payment status as error
        payfortHelpers.setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, e.message);

        returnError(
            order,
            StringUtils.format('Error on PayFortReturn-Tokenization, exception occured, orderNo: {0}, message: {1}, payload: {2}',
                order.orderNo,
                e.message, payfortHelpers.getLogablePayload(postedPayload)
            )
        );
        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
        return next();
    }

    if (authorizeTokenizedCreditCardResult.success) {
        return continueAuthorization(req, res, next, 'Tokenization', order, paymentInstrument, paymentProcessor, postedPayload);
    } else if (authorizeTokenizedCreditCardResult.redirect) {
        // one more redirect to 3-D Secure check, redirect to order 3-D url
        res.redirect(authorizeTokenizedCreditCardResult.redirectUrl);
    } else if (authorizeTokenizedCreditCardResult.error) {
        // error, please check log files and/or order note
        returnError(
            order,
            StringUtils.format('Error on PayFortReturn-Tokenization, authorizeTokenizedCreditCardResult is not succeeded, orderNo: {0}, message: {1} payload: {2}',
                order.orderNo,
                authorizeTokenizedCreditCardResult.message,
                payfortHelpers.getLogablePayload(postedPayload)
            )
        );
        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
    }

    return next();
});

/**
 * Controller To Authorization Return URL
 * to be called by payfort with Authorization result
 */
server.post('Authorization', function (req, res, next) {
    var responseResult = processResponse(req, res, 'Authorization');
    if (responseResult.error) {
        return next();
    }

    var payfortServiceHelpers = require('*/cartridge/scripts/helpers/payfortServiceHelpers');

    var postedPayload = responseResult.postedPayload;
    var order = responseResult.order;
    var paymentInstrument = responseResult.paymentInstrument;
    var paymentProcessor = responseResult.paymentProcessor;

    // validate authorization response signature
    var serviceCredential = payfortServiceHelpers.getServiceCredential(payfortServiceHelpers.payFortService, order, '');
    if (!payfortServiceHelpers.validateSignature(postedPayload, serviceCredential)) {
        returnError(
            order,
            StringUtils.format('Error on PayFortReturn-Authorization, Authorization Response Signature mismatch!, orderNo: {0}, payload: {1}',
                order.orderNo,
                payfortHelpers.getLogablePayload(postedPayload)
            )
        );
        res.redirect(URLUtils.url('Checkout-Begin', 'stage', 'payment', 'psperror', '1'));
    }

    return continueAuthorization(req, res, next, 'Authorization', order, paymentInstrument, paymentProcessor, postedPayload);
});

/**
 * Controller To Feedback Return URL
 * to be called by payfort with payment status updates
 */
server.post('Feedback', function (req, res, next) {
    var postedPayload = req.form;
    // check the posted form and merchant_reference in it
    if (!postedPayload || !postedPayload.merchant_reference) {
        // res.setStatusCode(500);
        res.render('feedback/feedback', {
            message: 'Error on PayFortReturn-Feedback, posted payload or merchant_reference is empty'
        });
        return next();
    }

    var OrderMgr = require('dw/order/OrderMgr');

    // check the order is found
    var order = OrderMgr.getOrder(postedPayload.merchant_reference);
    if (!order) {
        // res.setStatusCode(500);
        res.render('feedback/feedback', {
            message: StringUtils.format('Error on PayFortReturn-Feedback, Order could not be found, orderNo: {0}', postedPayload.merchant_reference)
        });
        return next();
    }

    payfortHelpers.addFeedback(order.orderNo, postedPayload.status, payfortHelpers.getLogablePayload(postedPayload));

    res.render('feedback/feedback', {
        message: 'Success'
    });
    return next();
});


module.exports = server.exports();
