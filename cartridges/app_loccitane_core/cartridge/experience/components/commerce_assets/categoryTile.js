'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var URLUtils = require('dw/web/URLUtils');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();
    model.image = ImageTransformation.getScaledImage(content.image);
    model.alt = content.alt || '';
    model.title = content.title || '';
    model.header = content.header || '';
    model.description = content.description || '';
    model.buttonName = content.buttonName || '';
    model.frame = content.frame === 'none' ? '' : content.frame;
    model.link = content.category ? URLUtils.url('Search-Show', 'cgid', content.category.getID()).toString() : content.link;

    return new Template('experience/components/commerce_assets/categoryTile').render(model).text;
};
