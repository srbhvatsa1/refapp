'use strict';

var server = require('server');
server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');

/**
 * This controller for render CLP
 */
server.get('ShowCatLanding', server.middleware.get, cache.applyShortPromotionSensitiveCache, consentTracking.consent, function (req, res, next) {
    var ProductSearchModel = require('dw/catalog/ProductSearchModel');
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var URLUtils = require('dw/web/URLUtils');
    var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
    var ProductSearch = require('*/cartridge/models/search/productSearch');
    var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');

    var COUNT_PRODUCTS = 8;// the maximum number of products on the catalog page
    var categories = [];
    var canonicalUrl = URLUtils.url('Search-ShowCatLanding', 'cgid', req.querystring.cgid);
    var apiProductSearch = new ProductSearchModel();
    var mainCategory = searchHelper.setupSearch(apiProductSearch, req.querystring);
    mainCategory.search();
    pageMetaHelper.setPageMetaTags(req.pageMetaData, mainCategory);

    mainCategory.category.subCategories.toArray().forEach(function (categoryLevelTwo) {
        var subCategories = categoryLevelTwo.subCategories;

        if (subCategories.length) {
            subCategories.toArray().forEach(function (category) {
                apiProductSearch = new ProductSearchModel();
                var params = {
                    sz: COUNT_PRODUCTS,
                    cgid: category.ID
                };

                apiProductSearch = searchHelper.setupSearch(apiProductSearch, params);
                apiProductSearch.search();

                var productSearch = new ProductSearch(
                    apiProductSearch,
                    params,
                    req.querystring.srule,
                    CatalogMgr.getSortingOptions(),
                    CatalogMgr.getSiteCatalog().getRoot()
                );

                pageMetaHelper.setPageMetaTags(req.pageMetaData, productSearch);

                if (Object.prototype.hasOwnProperty.call(productSearch, 'category') && productSearch.count) {
                    var newCategoryName = category.custom.clpCategoryName;

                    if (newCategoryName) {
                        productSearch.category.name = newCategoryName; // set new category name in object
                    }

                    categories.push(productSearch);
                }
            }, this);
        }
    }, this);

    res.render('rendering/category/catLanding', {
        canonicalUrl: canonicalUrl,
        categories: categories,
        mainCategory: mainCategory
    });
    next();
});

module.exports = server.exports();
