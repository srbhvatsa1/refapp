'use strict';

$(document).ready(function () {
    $('.js-view-more-button').click(function () {
        $('.js-load-more').removeClass('d-none');
        $('.js-view-more-button').addClass('d-none');
    });
});
