'use strict';

var config = require('*/cartridge/config/globalCarousel').option;
var URLUtils = require('dw/web/URLUtils');

/**
 * Counts the category: online and localeOffline!=true and searchable products
 * @param {dw.catalog.Category} category - Current category
 * @param {Object} custom - slot custom properties
 * @returns {Array} - array of products to be shown in recomendation area for this category
 */
function getRecomendationAreaContent(category, custom) {
    var result = {
        products: []
    };
    var customConfig = JSON.parse(custom.carouselConfig);
    result.carouselConfig = {
        desktop: JSON.stringify(customConfig.desktopConfig || config.desktopConfig),
        mobile: JSON.stringify(customConfig.tabletConfig || config.tabletConfig),
        tablet: JSON.stringify(customConfig.mobileConfig || config.mobileConfig)
    };
    var productsRecomend = category.getOnlineProducts().toArray().slice(0, custom.productCount || 12); // getProducts(); move 12 to category.custome.number of products in recomendation area?
    productsRecomend.forEach(function (element) {
        result.products.push({
            productID: element.getID(),
            productSearchHit: element
        });
    });

    return result;
}

/**
 * Get category url
 * @param {dw.catalog.Category} category - Current category
 * @returns {string} - Url of the category
 */
function getCategoryUrl(category) {
    return category.custom && 'alternativeUrl' in category.custom && category.custom.alternativeUrl
        ? category.custom.alternativeUrl
        : URLUtils.url('Search-Show', 'cgid', category.getID()).toString();
}

/**
 * Represents a single category with all of it's children
 * @param {dw.campaign.SlotContent} content - contentTop level categories
 * @param {Object} type - custom
 * @constructor
 */
function slot(content, type) {
    this.calloutMsg = content.getCalloutMsg();
    this.slotID = content.getSlotID();
    var custom = content.custom;
    switch (type) {
        case 'category':
            var category = content.getContent()[0];
            this.carouselContent = getRecomendationAreaContent(category, custom);
            this.categoryURL = getCategoryUrl(category);
            break;
        case 'product':
            var customConfig = JSON.parse(custom.carouselConfig);
            this.carouselContent = {
                carouselConfig: {
                    desktop: JSON.stringify(customConfig.desktopConfig || config.desktopConfig),
                    mobile: JSON.stringify(customConfig.tabletConfig || config.tabletConfig),
                    tablet: JSON.stringify(customConfig.mobileConfig || config.mobileConfig)
                }
            };
            break;
        default:
            break;
    }
}

module.exports = slot;
