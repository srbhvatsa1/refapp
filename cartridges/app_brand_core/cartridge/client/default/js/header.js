'use strict';

window.jQuery = window.$ = require('jquery');
var debounce = require('lodash/debounce');
var animationDuration = 300;
var scrollbarWidth = require('./helpers/scrollbarWidth');
var userAgent = require('./helpers/userAgent');
var isRtl = window.RA_DIRECTION === 'rtl';
var $body = $('body');
var lastOpenedDropdownClassName = '';

$(document).ready(function () {
    $('.nav-item').each(function () {
        // Underlines the category of the active page
        var $this = $(this);
        var $a = $this.find('> a');
        // ..../about-us.html relative path or full link
        if (window.location.href.indexOf($a.attr('href')) !== -1) {
            $a.addClass('current');
        }
    });
});

/**
 * Sets padding top of navbar to header height
 * in order to make header visible while navbar is opened.
 * @param {jQuery} $navbar Navbar element
 * @param {jQuery} $header Header element
 * @param {jQuery} $swsMobile Mobile search container element
 */
function setNavbarOffset($navbar, $header, $swsMobile) {
    var searchInputHeight = $swsMobile.hasClass('search-visible') ? $swsMobile.height() : 0;
    var offsetTop = $header.height() - searchInputHeight;
    $navbar.css('top', offsetTop);
}

/**
 * Checks if menu is rendered on desktop or mobile, needs to be called with context of an DOM element
 * @returns {boolean} true if desktop, false if mobile
 */
function isDesktop() {
    return $(this).closest('.js-main-menu').css('position') !== 'fixed';
}

/**
 * Hides scrollbar
 * @param {jQuery} $header Header element
 */
function hideScrollbar($header) {
    if (document.body.clientHeight > window.innerHeight) {
        document.body.style.overflow = 'hidden';
        document.body.style.paddingRight = scrollbarWidth + 'px';
        $header.get(0).style.right = scrollbarWidth + 'px'; // eslint-disable-line
        $('.js-v-scrollbar-shadow').width(scrollbarWidth).removeClass('d-none');
    }
}

/**
 * Shows scrollbar
 * @param {jQuery} $header Header element
 */
function showScrollbar($header) {
    document.body.style.overflow = 'auto';
    document.body.style.paddingRight = null;
    $header.get(0).style.right = null; // eslint-disable-line
    $('.js-v-scrollbar-shadow').addClass('d-none');
}

/**
 * Hides menu dropdown, needs to be called with context of an DOM element
 * @param {jQuery} $header Header element
 */
function hideMenuDropdown($header) {
    // If menu item is not hovered on desktop, don't go further
    if (isDesktop.call(this) === false) return;

    var $dropdown = $(this);
    var isAlreadyHidden = $dropdown.hasClass('show') === false;
    var $headerBackdrop = $('.js-header-backdrop');

    // If dropdown is already hidden, don't go further
    if (isAlreadyHidden) {
        return;
    }

    var $dropdownMenu = $dropdown.children('.dropdown-menu');
    var $dropdownToggle = $dropdown.children('.dropdown-toggle');
    var isMainMenuItem = $dropdown.hasClass('js-nav-item');

    if (isMainMenuItem) {
        $dropdownMenu.addClass('hiding');

        // Show scrollbar
        showScrollbar($header);

        $body.removeClass(lastOpenedDropdownClassName);

        // Hide dropdown
        $dropdownMenu[0].style.zIndex = null;
        $dropdownMenu[0].style.borderBottomWidth = 0;
        $dropdownMenu.slideUp({
            duration: animationDuration,
            done: function () {
                $dropdownMenu.removeClass('d-flex');
                $dropdownMenu[0].style.left = null;
                $dropdownMenu[0].style.width = null;
                $dropdownMenu[0].style.height = null;
                $dropdownMenu[0].style.maxHeight = null;
                $dropdownMenu.removeClass('hiding');
            }
        });
    } else {
        $dropdownMenu.removeClass('show');
    }

    $dropdown.removeClass('show');
    $dropdownToggle.removeClass('expanded active');
    $dropdownToggle.attr('aria-expanded', false);
    $headerBackdrop.hide();
}

/**
 * Shows menu dropdown, needs to be called with context of an DOM element
 * @param {jQuery} $navbar Navbar element
 * @param {jQuery} $header Header element
 * @param {string} dropdownName dropdownName
 */
function showMenuDropdown($navbar, $header, dropdownName) {
    // If menu item is not hovered on desktop, don't go further
    if (isDesktop.call(this) === false) return;

    var $dropdownToggle = $(this);
    var $dropdown = $dropdownToggle.parent();
    var isAlreadyShown = $dropdown.hasClass('show');
    var $headerBackdrop = $('.js-header-backdrop');

    // If dropdown is already shown, don't go further
    if (isAlreadyShown) return;

    var $dropdownMenu = $dropdown.children('.dropdown-menu');
    var isMainMenuItem = $dropdown.hasClass('js-nav-item');

    if (isMainMenuItem) {
        // The event is fired on mega menu

        if ($dropdownMenu.hasClass('hiding') === false) {
            var $slots = $dropdownMenu.find('.js-main-menu-slot');
            $slots.each(function () {
                var $slot = $(this);
                if ($slot.text().trim().length === 0) {
                    // Remove slot containers if it has no content
                    $slot.remove();
                } else {
                    // Move slot containers to the end of menu items
                    $slot.parent().append($slot);
                }
            });

            // Move view all link to the end of list
            var $viewAllLink = $dropdownMenu.find('.js-link-main-menu-view-all');
            $viewAllLink.parent().append($viewAllLink);

            // Make dropdown menu invisible to calculate its actual height
            $dropdownMenu.css('border-bottom-width', '0.0625rem');
            $dropdownMenu.addClass('invisible d-flex');
            $dropdownMenu.show();

            // Set dropdown menu dimensions and position
            var dropdownRect = $dropdown[0].getBoundingClientRect();
            var headerWidth = $header.width();
            $dropdownMenu.css('z-index', 1001);
            $dropdownMenu.css('width', headerWidth);
            $dropdownMenu.css(isRtl ? 'right' : 'left',
                isRtl ? -(headerWidth - dropdownRect.right) : -dropdownRect.left);
            $dropdownMenu.css('height', $dropdownMenu.outerHeight());
            $dropdownMenu.css('max-height', 'calc(100vh - ' + ($header.outerHeight() - 9) + 'px)');

            // Hide dropdown menu again, just before showing it again :)
            $dropdownMenu.hide();
            $dropdownMenu.removeClass('invisible d-flex');
            $dropdownMenu.css('border-bottom-width', 0);

            // Hide visible sibling dropdowns
            $dropdown.siblings('.show').each(function () {
                hideMenuDropdown.call(this, $header);
            });

            // Hide scrollbar
            hideScrollbar($header);

            if (dropdownName) {
                lastOpenedDropdownClassName = 'dropdown-' + dropdownName + '-opened';

                $body.addClass(lastOpenedDropdownClassName);
            }

            // Finally, fire slide animation
            $dropdownMenu.slideDown({
                duration: animationDuration,
                start: function () {
                    $dropdownMenu.addClass('d-flex');
                },
                done: function () {
                    $dropdownMenu.css('border-bottom-width', '0.0625rem');
                }
            });
        }
    } else {
        // Regular dropdown
        $dropdownMenu.addClass('show');
    }

    $dropdown.addClass('show');
    $dropdownToggle.addClass('active').attr('aria-expanded', true);
    $headerBackdrop.show();
}


/**
 * @param {jQuery} $eventTarget eventTarget element
 * @return {string} dropDownName
 */
function getDropdownName($eventTarget) {
    var $dropdownHolder = $eventTarget.closest('.js-dropdown-holder');
    var dropDownName = null;

    if ($dropdownHolder.length > 0) {
        dropDownName = $dropdownHolder.data('dropdownName');
    }

    return dropDownName;
}

module.exports = function () {
    var $navbar = $('.js-main-menu');
    var $header = $('.js-sticky-header');
    var $swsMobile = $('.js-sws-mobile-container');
    var $dropdownToggleButtonsClick = $('.js-dropdown-click:not(.disabled) .js-dropdown-toggle-btn');
    var $dropdownToggleButtonsHover = $('.js-dropdown-hover:not(.disabled) > .js-dropdown-toggle-btn');
    var $toggleSearchMobileBtn = $('.js-toggle-search-mobile');

    // Move dropdown login form to its actual place
    var $dropdownLoginPlaceholder = $('.js-dropdown-login-placeholder');
    var $dropdownLoginContainer = $('.js-dropdown-login-container');
    if ($dropdownLoginPlaceholder.length) {
        $dropdownLoginPlaceholder.append($dropdownLoginContainer);
        $dropdownLoginContainer.removeClass('d-none');
    }

    // Expand search input horizontally if header is sticked on top of the page
    $('.js-show-search-btn').on('click', function () {
        $('.js-sws-container').addClass('open');
        $('.js-sws-input').focus();
    });

    // Collapse search input horizontally if it's empty
    $('.js-sws-input').on('blur', function () {
        if (!this.value) {
            $('.js-sws-container').removeClass('open');
        }
    });

    // Make header sticky
    $header
        .on('sticky:unstick', function () {
            $('.js-sws-container').removeClass('open');
            $header.sticky('resize');
        })
        .sticky();

    // Expand/collapse dropdown menu on mobile
    $dropdownToggleButtonsClick
        .on('click', function (e) {
            // If menu item is clicked on desktop, don't go further
            if (isDesktop.call(this)) return;

            // Add accordion functionality to mobile menu
            e.preventDefault();
            var $dropdownToggle = $(this);
            var $dropdown = $dropdownToggle.parent();
            var $dropdownMenu = $dropdown.children('.dropdown-menu');
            $dropdownToggle.attr('aria-expanded', true);
            $dropdownToggle.toggleClass('expanded');
            $dropdownMenu.slideToggle(animationDuration, function () {
                $dropdown.toggleClass('show');
                this.style.display = null;
            });
        });

    // Show dropdown menu on desktop
    var debouncedShowMenuDropdown = debounce(showMenuDropdown, animationDuration);
    var debouncedHideMenuDropdown = debounce(hideMenuDropdown, animationDuration);
    $dropdownToggleButtonsHover
        .on('mouseenter', function (event) {
            debouncedShowMenuDropdown.call(this, $navbar, $header, getDropdownName($(event.target)));
        });

    // Hide dropdown menu on desktop
    $dropdownToggleButtonsHover.parent()
        .on('mouseleave', function () {
            debouncedShowMenuDropdown.cancel();
            debouncedHideMenuDropdown.call(this, $header);
        });

    // Hide visible sibling dropdowns
    $dropdownToggleButtonsHover.parent().siblings(':not(.js-nav-item)')
        .on('mouseenter', function () {
            $(this).siblings('.show').each(function () {
                hideMenuDropdown.call(this, $header);
            });
        });

    $('body').on('product:afterAddToCart', function () {
        var $currentMenu = $('.js-dropdown-hover.show');
        if ($currentMenu.length) {
            hideMenuDropdown.call($currentMenu[0], $header);
        }
    });

    // Recalculate page offset once consent tracking is ready
    $('.js-consent-tracking')
        .on('consent:ready consent:close', function () {
            $header.sticky('resize');
            setNavbarOffset($navbar, $header, $swsMobile);
        });

    $toggleSearchMobileBtn.on('click', function () {
        $header.addClass('mobile-search-sliding');
        $swsMobile.slideToggle(animationDuration, function () {
            $header.removeClass('mobile-search-sliding');
            $header.sticky('resize');
            $swsMobile.toggleClass('search-visible');
            if (!$toggleSearchMobileBtn.attr('data-no-focus')) {
                $swsMobile.find('.js-sws-input').focus();
            }
            $toggleSearchMobileBtn.removeAttr('data-no-focus');
        });
    });

    // Iphone-Ipad Safari Search force to focus on Input and open keyboard
    if (userAgent.isMobileWebOS) {
        var onKeypressHandler = function () {
            if (!$toggleSearchMobileBtn.attr('data-no-focus')) {
                $swsMobile.find('.js-sws-input').blur().focus();
            }
        };
        var onClickHandler = function () {
            $toggleSearchMobileBtn.trigger('keypress');
        };
        $swsMobile.find('.js-sws-input').bind('focus');
        $toggleSearchMobileBtn.bind('click', onClickHandler);
        $toggleSearchMobileBtn.bind('keypress', onKeypressHandler);
    }

    // Toggle navbar on mobile screens
    $('.js-toggle-navbar').click(function () {
        setNavbarOffset($navbar, $header, $swsMobile);
        $('body').toggleClass('mobile-navbar-open');
        $header.toggleClass('navbar-visible');
        $navbar.fadeToggle();
    });

    $(window).resize(function () {
        if ($header.hasClass('navbar-visible')) {
            setNavbarOffset($navbar, $header, $swsMobile);
        }
    });
};
