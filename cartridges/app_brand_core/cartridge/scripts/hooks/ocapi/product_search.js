'use strict';

/**
 * product_search.js
 *
 * Handles OCAPI hooks for product search calls
 */

var Status = require('dw/system/Status');

/**
 * The modifyGETResponse hook - called after product search get
 * @param {Object} doc the document
 * @return {dw.system.Status} returns a status code
 */
exports.modifyGETResponse = function (doc) {
    if (!doc.hits) {
        return new Status(Status.OK);
    }
    var hookHelpers = require('*/cartridge/scripts/helpers/hookHelpers');

    hookHelpers.addProductInformation(doc.hits, 'tile');

    return new Status(Status.OK);
};
