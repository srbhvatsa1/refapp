var StringUtils = require('dw/util/StringUtils');
var Locale = require('dw/util/Locale');
var Resource = require('dw/web/Resource');
var rrTemplateHelper = require('*/cartridge/scripts/helpers/rrTemplateHelper');

/**
 * Gets the combined key value for custom object in a correct format (<productId>-<customerNo>)
 * @param {string} productId productId
 * @param {string} customerNo customerNo
 * @returns {string} key
 */
function newCustomObjectKey(productId, customerNo) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    // eslint-disable-next-line no-use-before-define
    var sorting = getSortingString('rating');

    var ratingsReviewsIterator =
    CustomObjectMgr.queryCustomObjects('RatingReview',
        'custom.id like {0}',
        sorting, productId + '-' + customerNo + '*');
    var counter = 0;
    if (ratingsReviewsIterator) {
        counter = ratingsReviewsIterator.count; /* get last key from db and create +1 */
    }
    return StringUtils.format('{0}-{1}-{2}', productId, customerNo, counter);
}

/**
 * Checks if the customer has bought this product
 * @param {string} customerNo current customers no
 * @param {string} productId productId to check
 * @returns {boolean} true if customer bought this product, false otherwise
 */
function customerBoughtProduct(customerNo, productId) {
    var OrderMgr = require('dw/order/OrderMgr');
    var Order = require('dw/order/Order');
    var orders = OrderMgr.searchOrders('customerNo={0} and status={1}', null, customerNo, Order.ORDER_STATUS_COMPLETED);

    var productBought = false;

    while (orders.hasNext() && !productBought) {
        var order = orders.next();
        var lineItems = order.getAllProductLineItems();
        for (var i = 0; i < lineItems.length; i++) {
            var productLineItem = lineItems[i];
            if (productLineItem.productID === productId) {
                productBought = true;
                break;
            }
        }
    }

    return productBought;
}

/**
 * Given the sorting criteria generates the sorting string with predefined rules that CustomObjectMgr accepts
 * @param {string} sortingCriteria sorting criteria customer requested
 * @returns {string} sorting string to be sent to query function of CustomObjectMgr
 */
function getSortingString(sortingCriteria) {
    var sorting = '';
    if (sortingCriteria === 'rating') {
        sorting = 'custom.rating desc, custom.date desc';
    } else if (sortingCriteria === 'date') {
        sorting = 'custom.date desc, custom.rating desc';
    } else if (sortingCriteria === 'positiveRelevancy') {
        sorting = 'custom.positiveRelevancy desc, custom.rating desc';
    } else {
        sorting = 'custom.rating desc, custom.date desc';
    }
    return sorting;
}

/**
 * Checks if given customer is allowed to review the given product
 * @param {dw.customer.Profile} currentCustomerProfile req.currentCustomer.profile
 * @param {string} productId id of the product to be checked
 * @returns {Object} plain model that holds can review data
 */
function canCustomerReviewProduct(currentCustomerProfile, productId) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var reviewAlreadySubmitted = false;
    var userLoggedIn = false;
    var productBought = false;
    var allowWriteReview = true;
    if (currentCustomerProfile) {
        userLoggedIn = true;

        var checkBoughtProduct = rrTemplateHelper.customerBoughtProduct();
        var checkAlreadySubmitted = rrTemplateHelper.reviewAlreadySubmitted();
        productBought = customerBoughtProduct(currentCustomerProfile.customerNo, productId);
        var rrCustomObject = CustomObjectMgr.queryCustomObject('RatingReview', 'custom.customerNo = {0} AND custom.productId = {1}', currentCustomerProfile.customerNo, productId);
        if (rrCustomObject) {
            reviewAlreadySubmitted = true;
        }

        if (checkBoughtProduct && !productBought) {
            allowWriteReview = false;
        }

        if (checkAlreadySubmitted && reviewAlreadySubmitted) {
            allowWriteReview = false;
        }
    }

    return {
        reviewAlreadySubmitted: reviewAlreadySubmitted,
        userLoggedIn: userLoggedIn,
        productBought: productBought,
        allowWriteReview: allowWriteReview
    };
}

/**
 * Collects all reviews submitted by current customer
 * @param {dw.customer.Profile} currentCustomerProfile req.currentCustomer.profile
 * @returns {Object} plain object holding approved and not approved reviews
 */
function collectMyReviews(currentCustomerProfile) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var ReviewModel = require('*/cartridge/models/product/review');

    if (!currentCustomerProfile) {
        return null;
    }
    var ratingsReviewsIterator =
        CustomObjectMgr.queryCustomObjects('RatingReview', 'custom.id like {0}', 'custom.date desc',
            '*-' + currentCustomerProfile.customerNo);
    var reviews = [];
    while (ratingsReviewsIterator.hasNext()) {
        reviews.push(new ReviewModel(ratingsReviewsIterator.next()));
    }

    return {
        approvedReviews: reviews.filter(function (review) { return review.approved; }),
        notApprovedReviews: reviews.filter(function (review) { return !review.approved; })
    };
}

/**
 * Gets customer name given profile
 * @param {dw.customer.Profile} profile req.currentCustomer.profile
 * @returns {string} name string of customer
 */
function getCustomerName(profile) {
    return StringUtils.format('{0} {1}', profile.firstName, profile.lastName);
}

/**
 * Gets customer name given profile
 * @param {string} nickname nickname from the review form
 * @param {dw.customer.Profile} profile req.currentCustomer.profile
 * @returns {string} name string of customer
 */
function getNickName(nickname, profile) {
    return nickname || StringUtils.format('{0}.{1}.', profile.firstName[0], profile.lastName[0]);
}

/**
 * Checks the validity of review, saves it into custom objects
 * @param {dw.customer.Profile} currentCustomerProfile req.currentCustomer.profile
 * @param {string} localeId locale Id from request
 * @param {dw.web.Form} form submitted review form
 * @returns {Object} plain object that holds success flag and a related message
 */
function submitReview(currentCustomerProfile, localeId, form) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Transaction = require('dw/system/Transaction');
    var Site = require('dw/system/Site');

    // check if customer logged in
    if (!currentCustomerProfile) {
        form.clear();
        return {
            success: false,
            message: Resource.msg('message.review.submitted.not.logged.in.user.error', 'ratingsReviews', null)
        };
    }

    // check if this customer has already reviewed this - no need to be an approved review
    var checkAlreadySubmitted = rrTemplateHelper.reviewAlreadySubmitted();
    var rrCustomObject =
        CustomObjectMgr.getCustomObject('RatingReview',
            StringUtils.format('{0}-{1}', form.pid.value, currentCustomerProfile.customerNo));
    if (checkAlreadySubmitted && rrCustomObject) {
        form.clear();
        return {
            success: false,
            message: Resource.msg('message.review.already.submitted', 'ratingsReviews', null)
        };
    }

    var currentLocale = Locale.getLocale(localeId);
    var approved = Site.getCurrent().getCustomPreferenceValue('rrApprovalStatus');
    try {
        // create the review in the system
        Transaction.wrap(function () {
            rrCustomObject = CustomObjectMgr.createCustomObject('RatingReview', newCustomObjectKey(form.pid.value, currentCustomerProfile.customerNo));
            rrCustomObject.custom.productId = form.pid.value;
            rrCustomObject.custom.customerNo = currentCustomerProfile.customerNo;
            rrCustomObject.custom.customerName = getCustomerName(currentCustomerProfile);
            rrCustomObject.custom.customerNickName = getNickName(form.nickname.value, currentCustomerProfile);
            rrCustomObject.custom.locale = currentLocale.toString();
            rrCustomObject.custom.date = new Date();
            rrCustomObject.custom.title = form.title.value;
            rrCustomObject.custom.message = form.message.value;
            rrCustomObject.custom.rating = parseInt(form.rating.value, 10);
            rrCustomObject.custom.recommended = form.recommended.value === 'true';
            rrCustomObject.custom.positiveRelevancy = 0;
            rrCustomObject.custom.negativeRelevancy = 0;
            rrCustomObject.custom.approved = approved;
        });
    } catch (e) {
        var logger = require('dw/system/Logger').getLogger('RatingsAndReviews');
        logger.error('An unpexted error occured : {0}', e.message);
        form.clear();
        return {
            success: false,
            message: Resource.msg('message.review.could.not.be.submitted', 'ratingsReviews', null)
        };
    }

    // everything went well
    form.clear();

    if (approved) {
        return {
            success: true,
            reload: true
        };
    }
    return {
        success: true,
        message: Resource.msg('message.review.submitted', 'ratingsReviews', null)
    };
}

/**
 * Increases positive/negative relevance by voting
 * @param {string} reviewId Id of the review to be voted
 * @param {boolean} voteUp flag to specify if the request is to vote up or down
 * @returns {Object} plain object that holds success flag and a related message
 */
function voteReview(reviewId, voteUp) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Transaction = require('dw/system/Transaction');

    if (!reviewId) {
        return {
            success: false,
            message: Resource.msg('message.review.voted.without.review.id', 'ratingsReviews', null)
        };
    }

    // find review
    var rrCustomObject = CustomObjectMgr.getCustomObject('RatingReview', reviewId);
    if (!rrCustomObject) {
        return {
            success: false,
            message: Resource.msg('message.review.voted.is.not.found', 'ratingsReviews', null)
        };
    }

    // update relevant field
    try {
        Transaction.wrap(function () {
            if (voteUp) {
                rrCustomObject.custom.positiveRelevancy += 1;
            } else {
                rrCustomObject.custom.negativeRelevancy += 1;
            }
        });
    } catch (e) {
        var logger = require('dw/system/Logger').getLogger('RatingsAndReviews');
        logger.error('An unpexted error occured : {0}', e.message);
        return {
            success: false,
            message: Resource.msg('message.review.could.not.be.submitted', 'ratingsReviews', null)
        };
    }

    // generate new label text
    var newLabelText = voteUp ?
        Resource.msgf('button.vote.yes.x', 'ratingsReviews', null, rrCustomObject.custom.positiveRelevancy) :
        Resource.msgf('button.vote.no.x', 'ratingsReviews', null, rrCustomObject.custom.negativeRelevancy);

    return {
        success: true,
        label: newLabelText
    };
}

/**
 * Gets reviews paged for given product
 * @param {string} productId Id of the product to collect the reviews of
 * @param {int} start starting index
 * @param {int} limit how many to collect
 * @param {string} sortingCriteria type of sorting user asked (expected inputs: 'rating', 'date', 'positiveRelevancy')
 * @param {string} localeId localeId from request
 * @returns {Object} plain object that holds reviews and more exists flag
 */
function collectReviewsForProduct(productId, start, limit, sortingCriteria, localeId) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var ReviewModel = require('*/cartridge/models/product/review');

    // creates locale parameter - allows to select all locales with current language
    var currentLocale = Locale.getLocale(localeId);
    var localeParameter = StringUtils.format('{0}_*', currentLocale.language);

    // creates a sorting string that CustomObjectMgr accepts
    var sorting = getSortingString(sortingCriteria);

    // query the approved reviews related with given product
    var ratingsReviewsIterator =
        CustomObjectMgr.queryCustomObjects('RatingReview',
            'custom.id like {0} AND custom.approved=true AND custom.locale like {1}',
            sorting, productId + '-*', localeParameter);
    var reviews = [];

    // place the iterator - +1 is for checking if there is any more reviews or not
    ratingsReviewsIterator.forward(start, limit + 1);
    while (ratingsReviewsIterator.hasNext() && reviews.length < limit) {
        reviews.push(new ReviewModel(ratingsReviewsIterator.next()));
    }
    var moreExists = ratingsReviewsIterator.hasNext();
    ratingsReviewsIterator.close();

    return {
        reviews: reviews,
        moreExists: moreExists
    };
}

module.exports = {
    canCustomerReviewProduct: canCustomerReviewProduct,
    submitReview: submitReview,
    voteReview: voteReview,
    collectReviewsForProduct: collectReviewsForProduct,
    collectMyReviews: collectMyReviews
};
