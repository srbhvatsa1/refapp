'use strict';

var emailRegExp = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-zA-Z]{2,6}(?:\.[a-zA-Z]{2})?)$/i;
// regexp has been taken from: https://regex101.com/library/mX1xW0

/**
 * Validates the given field's value with email regexp
 * @param {Object} field - the html input
 * @returns {boolean} true if validated, false otherwise
 */
function validateEmail(field) {
    return emailRegExp.test(field.value);
}

/**
 * Gets the email regexp as string
 * @returns {string} A String version of the characters used to create the regular expression. The value does not include the forward slash delimiters that surround the expression.
 */
function getEmailRegExpAsString() {
    return emailRegExp.source;
}

module.exports = {
    validateEmail: validateEmail,
    getEmailRegExp: getEmailRegExpAsString
};
