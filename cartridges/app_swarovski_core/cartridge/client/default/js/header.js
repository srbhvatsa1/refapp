'use strict';

$(document).ready(function () {
    var $swsSticky = $('.js-sws-mobile-container-sticky');

    $('.mobile-menu-overlay, .js-main-menu-close-overlay').on('click', function () {
        $('.js-toggle-navbar').first().click();
    });

    // Hide dropdown menu on desktop
    $('.js-dropdown-menu-close').on('click', function (e) {
        e.preventDefault();
        $('.dropdown-menu').slideUp({
            duration: 300,
            done: function () {
                $('.js-dropdown-toggle-btn').removeClass('active');
                $('.dropdown-menu').removeClass('d-flex');
                $('.dropdown-menu').removeClass('hiding');
            }
        });
    });

    $(document).on('minicart:empty', function () {
        var $minicart = $('.minicart');
        var $popover = $minicart.find('.popover');

        $popover.addClass('show');
    });

    $(document).on('click', '.js-toggle-search-sticky', function () {
        $swsSticky.slideToggle(300, function () {
            $('.js-sticky-header').sticky('resize');
            $swsSticky.toggleClass('search-visible');
            if (!$swsSticky.attr('data-no-focus')) {
                $swsSticky.find('.js-sws-input').focus();
            }
            $swsSticky.removeAttr('data-no-focus');
        });
    });
});
