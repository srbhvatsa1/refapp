'use strict';

module.exports = {
    removeFromWishlist: function () {
        $('body').on('click', '.remove-from-wishlist', function (e) {
            e.preventDefault();
            var url = $(this).data('url');
            var elMyAccount = $('.wishlist-product-col').length;

            // User is in my account page, call removeWishlistAccount() end point, re-render wishlist cards
            var itemToBeRemoved = this.closest('.wishlist-product-col');
            if (elMyAccount > 0) {
                $('.wishlist-row').spinner().start();
                $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'html',
                    data: {},
                    success: function () {
                        itemToBeRemoved.remove();
                        if ($('.wishlist-product-col').length === 0) {
                            $('.wishlist-row > .no-records-container').removeClass('d-none');
                        }
                        $('.wishlist-row').spinner().stop();
                    },
                    error: function () {
                        $('.wishlist-row').spinner().stop();
                    }
                });
            }
        });
    }
};
