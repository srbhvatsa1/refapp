'use strict';

var base = require('base/product/base');

var formValidation = require('base/components/formValidation');
var clientSideValidation = require('brand_core/components/clientSideValidation');
var cart = require('brand_core/cart/cart');

/**
 * Get the validation status for the form.js-giftcard-add-update-form
 * @param {selector} $form the form to be validated
 * @return {boolean} true in case if all fields are valid
 */
function getFieldsValidationStatus($form) {
    return clientSideValidation.functions.validateForm($form[0]);
}

/**
 * Get the object contains names and values of all form.js-giftcard-add-update-form input fields
 * @param {selector} $form product detail gift card form container
 * @return {Object} object contains pairs of field names and its values
 */
function getFieldsData($form) {
    var fieldsData = {};
    $form.find('input').each(function () {
        var $this = $(this);
        var attrName = $(this).attr('name');
        // check attrName
        if (typeof attrName !== typeof undefined && attrName !== false && attrName !== null && attrName !== 'null') {
            fieldsData[attrName] = $this.val();
        }
    });

    return fieldsData;
}

module.exports = {
    init: function () {

    },

    /**
     * Checkbox for gift message show/hide
     */
    toggleGiftMessage: function () {
        $('body').on('click', '.js-add-to-gift-message', function () {
            var $this = $(this);
            var $form = $this.closest('form.js-giftcard-add-update-form');
            if ($this.is(':checked')) {
                $('.js-gift-message', $form).removeClass('d-none');
            } else {
                $('.js-gift-message', $form).addClass('d-none');
            }
        });
    },

    updateAddToCartFormData: function () {
        $('body').on('product:beforeAddToCart', function (evt, element) {
            var $this = $(element);
            var $form = $('form.js-giftcard-add-update-form', $this.closest('.product-detail, .modal-content'));

            $(element).on('updateAddToCartFormData', function (e, formObj) {
                var giftCardDetailsFormData = getFieldsData($form);
                $.extend(formObj, giftCardDetailsFormData);
            });
        });
    },

    handleFormValidation: function () {
        $('body').on('product:afterAddToCart', function (e, data) {
            if (data && !data.success) {
                formValidation($('form.js-giftcard-add-update-form'), data);
            }
        });
    },

    handleAddOrUpdateCardToCart: function () {
        $('body').on('click', 'button.add-to-cart, button.add-to-cart-global, button.update-cart-product-giftcard', function (e) {
            var $this = $(this);
            var $form = $('form.js-giftcard-add-update-form', $this.closest('.product-detail, .modal-content'));
            if ($form.length === 0) {
                return true;
            }

            var isFormValid = getFieldsValidationStatus($form);
            if (!isFormValid) {
                e.preventDefault();
                return false;
            }

            if ($this.hasClass('update-cart-product-giftcard')) {
                $('body').trigger('giftcard:updateCart', $this[0]);
            }

            return true;
        });
    },

    updateToCart: function () {
        $('body').on('giftcard:updateCart', function (e, element) {
            $('body').trigger('product:beforeAddToCart', element);

            var $this = $(element);
            var updateProductUrl = $this.closest('.cart-and-ipay').find('.update-cart-url').val();
            var selectedQuantity = $this.closest('.cart-and-ipay').find('.update-cart-url').data('selected-quantity');
            var uuid = $this.closest('.cart-and-ipay').find('.update-cart-url').data('uuid');

            var form = {
                uuid: uuid,
                pid: base.getPidValue($(this)),
                quantity: selectedQuantity
            };

            $this.trigger('updateAddToCartFormData', form);
            $.spinner().start();

            if (updateProductUrl) {
                $.ajax({
                    url: updateProductUrl,
                    type: 'post',
                    context: this,
                    data: form,
                    dataType: 'json',
                    success: function (data) {
                        $('body').trigger('product:afterAddToCart', data);
                        if (data && !data.success) {
                            return;
                        }
                        $('body').trigger('product:afterUpdate', [data, uuid]);
                    },
                    error: function (err) {
                        if (!err.responseJSON) return;
                        if (err.responseJSON.redirectUrl) {
                            window.location.href = err.responseJSON.redirectUrl;
                        } else if (err.responseJSON.errorMessage) {
                            cart.createErrorNotification(err.responseJSON.errorMessage);
                        }
                    },
                    complete: function () {
                        $.spinner().stop();
                    }
                });
            }
            $.spinner().stop();
        });
    }
};
