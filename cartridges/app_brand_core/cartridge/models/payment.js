'use strict';

var base = module.superModule;

var PaymentMgr = require('dw/order/PaymentMgr');
var collections = require('*/cartridge/scripts/util/collections');
var checkoutHelper = require('*/cartridge/scripts/checkout/checkoutHelpers');
var formatCurrency = require('*/cartridge/scripts/util/formatting').formatCurrency;
var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');
var PaymentInstrument = require('dw/order/PaymentInstrument');

/**
 * Creates an array of objects containing applicable payment methods
 * @param {dw.util.ArrayList<dw.order.dw.order.PaymentMethod>} paymentMethods - An ArrayList of
 *      applicable payment methods that the user could use for the current basket.
 * @param {string} currencyCode - Currency defined in current basket
 * @param {dw.order.Basket} currentBasket - the target Basket object
 * @returns {Array} of object that contain information about the applicable payment methods for the
 *      current cart
 */
function getPaymentMethodsModel(paymentMethods, currencyCode, currentBasket) {
    // Stop here if paymentMethods is undefined
    if (!paymentMethods) return null;
    var cartHelpers = require('*/cartridge/scripts/cart/cartHelpers');
    var isServiceFeeExcluded = cartHelpers.checkIfServiceFeeExcluded(currentBasket);
    return collections.map(paymentMethods,
        function (method) {
            return {
                ID: method.ID,
                name: method.name,
                image: method.image ? method.image.URL.toString() : null,
                serviceFee: method.custom && method.custom.serviceFee && !isServiceFeeExcluded ? formatCurrency(method.custom.serviceFee, currencyCode) : null,
                note: method.custom && method.custom.note ? method.custom.note : null
            };
        });
}

/**
 * Creates an array of elected payment method names
 * @param {dw.util.ArrayList<dw.order.PaymentInstrument>} selectedPaymentInstruments - ArrayList
 *      of payment instruments that the user is using to pay for the current basket
 * @returns {Array} Array of payment method names
 */
function getAppliedPaymentInstruments(selectedPaymentInstruments) {
    if (selectedPaymentInstruments) {
        return collections.map(selectedPaymentInstruments, function (paymentInstrument) {
            return paymentInstrument.paymentMethod;
        });
    }
    return [];
}

/**
 * Creates an array of objects containing selected payment information
 * @param {dw.util.ArrayList<dw.order.PaymentInstrument>} selectedPaymentInstruments - ArrayList
 *      of payment instruments that the user is using to pay for the current basket
 * @returns {Array} Array of objects that contain information about the selected payment instruments
 */
function getSelectedPaymentInstruments(selectedPaymentInstruments) {
    return collections.map(selectedPaymentInstruments, function (paymentInstrument) {
        var results = {
            paymentMethod: paymentInstrument.paymentMethod,
            amount: paymentInstrument.paymentTransaction.amount.value
        };
        if (paymentInstrument.paymentMethod === PaymentInstrument.METHOD_CREDIT_CARD) {
            results.lastFour = paymentInstrument.creditCardNumberLastDigits;
            results.owner = paymentInstrument.creditCardHolder;
            results.expirationYear = paymentInstrument.creditCardExpirationYear;
            results.type = paymentInstrument.creditCardType;
            results.maskedCreditCardNumber = paymentInstrument.maskedCreditCardNumber;
            results.expirationMonth = paymentInstrument.creditCardExpirationMonth;
        } else if (paymentInstrument.paymentMethod === PaymentInstrument.METHOD_GIFT_CERTIFICATE) {
            results.giftCertificateCode = paymentInstrument.giftCertificateCode;
            results.maskedGiftCertificateCode = paymentInstrument.maskedGiftCertificateCode;
        } else if (paymentInstrument.paymentMethod === paymentMethodHelpers.getKnetPaymentMethodId()) {
            if (paymentInstrument.custom) {
                results.knetPaymentId = paymentInstrument.custom.knetPaymentId;
                results.knetTransactionId = paymentInstrument.custom.knetTransactionId;
            }
        }

        return results;
    });
}

/**
 * Gets if the current payment processor's save card functionality is disabled
 * @returns {boolean} true if the current active payment processor's save card functionality is disabled, false otherwise
 */
function getIsSaveCardDisabled() {
    var HookMgr = require('dw/system/HookMgr');
    var StringUtils = require('dw/util/StringUtils');

    var isSaveCardDisabled = false;
    var creditCardPaymentMethod = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD);
    if (creditCardPaymentMethod && creditCardPaymentMethod.paymentProcessor) {
        var hook = StringUtils.format('app.payment.processor.{0}', creditCardPaymentMethod.paymentProcessor.ID.toLowerCase());
        if (HookMgr.hasHook(hook)) {
            isSaveCardDisabled = HookMgr.callHook(hook, 'IsSaveCardDisabled');
        }
    }

    return isSaveCardDisabled || false;
}


/**
 * Payment class that represents payment information for the current basket
 * @param {dw.order.Basket} currentBasket - the target Basket object
 * @param {dw.customer.Customer} currentCustomer - the associated Customer object
 * @param {string} countryCode - the associated Site countryCode
 * @constructor
 */
function Payment(currentBasket, currentCustomer, countryCode) {
    base.call(this, currentBasket, currentCustomer, countryCode);

    var paymentInstruments = currentBasket.paymentInstruments;
    var paymentAmount = currentBasket.totalGrossPrice;
    var paymentMethods = PaymentMgr.getApplicablePaymentMethods(
        currentCustomer,
        countryCode,
        paymentAmount.value
    );

    this.appliedPaymentInstruments = getAppliedPaymentInstruments(paymentInstruments);
    this.isSaveCardDisabled = getIsSaveCardDisabled();

    /**
     * (copied from storefront_base):
     * Should compare currentBasket and currentCustomer
     * and countryCode to see if we need them or not
     */
    this.applicablePaymentMethods = getPaymentMethodsModel(
        checkoutHelper.getApplicablePaymentMethods(paymentMethods, currentBasket),
        currentBasket.currencyCode,
        currentBasket
    );

    this.selectedPaymentInstruments = paymentInstruments ?
        getSelectedPaymentInstruments(paymentInstruments) : null;
}

module.exports = Payment;
