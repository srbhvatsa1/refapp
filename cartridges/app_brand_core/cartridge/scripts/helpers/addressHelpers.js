'use strict';

var base = module.superModule;

var Locale = require('dw/util/Locale');
var StringUtils = require('dw/util/StringUtils');
var data = require('*/cartridge/config/cities');
var Site = require('dw/system/Site');

/**
 * Gets available languages 2 letter lower case
 * @returns {Array} lang array
 */
function getAvailableLanguages() {
    var availableLocales = Site.current.getAllowedLocales();
    var langs = [];

    if (availableLocales) {
        for (var i = 0; i < availableLocales.length; i++) {
            var loc = Locale.getLocale(availableLocales[i]);
            if (langs.indexOf(loc.getLanguage()) === -1) {
                langs.push(loc.getLanguage());
            }
        }
    }
    return langs;
}

var AVAILABLE_LANGUAGES = getAvailableLanguages();

/**
 * Gets array of map with provided fieldKey & locale
 * @param {string} fieldKey field base
 * @param {string} locale locale id en_AE
 * @returns {Array} array of data
 */
function filterData(fieldKey, locale) {
    var loc = Locale.getLocale(locale);
    var fieldAttr = StringUtils.format('{0}_{1}', fieldKey, loc.getLanguage());
    return data.filter(function (row) {
        return row.country === loc.getISO3Country();
    }).map(function (row) {
        var obj = row;
        obj.value = row[fieldAttr];
        return obj;
    }).sort(function (a, b) {
        return a.value.localeCompare(b.value);
    });
}
/**
 * Gets value based on actualValue and currentLocale, such as English data will be converted to Arabic
 * @param {string} fieldKey keyField to process
 * @param {string} actualValue current Value
 * @param {string} targetLocale locale
 * @returns {string} found or actual value
 */
function findCurrentValue(fieldKey, actualValue, targetLocale) {
    if (!actualValue) {
        return actualValue;
    }

    var originLoc = Locale.getLocale(targetLocale);
    var attributesToLookup = AVAILABLE_LANGUAGES.map(function (lang) {
        return StringUtils.format('{0}_{1}', fieldKey, lang);
    });
    var attrToReturn = StringUtils.format('{0}_{1}', fieldKey, originLoc.getLanguage());
    var result;
    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        if (obj.country !== originLoc.getISO3Country()) {
            continue;
        }
        for (var j = 0; j < attributesToLookup.length; j++) {
            if (attributesToLookup[j] in obj && obj[attributesToLookup[j]] && obj[attributesToLookup[j]].trim().equalsIgnoreCase(actualValue.trim())) {
                result = obj;
                break;
            }
        }
        if (result) {
            break;
        }
    }
    if (result) {
        return result[attrToReturn];
    }
    return actualValue;
}

/**
 * Gets value of target locale from original value
 * @param {string} fieldKey field base
 * @param {string} actualValue actual value
 * @param {string} originLocale original locale
 * @returns {string} value of target locale
 */
function findReplacementValue(fieldKey, actualValue, originLocale) {
    if (!actualValue) {
        return actualValue;
    }

    var originLoc = Locale.getLocale(originLocale);
    var attrToLookup = StringUtils.format('{0}_{1}', fieldKey, originLoc.getLanguage());
    var attrToReturn = StringUtils.format('{0}_code', fieldKey);

    var selectedData = null;

    for (var i = 0; i < data.length; i++) {
        if (data[i].country !== originLoc.getISO3Country()) {
            continue;
        }
        if (data[i][attrToLookup] && data[i][attrToLookup].trim().equalsIgnoreCase(actualValue.trim())) {
            selectedData = data[i];
            break;
        }
    }

    // if we found it return the value of target Locale
    if (selectedData !== null) {
        return selectedData[attrToReturn];
    }
    // if it is not found, return the actual value
    return actualValue;
}

/**
 * Gets array of state map with code
 * @param {string} locale locale
 * @returns {Array} unique state set
 */
function getStates(locale) {
    var states = filterData('state', locale);
    var uniqueStates = {};
    var uniqueStateSet = [];
    states.forEach(function (state) {
        if (!(state.code in uniqueStates)) {
            uniqueStates[state.code] = '';
            uniqueStateSet.push(state);
        }
    });
    // Object.values(uniqueStates); fails
    return uniqueStateSet;
}
/**
 * Gets array of city map with code
 * @param {string} locale locale
* @returns {Array} city set
 */
function getCities(locale) {
    return filterData('city', locale);
}

/**
 * Gets value of target locale from original value
 * @param {string} state actual state value
 * @param {string} originLocale original locale
 * @returns {string} value of target locale
 */
function getStateReplacement(state, originLocale) {
    return findReplacementValue('state', state, originLocale);
}
/**
 * Gets value of target locale from original value
 * @param {string} city actual city value
 * @param {string} originLocale original locale
 * @returns {string} value of target locale
 */
function getCityReplacement(city, originLocale) {
    return findReplacementValue('city', city, originLocale);
}

/**
 * Gets value based on actualValue and currentLocale, such as English data will be converted to Arabic
 * @param {string} actualValue current Value
 * @param {string} targetLocale locale
 * @returns {string} found or actual value
 */
function findCurrentCityValue(actualValue, targetLocale) {
    return findCurrentValue('city', actualValue, targetLocale);
}

/**
 * Gets value based on actualValue and currentLocale, such as English data will be converted to Arabic
 * @param {string} actualValue current Value
 * @param {string} targetLocale locale
 * @returns {string} found or actual value
 */
function findCurrentStateValue(actualValue, targetLocale) {
    return findCurrentValue('state', actualValue, targetLocale);
}

/**
 * Copy information from address object and save it in the system
 * @param {dw.customer.CustomerAddress} newAddress - newAddress to save information into
 * @param {FormObject} address - Address to copy from
 */
function updateAddressFields(newAddress, address) {
    newAddress.setAddress1(address.address1 || '');
    newAddress.setAddress2(address.address2 || '');
    newAddress.setCity(address.city || '');
    newAddress.setFirstName(address.firstName || '');
    newAddress.setLastName(address.lastName || '');
    newAddress.setPhone(address.phone || '');
    newAddress.setPostalCode(address.postalCode || '');

    if (address.states && address.states.stateCode) {
        newAddress.setStateCode(address.states.stateCode);
    }

    newAddress.setCountryCode(address.country || require('*/cartridge/scripts/helpers/localeHelpers').getCurrentCountryCode());
    newAddress.setJobTitle(address.jobTitle || '');
    newAddress.setPostBox(address.postBox || '');
    newAddress.setSalutation(address.salutation || '');
    newAddress.setSecondName(address.secondName || '');
    newAddress.setCompanyName(address.companyName || '');
    newAddress.setSuffix(address.suffix || '');
    newAddress.setSuite(address.suite || '');
    newAddress.setJobTitle(address.title || '');
    newAddress.setTitle(address.title ? address.title.titleList : '');
}

base.getStates = getStates;
base.getCities = getCities;
base.getStateReplacement = getStateReplacement;
base.getCityReplacement = getCityReplacement;
base.findCurrentCityValue = findCurrentCityValue;
base.findCurrentStateValue = findCurrentStateValue;
base.updateAddressFields = updateAddressFields;

module.exports = base;
