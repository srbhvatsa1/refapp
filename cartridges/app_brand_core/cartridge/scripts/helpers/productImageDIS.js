'use strict';

var ArrayList = require('dw/util/ArrayList');
var Site = require('dw/system/Site');

var ProductVariationAttributeValue = require('dw/catalog/ProductVariationAttributeValue');
var ProductVariationModel = require('dw/catalog/ProductVariationModel');
var cvDISConfiguration = require('*/cartridge/config/image_config_DIS');

/**
* @constructor
* @classdesc Initializes the  ProductImage wrapper object
* @param {Object} imageObject Product or ProductVariationAttributeValue (required)
* @param {string} viewType type of view (resolution) that should be generated (required)
* @param {number} index Number position of the image in the list of images for the view type. Defaults to 0 if not provided
*/
function ProductImage(imageObject, viewType, index) {
    this.image = null;
    this.scaleImage = false;
    this.viewType = viewType;
    this.imageObject = imageObject;

    if (this.imageObject === null) {
        return;
    }

    if (this.imageObject instanceof ProductVariationAttributeValue) {
        this.referenceType = 'ProductVariationAttributeValue';
    } else if (this.imageObject instanceof ProductVariationModel) {
        this.referenceType = 'Product';
        this.imageObject = imageObject.selectedVariants.length > 0 ? imageObject.selectedVariants[0] : imageObject.master;
    } else {
        this.referenceType = 'Product';
    }

    this.scalableViewType = null;
    this.index = !index ? 0 : index;
    this.scaleImage = true;
    this.transformationObj = Object.hasOwnProperty.call(cvDISConfiguration, viewType) ? cvDISConfiguration[viewType] : {};
    this.scalableViewType = this.viewType;

    if (Object.prototype.hasOwnProperty.call(cvDISConfiguration, 'viewTypeMapping') && cvDISConfiguration.viewTypeMapping[this.viewType]) {
        var viewTypeMapping = cvDISConfiguration.viewTypeMapping[this.viewType];
        var type;
        var scaleableImage;
        if (viewTypeMapping instanceof Array) {
            for (var i = 0; i < viewTypeMapping.length; i++) {
                type = viewTypeMapping[i];

                scaleableImage = this.imageObject.getImage(type, this.index);
                if (scaleableImage) {
                    this.scalableViewType = type;
                    this.scaleableImage = scaleableImage;

                    break;
                }
            }
        } else {
            type = cvDISConfiguration.viewTypeMapping[this.viewType];

            scaleableImage = this.imageObject.getImage(type, this.index);
            if (scaleableImage) {
                this.scalableViewType = type;
                this.scaleableImage = scaleableImage;
            }
        }
    }

    if (!this.scaleableImage) {
        this.scaleableImage = this.imageObject.getImage(this.scalableViewType, this.index);
    }

    this.image = this.imageObject.getImage(this.viewType, this.index);

    if (!this.image) {
        this.image = this.scaleableImage;
    }
    this.alt = this.getAlt();
    this.title = this.getTitle();
}

ProductImage.prototype.getURL = function () {
    if (this.imageObject === null) {
        return null;
    }
    return this.getImageURL();
};

ProductImage.prototype.getHttpURL = function () {
    if (this.imageObject === null) {
        return null;
    }
    return this.getImageURL('Http');
};

ProductImage.prototype.getHttpsURL = function () {
    if (this.imageObject === null) {
        return null;
    }
    return this.getImageURL('Https');
};

ProductImage.prototype.getAbsURL = function () {
    if (this.imageObject === null) {
        return null;
    }
    return this.getImageURL('Abs');
};

/**
 * Gets the actual image URL for different API calls.
 * @param {string} imageFunctionID function id
 * @returns {string} url string
 */
ProductImage.prototype.getImageURL = function (imageFunctionID) {
    if (this.imageObject === null) {
        return null;
    }
    if (!this.image) {
        return null;
    }
    if (this.scaleImage) {
        return this.getFinalUrlAsString(this.image[imageFunctionID ? ('get' + imageFunctionID + 'ImageURL') : 'getImageURL'](this.transformationObj));
    }
    return this.getFinalUrlAsString(this.image[imageFunctionID ? ('get' + imageFunctionID + 'URL') : 'getURL']());
};

/**
 * If a URL replacement is used it would return the final URL, otherwise the given URL object
 * @param {dw.web.URL} imageURL image url
 * @returns {string} alt string
 */
ProductImage.prototype.getFinalUrlAsString = function (imageURL) {
    var current = imageURL.toString();
    var replacement = Site.current.getCustomPreferenceValue('disImageSourceEnvironment');
    if (replacement && replacement.value) {
        return current.replace(/(^.*_)[a-zA-Z0-9]{3}(\/on\/demandware.*$)/, '$1' + replacement.value + '$2');
    }
    return current;
};

/**
 * Gets the tile for images.
 * @returns {string} title string
 */
ProductImage.prototype.getTitle = function () {
    if (this.imageObject === null) {
        return null;
    }
    if (this.referenceType === 'ProductVariationAttributeValue' && this.viewType === 'swatch') {
        return this.imageObject.displayValue;
    }
    if (!this.image || !this.image.title) {
        if (cvDISConfiguration.imageMissingText) {
            return cvDISConfiguration.imageMissingText;
        } else if (this.referenceType === 'Product') {
            return this.imageObject.name;
        }
        return this.imageObject.displayValue;
    }
    return this.image.title;
};


/**
 * Gets the alternative text for images.
 * @returns {string} alt string
 */
ProductImage.prototype.getAlt = function () {
    if (this.imageObject === null) {
        return null;
    }
    if (this.referenceType === 'ProductVariationAttributeValue' && this.viewType === 'swatch') {
        return this.imageObject.displayValue;
    }
    if (!this.image || !this.image.alt) {
        if (cvDISConfiguration.imageMissingText) {
            return cvDISConfiguration.imageMissingText;
        } else if (this.referenceType === 'Product') {
            return this.imageObject.name;
        }
        return this.imageObject.displayValue;
    }
    return this.image.alt;
};


/**
 * Gets all images for the given view type and image object
 * @returns {dw.util.ArrayList} array list
 */
ProductImage.prototype.getImages = function () {
    return this.getAllImages();
};

/**
 * Returns a Collection of ProductImage Instances of the productimages assigned for this viewtype.
 * @returns {dw.util.ArrayList} array list
 */
ProductImage.prototype.getAllImages = function () {
    var result = new ArrayList();
    if (this.imageObject !== null) {
        var images = this.imageObject.getImages(this.viewType);
        if (!images || !images.length) {
            images = this.imageObject.getImages(this.scalableViewType);
        }
        if (images) {
            for (var i = 0; i < images.length; i++) {
                if (i === this.index) {
                    result.add(this);
                } else {
                    result.add(ProductImage.getImage(this.imageObject, this.viewType, i));
                }
            }
        }
    }
    return result;
};

/**
* Gets a new Product Image Wrapper object if it hasn't been initialized during the request,
* otherwise the already initialzed version will be returned.
*
* @param {Object} imageObject Product or ProductVariationAttributeValue(required)
* @param {string} viewType Type of view (resolution) that should be generated (required)
* @param {number} index Position of the image in the list of images for the view type. Defaults to 0 if not provided
*
* @returns {ProductImage} A wrapped image that does not need to be initialized if already created in context of the current request.
*/
ProductImage.getImage = function (imageObject, viewType, index) {
    if (!imageObject || !viewType) {
        return null;
    }
    return new ProductImage(imageObject, viewType, index);
};


/**
* Gets a all images for a given image object
*
* @param {Object} imageObject Product or ProductVariationAttributeValue(required)
* @param {string} viewType Type of view (resolution) that should be generated (required)
*
* @returns {Collection} Collection of images assiciated with the image object and the view type
*/
ProductImage.getImages = function (imageObject, viewType) {
    if (!imageObject || !viewType) {
        return null;
    }
    return ProductImage.getImage(imageObject, viewType, 0).getImages();
};

module.exports = ProductImage;
