/**
 * If next link fails, this method catches and checks if status
 * code is 401 and sets authentication specific response codes
 *
 * @param {Object} ctx koa context
 * @param {Object} next next link in middleware chain
 */
exports.auth = async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        if (err.status === 401) {
            ctx.status = 401;
            ctx.set('WWW-Authenticate', 'Basic');
            ctx.body = 'Authentication Failure';
        } else {
            throw err;
        }
    }
}