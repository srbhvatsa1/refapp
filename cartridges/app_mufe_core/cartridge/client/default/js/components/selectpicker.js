'use strict';

module.exports = function (selector) {
    $.fn.selectpicker.Constructor.BootstrapVersion = '4';

    $(selector || '.custom-select').selectpicker({
        style: '',
        showSubtext: true
    });
};
