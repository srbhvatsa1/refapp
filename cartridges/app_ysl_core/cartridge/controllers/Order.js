'use strict';

var server = require('server');

server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');

var gtmDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmDataLayerHelpers');
var gtmEnhancedEcommerceDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmEnhancedEcommerceDataLayerHelpers');

server.append(
    'Track',
    consentTracking.consent,
    server.middleware.https,
    server.middleware.get,
    csrfProtection.validateRequest,
    csrfProtection.generateToken,
    function (req, res, next) {
        var viewData = res.getViewData();
        var redirectTemplate = req.querystring.redirectTemplate;
        if (viewData.orderTrackFormError && redirectTemplate && redirectTemplate !== 'null') {
            res.render(redirectTemplate);
        }

        next();
    }
);

/**
 * Extend viewdata by setting page data layer object
 */
server.append('Confirm', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    gtmEnhancedEcommerceDataLayerHelpers.purchase(req, res);
    next();
});

/**
 * Extend viewdata by setting page data layer object
 */
server.append('History', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    next();
});

/**
 * Extend viewdata by setting page data layer object
 */
server.append('Details', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    next();
});

/**
 * Extend viewdata by setting page data layer object
 */
server.append('DataLayer', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    gtmEnhancedEcommerceDataLayerHelpers.forcePurchase(req, res);
    next();
});

module.exports = server.exports();
