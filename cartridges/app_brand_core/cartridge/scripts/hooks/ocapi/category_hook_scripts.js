'use strict';
var Status = require('dw/system/Status');
/**
 * Iteratres categories recursively.
 * @param {dw.util.ArrayList} categories categories from API
 */
function extendCategories(categories) {
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var catItr = categories.iterator();
    while (catItr.hasNext()) {
        var category = catItr.next();
        var apiCategory = CatalogMgr.getCategory(category.ID);
        if (apiCategory.template) {
            category.c_template = apiCategory.template;
        }
        if (category.categories && category.categories.length) {
            extendCategories(category.categories);
        }
    }
}


exports.modifyGETResponse = function (scriptCategory, doc) { // eslint-disable-line
    if (doc.categories && doc.categories.length) {
        extendCategories(doc.categories);
    }
    return new Status(Status.OK);
};
