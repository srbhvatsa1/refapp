'use strict';

module.exports = function () {
    var $body = $('body');

    $body.on('click', '.js-pt-add-to-cart', function () {
        $body.trigger('product:beforeAddToCart', this);

        var $this = $(this);
        var form = {
            pid: $this.attr('data-pid'),
            quantity: 1
        };

        $this.trigger('updateAddToCartFormData', form);

        $.spinner().start();
        $.ajax({
            url: window.RA_URL['Cart-AddProduct'],
            method: 'POST',
            data: form,
            success: function (data) {
                $body.trigger('product:afterAddToCart', data);
                $.spinner().stop();
            },
            error: function () {
                $.spinner().stop();
            }
        });
    });
};
